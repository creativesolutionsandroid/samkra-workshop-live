package com.cs.samkraworkshop.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Activity.FinalBidActivity;
import com.cs.samkraworkshop.Activity.OfferDetailsActivity;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.R;

import java.util.ArrayList;

/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class ImagesAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<String> images;
    Context context;
    String language;


    public ImagesAdapter(Context context, ArrayList<String> images, String language) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = images;
        this.language = language;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 2.7f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_image, container, false);

        ImageView store_image = (ImageView) itemView.findViewById(R.id.image);
        Glide.with(context).load(Constants.USER_IMAGES +images.get(position)).into(store_image);
        container.addView(itemView);

        if (language.equalsIgnoreCase("Ar")){
            itemView.setRotationY(180);
        }


        store_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    OfferDetailsActivity.bigImageLayout.setVisibility(View.VISIBLE);
                    OfferDetailsActivity.bigImageView.setCurrentItem(position);
                } catch (Exception e) {
                    e.printStackTrace();
                    FinalBidActivity.bigImageLayout.setVisibility(View.VISIBLE);
                    FinalBidActivity.bigImageView.setCurrentItem(position);
                }
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}