package com.cs.samkraworkshop.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Activity.OfferDetailsActivity;
import com.cs.samkraworkshop.Activity.SignInActivity;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.CancelRequestResponse;
import com.cs.samkraworkshop.Models.NewRequestsResponse;
import com.cs.samkraworkshop.Models.SwipeDeckData;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SwipeDeckAdapter extends BaseAdapter {

    private ArrayList<NewRequestsResponse.Data> data;
    private Context context;
    TextView st1,st2,st3,st4,st5,st6;
    ImageView carImage;
    Button cancelBtn, bidBtn;
    TextView make, year, requestcode, bidNumber;
    TextView model;
    String inputStr;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    ArrayList<SwipeDeckData> deckData;
    long remainingMillis = 60 * 1440 * 1000;
    TextView requestCount;
    String language;
    AlertDialog loaderDialog;
    SharedPreferences languagePrefs;
    public LayoutInflater inflater;

    public SwipeDeckAdapter(ArrayList<NewRequestsResponse.Data> data, ArrayList<SwipeDeckData> deckData, Context context) {
        this.data = data;
        this.deckData = deckData;
        this.context = context;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        Log.d("TAG", "getCount: "+deckData.size());
        return deckData.size();
    }

    @Override
    public Object getItem(int position) {
        return deckData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v == null){
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            // normally use a viewholder
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
                v = inflater.inflate(R.layout.list_bids, parent, false);
            }
            else{
                v = inflater.inflate(R.layout.list_bids_ar, parent, false);
            }

        }

        carImage =(ImageView) v.findViewById(R.id.car_image);
        make = (TextView) v.findViewById(R.id.car_make);
        model = (TextView) v.findViewById(R.id.car_model);
        year = (TextView) v.findViewById(R.id.car_year);
        requestcode = (TextView) v.findViewById(R.id.request_code);
        bidNumber = (TextView) v.findViewById(R.id.bid_count);
        requestCount = (TextView) v.findViewById(R.id.bids_count);
//        address = (TextView) v.findViewById(R.id.address);

        st1=(TextView) v.findViewById(R.id.st1);
        st2=(TextView) v.findViewById(R.id.st2);
        st3=(TextView) v.findViewById(R.id.st3);
        st4=(TextView) v.findViewById(R.id.st4);
        st5=(TextView) v.findViewById(R.id.st5);
        st6=(TextView) v.findViewById(R.id.st6);

        bidBtn = (Button) v.findViewById(R.id.bid_button);
        cancelBtn = (Button) v.findViewById(R.id.cancel_button);

        Log.d("TAG", "getExpiresOn: "+deckData.get(position).getExpiresOn());

        try {
            requestCount.setText(""+deckData.get(position).getRequestCount());
            if (language.equalsIgnoreCase("En")){
                make.setText(deckData.get(position).getModelName());
            }
            else {
                make.setText(deckData.get(position).getModelName());
            }

            year.setText(deckData.get(position).getDistance()+" KM");
            requestcode.setText(""+deckData.get(position).getRequestCode());
            for (int i = 0; i < data.size(); i++) {
                if (deckData.get(position).getRequestCode().equals(data.get(i).getRequestCode())) {
                    bidNumber.setText(""+(i+1));
                    break;
                }
            }

            Glide.with(context).load(Constants.USER_IMAGES +deckData.get(position).getImage())
                   .into(carImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        bidBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OfferDetailsActivity.class);
                intent.putExtra("pos", position);
                intent.putExtra("data", data);
                context.startActivity(intent);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               inputStr = prepareJson(position);
                String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new getNewRequestsApi().execute();
                }
                else{
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(context, context.getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, context.getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        if (language.equalsIgnoreCase("Ar")) {
          make.setTypeface(Constants.getarbooldTypeFace(context));
            model.setTypeface(Constants.getarbooldTypeFace(context));
            year.setTypeface(Constants.getarbooldTypeFace(context));
            requestCount.setTypeface(Constants.getarbooldTypeFace(context));
            bidBtn.setTypeface(Constants.getarTypeFace(context));
            cancelBtn.setTypeface(Constants.getarTypeFace(context));
            st1.setTypeface(Constants.getarTypeFace(context));
            st2.setTypeface(Constants.getarTypeFace(context));
            st3.setTypeface(Constants.getarTypeFace(context));
            st4.setTypeface(Constants.getarTypeFace(context));
            st5.setTypeface(Constants.getarTypeFace(context));
            st6.setTypeface(Constants.getarTypeFace(context));


        }
        try {
            Date bookedDate = null;
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

            String[] reservDate = deckData.get(position).getExpiresOn().replace("T", " ").split(" ");
            String checkInDate = reservDate[0]+" "+reservDate[1];

            try {
                bookedDate = DateFormat.parse(checkInDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long endMillis = bookedDate.getTime();
            long currentMillis = calendar.getTimeInMillis();
//            long endMillis = startMillis + (60 * 1440 * 1000);  /*24 hours*/
            remainingMillis = endMillis - currentMillis;
            if (currentMillis > endMillis) {
                remainingMillis = 0;
            }
//        model.start(remainingMillis);

            String hms = "00h 00m";
            if (remainingMillis > (60 * 1440 * 1000)) {
                hms = String.format("%02dd %02dh", TimeUnit.MILLISECONDS.toDays(remainingMillis),
                        TimeUnit.MILLISECONDS.toHours(remainingMillis) % TimeUnit.DAYS.toHours(1));
            }
            else {
                hms = String.format("%02dh %02dm", TimeUnit.MILLISECONDS.toHours(remainingMillis),
                        TimeUnit.MILLISECONDS.toMinutes(remainingMillis) % TimeUnit.HOURS.toMinutes(1));
            }
            if (currentMillis > endMillis) {
                model.setText("00h 00m");
            } else {
                model.setText(Constants.convertToArabic(hms));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return v;
    }

    private String prepareJson(int pos){
        userPrefs = context.getSharedPreferences("USER_PREFS",Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId","");

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("WorkshopId", userId);
            parentObj.put("RequestId", data.get(pos).getRequestId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("TAG", "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private class getNewRequestsApi extends AsyncTask<String, Integer, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<CancelRequestResponse> call = apiService.CancelRequest(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<CancelRequestResponse>() {
                @Override
                public void onResponse(Call<CancelRequestResponse> call, Response<CancelRequestResponse> response) {
                    if(response.isSuccessful()){
                        CancelRequestResponse requestsResponse = response.body();
                        try {
                            if(requestsResponse.getStatus()){
                                Intent intent = new Intent("cancel_request");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = requestsResponse.getMessage();
                                    Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    String failureResponse = requestsResponse.getMessageAr();
                                    Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessage();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CancelRequestResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }
}
