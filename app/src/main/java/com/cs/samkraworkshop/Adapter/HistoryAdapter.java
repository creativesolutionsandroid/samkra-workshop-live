package com.cs.samkraworkshop.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Activity.InvoiceActivity;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.HistoryResponce;
import com.cs.samkraworkshop.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HistoryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<HistoryResponce.Data> historyArrayList = new ArrayList<>();
    String language;
    SharedPreferences languagePrefs;

    public HistoryAdapter(Context context, ArrayList<HistoryResponce.Data> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView mrequest_code, mdate, mcar_right, mcar_left, mcar_front, mcar_back, car_make, car_model, car_year, bids_count, min_price, max_price, final_price,st1,st2,st3,st4,st5,st6;
        LinearLayout mprice_layout, back_layout, front_layout, left_layout, right_layout;
        RelativeLayout mbids_count_layout;
        ViewPager img_list;
        ImageView back_side, front_side, left_side, right_side, dropdown;
        Button button;
        LinearLayout imagesLayout;
        RelativeLayout mainLayout;
        ImageView back_dot, front_dot, left_dot, right_dot;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.inbox_list, null);
            }
            else{
                convertView = inflater.inflate(R.layout.inbox_list_ar, null);
            }


            holder.mrequest_code = (TextView) convertView.findViewById(R.id.request_code);
            holder.mdate = (TextView) convertView.findViewById(R.id.date);
            holder.mcar_back = (TextView) convertView.findViewById(R.id.car_back);
            holder.mcar_left = (TextView) convertView.findViewById(R.id.car_left);
            holder.mcar_right = (TextView) convertView.findViewById(R.id.car_right);
            holder.mcar_front = (TextView) convertView.findViewById(R.id.car_front);
            holder.car_make = (TextView) convertView.findViewById(R.id.car_make);
            holder.car_model = (TextView) convertView.findViewById(R.id.car_model);
            holder.car_year = (TextView) convertView.findViewById(R.id.car_year);
            holder.bids_count = (TextView) convertView.findViewById(R.id.bids_count);
            holder.min_price = (TextView) convertView.findViewById(R.id.min_price);
            holder.max_price = (TextView) convertView.findViewById(R.id.max_price);
            holder.final_price = (TextView) convertView.findViewById(R.id.final_price);
            holder.button = (Button) convertView.findViewById(R.id.button);
            holder.mcar_back = (TextView) convertView.findViewById(R.id.car_back);
            holder.mcar_left = (TextView) convertView.findViewById(R.id.car_left);
            holder.mcar_right = (TextView) convertView.findViewById(R.id.car_right);
            holder.mcar_front = (TextView) convertView.findViewById(R.id.car_front);

            holder.mbids_count_layout = (RelativeLayout) convertView.findViewById(R.id.bids_count_layout);
            holder.mainLayout = (RelativeLayout) convertView.findViewById(R.id.layout);

            holder.mprice_layout = (LinearLayout) convertView.findViewById(R.id.price_layount);
            holder.back_layout = (LinearLayout) convertView.findViewById(R.id.back_layout);
            holder.left_layout = (LinearLayout) convertView.findViewById(R.id.left_layout);
            holder.right_layout = (LinearLayout) convertView.findViewById(R.id.right_layout);
            holder.front_layout = (LinearLayout) convertView.findViewById(R.id.front_layout);

            holder.img_list = (ViewPager) convertView.findViewById(R.id.view_pager);

            holder.back_side = (ImageView) convertView.findViewById(R.id.back_side);
            holder.front_side = (ImageView) convertView.findViewById(R.id.front_side);
            holder.left_side = (ImageView) convertView.findViewById(R.id.left_side);
            holder.right_side = (ImageView) convertView.findViewById(R.id.right_side);
            holder.dropdown = (ImageView) convertView.findViewById(R.id.down);
            holder.imagesLayout = (LinearLayout) convertView.findViewById(R.id.images_layout);

            holder.back_dot = (ImageView) convertView.findViewById(R.id.back_dot);
            holder.front_dot = (ImageView) convertView.findViewById(R.id.front_dot);
            holder.left_dot = (ImageView) convertView.findViewById(R.id.left_dot);
            holder.right_dot = (ImageView) convertView.findViewById(R.id.right_dot);
            holder.st1=(TextView) convertView.findViewById(R.id.st1);
            holder.st2=(TextView) convertView.findViewById(R.id.st2);
            holder.st3=(TextView) convertView.findViewById(R.id.st3);
            holder.st4=(TextView) convertView.findViewById(R.id.st4);
            holder.st5=(TextView) convertView.findViewById(R.id.st5);
            holder.st6=(TextView) convertView.findViewById(R.id.st6);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mrequest_code.setText("#"+historyArrayList.get(position).getRequestCode());

        if (language.equalsIgnoreCase("Ar")) {
            holder.mrequest_code.setTypeface(Constants.getarbooldTypeFace(context));
            holder.mdate.setTypeface(Constants.getarbooldTypeFace(context));
            holder.mcar_right.setTypeface(Constants.getarTypeFace(context));
            holder.mcar_left.setTypeface(Constants.getarTypeFace(context));
            holder.mcar_front.setTypeface(Constants.getarTypeFace(context));
            holder.mcar_back.setTypeface(Constants.getarTypeFace(context));
            holder.car_make.setTypeface(Constants.getarbooldTypeFace(context));
            holder.car_model.setTypeface(Constants.getarbooldTypeFace(context));
            holder.car_year.setTypeface(Constants.getarbooldTypeFace(context));
            holder.bids_count.setTypeface(Constants.getarTypeFace(context));
            holder.min_price.setTypeface(Constants.getarTypeFace(context));
            holder.max_price.setTypeface(Constants.getarTypeFace(context));
            holder.final_price.setTypeface(Constants.getarTypeFace(context));
            holder.button.setTypeface(Constants.getarTypeFace(context));
            holder.st1.setTypeface(Constants.getarTypeFace(context));
            holder.st2.setTypeface(Constants.getarTypeFace(context));
            holder.st3.setTypeface(Constants.getarTypeFace(context));
            holder.st4.setTypeface(Constants.getarTypeFace(context));
            holder.st5.setTypeface(Constants.getarTypeFace(context));
            holder.st6.setTypeface(Constants.getarTypeFace(context));

        }


        String date = historyArrayList.get(position).getCreatedOn();
        date = date.replace("T"," ");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

        try {
            Date datetime = sdf.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.mdate.setText(""+Constants.convertToArabic(date));

//        String date1 = historyArrayList.get(position).getCreatedOn();
//
//        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
//
//        try {
//            Date datetime = sdf2.parse(date1);
//            date = sdf3.format(datetime);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//

        if (language.equalsIgnoreCase("En")){
            holder.car_make.setText(""+historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getCarMakerNameEn());
            holder.car_model.setText(""+historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getModelNameEn());
            holder.car_year.setText(""+historyArrayList.get(position).getRequestDetails().get(0).getYear());
        }
        else {
            holder.car_make.setText(""+historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getCarMakerNameAr());
            holder.car_model.setText(""+historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getModelNameAr());
            holder.car_year.setText(""+historyArrayList.get(position).getRequestDetails().get(0).getYear());

        }

        ArrayList<String> images = new ArrayList<>();
        boolean isRight = false;
        boolean isLeft = false;
        boolean isFront = false;
        boolean isBack = false;
        for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
            if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 1) {
                isRight = true;
            }
            if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 3) {
                isLeft = true;
            }
            if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 2) {
                isFront = true;
            }
            if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 4) {
                isBack = true;
            }
        }
        Log.d("TAG", "getView: "+position+": "+isRight+","+isLeft+","+isFront+","+isBack);

        if(!isRight){
            holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_right));
            holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
        }
        else{
            holder.right_layout.setEnabled(true);
            holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_right_blue));
            holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
            for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
                if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 1) {
                    images.add(historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentLocation());
                }
                else {

                }
            }
        }

        if(!isLeft){
            holder.left_layout.setEnabled(false);
            holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_left));
            holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
        }
        else{
            holder.left_layout.setEnabled(true);
            holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_right_blue));
            if(!isRight) {
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
                    if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 3) {
                        images.add(historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentLocation());
                    } else {

                    }
                }
            }
            else{

            }
        }

        if(!isBack){
            holder.back_layout.setEnabled(false);
            holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_front));
            holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
        }
        else{
            holder.back_layout.setEnabled(true);
            holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_front_selected));
            if(!isRight && !isLeft && !isFront) {
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
                    if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(0).getDocumentType() == 4) {
                        images.add(historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentLocation());
                    } else {

                    }
                }
            }
            else{

            }
        }

        if(!isFront){
            holder.front_layout.setEnabled(false);
            holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_front));
            holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
        }
        else{
            holder.front_layout.setEnabled(true);
            holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_front_selected));
            if(!isRight && !isLeft) {
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
                    if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 2) {
                        images.add(historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentLocation());
                    } else {

                    }
                }
            }
            else{

            }
        }

//        for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
//            images.add(historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentLocation());
//        }

        holder.imagesLayout.removeAllViews();
        for (int i = 0; i < images.size(); i++) {
            View v = inflater.inflate(R.layout.list_image, null);
            ImageView imageView = v.findViewById(R.id.image);
            Glide.with(context).load(Constants.USER_IMAGES+images.get(i)).into(imageView);
            holder.imagesLayout.addView(v);
        }

        holder.right_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
                    if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 1) {
                        images.add(historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentLocation());
                    }
                    else {

                    }
                }

                holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));

                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.USER_IMAGES+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }

            }
        });

        holder.left_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));

                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
                    if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 3) {
                        images.add(historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentLocation());
                    }
                    else {

                    }
                }

                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.USER_IMAGES+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }

            }
        });

        holder.front_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));

                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
                    if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 2) {
                        images.add(historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentLocation());
                    }
                    else {

                    }
                }

                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.USER_IMAGES+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }

            }
        });

        holder.back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().size(); i++) {
                    if (historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentType() == 4) {
                        images.add(historyArrayList.get(position).getRequestDetails().get(0).getCM().get(0).getMDL().get(0).getRCI().get(i).getDocumentLocation());
                    }
                    else {

                    }
                }

                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.USER_IMAGES+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }
            }
        });

        holder.mbids_count_layout.setVisibility(View.GONE);
        holder.mprice_layout.setVisibility(View.VISIBLE);

        holder.min_price.setText(Constants.convertToArabic(Constants.priceFormat.format(historyArrayList.get(position).getWs().get(0).getMinquote())));
        holder.max_price.setText(Constants.convertToArabic(Constants.priceFormat.format(historyArrayList.get(position).getWs().get(0).getMaxquote())));
        holder.final_price.setText(Constants.convertToArabic(Constants.priceFormat.format(historyArrayList.get(position).getNetAmount())));

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, InvoiceActivity.class);
                intent.putExtra("data", historyArrayList);
                intent.putExtra("pos",position);
                context.startActivity(intent);
            }
        });
        return convertView;
    }
}