package com.cs.samkraworkshop.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.NotificationResponce;
import com.cs.samkraworkshop.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class NotificationAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<NotificationResponce.Data> historyArrayList = new ArrayList<>();
    String language;
    SharedPreferences languagePrefs;

    public NotificationAdapter(Context context, ArrayList<NotificationResponce.Data> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView headertext, summerytext,date;
      ImageView iamgeview;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
            convertView = inflater.inflate(R.layout.notification_child, null);
            }
            else{
                convertView = inflater.inflate(R.layout.notification_child_ar, null);
            }

            holder.headertext = (TextView) convertView.findViewById(R.id.n_header);
            holder.summerytext = (TextView) convertView.findViewById(R.id.n_summerytext);
          holder.date=(TextView) convertView.findViewById(R.id.n_date);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String str = historyArrayList.get(position).getSentdate();
//        String[] array = str.split(" ");
//
        if (language.equalsIgnoreCase("En")){
            holder.headertext.setText(historyArrayList.get(position).getTitleen());
            holder.summerytext.setText(historyArrayList.get(position).getMessageen());
            holder.date.setText(Constants.convertToArabic(historyArrayList.get(position).getSentdate()));
        }
        else {
            holder.headertext.setText(historyArrayList.get(position).getTitlear());
            holder.summerytext.setText(historyArrayList.get(position).getMessagear());
            holder.date.setText(Constants.convertToArabic(historyArrayList.get(position).getSentdate()));

        }
        if (language.equalsIgnoreCase("Ar")) {

            holder.headertext.setTypeface(Constants.getarTypeFace(context));
            holder.summerytext.setTypeface(Constants.getarTypeFace(context));

        }
//
//
//        holder.date.setText(array[0]);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy hh:mm a",Locale.US);
//
//        String tt = array[1];
        try {
            Date time = sdf.parse(str);
            str = sdf1.format(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.date.setText(Constants.convertToArabic(str));

        return convertView;
    }




}