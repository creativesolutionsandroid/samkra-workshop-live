package com.cs.samkraworkshop.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.R;

public class SideMenuAdapter extends ArrayAdapter<String> {

    Context context;
    String[] menuItems;
    Integer[] menuImages;
    int layoutResourceId;
    int selectedPosition;
    private Typeface regular;
    String language;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    public SideMenuAdapter(Context context, int layoutResourceId, String[] menuItems, Integer[] menuImages, int selectedPosition){
        super(context, layoutResourceId, menuItems);
        this.context = context;
        this.menuItems = menuItems;
        this.menuImages = menuImages;
        this.layoutResourceId = layoutResourceId;
        this.selectedPosition = selectedPosition;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            languagePrefsEditor  = languagePrefs.edit();
            language = languagePrefs.getString("language", "En");


            holder.textTitle = (TextView) row.findViewById(R.id.menu_title);
            holder.menuImage = (ImageView) row.findViewById(R.id.menu_image);
            holder.textLanguage = (TextView) row.findViewById(R.id.menu_subtitle);
            holder.menuItemLayout = (LinearLayout) row.findViewById(R.id.menu_item_layout);


            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        if (language.equalsIgnoreCase("Ar")){
            holder.textTitle.setTypeface(Constants.getarTypeFace(context));
            holder.textLanguage.setTypeface(Constants.getarTypeFace(context));
        }


//        holder.textTitle.setTypeface(regular);
        holder.textTitle.setText(menuItems[position]);
        holder.menuImage.setImageDrawable(context.getResources().getDrawable(menuImages[position]));

        if(position == 8){
            holder.textLanguage.setVisibility(View.VISIBLE);
        }
        else{
            holder.textLanguage.setVisibility(View.INVISIBLE);
        }

        return row;

    }

    static class ViewHolder {
        TextView textTitle, textLanguage;
        ImageView menuImage;
        LinearLayout menuItemLayout;
    }
}
