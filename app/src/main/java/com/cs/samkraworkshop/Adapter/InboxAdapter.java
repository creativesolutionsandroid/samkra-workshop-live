package com.cs.samkraworkshop.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.cs.samkraworkshop.Activity.CreateInvoiceActivity;
import com.cs.samkraworkshop.Activity.FinalBidActivity;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.InboxResponse;
import com.cs.samkraworkshop.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 15-12-2018.
 */
public class InboxAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<InboxResponse.Data> historyArrayList = new ArrayList<>();
    String language;
    SharedPreferences languagePrefs;

    public InboxAdapter(Context context, ArrayList<InboxResponse.Data> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView requestCode, date, status, minQuote, maxQuote, finalPrice, make, type, year, statusText,st1,st2,st3,st4,st5,st6,st7,st8,st9;
        ImageView statusImage;
        Button button;
        LinearLayout finalPriceLayout, layout;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.inbox_child, null);
            }
            else{
                convertView = inflater.inflate(R.layout.inbox_child_ar, null);
            }
//
            holder.requestCode = (TextView) convertView.findViewById(R.id.request_code);
            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.status = (TextView) convertView.findViewById(R.id.status);
            holder.minQuote = (TextView) convertView.findViewById(R.id.min_quote);
            holder.maxQuote = (TextView) convertView.findViewById(R.id.max_quote);
            holder.finalPrice = (TextView) convertView.findViewById(R.id.final_price);
            holder.make = (TextView) convertView.findViewById(R.id.car_make);
            holder.type = (TextView) convertView.findViewById(R.id.car_model);
            holder.year = (TextView) convertView.findViewById(R.id.car_year);
            holder.statusText = (TextView) convertView.findViewById(R.id.status_text);
            holder.st1=(TextView) convertView.findViewById(R.id.st1);
            holder.st2=(TextView) convertView.findViewById(R.id.st2);
            holder.st3=(TextView) convertView.findViewById(R.id.st3);
            holder.st4=(TextView) convertView.findViewById(R.id.st4);
            holder.st5=(TextView) convertView.findViewById(R.id.st5);
            holder.st6=(TextView) convertView.findViewById(R.id.st6);
            holder.st7=(TextView) convertView.findViewById(R.id.st7);
            holder.st8=(TextView) convertView.findViewById(R.id.st8);
            holder.st9=(TextView) convertView.findViewById(R.id.st9);

            holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
            holder.finalPriceLayout = (LinearLayout) convertView.findViewById(R.id.final_price_layout);
            holder.button = (Button) convertView.findViewById(R.id.button);
            holder.statusImage = (ImageView) convertView.findViewById(R.id.status_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.requestCode.setText("#"+historyArrayList.get(position).getRequestCode());
        holder.minQuote.setText(Constants.convertToArabic(Constants.priceFormat1.format(historyArrayList.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getMinQuote())));
        holder.maxQuote.setText(Constants.convertToArabic(Constants.priceFormat1.format(historyArrayList.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getMaxQuote())));
      if (language.equalsIgnoreCase("En")){
          holder.make.setText(historyArrayList.get(position).getCM().get(0).getCarMakerNameEn());
          holder.type.setText(historyArrayList.get(position).getCM().get(0).getMDL().get(0).getModelNameEn());

      }
      else {
          holder.make.setText(historyArrayList.get(position).getCM().get(0).getCarMakerNameAr());
          holder.type.setText(historyArrayList.get(position).getCM().get(0).getMDL().get(0).getModelNameAr());
      }

        holder.year.setText(""+historyArrayList.get(position).getYear());

        if(historyArrayList.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getFinalPrice() != 0){
            holder.finalPriceLayout.setVisibility(View.VISIBLE);
            holder.finalPrice.setText(Constants.convertToArabic(Constants.priceFormat1.format(historyArrayList.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getFinalPrice())));
        }
        else {
            holder.finalPriceLayout.setVisibility(View.INVISIBLE);
            holder.finalPrice.setText("");
        }


        if (language.equalsIgnoreCase("Ar")) {
            holder.requestCode.setTypeface(Constants.getarbooldTypeFace(context));
            holder.date.setTypeface(Constants.getarbooldTypeFace(context));
            holder.status.setTypeface(Constants.getarTypeFace(context));
            holder.minQuote.setTypeface(Constants.getarbooldTypeFace(context));
            holder.maxQuote.setTypeface(Constants.getarbooldTypeFace(context));
            holder.finalPrice.setTypeface(Constants.getarbooldTypeFace(context));
            holder.make.setTypeface(Constants.getarbooldTypeFace(context));
            holder.type.setTypeface(Constants.getarbooldTypeFace(context));
            holder.year.setTypeface(Constants.getarbooldTypeFace(context));
            holder.statusText.setTypeface(Constants.getarTypeFace(context));
            holder.st1.setTypeface(Constants.getarTypeFace(context));
            holder.st2.setTypeface(Constants.getarTypeFace(context));
            holder.st3.setTypeface(Constants.getarTypeFace(context));
            holder.st4.setTypeface(Constants.getarTypeFace(context));
            holder.st5.setTypeface(Constants.getarTypeFace(context));
            holder.st6.setTypeface(Constants.getarTypeFace(context));
            holder.button.setTypeface(Constants.getarTypeFace(context));
        }


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        SimpleDateFormat requiredFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.US);
        String dateStr = historyArrayList.get(position).getCreatedOn();
        dateStr = dateStr.replace("T", " ");
        Calendar calendar = Calendar.getInstance();
        try {
            Date dt = sdf.parse(dateStr);
            calendar.setTime(dt);
            dateStr = requiredFormat.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.date.setText(Constants.convertToArabic(dateStr));

        Log.d("TAG", "getView: "+historyArrayList.get(position).getRequestStatus());
        if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Offer Pending")){
           if (language.equalsIgnoreCase("En")){
               holder.status.setText("Pending");
               holder.button.setText("Final Price");
               holder.statusText.setText("Your bid has been placed, waiting for customer approval.");
           }
           else {
               holder.status.setText("قيد الانتظار");
               holder.button.setText("السعر النهائي");
               holder.statusText.setText("تم ارسال عرضك، بانتظار موافقة العميل.");
           }
            holder.button.setAlpha(0.3f);
            holder.button.setVisibility(View.VISIBLE);
            holder.statusImage.setImageDrawable(context.getResources().getDrawable(R.drawable.status_inprogress));

        }
        else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Offer Accepted")){
            if (language.equalsIgnoreCase("En")){
                holder.status.setText("In Progress");
                holder.button.setText("Final Price");
                holder.statusText.setText("Your bid has been accepted by customer, place final price.");
            }
            else {
                holder.status.setText("تحت التنفيذ");
                holder.button.setText("السعر النهائي");
                holder.statusText.setText("مبروك! تم قبول عرضك من العميل، ارجو وضع السعر النهائي.");
            }
            holder.button.setAlpha(1.0f);
            holder.button.setVisibility(View.VISIBLE);
            holder.statusImage.setImageDrawable(context.getResources().getDrawable(R.drawable.status_inprogress));

        }
        else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Final price Pending")){
            if (language.equalsIgnoreCase("En")){
                holder.status.setText("In Progress");
                holder.button.setText("Invoice");
                holder.statusText.setText("Your final price has been placed, waiting for customer approval.");
            }
           else {
                holder.status.setText("تحت التنفيذ");
                holder.button.setText("الفاتورة");
                holder.statusText.setText("تم تقديم سعرك النهائي وننتظر موافقة العميل.");
            }
            holder.button.setAlpha(0.3f);
            holder.button.setVisibility(View.VISIBLE);
            holder.statusImage.setImageDrawable(context.getResources().getDrawable(R.drawable.status_inprogress));

        }
        else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Final price Accepted")){
          if (language.equalsIgnoreCase("En")){
              holder.status.setText("Done");
              holder.button.setText("Invoice");
              SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
              SimpleDateFormat sd2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm a", Locale.US);
              Date date = null;
              String date_time;
              try {
                  date = sd1.parse(historyArrayList.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getEstimatedDate());
              } catch (ParseException e) {
                  e.printStackTrace();
              }
              date_time = sd2.format(date);
              holder.statusText.setText("Create invoice after finishing job, Estimated Repair Time : " + date_time + ".");
          }
          else {
              holder.status.setText("مكتمل");
              holder.button.setText("الفاتورة");
              SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
              SimpleDateFormat sd2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm a", Locale.US);
              Date date = null;
              String date_time;
              try {
                  date = sd1.parse(historyArrayList.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getEstimatedDate());
              } catch (ParseException e) {
                  e.printStackTrace();
              }
              date_time = sd2.format(date);
              holder.statusText.setText("اصدار فاتورة, الوقت المتوقع لتصليح السيارة : \n" + date_time + "." );

          }
             holder.button.setAlpha(1.0f);
            holder.button.setVisibility(View.VISIBLE);
            holder.statusImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_submit));

        }
        else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Offer Rejected")){
            if (language.equalsIgnoreCase("En")){
                holder.status.setText("Cancel");
                holder.statusText.setText("Your bid has been not accepted by customer.");
            }
            else {
                holder.status.setText("الرجاء قبول");
                holder.statusText.setText("عرض السعر لم يقبل للاسف.");
            }
            holder.button.setVisibility(View.GONE);
            holder.statusImage.setImageDrawable(context.getResources().getDrawable(R.drawable.status_cancel));
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Offer Pending")){
                    Intent intent = new Intent(context, FinalBidActivity.class);
                    intent.putExtra("data", historyArrayList);
                    intent.putExtra("pos", position);
                    context.startActivity(intent);
                }
                else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Offer Accepted")){
                    Intent intent = new Intent(context, FinalBidActivity.class);
                    intent.putExtra("data", historyArrayList);
                    intent.putExtra("pos", position);
                    context.startActivity(intent);
                }
                else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Final price Pending")){
                    Intent intent = new Intent(context, FinalBidActivity.class);
                    intent.putExtra("data", historyArrayList);
                    intent.putExtra("pos", position);
                    context.startActivity(intent);
                }
                else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Final price Accepted")){
                    Intent intent = new Intent(context, FinalBidActivity.class);
                    intent.putExtra("data", historyArrayList);
                    intent.putExtra("pos", position);
                    context.startActivity(intent);
                }
            }
        });

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Offer Pending")){
                    Intent intent = new Intent(context, FinalBidActivity.class);
                    intent.putExtra("data", historyArrayList);
                    intent.putExtra("pos", position);
                    context.startActivity(intent);
                }
                else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Offer Accepted")){
                    Intent intent = new Intent(context, FinalBidActivity.class);
                    intent.putExtra("data", historyArrayList);
                    intent.putExtra("pos", position);
                    context.startActivity(intent);
                }
                else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Final price Pending")){
                    Intent intent = new Intent(context, FinalBidActivity.class);
                    intent.putExtra("data", historyArrayList);
                    intent.putExtra("pos", position);
                    context.startActivity(intent);
                }
                else if(historyArrayList.get(position).getRequestStatus().equalsIgnoreCase("Final price Accepted")){
                    Intent intent = new Intent(context, CreateInvoiceActivity.class);
                    intent.putExtra("data", historyArrayList);
                    intent.putExtra("pos", position);
                    context.startActivity(intent);
                }
            }
        });

        return convertView;
    }
}