package com.cs.samkraworkshop.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.HistoryResponce;
import com.cs.samkraworkshop.R;

import java.util.ArrayList;

public class InvoiceItemAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<HistoryResponce.II> invoiceArrayList = new ArrayList<>();
    String language;
    int invoicepos;
    SharedPreferences languagePrefs;

    public InvoiceItemAdapter(Context context, ArrayList<HistoryResponce.II> orderList, int invoicepos, String language) {
        this.context = context;
        this.invoiceArrayList = orderList;
        this.invoicepos = invoicepos;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return invoiceArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView mitem,amt,units;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")) {
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.invoice_list, null);
            }
            else{
                convertView = inflater.inflate(R.layout.invoice_list_ar, null);
            }

//

            holder.mitem = (TextView) convertView.findViewById(R.id.item);
            holder.amt = (TextView) convertView.findViewById(R.id.amt);
            holder.units = (TextView) convertView.findViewById(R.id.units);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mitem.setText(""+invoiceArrayList.get(position).getItem());
        holder.amt.setText(Constants.convertToArabic(Constants.priceFormat.format(invoiceArrayList.get(position).getItemTotal())));

        if (language.equalsIgnoreCase("Ar")) {

            holder.mitem.setTypeface(Constants.getarTypeFace(context));
            holder.amt.setTypeface(Constants.getarTypeFace(context));
            holder.units.setTypeface(Constants.getarTypeFace(context));

        }
        return convertView;
    }



}
