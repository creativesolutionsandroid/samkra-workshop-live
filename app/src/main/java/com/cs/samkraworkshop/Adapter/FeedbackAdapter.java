package com.cs.samkraworkshop.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.FeedbackResponse;
import com.cs.samkraworkshop.Models.HistoryResponce;
import com.cs.samkraworkshop.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FeedbackAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<FeedbackResponse.Wr> historyArrayList = new ArrayList<>();
    String language;
    SharedPreferences languagePrefs;
    public FeedbackAdapter(Context context, ArrayList<FeedbackResponse.Wr> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView requestCode, rating, review;
        RatingBar ratingBar;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_feedback, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_feedback_ar, null);
            }


            holder.requestCode = (TextView) convertView.findViewById(R.id.request_code);
            holder.rating = (TextView) convertView.findViewById(R.id.rating);
            holder.review = (TextView) convertView.findViewById(R.id.review);

            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingbar);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.requestCode.setText("#"+historyArrayList.get(position).getRequestId());
        holder.rating.setText(historyArrayList.get(position).getRating() + "/5");
        holder.review.setText(historyArrayList.get(position).getComments());
        holder.ratingBar.setRating(historyArrayList.get(position).getRating());

        return convertView;
    }



}