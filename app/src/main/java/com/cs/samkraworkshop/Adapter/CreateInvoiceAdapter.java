package com.cs.samkraworkshop.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.samkraworkshop.Activity.CreateInvoiceActivity;
import com.cs.samkraworkshop.Activity.FinalBidActivity;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.InboxResponse;
import com.cs.samkraworkshop.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 15-06-2016.
 */
public class CreateInvoiceAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    String language;
    SharedPreferences languagePrefs;
    int count;

    public CreateInvoiceAdapter(Context context, int count, String language) {
        this.context = context;
        this.count = count;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return CreateInvoiceActivity.childCount;
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView itemTitle, qty, price;
        ImageView edit;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.child_create_invoice, null);

            }
            else{
                convertView = inflater.inflate(R.layout.child_create_invoice_ar, null);
            }


            holder.itemTitle = (TextView) convertView.findViewById(R.id.item_title);
            holder.qty = (TextView) convertView.findViewById(R.id.qty_title);
            holder.price = (TextView) convertView.findViewById(R.id.price_title);
            holder.edit = (ImageView) convertView.findViewById(R.id.edit_item);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.itemTitle.setText(CreateInvoiceActivity.itemName.get(position));
        holder.qty.setText(CreateInvoiceActivity.itemQty.get(position)+" "+
                CreateInvoiceActivity.itemUnit.get(position));
        holder.price.setText(Constants.convertToArabic(Constants.priceFormat1.format(Float.parseFloat(CreateInvoiceActivity.itemPrices.get(position))
        * Integer.parseInt(CreateInvoiceActivity.itemQty.get(position)))));

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("edit_invoice");
                Bundle b = new Bundle();
                b.putInt("position", position);
                intent.putExtra("position", b);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });
        if (language.equalsIgnoreCase("Ar")) {

            holder.itemTitle.setTypeface(Constants.getarTypeFace(context));
            holder.qty.setTypeface(Constants.getarTypeFace(context));





        }

        return convertView;
    }
}