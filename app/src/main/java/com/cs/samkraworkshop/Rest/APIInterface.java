package com.cs.samkraworkshop.Rest;

import com.cs.samkraworkshop.Activity.ChangePassword;
import com.cs.samkraworkshop.Models.CancelRequestResponse;
import com.cs.samkraworkshop.Models.ChangePasswordResponse;
import com.cs.samkraworkshop.Models.FeedbackResponse;
import com.cs.samkraworkshop.Models.HistoryResponce;
import com.cs.samkraworkshop.Models.ImageDeleteResponse;
import com.cs.samkraworkshop.Models.LanguageResponce;
import com.cs.samkraworkshop.Models.NotificationResponce;
import com.cs.samkraworkshop.Models.PlaceBidResponse;
import com.cs.samkraworkshop.Models.DashboardResponse;
import com.cs.samkraworkshop.Models.FinalBidResponse;
import com.cs.samkraworkshop.Models.GetListResponse;
import com.cs.samkraworkshop.Models.ImagesResponse;
import com.cs.samkraworkshop.Models.InboxResponse;
import com.cs.samkraworkshop.Models.InvoiceResponse;
import com.cs.samkraworkshop.Models.NewRequestsResponse;
import com.cs.samkraworkshop.Models.SignInResponse;
import com.cs.samkraworkshop.Models.UpdateDetailsResponse;
import com.cs.samkraworkshop.Models.UserResponse;
import com.cs.samkraworkshop.Models.VerifyMobileResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {

    @POST("WorkshopAppAPI/VerfiyMobileEmail")
    Call<VerifyMobileResponse> verfiyMobileNumber(@Body RequestBody body);

    @POST("WorkshopAppAPI/SignupUser")
    Call<UserResponse> signUp(@Body RequestBody body);

    @POST("WorkshopAppAPI/SignInUser")
    Call<SignInResponse> signIn(@Body RequestBody body);

    @POST("WorkshopAppAPI/NewGetList")
    Call<GetListResponse> getList(@Body RequestBody body);

    @POST("WorkshopAppAPI/UpdateProfile")
    Call<UpdateDetailsResponse> updateDetails(@Body RequestBody body);

//    @Multipart
//    @POST("WorkshopAppAPI/PostFormData")
//    Call<List<ImagesResponse>> uploadImage(@Part MultipartBody.Part[] image);

    @POST("CommanAPI/ChangeLanguage")
    Call<LanguageResponce> langyagechange(@Body RequestBody body);

    @POST("WorkshopAppAPI/ForgotPassword")
    Call<VerifyMobileResponse> forgotPassword(@Body RequestBody body);

    @POST("WorkshopAppAPI/SetNewPassword")
    Call<ChangePasswordResponse> resetPassword(@Body RequestBody body);

    @POST("WorkshopAppAPI/GetWorkshopNewRequests")
    Call<NewRequestsResponse> GetWorkshopNewRequests(@Body RequestBody body);

    @POST("WorkshopAppAPI/PlaceBid")
    Call<PlaceBidResponse> PlaceBid(@Body RequestBody body);

    @POST("WorkshopAppAPI/GetAllRequests")
    Call<InboxResponse> InboxResponse(@Body RequestBody body);

    @POST("WorkshopAppAPI/PlaceBidFinalPrice")
    Call<FinalBidResponse> FinalBid(@Body RequestBody body);

    @POST("WorkshopAppAPI/GetDashboard")
    Call<DashboardResponse> GetDashBoard(@Body RequestBody body);

    @POST("WorkshopAppAPI/CreateInvoice")
    Call<InvoiceResponse> CreateInvoice(@Body RequestBody body);

    @POST("WorkshopAppAPI/CancelNewRequestBid")
    Call<CancelRequestResponse> CancelRequest(@Body RequestBody body);

    @POST("WorkshopAppAPI/GetListInvoicePrepared")
    Call<HistoryResponce> GetListInvoicePrepared(@Body RequestBody body);

    @POST("WorkshopAppAPI/GetListNotifications")
    Call<NotificationResponce> Notification(@Body RequestBody body);

    @POST("WorkshopAppAPI/DeleteWorkhopImage")
    Call<ImageDeleteResponse> DeleteWorkhopImage(@Body RequestBody body);

    @POST("WorkshopAppAPI/GetListFeedback")
    Call<FeedbackResponse> GetListFeedback(@Body RequestBody body);

    @POST("WorkshopAppAPI/ResetPassword")
    Call<ChangePasswordResponse> changePassword(@Body RequestBody body);

    @POST("WorkshopAppAPI/UploadImage")
    Call<String> uploadImage(@Body RequestBody body);
 }
