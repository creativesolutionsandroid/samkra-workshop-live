package com.cs.samkraworkshop;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cs.samkraworkshop.Activity.MainActivity;
import com.cs.samkraworkshop.Activity.SignInActivity;
import com.cs.samkraworkshop.Activity.SignupActivity;
import com.cs.samkraworkshop.Activity.UpdateActivity;
import com.cs.samkraworkshop.Firebase.Config;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

/**
 * Created by SKT on 31-12-2015.
 */
public class SplashScreen extends Activity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;
    SharedPreferences userPrefs;
    String stage;
    Context context;
    BroadcastReceiver mRegistrationBroadcastReceiver;
    public static String regId = "";
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        stage = userPrefs.getString("stage", "0");
        context = getApplicationContext();

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();

        String language = languagePrefs.getString("language", "");
        if(language.equalsIgnoreCase("")){
            languagePrefsEditor.putString("language", "Ar");
            languagePrefsEditor.commit();
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    regId = pref.getString("regId", null);

                    Log.i("TAG", "Firebase reg id: " + regId);
                }
            }
        };

//        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
//        regId = pref.getString("regId", null);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        regId = task.getResult().getToken();
                        Log.i("TAG", "reg id: " + regId);
                    }
                });


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (stage.equalsIgnoreCase("0")) {
                    Intent i = new Intent(SplashScreen.this, SignInActivity.class);
                    startActivity(i);
                } else if (stage.equalsIgnoreCase("1") || stage.equalsIgnoreCase("2")
                        || stage.equalsIgnoreCase("3")) {
                    Intent i = new Intent(SplashScreen.this, UpdateActivity.class);
                    startActivity(i);
                }else if (stage.equalsIgnoreCase("4")) {
                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
