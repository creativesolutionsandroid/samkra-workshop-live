package com.cs.samkraworkshop.Firebase;

/**
 * Created by CS on 10/27/2016.
 */

public class Config {
    public static final String TOPIC_GLOBAL = "globle";

    public static final String REGISTRATION_COMPLETE = "registrationcomplete";
    public static final String PUSH_NOTIFICATION = "pushnotification";

    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
}
