package com.cs.samkraworkshop.Firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.cs.samkraworkshop.Activity.MainActivity;
import com.cs.samkraworkshop.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "TAG";
    String title, icon;
    Intent intent;
    Context mContext;

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        storeRegIdInPref(token);

//        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
//        registrationComplete.putExtra("Token", token);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
//        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }
    private void storeRegIdInPref(String Token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", Token);
        editor.commit();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "onMessageReceived: got message");
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("type", "2");
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

            Notification noti = new NotificationCompat.Builder(MyFirebaseMessagingService.this, "1")
                    .setContentTitle(title)
                    .setContentText(remoteMessage.getNotification().getBody())
                    .setSmallIcon(R.drawable.main)
                    .setColor(Color.parseColor("#000000"))
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(remoteMessage.getNotification().getBody()))
                    .setContentIntent(resultPendingIntent).build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // hide the notification after its selected
            noti.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify((int) System.currentTimeMillis(), noti);
        }

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Data Payload: " + remoteMessage.getData().toString());
            String message = "";
            int pnType = 1;

            Map<String, String> notificationMap = remoteMessage.getData();
            for (Map.Entry<String, String> e : notificationMap.entrySet()) {
                if(e.getKey().equals("text")){
                    message = e.getValue();
                }
                if(e.getKey().equals("PnType")){
                    pnType = Integer.parseInt(e.getValue());
                }
            }

//            Intent intent = null;
//            if(pnType == 1) {
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("type", "2");
//            }
//            else if(pnType == 2) {
//                intent = new Intent(this, MainActivity.class);
//            }
//            else if(pnType == 3) {
//                intent = new Intent(this, BidsFragment.class);
//            }
//            else if(pnType == 4) {
//                intent = new Intent(this, InboxFragment.class);
//            }
//            else if(pnType == 5) {
//                intent = new Intent(this, InboxFragment.class);
//            }
//            else if(pnType == 6) {
//                intent = new Intent(this, InboxFragment.class);
//            }
//            else if(pnType == 7) {
//                intent = new Intent(this, HistoryFragment.class);
//            }
//            else{
//                intent = new Intent(this, MainActivity.class);
//            }

            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

            Notification noti = new NotificationCompat.Builder(MyFirebaseMessagingService.this, "1")
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.main)
                    .setColor(Color.parseColor("#000000"))
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setStyle(new NotificationCompat.BigTextStyle()
                             .bigText(message))
                    .setContentIntent(resultPendingIntent).build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // hide the notification after its selected
            noti.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify((int) System.currentTimeMillis(), noti);
        }
    }
}
