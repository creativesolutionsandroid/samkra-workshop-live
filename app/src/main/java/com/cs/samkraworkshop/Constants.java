package com.cs.samkraworkshop;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.cs.samkraworkshop.Activity.CreateInvoiceActivity;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class Constants {

    public static String Country_Code = "+966 ";
    public static String USER_IMAGES = "http://api.samkra.com/UploadFiles/";
    public static String WORKSHOP_IMAGES = "http://api.samkra.com/WorkshopUploads/";
    public static String INVOICE_PATH = "http://api.samkra.com/Invoices/";
//    public static String USER_IMAGES = "http://csadms.com/samkraservices/UploadFiles/";
//    public static String WORKSHOP_IMAGES = "http://csadms.com/samkraservices/WorkshopUploads/";
//    public static String INVOICE_PATH = "http://csadms.com/samkraservices/Invoices/";
    public static String IMAGE_UPLOAD_URL = "http://api.samkra.com/api/WorkshopAppAPI/PostFormData";
    //    public static NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
//    public static DecimalFormat priceFormat = (DecimalFormat)nf;
    public static DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
    public static final DecimalFormat priceFormat = new DecimalFormat("##,##,##0.00", symbols);
    public static final DecimalFormat priceFormat1 = new DecimalFormat("##,##,###.##", symbols);

    public static String getDeviceType(Context context) {
        String device = "Andr v";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            device = device + version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return device;
    }

    public static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void requestEditTextFocus(View view, Activity activity) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    public static Typeface getarTypeFace(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                "cairoregular.ttf");
        return typeface;
    }

    public static Typeface getarbooldTypeFace(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                "cairo_arbold.ttf");
        return typeface;
    }


    public static void showOneButtonAlertDialog(String descriptionStr, String titleStr, String buttonStr, Activity context) {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        SharedPreferences languagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        String language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("Ar")) {
            desc.setTypeface(Constants.getarbooldTypeFace(context));
            title.setTypeface(Constants.getarbooldTypeFace(context));
            no.setTypeface(Constants.getarbooldTypeFace(context));
            yes.setTypeface(Constants.getarbooldTypeFace(context));
        }

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public static void showOneButtonAlertDialogWithFinish(String descriptionStr, String titleStr, String buttonStr, final Activity context) {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
                context.finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public static String convertToArabic(String value) {
        String newValue = (value
                .replaceAll("١", "1").replaceAll("٢", "2")
                .replaceAll("٣", "3").replaceAll("٤", "4")
                .replaceAll("٥", "5").replaceAll("٦", "6")
                .replaceAll("٧", "7").replaceAll("٨", "8")
                .replaceAll("٩", "9").replaceAll("٠", "0")
                .replaceAll("٫", ".").replaceAll("ص", "AM")
                .replaceAll("م", "PM").replaceAll("،", ","));
        return newValue;
    }
}
