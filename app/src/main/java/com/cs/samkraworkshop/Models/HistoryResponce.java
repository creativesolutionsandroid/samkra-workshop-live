package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class HistoryResponce implements Serializable {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("MessageEn")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("ws")
        private ArrayList<Ws> ws;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("NetAmount")
        private float NetAmount;
        @Expose
        @SerializedName("VatCharges")
        private float VatCharges;
        @Expose
        @SerializedName("Discount")
        private float Discount;
        @Expose
        @SerializedName("SubTotal")
        private float SubTotal;
        @Expose
        @SerializedName("TotalAmount")
        private float TotalAmount;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("WorkshopId")
        private int WorkshopId;
        @Expose
        @SerializedName("BidId")
        private int BidId;
        @Expose
        @SerializedName("RequestCode")
        private String RequestCode;
        @Expose
        @SerializedName("PdfLocations")
        private String PdfLocations;
        @Expose
        @SerializedName("RequestId")
        private int RequestId;
        @Expose
        @SerializedName("InvoiceId")
        private int InvoiceId;
        @Expose
        @SerializedName("RequestDetails")
        private ArrayList<RequestDetails> RequestDetails;

        public ArrayList<Ws> getWs() {
            return ws;
        }

        public void setWs(ArrayList<Ws> ws) {
            this.ws = ws;
        }

        public String getPdfLocations() {
            return PdfLocations;
        }

        public void setPdfLocations(String pdfLocations) {
            PdfLocations = pdfLocations;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String CreatedOn) {
            this.CreatedOn = CreatedOn;
        }

        public float getNetAmount() {
            return NetAmount;
        }

        public void setNetAmount(float NetAmount) {
            this.NetAmount = NetAmount;
        }

        public float getVatCharges() {
            return VatCharges;
        }

        public void setVatCharges(float VatCharges) {
            this.VatCharges = VatCharges;
        }

        public float getDiscount() {
            return Discount;
        }

        public void setDiscount(float Discount) {
            this.Discount = Discount;
        }

        public float getSubTotal() {
            return SubTotal;
        }

        public void setSubTotal(float SubTotal) {
            this.SubTotal = SubTotal;
        }

        public float getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(float TotalAmount) {
            this.TotalAmount = TotalAmount;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public int getWorkshopId() {
            return WorkshopId;
        }

        public void setWorkshopId(int WorkshopId) {
            this.WorkshopId = WorkshopId;
        }

        public int getBidId() {
            return BidId;
        }

        public void setBidId(int BidId) {
            this.BidId = BidId;
        }

        public String getRequestCode() {
            return RequestCode;
        }

        public void setRequestCode(String RequestCode) {
            this.RequestCode = RequestCode;
        }

        public int getRequestId() {
            return RequestId;
        }

        public void setRequestId(int RequestId) {
            this.RequestId = RequestId;
        }

        public int getInvoiceId() {
            return InvoiceId;
        }

        public void setInvoiceId(int InvoiceId) {
            this.InvoiceId = InvoiceId;
        }

        public ArrayList<RequestDetails> getRequestDetails() {
            return RequestDetails;
        }

        public void setRequestDetails(ArrayList<RequestDetails> RequestDetails) {
            this.RequestDetails = RequestDetails;
        }
    }

    public static class Ws implements Serializable{
        @Expose
        @SerializedName("II")
        private ArrayList<II> II;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("StreetNameAr")
        private String StreetNameAr;
        @Expose
        @SerializedName("StreetNameEn")
        private String StreetNameEn;
        @Expose
        @SerializedName("BuildingNameAr")
        private String BuildingNameAr;
        @Expose
        @SerializedName("BuildingNameEn")
        private String BuildingNameEn;
        @Expose
        @SerializedName("WorkshopNameAr")
        private String WorkshopNameAr;
        @Expose
        @SerializedName("WorkshopNameEn")
        private String WorkshopNameEn;
        @Expose
        @SerializedName("Minquote")
        private float Minquote;
        @Expose
        @SerializedName("Maxquote")
        private float Maxquote;

        public float getMinquote() {
            return Minquote;
        }

        public void setMinquote(float minquote) {
            Minquote = minquote;
        }

        public float getMaxquote() {
            return Maxquote;
        }

        public void setMaxquote(float maxquote) {
            Maxquote = maxquote;
        }

        public ArrayList<II> getII() {
            return II;
        }

        public void setII(ArrayList<II> II) {
            this.II = II;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getStreetNameAr() {
            return StreetNameAr;
        }

        public void setStreetNameAr(String StreetNameAr) {
            this.StreetNameAr = StreetNameAr;
        }

        public String getStreetNameEn() {
            return StreetNameEn;
        }

        public void setStreetNameEn(String StreetNameEn) {
            this.StreetNameEn = StreetNameEn;
        }

        public String getBuildingNameAr() {
            return BuildingNameAr;
        }

        public void setBuildingNameAr(String BuildingNameAr) {
            this.BuildingNameAr = BuildingNameAr;
        }

        public String getBuildingNameEn() {
            return BuildingNameEn;
        }

        public void setBuildingNameEn(String BuildingNameEn) {
            this.BuildingNameEn = BuildingNameEn;
        }

        public String getWorkshopNameAr() {
            return WorkshopNameAr;
        }

        public void setWorkshopNameAr(String WorkshopNameAr) {
            this.WorkshopNameAr = WorkshopNameAr;
        }

        public String getWorkshopNameEn() {
            return WorkshopNameEn;
        }

        public void setWorkshopNameEn(String WorkshopNameEn) {
            this.WorkshopNameEn = WorkshopNameEn;
        }
    }

    public static class II implements Serializable{
        @Expose
        @SerializedName("ItemTotal")
        private float ItemTotal;
        @Expose
        @SerializedName("UnitPrice")
        private float UnitPrice;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("Item")
        private String Item;
        @Expose
        @SerializedName("InvoiceId")
        private int InvoiceId;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("MeasuringUnits")
        private String MeasuringUnits;
        @Expose
        @SerializedName("MeasuringUnitsAr")
        private String MeasuringUnitsAr;

        public String getMeasuringUnitsAr() {
            return MeasuringUnitsAr;
        }

        public void setMeasuringUnitsAr(String measuringUnitsAr) {
            MeasuringUnitsAr = measuringUnitsAr;
        }

        public String getMeasuringUnits() {
            return MeasuringUnits;
        }

        public void setMeasuringUnits(String measuringUnits) {
            MeasuringUnits = measuringUnits;
        }

        public float getItemTotal() {
            return ItemTotal;
        }

        public void setItemTotal(float ItemTotal) {
            this.ItemTotal = ItemTotal;
        }

        public float getUnitPrice() {
            return UnitPrice;
        }

        public void setUnitPrice(float UnitPrice) {
            this.UnitPrice = UnitPrice;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public String getItem() {
            return Item;
        }

        public void setItem(String Item) {
            this.Item = Item;
        }

        public int getInvoiceId() {
            return InvoiceId;
        }

        public void setInvoiceId(int InvoiceId) {
            this.InvoiceId = InvoiceId;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }
    }

    public static class RequestDetails implements Serializable{
        @Expose
        @SerializedName("CM")
        private ArrayList<CM> CM;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("UserComments")
        private String UserComments;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("Year")
        private int Year;
        @Expose
        @SerializedName("ModelId")
        private int ModelId;
        @Expose
        @SerializedName("MakerId")
        private int MakerId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("RequestCode")
        private String RequestCode;
        @Expose
        @SerializedName("RequestId")
        private int RequestId;

        public ArrayList<CM> getCM() {
            return CM;
        }

        public void setCM(ArrayList<CM> CM) {
            this.CM = CM;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String CreatedOn) {
            this.CreatedOn = CreatedOn;
        }

        public boolean getStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public String getUserComments() {
            return UserComments;
        }

        public void setUserComments(String UserComments) {
            this.UserComments = UserComments;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public int getYear() {
            return Year;
        }

        public void setYear(int Year) {
            this.Year = Year;
        }

        public int getModelId() {
            return ModelId;
        }

        public void setModelId(int ModelId) {
            this.ModelId = ModelId;
        }

        public int getMakerId() {
            return MakerId;
        }

        public void setMakerId(int MakerId) {
            this.MakerId = MakerId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getRequestCode() {
            return RequestCode;
        }

        public void setRequestCode(String RequestCode) {
            this.RequestCode = RequestCode;
        }

        public int getRequestId() {
            return RequestId;
        }

        public void setRequestId(int RequestId) {
            this.RequestId = RequestId;
        }
    }

    public static class CM implements Serializable{
        @Expose
        @SerializedName("MDL")
        private ArrayList<MDL> MDL;
        @Expose
        @SerializedName("CarMakerNameAr")
        private String CarMakerNameAr;
        @Expose
        @SerializedName("CarMakerNameEn")
        private String CarMakerNameEn;
        @Expose
        @SerializedName("CarMakerId")
        private int CarMakerId;

        public ArrayList<MDL> getMDL() {
            return MDL;
        }

        public void setMDL(ArrayList<MDL> MDL) {
            this.MDL = MDL;
        }

        public String getCarMakerNameAr() {
            return CarMakerNameAr;
        }

        public void setCarMakerNameAr(String CarMakerNameAr) {
            this.CarMakerNameAr = CarMakerNameAr;
        }

        public String getCarMakerNameEn() {
            return CarMakerNameEn;
        }

        public void setCarMakerNameEn(String CarMakerNameEn) {
            this.CarMakerNameEn = CarMakerNameEn;
        }

        public int getCarMakerId() {
            return CarMakerId;
        }

        public void setCarMakerId(int CarMakerId) {
            this.CarMakerId = CarMakerId;
        }
    }

    public static class MDL implements Serializable{
        @Expose
        @SerializedName("RCI")
        private ArrayList<RCI> RCI;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("ModelNameAr")
        private String ModelNameAr;
        @Expose
        @SerializedName("ModelNameEn")
        private String ModelNameEn;
        @Expose
        @SerializedName("ModelId")
        private int ModelId;

        public ArrayList<RCI> getRCI() {
            return RCI;
        }

        public void setRCI(ArrayList<RCI> RCI) {
            this.RCI = RCI;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getModelNameAr() {
            return ModelNameAr;
        }

        public void setModelNameAr(String ModelNameAr) {
            this.ModelNameAr = ModelNameAr;
        }

        public String getModelNameEn() {
            return ModelNameEn;
        }

        public void setModelNameEn(String ModelNameEn) {
            this.ModelNameEn = ModelNameEn;
        }

        public int getModelId() {
            return ModelId;
        }

        public void setModelId(int ModelId) {
            this.ModelId = ModelId;
        }
    }

    public static class RCI implements Serializable{
        @Expose
        @SerializedName("DocumentLocation")
        private String DocumentLocation;
        @Expose
        @SerializedName("DocumentType")
        private int DocumentType;

        public String getDocumentLocation() {
            return DocumentLocation;
        }

        public void setDocumentLocation(String DocumentLocation) {
            this.DocumentLocation = DocumentLocation;
        }

        public int getDocumentType() {
            return DocumentType;
        }

        public void setDocumentType(int DocumentType) {
            this.DocumentType = DocumentType;
        }
    }
}
