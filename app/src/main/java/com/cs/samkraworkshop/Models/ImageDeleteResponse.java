package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageDeleteResponse {


    @Expose
    @SerializedName("Data")
    private int Data;
    @Expose
    @SerializedName("MessageEn")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public int getData() {
        return Data;
    }

    public void setData(int Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }
}
