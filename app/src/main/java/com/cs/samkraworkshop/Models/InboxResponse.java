package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class InboxResponse implements Serializable {


    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("MessageEn")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("CM")
        private ArrayList<CM> CM;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("UserComments")
        private String UserComments;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Year")
        private int Year;
        @Expose
        @SerializedName("ModelId")
        private int ModelId;
        @Expose
        @SerializedName("RequestStatus")
        private String RequestStatus;
        @Expose
        @SerializedName("MakerId")
        private int MakerId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("RequestId")
        private int RequestId;
        @Expose
        @SerializedName("RequestCode")
        private String RequestCode;
        @Expose
        @SerializedName("Username")
        private String Username;
        @Expose
        @SerializedName("Phone")
        private String Phone;

        public String getUsername() {
            return Username;
        }

        public void setUsername(String username) {
            Username = username;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getRequestCode() {
            return RequestCode;
        }

        public void setRequestCode(String requestCode) {
            RequestCode = requestCode;
        }

        public ArrayList<CM> getCM() {
            return CM;
        }

        public void setCM(ArrayList<CM> CM) {
            this.CM = CM;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String CreatedOn) {
            this.CreatedOn = CreatedOn;
        }

        public boolean getStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public String getUserComments() {
            return UserComments;
        }

        public void setUserComments(String UserComments) {
            this.UserComments = UserComments;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public int getYear() {
            return Year;
        }

        public void setYear(int Year) {
            this.Year = Year;
        }

        public int getModelId() {
            return ModelId;
        }

        public void setModelId(int ModelId) {
            this.ModelId = ModelId;
        }

        public String getRequestStatus() {
            return RequestStatus;
        }

        public void setRequestStatus(String RequestStatus) {
            this.RequestStatus = RequestStatus;
        }

        public int getMakerId() {
            return MakerId;
        }

        public void setMakerId(int MakerId) {
            this.MakerId = MakerId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getRequestId() {
            return RequestId;
        }

        public void setRequestId(int RequestId) {
            this.RequestId = RequestId;
        }
    }

    public static class CM implements Serializable {
        @Expose
        @SerializedName("MDL")
        private ArrayList<MDL> MDL;
        @Expose
        @SerializedName("CarMakerNameAr")
        private String CarMakerNameAr;
        @Expose
        @SerializedName("CarMakerNameEn")
        private String CarMakerNameEn;
        @Expose
        @SerializedName("CarMakerId")
        private int CarMakerId;

        public ArrayList<MDL> getMDL() {
            return MDL;
        }

        public void setMDL(ArrayList<MDL> MDL) {
            this.MDL = MDL;
        }

        public String getCarMakerNameAr() {
            return CarMakerNameAr;
        }

        public void setCarMakerNameAr(String CarMakerNameAr) {
            this.CarMakerNameAr = CarMakerNameAr;
        }

        public String getCarMakerNameEn() {
            return CarMakerNameEn;
        }

        public void setCarMakerNameEn(String CarMakerNameEn) {
            this.CarMakerNameEn = CarMakerNameEn;
        }

        public int getCarMakerId() {
            return CarMakerId;
        }

        public void setCarMakerId(int CarMakerId) {
            this.CarMakerId = CarMakerId;
        }
    }

    public static class MDL implements Serializable{
        @Expose
        @SerializedName("RB")
        private ArrayList<RB> RB;
        @Expose
        @SerializedName("ModelNameAr")
        private String ModelNameAr;
        @Expose
        @SerializedName("ModelNameEn")
        private String ModelNameEn;
        @Expose
        @SerializedName("ModelId")
        private int ModelId;

        public ArrayList<RB> getRB() {
            return RB;
        }

        public void setRB(ArrayList<RB> RB) {
            this.RB = RB;
        }

        public String getModelNameAr() {
            return ModelNameAr;
        }

        public void setModelNameAr(String ModelNameAr) {
            this.ModelNameAr = ModelNameAr;
        }

        public String getModelNameEn() {
            return ModelNameEn;
        }

        public void setModelNameEn(String ModelNameEn) {
            this.ModelNameEn = ModelNameEn;
        }

        public int getModelId() {
            return ModelId;
        }

        public void setModelId(int ModelId) {
            this.ModelId = ModelId;
        }
    }

    public static class RB implements Serializable{
        @Expose
        @SerializedName("RCI")
        private ArrayList<RCI> RCI;
        @Expose
        @SerializedName("WSFinalPriceOn")
        private String WSFinalPriceOn;
        @Expose
        @SerializedName("FinalPriceWSComment")
        private String FinalPriceWSComment;
        @Expose
        @SerializedName("FinalPrice")
        private float FinalPrice;
        @Expose
        @SerializedName("IsWSFinalPrice")
        private boolean IsWSFinalPrice;
        @Expose
        @SerializedName("AcceptedOn")
        private String AcceptedOn;
        @Expose
        @SerializedName("UserAcceptedComment")
        private String UserAcceptedComment;
        @Expose
        @SerializedName("BidAccepted")
        private boolean BidAccepted;
        @Expose
        @SerializedName("UserViewedBid")
        private boolean UserViewedBid;
        @Expose
        @SerializedName("BidOn")
        private String BidOn;
        @Expose
        @SerializedName("WorkShopComments")
        private String WorkShopComments;
        @Expose
        @SerializedName("MaxQuote")
        private float MaxQuote;
        @Expose
        @SerializedName("MinQuote")
        private float MinQuote;
        @Expose
        @SerializedName("WorkshopId")
        private int WorkshopId;
        @Expose
        @SerializedName("BidId")
        private int BidId;
        @Expose
        @SerializedName("EstimatedDate")
        private String EstimatedDate;

        public String getEstimatedDate() {
            return EstimatedDate;
        }

        public void setEstimatedDate(String estimatedDate) {
            EstimatedDate = estimatedDate;
        }

        public ArrayList<RCI> getRCI() {
            return RCI;
        }

        public void setRCI(ArrayList<RCI> RCI) {
            this.RCI = RCI;
        }

        public String getWSFinalPriceOn() {
            return WSFinalPriceOn;
        }

        public void setWSFinalPriceOn(String WSFinalPriceOn) {
            this.WSFinalPriceOn = WSFinalPriceOn;
        }

        public String getFinalPriceWSComment() {
            return FinalPriceWSComment;
        }

        public void setFinalPriceWSComment(String FinalPriceWSComment) {
            this.FinalPriceWSComment = FinalPriceWSComment;
        }

        public float getFinalPrice() {
            return FinalPrice;
        }

        public void setFinalPrice(float FinalPrice) {
            this.FinalPrice = FinalPrice;
        }

        public boolean getIsWSFinalPrice() {
            return IsWSFinalPrice;
        }

        public void setIsWSFinalPrice(boolean IsWSFinalPrice) {
            this.IsWSFinalPrice = IsWSFinalPrice;
        }

        public String getAcceptedOn() {
            return AcceptedOn;
        }

        public void setAcceptedOn(String AcceptedOn) {
            this.AcceptedOn = AcceptedOn;
        }

        public String getUserAcceptedComment() {
            return UserAcceptedComment;
        }

        public void setUserAcceptedComment(String UserAcceptedComment) {
            this.UserAcceptedComment = UserAcceptedComment;
        }

        public boolean getBidAccepted() {
            return BidAccepted;
        }

        public void setBidAccepted(boolean BidAccepted) {
            this.BidAccepted = BidAccepted;
        }

        public boolean getUserViewedBid() {
            return UserViewedBid;
        }

        public void setUserViewedBid(boolean UserViewedBid) {
            this.UserViewedBid = UserViewedBid;
        }

        public String getBidOn() {
            return BidOn;
        }

        public void setBidOn(String BidOn) {
            this.BidOn = BidOn;
        }

        public String getWorkShopComments() {
            return WorkShopComments;
        }

        public void setWorkShopComments(String WorkShopComments) {
            this.WorkShopComments = WorkShopComments;
        }

        public float getMaxQuote() {
            return MaxQuote;
        }

        public void setMaxQuote(float MaxQuote) {
            this.MaxQuote = MaxQuote;
        }

        public float getMinQuote() {
            return MinQuote;
        }

        public void setMinQuote(float MinQuote) {
            this.MinQuote = MinQuote;
        }

        public int getWorkshopId() {
            return WorkshopId;
        }

        public void setWorkshopId(int WorkshopId) {
            this.WorkshopId = WorkshopId;
        }

        public int getBidId() {
            return BidId;
        }

        public void setBidId(int BidId) {
            this.BidId = BidId;
        }
    }

    public static class RCI implements Serializable {
        @Expose
        @SerializedName("DocumentLocation")
        private String DocumentLocation;
        @Expose
        @SerializedName("DocumentType")
        private int DocumentType;

        public String getDocumentLocation() {
            return DocumentLocation;
        }

        public void setDocumentLocation(String DocumentLocation) {
            this.DocumentLocation = DocumentLocation;
        }

        public int getDocumentType() {
            return DocumentType;
        }

        public void setDocumentType(int DocumentType) {
            this.DocumentType = DocumentType;
        }
    }
}
