package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LanguageResponce {

    @Expose
    @SerializedName("Data")
    public Data Data;
    @Expose
    @SerializedName("MessageEn")
    private String message;
    @Expose
    @SerializedName("MessageAr")
    private String messageAr;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public String getMessageAr() {
        return messageAr;
    }

    public void setMessageAr(String messageAr) {
        this.messageAr = messageAr;
    }

    public static class Data {
        @Expose
        @SerializedName("Language")
        public String Language;
        @Expose
        @SerializedName("UserId")
        public int UserId;
    }
}
