package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class DashboardResponse implements Serializable {


    @Expose
    @SerializedName("Data")
    private List<Data> Data;
    @Expose
    @SerializedName("MessageEn")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public List<Data> getData() {
        return Data;
    }

    public void setData(List<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("MeasuringUnits")
        private List<MeasuringUnits> MeasuringUnits;
        @Expose
        @SerializedName("ListFinalPrice")
        private List<ListFinalPrice> ListFinalPrice;
        @Expose
        @SerializedName("Workshopdocuments")
        private List<Workshopdocuments> Workshopdocuments;
        @Expose
        @SerializedName("Bids")
        private int Bids;
        @Expose
        @SerializedName("YearlyPrice")
        private float YearlyPrice;
        @Expose
        @SerializedName("MonthlyPrice")
        private float MonthlyPrice;
        @Expose
        @SerializedName("WeeklyPrice")
        private float WeeklyPrice;
        @Expose
        @SerializedName("Done")
        private int Done;
        @Expose
        @SerializedName("Accepted")
        private int Accepted;
        @Expose
        @SerializedName("Reviews")
        private int Reviews;
        @Expose
        @SerializedName("Offers")
        private int Offers;
        @Expose
        @SerializedName("ZipCode")
        private String ZipCode;
        @Expose
        @SerializedName("District")
        private List<District> District;
        @Expose
        @SerializedName("City")
        private List<City> City;
        @Expose
        @SerializedName("Country")
        private List<Country> Country;
        @Expose
        @SerializedName("StreetNameAr")
        private String StreetNameAr;
        @Expose
        @SerializedName("StreetNameEn")
        private String StreetNameEn;
        @Expose
        @SerializedName("BuildingNameAr")
        private String BuildingNameAr;
        @Expose
        @SerializedName("BuildingNameEn")
        private String BuildingNameEn;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("LandlineNo")
        private String LandlineNo;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("AboutAr")
        private String AboutAr;
        @Expose
        @SerializedName("AboutEn")
        private String AboutEn;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("WorkshopId")
        private int WorkshopId;

        public List<MeasuringUnits> getMeasuringUnits() {
            return MeasuringUnits;
        }

        public void setMeasuringUnits(List<MeasuringUnits> MeasuringUnits) {
            this.MeasuringUnits = MeasuringUnits;
        }

        public List<ListFinalPrice> getListFinalPrice() {
            return ListFinalPrice;
        }

        public void setListFinalPrice(List<ListFinalPrice> ListFinalPrice) {
            this.ListFinalPrice = ListFinalPrice;
        }

        public List<Workshopdocuments> getWorkshopdocuments() {
            return Workshopdocuments;
        }

        public void setWorkshopdocuments(List<Workshopdocuments> Workshopdocuments) {
            this.Workshopdocuments = Workshopdocuments;
        }

        public int getBids() {
            return Bids;
        }

        public void setBids(int Bids) {
            this.Bids = Bids;
        }

        public float getYearlyPrice() {
            return YearlyPrice;
        }

        public void setYearlyPrice(float YearlyPrice) {
            this.YearlyPrice = YearlyPrice;
        }

        public float getMonthlyPrice() {
            return MonthlyPrice;
        }

        public void setMonthlyPrice(float MonthlyPrice) {
            this.MonthlyPrice = MonthlyPrice;
        }

        public float getWeeklyPrice() {
            return WeeklyPrice;
        }

        public void setWeeklyPrice(float WeeklyPrice) {
            this.WeeklyPrice = WeeklyPrice;
        }

        public int getDone() {
            return Done;
        }

        public void setDone(int Done) {
            this.Done = Done;
        }

        public int getAccepted() {
            return Accepted;
        }

        public void setAccepted(int Accepted) {
            this.Accepted = Accepted;
        }

        public int getReviews() {
            return Reviews;
        }

        public void setReviews(int Reviews) {
            this.Reviews = Reviews;
        }

        public int getOffers() {
            return Offers;
        }

        public void setOffers(int Offers) {
            this.Offers = Offers;
        }

        public String getZipCode() {
            return ZipCode;
        }

        public void setZipCode(String ZipCode) {
            this.ZipCode = ZipCode;
        }

        public List<District> getDistrict() {
            return District;
        }

        public void setDistrict(List<District> District) {
            this.District = District;
        }

        public List<City> getCity() {
            return City;
        }

        public void setCity(List<City> City) {
            this.City = City;
        }

        public List<Country> getCountry() {
            return Country;
        }

        public void setCountry(List<Country> Country) {
            this.Country = Country;
        }

        public String getStreetNameAr() {
            return StreetNameAr;
        }

        public void setStreetNameAr(String StreetNameAr) {
            this.StreetNameAr = StreetNameAr;
        }

        public String getStreetNameEn() {
            return StreetNameEn;
        }

        public void setStreetNameEn(String StreetNameEn) {
            this.StreetNameEn = StreetNameEn;
        }

        public String getBuildingNameAr() {
            return BuildingNameAr;
        }

        public void setBuildingNameAr(String BuildingNameAr) {
            this.BuildingNameAr = BuildingNameAr;
        }

        public String getBuildingNameEn() {
            return BuildingNameEn;
        }

        public void setBuildingNameEn(String BuildingNameEn) {
            this.BuildingNameEn = BuildingNameEn;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getLandlineNo() {
            return LandlineNo;
        }

        public void setLandlineNo(String LandlineNo) {
            this.LandlineNo = LandlineNo;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getAboutAr() {
            return AboutAr;
        }

        public void setAboutAr(String AboutAr) {
            this.AboutAr = AboutAr;
        }

        public String getAboutEn() {
            return AboutEn;
        }

        public void setAboutEn(String AboutEn) {
            this.AboutEn = AboutEn;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public int getWorkshopId() {
            return WorkshopId;
        }

        public void setWorkshopId(int WorkshopId) {
            this.WorkshopId = WorkshopId;
        }
    }

    public static class MeasuringUnits {
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class ListFinalPrice {
        @Expose
        @SerializedName("CM")
        private List<CM> CM;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("UserComments")
        private String UserComments;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Year")
        private int Year;
        @Expose
        @SerializedName("ModelId")
        private int ModelId;
        @Expose
        @SerializedName("MakerId")
        private int MakerId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("RequestCode")
        private String RequestCode;
        @Expose
        @SerializedName("RequestId")
        private int RequestId;

        public List<CM> getCM() {
            return CM;
        }

        public void setCM(List<CM> CM) {
            this.CM = CM;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String CreatedOn) {
            this.CreatedOn = CreatedOn;
        }

        public boolean getStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public String getUserComments() {
            return UserComments;
        }

        public void setUserComments(String UserComments) {
            this.UserComments = UserComments;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public int getYear() {
            return Year;
        }

        public void setYear(int Year) {
            this.Year = Year;
        }

        public int getModelId() {
            return ModelId;
        }

        public void setModelId(int ModelId) {
            this.ModelId = ModelId;
        }

        public int getMakerId() {
            return MakerId;
        }

        public void setMakerId(int MakerId) {
            this.MakerId = MakerId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getRequestCode() {
            return RequestCode;
        }

        public void setRequestCode(String RequestCode) {
            this.RequestCode = RequestCode;
        }

        public int getRequestId() {
            return RequestId;
        }

        public void setRequestId(int RequestId) {
            this.RequestId = RequestId;
        }
    }

    public static class CM {
        @Expose
        @SerializedName("MDL")
        private List<MDL> MDL;
        @Expose
        @SerializedName("CarMakerNameAr")
        private String CarMakerNameAr;
        @Expose
        @SerializedName("CarMakerNameEn")
        private String CarMakerNameEn;
        @Expose
        @SerializedName("CarMakerId")
        private int CarMakerId;

        public List<MDL> getMDL() {
            return MDL;
        }

        public void setMDL(List<MDL> MDL) {
            this.MDL = MDL;
        }

        public String getCarMakerNameAr() {
            return CarMakerNameAr;
        }

        public void setCarMakerNameAr(String CarMakerNameAr) {
            this.CarMakerNameAr = CarMakerNameAr;
        }

        public String getCarMakerNameEn() {
            return CarMakerNameEn;
        }

        public void setCarMakerNameEn(String CarMakerNameEn) {
            this.CarMakerNameEn = CarMakerNameEn;
        }

        public int getCarMakerId() {
            return CarMakerId;
        }

        public void setCarMakerId(int CarMakerId) {
            this.CarMakerId = CarMakerId;
        }
    }

    public static class MDL {
        @Expose
        @SerializedName("RB")
        private List<RB> RB;
        @Expose
        @SerializedName("ModelNameAr")
        private String ModelNameAr;
        @Expose
        @SerializedName("ModelNameEn")
        private String ModelNameEn;
        @Expose
        @SerializedName("ModelId")
        private int ModelId;

        public List<RB> getRB() {
            return RB;
        }

        public void setRB(List<RB> RB) {
            this.RB = RB;
        }

        public String getModelNameAr() {
            return ModelNameAr;
        }

        public void setModelNameAr(String ModelNameAr) {
            this.ModelNameAr = ModelNameAr;
        }

        public String getModelNameEn() {
            return ModelNameEn;
        }

        public void setModelNameEn(String ModelNameEn) {
            this.ModelNameEn = ModelNameEn;
        }

        public int getModelId() {
            return ModelId;
        }

        public void setModelId(int ModelId) {
            this.ModelId = ModelId;
        }
    }

    public static class RB {
        @Expose
        @SerializedName("RCI")
        private List<RCI> RCI;
        @Expose
        @SerializedName("UserFinalPriceAcceptedOn")
        private String UserFinalPriceAcceptedOn;
        @Expose
        @SerializedName("UserFinalPriceAccepted")
        private boolean UserFinalPriceAccepted;
        @Expose
        @SerializedName("UserViewedFinalPrice")
        private boolean UserViewedFinalPrice;
        @Expose
        @SerializedName("WSFinalPriceOn")
        private String WSFinalPriceOn;
        @Expose
        @SerializedName("FinalPriceWSComment")
        private String FinalPriceWSComment;
        @Expose
        @SerializedName("FinalPrice")
        private float FinalPrice;
        @Expose
        @SerializedName("IsWSFinalPrice")
        private boolean IsWSFinalPrice;
        @Expose
        @SerializedName("AcceptedOn")
        private String AcceptedOn;
        @Expose
        @SerializedName("UserAcceptedComment")
        private String UserAcceptedComment;
        @Expose
        @SerializedName("BidAccepted")
        private boolean BidAccepted;
        @Expose
        @SerializedName("UserViewedBid")
        private boolean UserViewedBid;
        @Expose
        @SerializedName("BidOn")
        private String BidOn;
        @Expose
        @SerializedName("WorkShopComments")
        private String WorkShopComments;
        @Expose
        @SerializedName("MaxQuote")
        private float MaxQuote;
        @Expose
        @SerializedName("MinQuote")
        private float MinQuote;
        @Expose
        @SerializedName("WorkshopId")
        private int WorkshopId;
        @Expose
        @SerializedName("BidId")
        private int BidId;

        public List<RCI> getRCI() {
            return RCI;
        }

        public void setRCI(List<RCI> RCI) {
            this.RCI = RCI;
        }

        public String getUserFinalPriceAcceptedOn() {
            return UserFinalPriceAcceptedOn;
        }

        public void setUserFinalPriceAcceptedOn(String UserFinalPriceAcceptedOn) {
            this.UserFinalPriceAcceptedOn = UserFinalPriceAcceptedOn;
        }

        public boolean getUserFinalPriceAccepted() {
            return UserFinalPriceAccepted;
        }

        public void setUserFinalPriceAccepted(boolean UserFinalPriceAccepted) {
            this.UserFinalPriceAccepted = UserFinalPriceAccepted;
        }

        public boolean getUserViewedFinalPrice() {
            return UserViewedFinalPrice;
        }

        public void setUserViewedFinalPrice(boolean UserViewedFinalPrice) {
            this.UserViewedFinalPrice = UserViewedFinalPrice;
        }

        public String getWSFinalPriceOn() {
            return WSFinalPriceOn;
        }

        public void setWSFinalPriceOn(String WSFinalPriceOn) {
            this.WSFinalPriceOn = WSFinalPriceOn;
        }

        public String getFinalPriceWSComment() {
            return FinalPriceWSComment;
        }

        public void setFinalPriceWSComment(String FinalPriceWSComment) {
            this.FinalPriceWSComment = FinalPriceWSComment;
        }

        public float getFinalPrice() {
            return FinalPrice;
        }

        public void setFinalPrice(float FinalPrice) {
            this.FinalPrice = FinalPrice;
        }

        public boolean getIsWSFinalPrice() {
            return IsWSFinalPrice;
        }

        public void setIsWSFinalPrice(boolean IsWSFinalPrice) {
            this.IsWSFinalPrice = IsWSFinalPrice;
        }

        public String getAcceptedOn() {
            return AcceptedOn;
        }

        public void setAcceptedOn(String AcceptedOn) {
            this.AcceptedOn = AcceptedOn;
        }

        public String getUserAcceptedComment() {
            return UserAcceptedComment;
        }

        public void setUserAcceptedComment(String UserAcceptedComment) {
            this.UserAcceptedComment = UserAcceptedComment;
        }

        public boolean getBidAccepted() {
            return BidAccepted;
        }

        public void setBidAccepted(boolean BidAccepted) {
            this.BidAccepted = BidAccepted;
        }

        public boolean getUserViewedBid() {
            return UserViewedBid;
        }

        public void setUserViewedBid(boolean UserViewedBid) {
            this.UserViewedBid = UserViewedBid;
        }

        public String getBidOn() {
            return BidOn;
        }

        public void setBidOn(String BidOn) {
            this.BidOn = BidOn;
        }

        public String getWorkShopComments() {
            return WorkShopComments;
        }

        public void setWorkShopComments(String WorkShopComments) {
            this.WorkShopComments = WorkShopComments;
        }

        public float getMaxQuote() {
            return MaxQuote;
        }

        public void setMaxQuote(float MaxQuote) {
            this.MaxQuote = MaxQuote;
        }

        public float getMinQuote() {
            return MinQuote;
        }

        public void setMinQuote(float MinQuote) {
            this.MinQuote = MinQuote;
        }

        public int getWorkshopId() {
            return WorkshopId;
        }

        public void setWorkshopId(int WorkshopId) {
            this.WorkshopId = WorkshopId;
        }

        public int getBidId() {
            return BidId;
        }

        public void setBidId(int BidId) {
            this.BidId = BidId;
        }
    }

    public static class RCI {
        @Expose
        @SerializedName("DocumentLocation")
        private String DocumentLocation;
        @Expose
        @SerializedName("DocumentType")
        private int DocumentType;

        public String getDocumentLocation() {
            return DocumentLocation;
        }

        public void setDocumentLocation(String DocumentLocation) {
            this.DocumentLocation = DocumentLocation;
        }

        public int getDocumentType() {
            return DocumentType;
        }

        public void setDocumentType(int DocumentType) {
            this.DocumentType = DocumentType;
        }
    }

    public static class Workshopdocuments {
        @Expose
        @SerializedName("DocumentLocation")
        private String DocumentLocation;
        @Expose
        @SerializedName("DocumentType")
        private int DocumentType;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getDocumentLocation() {
            return DocumentLocation;
        }

        public void setDocumentLocation(String DocumentLocation) {
            this.DocumentLocation = DocumentLocation;
        }

        public int getDocumentType() {
            return DocumentType;
        }

        public void setDocumentType(int DocumentType) {
            this.DocumentType = DocumentType;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class District {
        @Expose
        @SerializedName("DistrictNameAr")
        private String DistrictNameAr;
        @Expose
        @SerializedName("DistrictNameEn")
        private String DistrictNameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getDistrictNameAr() {
            return DistrictNameAr;
        }

        public void setDistrictNameAr(String DistrictNameAr) {
            this.DistrictNameAr = DistrictNameAr;
        }

        public String getDistrictNameEn() {
            return DistrictNameEn;
        }

        public void setDistrictNameEn(String DistrictNameEn) {
            this.DistrictNameEn = DistrictNameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class City {
        @Expose
        @SerializedName("CityNameAr")
        private String CityNameAr;
        @Expose
        @SerializedName("CityNameEn")
        private String CityNameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getCityNameAr() {
            return CityNameAr;
        }

        public void setCityNameAr(String CityNameAr) {
            this.CityNameAr = CityNameAr;
        }

        public String getCityNameEn() {
            return CityNameEn;
        }

        public void setCityNameEn(String CityNameEn) {
            this.CityNameEn = CityNameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Country {
        @Expose
        @SerializedName("CountryNameAr")
        private String CountryNameAr;
        @Expose
        @SerializedName("CountryNameEn")
        private String CountryNameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getCountryNameAr() {
            return CountryNameAr;
        }

        public void setCountryNameAr(String CountryNameAr) {
            this.CountryNameAr = CountryNameAr;
        }

        public String getCountryNameEn() {
            return CountryNameEn;
        }

        public void setCountryNameEn(String CountryNameEn) {
            this.CountryNameEn = CountryNameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
