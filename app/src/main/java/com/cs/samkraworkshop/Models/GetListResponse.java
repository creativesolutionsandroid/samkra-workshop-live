package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetListResponse {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageEn")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("CarMakerList")
        private List<CarMakerList> CarMakerList;
        @Expose
        @SerializedName("WorkshopDocumentsList")
        private List<WorkshopDocumentsList> WorkshopDocumentsList;
        @Expose
        @SerializedName("SupportedCarMakerList")
        private List<SupportedCarMakerList> SupportedCarMakerList;
        @Expose
        @SerializedName("WorkshopDetails")
        private List<WorkshopDetails> WorkshopDetails;
        @Expose
        @SerializedName("DistrictList")
        private List<DistrictList> DistrictList;
        @Expose
        @SerializedName("CountryList")
        private List<CountryList> CountryList;
        @Expose
        @SerializedName("CityList")
        private List<CityList> CityList;

        public List<CarMakerList> getCarMakerList() {
            return CarMakerList;
        }

        public void setCarMakerList(List<CarMakerList> CarMakerList) {
            this.CarMakerList = CarMakerList;
        }

        public List<WorkshopDocumentsList> getWorkshopDocumentsList() {
            return WorkshopDocumentsList;
        }

        public void setWorkshopDocumentsList(List<WorkshopDocumentsList> WorkshopDocumentsList) {
            this.WorkshopDocumentsList = WorkshopDocumentsList;
        }

        public List<SupportedCarMakerList> getSupportedCarMakerList() {
            return SupportedCarMakerList;
        }

        public void setSupportedCarMakerList(List<SupportedCarMakerList> SupportedCarMakerList) {
            this.SupportedCarMakerList = SupportedCarMakerList;
        }

        public List<WorkshopDetails> getWorkshopDetails() {
            return WorkshopDetails;
        }

        public void setWorkshopDetails(List<WorkshopDetails> WorkshopDetails) {
            this.WorkshopDetails = WorkshopDetails;
        }

        public List<DistrictList> getDistrictList() {
            return DistrictList;
        }

        public void setDistrictList(List<DistrictList> DistrictList) {
            this.DistrictList = DistrictList;
        }

        public List<CountryList> getCountryList() {
            return CountryList;
        }

        public void setCountryList(List<CountryList> CountryList) {
            this.CountryList = CountryList;
        }

        public List<CityList> getCityList() {
            return CityList;
        }

        public void setCityList(List<CityList> CityList) {
            this.CityList = CityList;
        }
    }

    public static class CarMakerList {
        @Expose
        @SerializedName("Checked")
        private boolean Checked;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public boolean getChecked() {
            return Checked;
        }

        public void setChecked(boolean Checked) {
            this.Checked = Checked;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class WorkshopDocumentsList {
        @Expose
        @SerializedName("DocumentType")
        private int DocumentType;
        @Expose
        @SerializedName("DocumentLocation")
        private String DocumentLocation;
        @Expose
        @SerializedName("WorkshopId")
        private int WorkshopId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getDocumentType() {
            return DocumentType;
        }

        public void setDocumentType(int DocumentType) {
            this.DocumentType = DocumentType;
        }

        public String getDocumentLocation() {
            return DocumentLocation;
        }

        public void setDocumentLocation(String DocumentLocation) {
            this.DocumentLocation = DocumentLocation;
        }

        public int getWorkshopId() {
            return WorkshopId;
        }

        public void setWorkshopId(int WorkshopId) {
            this.WorkshopId = WorkshopId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class SupportedCarMakerList {
        @Expose
        @SerializedName("cm")
        private List<Cm> cm;
        @Expose
        @SerializedName("Checked")
        private boolean Checked;
        @Expose
        @SerializedName("Id")
        private int Id;

        public List<Cm> getCm() {
            return cm;
        }

        public void setCm(List<Cm> cm) {
            this.cm = cm;
        }

        public boolean getChecked() {
            return Checked;
        }

        public void setChecked(boolean Checked) {
            this.Checked = Checked;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Cm {
        @Expose
        @SerializedName("NameEn")
        private String NameEn;

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }
    }

    public static class WorkshopDetails {
        @Expose
        @SerializedName("profileUpdateStages")
        private int profileUpdateStages;
        @Expose
        @SerializedName("DistrictID")
        private int DistrictID;
        @Expose
        @SerializedName("CityID")
        private int CityID;
        @Expose
        @SerializedName("CountryID")
        private int CountryID;
        @Expose
        @SerializedName("AddressEn")
        private String AddressEn;
        @Expose
        @SerializedName("AboutAr")
        private String AboutAr;
        @Expose
        @SerializedName("AboutEn")
        private String AboutEn;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("WorkshopCode")
        private String WorkshopCode;
        @Expose
        @SerializedName("ApplicationStatus")
        private String ApplicationStatus;
        @Expose
        @SerializedName("Language")
        private String Language;
        @Expose
        @SerializedName("EmailID")
        private String EmailID;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("LandLineNo")
        private String LandLineNo;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getProfileUpdateStages() {
            return profileUpdateStages;
        }

        public void setProfileUpdateStages(int profileUpdateStages) {
            this.profileUpdateStages = profileUpdateStages;
        }

        public int getDistrictID() {
            return DistrictID;
        }

        public void setDistrictID(int DistrictID) {
            this.DistrictID = DistrictID;
        }

        public int getCityID() {
            return CityID;
        }

        public void setCityID(int CityID) {
            this.CityID = CityID;
        }

        public int getCountryID() {
            return CountryID;
        }

        public void setCountryID(int CountryID) {
            this.CountryID = CountryID;
        }

        public String getAddressEn() {
            return AddressEn;
        }

        public void setAddressEn(String AddressEn) {
            this.AddressEn = AddressEn;
        }

        public String getAboutAr() {
            return AboutAr;
        }

        public void setAboutAr(String AboutAr) {
            this.AboutAr = AboutAr;
        }

        public String getAboutEn() {
            return AboutEn;
        }

        public void setAboutEn(String AboutEn) {
            this.AboutEn = AboutEn;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public String getWorkshopCode() {
            return WorkshopCode;
        }

        public void setWorkshopCode(String WorkshopCode) {
            this.WorkshopCode = WorkshopCode;
        }

        public String getApplicationStatus() {
            return ApplicationStatus;
        }

        public void setApplicationStatus(String ApplicationStatus) {
            this.ApplicationStatus = ApplicationStatus;
        }

        public String getLanguage() {
            return Language;
        }

        public void setLanguage(String Language) {
            this.Language = Language;
        }

        public String getEmailID() {
            return EmailID;
        }

        public void setEmailID(String EmailID) {
            this.EmailID = EmailID;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getLandLineNo() {
            return LandLineNo;
        }

        public void setLandLineNo(String LandLineNo) {
            this.LandLineNo = LandLineNo;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class DistrictList {
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("CityId")
        private int CityId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getCityId() {
            return CityId;
        }

        public void setCityId(int CityId) {
            this.CityId = CityId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class CountryList {
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class CityList {
        @Expose
        @SerializedName("CountryId")
        private int CountryId;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getCountryId() {
            return CountryId;
        }

        public void setCountryId(int CountryId) {
            this.CountryId = CountryId;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
