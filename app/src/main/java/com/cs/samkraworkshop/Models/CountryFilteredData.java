package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CountryFilteredData {

    private String NameAr;
    private String NameEn;
    private int Id;
    private ArrayList<Cities> cities = new ArrayList<>();

    public String getNameAr() {
        return NameAr;
    }

    public void setNameAr(String nameAr) {
        NameAr = nameAr;
    }

    public String getNameEn() {
        return NameEn;
    }

    public void setNameEn(String nameEn) {
        NameEn = nameEn;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public ArrayList<Cities> getCities() {
        return cities;
    }

    public void setCities(ArrayList<Cities> cities) {
        this.cities = cities;
    }

    public static class Cities {

        private int CountryId;
        private String NameAr;
        private String NameEn;
        private int Id;
        private ArrayList<Districts> districts = new ArrayList<>();

        public int getCountryId() {
            return CountryId;
        }

        public void setCountryId(int countryId) {
            CountryId = countryId;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String nameAr) {
            NameAr = nameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String nameEn) {
            NameEn = nameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public ArrayList<Districts> getDistricts() {
            return districts;
        }

        public void setDistricts(ArrayList<Districts> districts) {
            this.districts = districts;
        }
    }

    public static class Districts {

        private String NameAr;
        private String NameEn;
        private int CityId;
        private int Id;

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String nameAr) {
            NameAr = nameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String nameEn) {
            NameEn = nameEn;
        }

        public int getCityId() {
            return CityId;
        }

        public void setCityId(int cityId) {
            CityId = cityId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }
    }
}
