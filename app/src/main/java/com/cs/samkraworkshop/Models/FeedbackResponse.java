package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class FeedbackResponse {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("MessageEn")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("wr")
        private ArrayList<Wr> wr;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Logo")
        private String Logo;
        @Expose
        @SerializedName("WorkshopId")
        private int WorkshopId;

        public ArrayList<Wr> getWr() {
            return wr;
        }

        public void setWr(ArrayList<Wr> wr) {
            this.wr = wr;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String Logo) {
            this.Logo = Logo;
        }

        public int getWorkshopId() {
            return WorkshopId;
        }

        public void setWorkshopId(int WorkshopId) {
            this.WorkshopId = WorkshopId;
        }
    }

    public static class Wr {
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("Comments")
        private String Comments;
        @Expose
        @SerializedName("Rating")
        private int Rating;
        @Expose
        @SerializedName("RequestCode")
        private String RequestId;
        @Expose
        @SerializedName("Username")
        private String Username;
        @Expose
        @SerializedName("UserId")
        private int UserId;

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public String getComments() {
            return Comments;
        }

        public void setComments(String Comments) {
            this.Comments = Comments;
        }

        public int getRating() {
            return Rating;
        }

        public void setRating(int Rating) {
            this.Rating = Rating;
        }

        public String getRequestId() {
            return RequestId;
        }

        public void setRequestId(String RequestId) {
            this.RequestId = RequestId;
        }

        public String getUsername() {
            return Username;
        }

        public void setUsername(String Username) {
            this.Username = Username;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }
    }
}
