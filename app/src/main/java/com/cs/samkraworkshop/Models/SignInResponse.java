package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignInResponse {


    @Expose
    @SerializedName("ApplicationStatus")
    private String ApplicationStatus;
    @Expose
    @SerializedName("WorkshopID")
    private int WorkshopID;
    @Expose
    @SerializedName("MessageEn")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("ProfileUpdateStages")
    private int ProfileUpdateStages;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }


    public int getProfileUpdateStages() {
        return ProfileUpdateStages;
    }

    public void setProfileUpdateStages(int profileUpdateStages) {
        ProfileUpdateStages = profileUpdateStages;
    }

    public String getApplicationStatus() {
        return ApplicationStatus;
    }

    public void setApplicationStatus(String ApplicationStatus) {
        this.ApplicationStatus = ApplicationStatus;
    }

    public int getWorkshopID() {
        return WorkshopID;
    }

    public void setWorkshopID(int WorkshopID) {
        this.WorkshopID = WorkshopID;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }
}
