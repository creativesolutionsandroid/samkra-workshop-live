package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class NewRequestsResponse implements Serializable {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data = new ArrayList<>();
    @Expose
    @SerializedName("MessageEn")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("CM")
        private ArrayList<CM> CM = new ArrayList<>();
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("UserComments")
        private String UserComments;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Year")
        private int Year;
        @Expose
        @SerializedName("ModelId")
        private int ModelId;
        @Expose
        @SerializedName("MakerId")
        private int MakerId;
        @Expose
        @SerializedName("RequestId")
        private int RequestId;
        @Expose
        @SerializedName("RequestCount")
        private int RequestCount;
        @Expose
        @SerializedName("RequestCode")
        private String RequestCode;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("ExpiresOn")
        private String ExpiresOn;
        @Expose
        @SerializedName("RemainingTime")
        private String RemainingTime;

        public String getRemainingTime() {
            return RemainingTime;
        }

        public void setRemainingTime(String remainingTime) {
            RemainingTime = remainingTime;
        }

        public String getExpiresOn() {
            return ExpiresOn;
        }

        public void setExpiresOn(String expiresOn) {
            ExpiresOn = expiresOn;
        }

        public int getRequestCount() {
            return RequestCount;
        }

        public void setRequestCount(int requestCount) {
            RequestCount = requestCount;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String createdOn) {
            CreatedOn = createdOn;
        }

        public ArrayList<CM> getCM() {
            return CM;
        }

        public void setCM(ArrayList<CM> CM) {
            this.CM = CM;
        }

        public boolean getStatus() {
            return Status;
        }

        public String getRequestCode() {
            return RequestCode;
        }

        public void setRequestCode(String requestCode) {
            RequestCode = requestCode;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public String getUserComments() {
            return UserComments;
        }

        public void setUserComments(String UserComments) {
            this.UserComments = UserComments;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public int getYear() {
            return Year;
        }

        public void setYear(int Year) {
            this.Year = Year;
        }

        public int getModelId() {
            return ModelId;
        }

        public void setModelId(int ModelId) {
            this.ModelId = ModelId;
        }

        public int getMakerId() {
            return MakerId;
        }

        public void setMakerId(int MakerId) {
            this.MakerId = MakerId;
        }

        public int getRequestId() {
            return RequestId;
        }

        public void setRequestId(int RequestId) {
            this.RequestId = RequestId;
        }
    }

    public static class CM implements Serializable{
        @Expose
        @SerializedName("MDL")
        private ArrayList<MDL> MDL = new ArrayList<>();
        @Expose
        @SerializedName("CarMakerNameAr")
        private String CarMakerNameAr;
        @Expose
        @SerializedName("CarMakerNameEn")
        private String CarMakerNameEn;
        @Expose
        @SerializedName("CarMakerId")
        private int CarMakerId;

        public ArrayList<MDL> getMDL() {
            return MDL;
        }

        public void setMDL(ArrayList<MDL> MDL) {
            this.MDL = MDL;
        }

        public String getCarMakerNameAr() {
            return CarMakerNameAr;
        }

        public void setCarMakerNameAr(String CarMakerNameAr) {
            this.CarMakerNameAr = CarMakerNameAr;
        }

        public String getCarMakerNameEn() {
            return CarMakerNameEn;
        }

        public void setCarMakerNameEn(String CarMakerNameEn) {
            this.CarMakerNameEn = CarMakerNameEn;
        }

        public int getCarMakerId() {
            return CarMakerId;
        }

        public void setCarMakerId(int CarMakerId) {
            this.CarMakerId = CarMakerId;
        }
    }

    public static class MDL implements Serializable{
        @Expose
        @SerializedName("RCI")
        private ArrayList<RCI> RCI = new ArrayList<>();
        @Expose
        @SerializedName("ModelNameAr")
        private String ModelNameAr;
        @Expose
        @SerializedName("ModelNameEn")
        private String ModelNameEn;
        @Expose
        @SerializedName("ModelId")
        private int ModelId;

        public ArrayList<RCI> getRCI() {
            return RCI;
        }

        public void setRCI(ArrayList<RCI> RCI) {
            this.RCI = RCI;
        }

        public String getModelNameAr() {
            return ModelNameAr;
        }

        public void setModelNameAr(String ModelNameAr) {
            this.ModelNameAr = ModelNameAr;
        }

        public String getModelNameEn() {
            return ModelNameEn;
        }

        public void setModelNameEn(String ModelNameEn) {
            this.ModelNameEn = ModelNameEn;
        }

        public int getModelId() {
            return ModelId;
        }

        public void setModelId(int ModelId) {
            this.ModelId = ModelId;
        }
    }

    public static class RCI implements Serializable{
        @Expose
        @SerializedName("DocumentLocation")
        private String DocumentLocation;
        @Expose
        @SerializedName("DocumentType")
        private int DocumentType;

        public String getDocumentLocation() {
            return DocumentLocation;
        }

        public void setDocumentLocation(String DocumentLocation) {
            this.DocumentLocation = DocumentLocation;
        }

        public int getDocumentType() {
            return DocumentType;
        }

        public void setDocumentType(int DocumentType) {
            this.DocumentType = DocumentType;
        }
    }
}
