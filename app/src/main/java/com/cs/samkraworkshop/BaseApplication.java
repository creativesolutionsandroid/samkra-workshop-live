package com.cs.samkraworkshop;

import android.app.Application;

import com.cs.samkraworkshop.Utils.LocaleHelper;


/**
 * Created by CS on 26-05-2016.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        LocaleHelper.onCreate(this, "en");
    }
}
