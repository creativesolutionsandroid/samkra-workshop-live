package com.cs.samkraworkshop.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.SignInResponse;
import com.cs.samkraworkshop.Models.VerifyMobileResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.SplashScreen;
import com.cs.samkraworkshop.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends Activity implements View.OnClickListener {

    TextView signUp, forgotPassword,dlanguage, mobile_name;
    EditText mobileEt, inputPassword;
    String strMobile, strPassword;
    Button continueBtn;
    String TAG = "TAG";
    AlertDialog loaderDialog;
    private final int SIGNUP = 1;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    String language;
    SharedPreferences.Editor languagePrefsEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        languagePrefsEditor  = languagePrefs.edit();
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_signin);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_signin_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        inputPassword= (EditText) findViewById(R.id.et_password);
        mobileEt = (EditText) findViewById(R.id.et_mobile);
        continueBtn = (Button) findViewById(R.id.continue_btn);
        signUp = (TextView) findViewById(R.id.signup);
        forgotPassword = (TextView) findViewById(R.id.forgot);
        dlanguage=(TextView)findViewById(R.id.language);
//        mobile_name = (TextView) findViewById(R.id.mobile_name);

        signUp.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
        continueBtn.setOnClickListener(this);

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
//            mobileEt.requestFocus();
//            mobileEt.setSelection(mobileEt.length()-1);
//
        }
//        int position = mobileEt.length();
//        Editable etext = mobileEt.getText();
//        Selection.setSelection(etext, position);

        dlanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    Intent i = new Intent(SignInActivity.this, SignInActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    finish();
                }else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    Intent i = new Intent(SignInActivity.this, SignInActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    finish();
                }

            }
        });
    }


    private void setTypeface(){
        signUp.setTypeface(Constants.getarTypeFace(SignInActivity.this));
        forgotPassword.setTypeface(Constants.getarTypeFace(SignInActivity.this));
        mobileEt.setTypeface(Constants.getarTypeFace(SignInActivity.this));
        inputPassword.setTypeface(Constants.getarTypeFace(SignInActivity.this));
        ((TextView) findViewById(R.id.signintext)).setTypeface(Constants.getarTypeFace(SignInActivity.this));
        ((TextView) findViewById(R.id.signup)).setTypeface(Constants.getarTypeFace(SignInActivity.this));
        ((TextView) findViewById(R.id.continue_btn)).setTypeface(Constants.getarTypeFace(SignInActivity.this));


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.continue_btn:
                if(validations()){
                    String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new signInApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")){

                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();

                        }
                        else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                break;

            case R.id.signup:
                Intent intent = new Intent(SignInActivity.this, SignupActivity.class);
                startActivityForResult(intent, SIGNUP);
                break;

            case R.id.forgot:
                Intent intent1 = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                startActivity(intent1);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == SIGNUP && requestCode == RESULT_OK){
            Intent i = new Intent(SignInActivity.this, MainActivity.class);
            i.putExtra("type", "1");
            startActivity(i);
        }
    }

    private boolean validations(){
        strMobile = mobileEt.getText().toString();
        strPassword = inputPassword.getText().toString();

        if (strMobile.length() == 0){
            if (language.equalsIgnoreCase("En")){
                mobileEt.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            }
            else {
                mobileEt.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }

            return false;
        }
        else if (strMobile.length() != 9){
            if (language.equalsIgnoreCase("En")){
                mobileEt.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            }
            else {
                mobileEt.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
            }

            return false;
        }
        else if (strPassword.length() == 0){
            if (language.equalsIgnoreCase("En")){
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            }
            else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }

            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20){
            if (language.equalsIgnoreCase("En")){
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            }
            else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));

            }
            return false;
        }
        return true;
    }

    private class signInApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SignInResponse> call = apiService.signIn(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SignInResponse>() {
                @Override
                public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()){
                        SignInResponse signInResponse = response.body();
                        try {
                            if(signInResponse.getStatus()){
                                if(signInResponse.getApplicationStatus().equalsIgnoreCase("approved")){
                                    userPrefsEditor.putString("userId", ""+signInResponse.getWorkshopID());
                                    userPrefsEditor.putString("stage", "4");
                                    userPrefsEditor.commit();
                                    Intent i = new Intent(SignInActivity.this, MainActivity.class);
                                    i.putExtra("type", "1");
                                    startActivity(i);
                                    finish();
                                }
                                else if(signInResponse.getApplicationStatus().equalsIgnoreCase("new")
                                        || signInResponse.getApplicationStatus().equalsIgnoreCase("pending")){
                                    if(signInResponse.getProfileUpdateStages() == 3 ) {
                                        if(language.equalsIgnoreCase("En")) {
                                            Constants.showOneButtonAlertDialog("Your account is pending for approval", getResources().getString(R.string.app_name),
                                                    getResources().getString(R.string.ok), SignInActivity.this);
                                        }
                                        else {
                                            Constants.showOneButtonAlertDialog("حسابك في انتظار الموافقة عليه", getResources().getString(R.string.samkra_ar),
                                                    getResources().getString(R.string.ok_ar), SignInActivity.this);
                                        }
                                    }
                                    else{
                                        userPrefsEditor.putString("userId", ""+signInResponse.getWorkshopID());
                                        userPrefsEditor.commit();
                                        Intent i = new Intent(SignInActivity.this, RejectedProfileActivity.class);
                                        startActivity(i);
                                    }
                                }
                                else if (signInResponse.getApplicationStatus().equalsIgnoreCase("rejected")){
                                    userPrefsEditor.putString("userId", ""+signInResponse.getWorkshopID());
                                    userPrefsEditor.commit();
//                                    showRejectedDialog("Your account is rejected because of improper data. Please update your workshop details.", getResources().getString(R.string.app_name),
//                                            getResources().getString(R.string.ok), SignInActivity.this);
                                    if(language.equalsIgnoreCase("En")) {
                                        showRejectedDialog("Your account is rejected because of improper data. Please update your workshop details.", getResources().getString(R.string.app_name),
                                                getResources().getString(R.string.ok), SignInActivity.this);
                                    }
                                    else {
                                        showRejectedDialog("تم رفض تسجيل الحساب لعدم وجود جميع المستندات والمعلومات المطلوبة. الرجاء ادخال باقي المتطلبات الأساسية لفتح الحساب", getResources().getString(R.string.app_name_ar),
                                                getResources().getString(R.string.ok_ar), SignInActivity.this);
                                    }
                                }
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = signInResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), SignInActivity.this);
                                }
                                else {
                                    String failureResponse = signInResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), SignInActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(SignInActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(SignInActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignInActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignInActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog!= null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<SignInResponse> call, Throwable t) {
                    Log.d(TAG, "onResponse: "+t.toString());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignInActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignInActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignInActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignInActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("MobileNo","966"+strMobile);
            mobileObj.put("Password",strPassword);
            mobileObj.put("Language","En");
            mobileObj.put("DeviceToken",SplashScreen.regId);
            mobileObj.put("DeviceVersion",Constants.getDeviceType(SignInActivity.this));
            Log.d(TAG, "prepareVerifyMobileJson: "+mobileObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    public void showRejectedDialog(String descriptionStr, String titleStr, String buttonStr, Activity context){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(SignInActivity.this));
            desc.setTypeface(Constants.getarTypeFace(SignInActivity.this));
            yes.setTypeface(Constants.getarTypeFace(SignInActivity.this));
            no.setTypeface(Constants.getarTypeFace(SignInActivity.this));
        }

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
                startActivity(new Intent(SignInActivity.this, RejectedProfileActivity.class));
                finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignInActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
