package com.cs.samkraworkshop.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Adapter.InvoiceItemAdapter;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.HistoryResponce;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Utils.PDFDownloader;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.html.HTML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.zip.Inflater;



public class InvoiceActivity extends AppCompatActivity {

    ImageView backBtn, print, download;
    TextView order_number, date, userName;
    TextView totalamt, vatamt, subamt, discountamt, netamt, finalamt;
    String TAG = "TAG";
    String Language = "En";
    ArrayList<HistoryResponce.Data> data = new ArrayList<>();
    String userId;
    int invoicePos;
    LinearLayout itemsLayout;
    File file12;
    AlertDialog loaderDialog;

    ScrollView mscrollview;
    String[] dt;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    String language;

    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")){
            setContentView(R.layout.invoices);
        }
        else {
            setContentView(R.layout.invoices_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        userName = (TextView) findViewById(R.id.user_name);
        backBtn = findViewById(R.id.hy_back_btn);
        download = findViewById(R.id.download);

        totalamt = findViewById(R.id.totalamt);
        vatamt = findViewById(R.id.vatamt);
        subamt = findViewById(R.id.subamt);
        discountamt = findViewById(R.id.discountamt);
        netamt = findViewById(R.id.netamt);
        finalamt = findViewById(R.id.finalamt);
//        comment = findViewById(R.id.comment);
        order_number = findViewById(R.id.order_number);
        date = findViewById(R.id.date);
        itemsLayout = (LinearLayout) findViewById(R.id.item_list);

        mscrollview = findViewById(R.id.scrollView);

        data = (ArrayList<HistoryResponce.Data>) getIntent().getSerializableExtra("data");
        invoicePos = getIntent().getIntExtra("pos", 0);

        order_number.setText("#" + data.get(invoicePos).getInvoiceNo());
        userName.setText(data.get(invoicePos).getWs().get(0).getUserName());

        String str = data.get(invoicePos).getRequestDetails().get(0).getCreatedOn();
        String[] array = str.split("T");
        dt = array[0].split("-");

        date.setText(Constants.convertToArabic(dt[2]+"-"+dt[1]+"-"+dt[0]));
        totalamt.setText(Constants.convertToArabic(Constants.priceFormat.format(data.get(invoicePos).getTotalAmount())));
        vatamt.setText(Constants.convertToArabic(Constants.priceFormat.format(data.get(invoicePos).getVatCharges())));
        subamt.setText(Constants.convertToArabic(Constants.priceFormat.format(data.get(invoicePos).getSubTotal())));
        discountamt.setText(Constants.convertToArabic(Constants.priceFormat.format(data.get(invoicePos).getDiscount())));
        netamt.setText(Constants.convertToArabic(Constants.priceFormat.format(data.get(invoicePos).getNetAmount())));
        finalamt.setText(Constants.convertToArabic(Constants.priceFormat.format(data.get(invoicePos).getNetAmount())));

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                else {
                    createPDF(data.get(invoicePos).getInvoiceNo());
                }
            }
        });

        itemsLayout.removeAllViews();
        for (int i = 0; i < data.get(invoicePos).getWs().get(0).getII().size(); i++) {
            LayoutInflater inflater = (LayoutInflater) this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            View v;
            if (language.equalsIgnoreCase("En")){
                 v = inflater.inflate(R.layout.invoice_list, null);
            }
            else {
                 v = inflater.inflate(R.layout.invoice_list_ar, null);
            }

            TextView mitem = (TextView) v.findViewById(R.id.item);
            TextView qty = (TextView) v.findViewById(R.id.qty);
            TextView units = (TextView) v.findViewById(R.id.units);
            TextView amt = (TextView) v.findViewById(R.id.amt);

            if (language.equalsIgnoreCase("En")){
                units.setText(data.get(invoicePos).getWs().get(0).getII().get(i).getMeasuringUnits());
            }
            else {
                ((TextView) v.findViewById(R.id.units)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
                units.setText(data.get(invoicePos).getWs().get(0).getII().get(i).getMeasuringUnitsAr());
            }
            qty.setText(""+data.get(invoicePos).getWs().get(0).getII().get(i).getQuantity());
            mitem.setText(data.get(invoicePos).getWs().get(0).getII().get(i).getItem());
            amt.setText(Constants.convertToArabic(Constants.priceFormat.format(data.get(invoicePos).getWs().get(0).getII().get(i).getItemTotal())));
            itemsLayout.addView(v);
        }
        if (language.equalsIgnoreCase("Ar")) {
            setTypeface();
        }
        }
    private void setTypeface() {
        totalamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        vatamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        subamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        discountamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        netamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        finalamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        date.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        order_number.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        userName.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        discountamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        discountamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.amt)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.st2)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.st3)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.st4)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.st5)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.st6)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.st7)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.st8)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.st9)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.order_number_txt)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.date_txt)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.Customer_txt)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.history)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView) findViewById(R.id.final_sar)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(InvoiceActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
                    createPDF(data.get(invoicePos).getInvoiceNo());
                }
                else {
                    Toast.makeText(InvoiceActivity.this, "Storage permission denied, Unable to share invoice", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void createPDF(String invoice){

        if(data.get(invoicePos).getPdfLocations()!= null) {
            try {
                File file = new File(Environment.getExternalStorageDirectory() + "/samkra/" + invoice + ".pdf");
                if (file.exists()) {
                    sharePdf(invoice);
                } else {
                    new PDFDownload().execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else{
            if (language.equalsIgnoreCase("En")){
                Toast.makeText(this, "Invoice not found", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(this, "لا يوجد فاتورة", Toast.LENGTH_SHORT).show();
            }

        }
    }

    // Method for opening a pdf file
    private void sharePdf(String invoice) {
        File outputFile = new File(Environment.getExternalStorageDirectory()+"/samkra/"+invoice+".pdf");
        Uri photoURI = FileProvider.getUriForFile(InvoiceActivity.this,
                getApplicationContext().getPackageName() + ".my.package.name.provider", outputFile);

        Intent share = new Intent();
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        share.setAction(Intent.ACTION_SEND);
        share.setType("application/pdf");
        share.putExtra(Intent.EXTRA_STREAM, photoURI);

        startActivity(share);
    }

    private class PDFDownload extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String extStorageDirectory = Environment.getExternalStorageDirectory()
                        .toString();
                File folder = new File(extStorageDirectory, "samkra");
                folder.mkdir();
                file12 = new File(folder, data.get(invoicePos).getInvoiceNo()+".pdf");
                try {
                    file12.createNewFile();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                FileOutputStream f = new FileOutputStream(file12);

                URL u = new URL(Constants.INVOICE_PATH+data.get(invoicePos).getPdfLocations());
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
//                c.setDoOutput(true);
                c.connect();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                Log.d(TAG, "doInBackground: "+buffer.length);
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (Exception e) {
                e.printStackTrace();
                if (language.equalsIgnoreCase("En")){
                    Toast.makeText(InvoiceActivity.this, "Invoice not found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(InvoiceActivity.this, "لا يوجد فاتورة", Toast.LENGTH_SHORT).show();
                }
            }
            loaderDialog.dismiss();
            sharePdf(data.get(invoicePos).getInvoiceNo());
            return null;
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(InvoiceActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
