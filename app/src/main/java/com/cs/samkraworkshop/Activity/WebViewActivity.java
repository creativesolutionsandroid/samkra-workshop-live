package com.cs.samkraworkshop.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cs.samkraworkshop.R;


/**
 * Created by CS on 06-07-2016.
 */
public class WebViewActivity extends AppCompatActivity {
    TextView screenTitle;
    private ProgressBar mProgressBar;
    ImageView backBtn;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.webview_activity);
        } else {
            setContentView(R.layout.webview_activity_arabic);
        }

        WebView wv = (WebView) findViewById(R.id.webView);
        screenTitle = (TextView) findViewById(R.id.header_title);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        mProgressBar.setVisibility(View.VISIBLE);

        screenTitle.setText(getIntent().getExtras().getString("title"));
        wv.loadUrl(getIntent().getExtras().getString("url"));
        wv.setWebViewClient(new MyWebViewClient());
        wv.getSettings().setLoadsImagesAutomatically(true);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mProgressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }
}
