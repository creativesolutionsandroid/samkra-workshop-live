package com.cs.samkraworkshop.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Adapter.SideMenuAdapter;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Fragment.BidsFragment;
import com.cs.samkraworkshop.Fragment.DashboardFragment;
import com.cs.samkraworkshop.Fragment.FeedbackFragment;
import com.cs.samkraworkshop.Fragment.HistoryFragment;
import com.cs.samkraworkshop.Fragment.InboxFragment;
import com.cs.samkraworkshop.Fragment.MoreFragment;
import com.cs.samkraworkshop.Fragment.NotificationFragment;
import com.cs.samkraworkshop.Fragment.ProfileFragment;
import com.cs.samkraworkshop.Models.LanguageResponce;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.SplashScreen;
import com.cs.samkraworkshop.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    LinearLayout mDrawerLinear;
    String[] sideMenuItems;
    Integer[] sideMenuImages;
    ListView sideMenuListView;
    SideMenuAdapter mSideMenuAdapter;
    int itemSelectedPostion = 0;
    public static DrawerLayout drawer;
    android.app.AlertDialog loaderDialog;

    FragmentManager fragmentManager = getSupportFragmentManager();

    public static com.lovejjfg.shadowcircle.CircleImageView profilePic;
    public static TextView workshopName;
    ImageView closeMenu, logout;
    String language;
    SharedPreferences languagePrefs;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    AlertDialog customDialog;
    String TAG = "TAG";
    SharedPreferences.Editor languagePrefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        languagePrefsEditor  = languagePrefs.edit();
        Log.d(TAG, "language "+language);
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_main);
        }
        else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_main_ar);
        }

//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLinear = (LinearLayout) findViewById(R.id.left_drawer);

        workshopName = (TextView) findViewById(R.id.name_workshop);
        profilePic = (com.lovejjfg.shadowcircle.CircleImageView) findViewById(R.id.profile_pic);
        closeMenu = (ImageView) findViewById(R.id.close_btn);
        logout = (ImageView) findViewById(R.id.logout_btn);

        workshopName.setText(userPrefs.getString("NameEn", ""));

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if(getIntent().getStringExtra("type").equals("2")){
            NotificationFragment homeScreenFragment = new NotificationFragment();
            fragmentTransaction.add(R.id.fragment_layout, homeScreenFragment);
            fragmentTransaction.commit();
        }
        else {
            DashboardFragment homeScreenFragment = new DashboardFragment();
            fragmentTransaction.add(R.id.fragment_layout, homeScreenFragment);
            fragmentTransaction.commit();
        }
        if (language.equalsIgnoreCase("En")){
            sideMenuItems = new String[]{getResources().getString(R.string.side_menu_home),
                    getResources().getString(R.string.side_menu_bids),
                    getResources().getString(R.string.side_menu_inbox),
                    getResources().getString(R.string.side_menu_history),
                    getResources().getString(R.string.side_menu_reviews),
                    getResources().getString(R.string.side_menu_notifications),
                    getResources().getString(R.string.side_menu_profile),
                    getResources().getString(R.string.side_menu_more),
                    getResources().getString(R.string.menu_language)
            };
        }
        else {
            sideMenuItems = new String[]{getResources().getString(R.string.menu_home_ar),
                    getResources().getString(R.string.side_menu_bids_ar),
                    getResources().getString(R.string.menu_inbox_ar),
                    getResources().getString(R.string.menu_invoices_ar),
                    getResources().getString(R.string.side_menu_reviews_ar),
                    getResources().getString(R.string.menu_notification_ar),
                    getResources().getString(R.string.profile_ar),
                    getResources().getString(R.string.menu_AboutUscreen_ar),
                    getResources().getString(R.string.menu_language_ar)
            };
        }

        sideMenuImages = new Integer[] {R.drawable.home_new,
                R.drawable.bids,
                R.drawable.inbox_ar,
                R.drawable.invoice_new,
                R.drawable.review,
                R.drawable.notification_new,
                R.drawable.profile_new,
                R.drawable.about_us,
                R.drawable.language_ar};



        sideMenuListView = (ListView) findViewById(R.id.side_menu_list_view);
        if (language.equalsIgnoreCase("En")){
            mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);
        }
        else {
            mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu_ar, sideMenuItems, sideMenuImages, itemSelectedPostion);
        }
        sideMenuListView.setAdapter(mSideMenuAdapter);
        sideMenuListView.setOnItemClickListener(new MainActivity.DrawerItemClickListener());

        closeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showtwoButtonsAlertDialog();
            }
        });

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment profileFragment = new ProfileFragment();
                FragmentManager fragmentManager12 = getSupportFragmentManager();

                FragmentTransaction merchandiseTransaction5 = fragmentManager12.beginTransaction();
                merchandiseTransaction5.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction5.replace(R.id.fragment_layout, profileFragment);
                merchandiseTransaction5.commit();
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
            }
        });
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }
    private void setTypeface() {
        workshopName.setTypeface(Constants.getarTypeFace(MainActivity.this));
    }
    public void showtwoButtonsAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        if (language.equalsIgnoreCase("En")){
            title.setText("Samkra");
            yes.setText(getResources().getString(R.string.yes));
            no.setText(getResources().getString(R.string.no));
            desc.setText("Do you want to logout?");
        }
        else {
            title.setText("سمكره");
            yes.setText(getResources().getString(R.string.yes_ar));
            no.setText(getResources().getString(R.string.no_ar));
            desc.setText("هل ترغب بسجيل الخروج؟");
        }

        if (language.equalsIgnoreCase("Ar")) {
            title.setTypeface(Constants.getarTypeFace(MainActivity.this));
            yes.setTypeface(Constants.getarTypeFace(MainActivity.this));
            no.setTypeface(Constants.getarTypeFace(MainActivity.this));
            desc.setTypeface(Constants.getarTypeFace(MainActivity.this));
        }


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                customDialog.dismiss();



                startActivity(new Intent(MainActivity.this, SignInActivity.class));
                finish();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
//        if (language.equalsIgnoreCase("Ar")){
//            setTypeface();
//        }
    }
    //    private void setTypeface() {
//        ((TextView)findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(MainActivity.this));
//        ((TextView)findViewById(R.id.st2)).setTypeface(Constants.getarTypeFace(MainActivity.this));
//
//    }
    private class DrawerItemClickListener implements ListView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    public void selectItem(int position){
        switch (position){
            case 0:
                Fragment mainFragment = new DashboardFragment();
                FragmentTransaction merchandiseTransaction = fragmentManager.beginTransaction();
                merchandiseTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction.replace(R.id.fragment_layout, mainFragment);
                merchandiseTransaction.commit();
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 1:
                Fragment bidsFragment = new BidsFragment();
                FragmentTransaction merchandiseTransaction1 = fragmentManager.beginTransaction();
                merchandiseTransaction1.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction1.replace(R.id.fragment_layout, bidsFragment);
                merchandiseTransaction1.commit();
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, bidsFragment).commit();
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 2:
                Fragment merchandiseFragment = new InboxFragment();
                Bundle mBundle7 = new Bundle();
                mBundle7.putString("mode", "");
                merchandiseFragment.setArguments(mBundle7);
                FragmentTransaction merchandiseTransaction2 = fragmentManager.beginTransaction();
                merchandiseTransaction2.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction2.replace(R.id.fragment_layout, merchandiseFragment);
                merchandiseTransaction2.commit();
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 3:
                Fragment historyFragment = new HistoryFragment();
                FragmentTransaction merchandiseTransaction3 = fragmentManager.beginTransaction();
                merchandiseTransaction3.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction3.replace(R.id.fragment_layout, historyFragment);
                merchandiseTransaction3.commit();
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, historyFragment).commit();
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 4:
                Fragment feedbackFragment = new FeedbackFragment();
                FragmentTransaction merchandiseTransaction4 = fragmentManager.beginTransaction();
                merchandiseTransaction4.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction4.replace(R.id.fragment_layout, feedbackFragment);
                merchandiseTransaction4.commit();
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, feedbackFragment).commit();
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 5:
                Fragment notificationFragment = new NotificationFragment();
                FragmentTransaction merchandiseTransaction7 = fragmentManager.beginTransaction();
                merchandiseTransaction7.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction7.replace(R.id.fragment_layout, notificationFragment);
                merchandiseTransaction7.commit();
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, profileFragment).commit();
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 6:
                Fragment profileFragment = new ProfileFragment();
                FragmentTransaction merchandiseTransaction5 = fragmentManager.beginTransaction();
                merchandiseTransaction5.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction5.replace(R.id.fragment_layout, profileFragment);
                merchandiseTransaction5.commit();
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, profileFragment).commit();
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 7:
                Fragment moreFragment = new MoreFragment();
                FragmentTransaction merchandiseTransaction6 = fragmentManager.beginTransaction();
                merchandiseTransaction6.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction6.replace(R.id.fragment_layout, moreFragment);
                merchandiseTransaction6.commit();
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, moreFragment).commit();
                if (language.equalsIgnoreCase("En")){
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;
            case 8:
                language = languagePrefs.getString("language", "En");
                if(language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    language = "Ar";
                    Intent i = new Intent(MainActivity.this, MainActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    finish();
                }
                else{
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    language = "En";
                    Intent i = new Intent(MainActivity.this, MainActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    finish();
                }
                break;        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!userPrefs.getString("userId", "").equals("")) {
            userLoginApi();
        }
    }

    private void userLoginApi() {
        String inputStr = prepareLoginJson();
//        showloaderAlertDialog();
        final String networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<LanguageResponce> call = apiService.langyagechange(
                RequestBody.create(MediaType.parse("application/json"), inputStr));

        call.enqueue(new Callback<LanguageResponce>() {
            @Override
            public void onResponse(Call<LanguageResponce> call, Response<LanguageResponce> response) {
                Log.d(TAG, "onResponse: " + response);
                if (response.isSuccessful()) {

                } else {

                }

//                loaderDialog.dismiss();
            }

            @Override
            public void onFailure(Call<LanguageResponce> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(MainActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                loaderDialog.dismiss();
            }
        });
    }
    private String prepareLoginJson () {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", userPrefs.getString("userId",""));
            language = languagePrefs.getString("language", "En");
            if (language.equalsIgnoreCase("En")) {
                parentObj.put("Language", "En");
            } else {
                parentObj.put("Language", "Ar");
            }
            parentObj.put("DeviceToken", SplashScreen.regId);
            parentObj.put("Language", language);
            parentObj.put("DeviceVersion", "Android");
            parentObj.put("UserType", 2);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj.toString());
        return parentObj.toString();
    }
    public void showloaderAlertDialog(){
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
