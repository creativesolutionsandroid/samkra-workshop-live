package com.cs.samkraworkshop.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkraworkshop.Adapter.BigImagesAdapter;
import com.cs.samkraworkshop.Adapter.ImagesAdapter;
import com.cs.samkraworkshop.Models.PlaceBidResponse;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.NewRequestsResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.cs.samkraworkshop.Utils.NumberTextWatcher;
import com.daasuu.bl.BubbleLayout;
import com.google.android.gms.dynamic.IFragmentWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferDetailsActivity extends Activity implements View.OnClickListener {

    private TextView requestId, make, model, year, expireTime, remainingChars;
    private EditText minQuote, maxQuote, userComments, workshopComments;
    private ImageView front, back, left, right;
    private TextView frontText, backText, leftText, rightText;
    private LinearLayout frontLayout, backLayout, leftLayout, rightLayout;
    private LinearLayout submitLayout, quitLayout;
    private BubbleLayout userCommentLayout;
    Boolean isRight = false, isLeft = false, isFront = false, isBack = false;
    float fMin = 0, fMax = 0;
    AlertDialog loaderDialog;
    String strComment = "No Comments";

    BigImagesAdapter mBigImageAdapter;
    ImagesAdapter mAdapter;
    public static ViewPager bigImageView;
    private ViewPager viewPager;

    private ImageView dummy1, dummy2;
    public static RelativeLayout bigImageLayout;

    int selected = 1;
    int position;
    ArrayList<NewRequestsResponse.Data> data = new ArrayList<>();
    ArrayList<String> rightImages = new ArrayList<>();
    ArrayList<String> leftImages = new ArrayList<>();
    ArrayList<String> frontImages = new ArrayList<>();
    ArrayList<String> backImages = new ArrayList<>();
    ScrollView scrollView;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    ImageView backBtn;
    ImageView back_dot, front_dot, left_dot, right_dot;
    long remainingMillis;
    CountDownTimer countDownTimer2;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_offer_details);
        }
        else {
            setContentView(R.layout.activity_offer_details_ar);
        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId","");

        position = getIntent().getIntExtra("pos", 0);
        data = (ArrayList<NewRequestsResponse.Data>) getIntent().getSerializableExtra("data");

        backBtn = (ImageView) findViewById(R.id.back_btn);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        requestId = (TextView) findViewById(R.id.request_code);
        make = (TextView) findViewById(R.id.car_make);
        model = (TextView) findViewById(R.id.car_model);
        year = (TextView) findViewById(R.id.year);
        expireTime = (TextView) findViewById(R.id.expire_time);
        remainingChars = (TextView) findViewById(R.id.comment_chars);

        minQuote = (EditText) findViewById(R.id.min_quote);
        maxQuote = (EditText) findViewById(R.id.max_quote);
        userComments = (EditText) findViewById(R.id.user_comment);
        workshopComments = (EditText) findViewById(R.id.workshop_comment);

        back_dot = (ImageView) findViewById(R.id.back_dot);
        front_dot = (ImageView) findViewById(R.id.front_dot);
        left_dot = (ImageView) findViewById(R.id.left_dot);
        right_dot = (ImageView) findViewById(R.id.right_dot);

        front = (ImageView) findViewById(R.id.front_side);
        back = (ImageView) findViewById(R.id.back_side);
        left = (ImageView) findViewById(R.id.left_side);
        right = (ImageView) findViewById(R.id.right_side);
        dummy1 = (ImageView) findViewById(R.id.dummyView1);
        dummy2 = (ImageView) findViewById(R.id.dummyView2);

        frontText = (TextView) findViewById(R.id.front_text);
        backText = (TextView) findViewById(R.id.back_text);
        leftText = (TextView) findViewById(R.id.left_text);
        rightText = (TextView) findViewById(R.id.right_text);

        frontLayout = (LinearLayout) findViewById(R.id.front_layout);
        backLayout = (LinearLayout) findViewById(R.id.back_layout);
        leftLayout = (LinearLayout) findViewById(R.id.left_layout);
        rightLayout = (LinearLayout) findViewById(R.id.right_layout);
        bigImageLayout = (RelativeLayout) findViewById(R.id.imagePopup);

        submitLayout = (LinearLayout) findViewById(R.id.submit_layout);
        quitLayout = (LinearLayout) findViewById(R.id.quit_layout);

        bigImageView = (ViewPager) findViewById(R.id.big_image_view);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        userCommentLayout = (BubbleLayout) findViewById(R.id.user_comment_layout);

        workshopComments.setImeOptions(EditorInfo.IME_ACTION_DONE);
        workshopComments.setRawInputType(InputType.TYPE_CLASS_TEXT);

        requestId.setText("#"+data.get(position).getRequestCode());
        if (language.equalsIgnoreCase("En")){
            make.setText(data.get(position).getCM().get(0).getCarMakerNameEn());
            model.setText(data.get(position).getCM().get(0).getMDL().get(0).getModelNameEn());
        }
        else {
            make.setText(data.get(position).getCM().get(0).getCarMakerNameAr());
            model.setText(data.get(position).getCM().get(0).getMDL().get(0).getModelNameAr());
        }

        year.setText(""+data.get(position).getYear());

        if(data.get(position).getUserComments() == null || data.get(position).getUserComments().length() == 0){
            userCommentLayout.setVisibility(View.GONE);
        }
        else{
            userComments.setText(data.get(position).getUserComments());
        }

        workshopComments.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                int len = 300 - (text.length());
                if (language.equalsIgnoreCase("En")){
                    remainingChars.setText(len+" characters left");
                }
                else {
                    remainingChars.setText(len+" حرف متبقي");
                }

                if (text.length() > 0) {
                    strComment = text;
                }
                else {
                    strComment = "No Comments";
                }
            }
        });

        getImages();

        try {
            Date bookedDate = null;
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

            String[] reservDate = data.get(position).getExpiresOn().replace("T", " ").split(" ");
            String checkInDate = reservDate[0]+" "+reservDate[1];

            try {
                bookedDate = DateFormat.parse(checkInDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long endMillis = bookedDate.getTime();
            long currentMillis = calendar.getTimeInMillis();
//            long endMillis = startMillis + (60 * 1440 * 1000);  /*24 hours*/
            remainingMillis = endMillis - currentMillis;
            if (currentMillis > endMillis) {
                remainingMillis = 0;
            }
//        model.start(remainingMillis);

            countDownTimer2 = new CountDownTimer(remainingMillis, 1000) {
                // 1000 means, onTick function will be called at every 1000 milliseconds

                @Override
                public void onTick(long leftTimeInMilliseconds) {
                    String hms = "00:00:00";
                    if (leftTimeInMilliseconds > (60 * 1440 * 1000)) {
                        hms = String.format("%02d:%02d:%02d:%02d", TimeUnit.MILLISECONDS.toDays(leftTimeInMilliseconds),
                                TimeUnit.MILLISECONDS.toHours(leftTimeInMilliseconds) % TimeUnit.DAYS.toHours(1),
                                TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds) % TimeUnit.HOURS.toMinutes(1),
                                TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));
                    }
                    else {
                        hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(leftTimeInMilliseconds),
                                TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds) % TimeUnit.HOURS.toMinutes(1),
                                TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));
                    }
                    expireTime.setText(Constants.convertToArabic(hms));
                }
                @Override
                public void onFinish() {
                    expireTime.setText("00:00:00");
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        front.setOnClickListener(this);
        back.setOnClickListener(this);
        left.setOnClickListener(this);
        right.setOnClickListener(this);
        dummy1.setOnClickListener(this);
        dummy2.setOnClickListener(this);
        submitLayout.setOnClickListener(this);
        quitLayout.setOnClickListener(this);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
        minQuote.addTextChangedListener(new NumberTextWatcher(minQuote, "##,##,###.##"));
        maxQuote.addTextChangedListener(new NumberTextWatcher(maxQuote, "##,##,###.##"));
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }
    private void setTypeface() {
        ((TextView) findViewById(R.id.request_text)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.request_code)).setTypeface(Constants.getarbooldTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.expire_text)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.expire_time)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.car_back)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.car_left)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.car_front)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.car_right)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.workshop_comment)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.comment_chars)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.st2)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.st3)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
//        ((TextView) findViewById(R.id.st4)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.st5)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.st6)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.st7)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.year)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.st9)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.car_model)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView) findViewById(R.id.car_make)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));




    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submit_layout:
//                strComment = workshopComments.getText().toString();
                if(minQuote.length() == 0) {
                    scrollView.fullScroll(View.FOCUS_DOWN);
                    if (language.equalsIgnoreCase("En")){
                        minQuote.setError("Please enter bid");
                    }
                    else {
                        minQuote.setError("ارجوا ادخالمعلومات");
                    }

                    minQuote.requestFocus();
                }
                else {
                    String min = minQuote.getText().toString().replaceAll(",","");
                    fMin = Float.parseFloat(Constants.convertToArabic(min));
                }
                if(maxQuote.length() == 0) {
                    scrollView.fullScroll(View.FOCUS_DOWN);
                    if (language.equalsIgnoreCase("En")){
                        maxQuote.setError("Please enter bid");
                    }
                    else {
                        maxQuote.setError("ارجوا ادخالمعلومات");
                    }

                    minQuote.requestFocus();
                }
                else {
                    String min = maxQuote.getText().toString().replaceAll(",","");
                    fMax = Float.parseFloat(Constants.convertToArabic(min));
                }
                if(fMax == 0 || fMin == 0){
                    scrollView.fullScroll(View.FOCUS_DOWN);
                }
//                else if(strComment.length() == 0){
//                    scrollView.fullScroll(View.FOCUS_DOWN);
//
//                    if (language.equalsIgnoreCase("En")){
//                        workshopComments.setError("Please enter comment");
//                    }
//                    else {
//                        workshopComments.setError("الرجاء إدخال الرسالة");
//                    }
//
//                    minQuote.requestFocus();
//                }
                else if(fMin > fMax){
                    if (language.equalsIgnoreCase("En")){
                        Constants.showOneButtonAlertDialog("Maximum quote should be greater than minimum quote", getResources().getString(R.string.error),
                                getResources().getString(R.string.ok), OfferDetailsActivity.this);
                    }
                    else {
                        Constants.showOneButtonAlertDialog("الحد الأعلى يجيب ان يكون اعلى سعرا من الحد الأدنى", getResources().getString(R.string.error_ar),
                                getResources().getString(R.string.ok_ar), OfferDetailsActivity.this);
                    }

                }
                else{

                    String networkStatus = NetworkUtil.getConnectivityStatusString(OfferDetailsActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new placeBidApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;

            case R.id.quit_layout:
                finish();
                break;

            case R.id.right_side:
                if(isRight) {
                    selected = 1;
                    setImages();
                    right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
                    left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                }
//                right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_selected));
//                left.setImageDrawable(getResources().getDrawable(R.drawable.car_left_unselected));
//                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_unselected));
//                back.setImageDrawable(getResources().getDrawable(R.drawable.car_back_unselected));
//                rightText.setTextColor(getResources().getColor(R.color.colorPrimary));
//                leftText.setTextColor(getResources().getColor(R.color.bel_grey_text));
//                frontText.setTextColor(getResources().getColor(R.color.bel_grey_text));
//                backText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                break;

            case R.id.left_side:
                if(isLeft) {
                    selected = 2;
                    setImages();
                    right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
                    back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                }
//                right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_unselected));
//                left.setImageDrawable(getResources().getDrawable(R.drawable.car_left_selected));
//                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_unselected));
//                back.setImageDrawable(getResources().getDrawable(R.drawable.car_back_unselected));
//                rightText.setTextColor(getResources().getColor(R.color.bel_grey_text));
//                leftText.setTextColor(getResources().getColor(R.color.colorPrimary));
//                frontText.setTextColor(getResources().getColor(R.color.bel_grey_text));
//                backText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                break;

            case R.id.front_side:
                if(isFront) {
                    selected = 3;
                    setImages();

                    right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
                }
//                right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_unselected));
//                left.setImageDrawable(getResources().getDrawable(R.drawable.car_left_unselected));
//                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
//                back.setImageDrawable(getResources().getDrawable(R.drawable.car_back_unselected));
//                rightText.setTextColor(getResources().getColor(R.color.bel_grey_text));
//                leftText.setTextColor(getResources().getColor(R.color.bel_grey_text));
//                frontText.setTextColor(getResources().getColor(R.color.colorPrimary));
//                backText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                break;

            case R.id.back_side:
                if(isBack) {
                    selected = 4;
                    setImages();

                    right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
                    front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                }
//                right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_unselected));
//                left.setImageDrawable(getResources().getDrawable(R.drawable.car_left_unselected));
//                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_unselected));
//                back.setImageDrawable(getResources().getDrawable(R.drawable.car_back_selected));
//                rightText.setTextColor(getResources().getColor(R.color.bel_grey_text));
//                leftText.setTextColor(getResources().getColor(R.color.bel_grey_text));
//                frontText.setTextColor(getResources().getColor(R.color.bel_grey_text));
//                backText.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;

            case R.id.dummyView1:
                bigImageLayout.setVisibility(View.GONE);
                break;

            case R.id.dummyView2:
                bigImageLayout.setVisibility(View.GONE);
                break;
        }
    }

    private void getImages(){
        ArrayList<NewRequestsResponse.RCI> images = new ArrayList<>();
        images.addAll(data.get(position).getCM().get(0).getMDL().get(0).getRCI());

        for (int i = 0; i < images.size(); i++){
           if(images.get(i).getDocumentType() == 1 ){
               isRight = true;
               rightImages.add(images.get(i).getDocumentLocation());
           }
           else if(images.get(i).getDocumentType() == 2 ){
               isFront = true;
               frontImages.add(images.get(i).getDocumentLocation());
           }
           else if(images.get(i).getDocumentType() == 3 ){
               isLeft = true;
               leftImages.add(images.get(i).getDocumentLocation());
           }
           else if(images.get(i).getDocumentType() == 4 ){
               isBack = true;
               backImages.add(images.get(i).getDocumentLocation());
           }
        }

        if(!isRight){
            rightLayout.setEnabled(false);
            right.setImageDrawable(getResources().getDrawable(R.drawable.car_right));
            right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
        }
        else {
            selected = 1;
            right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_blue));
            right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
        }

        if(!isLeft){
            leftLayout.setEnabled(false);
            left.setImageDrawable(getResources().getDrawable(R.drawable.car_left));
            left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
        }
        else {
            left.setImageDrawable(getResources().getDrawable(R.drawable.car_right_blue));
            if(!isRight && !isFront) {
                selected = 2;
                left.setImageDrawable(getResources().getDrawable(R.drawable.car_right_blue));
                left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
            }
        }

        if(!isFront){
            frontLayout.setEnabled(false);
            front.setImageDrawable(getResources().getDrawable(R.drawable.car_front));
            front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
        }
        else {
            front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
            if(!isRight) {
                selected = 3;
                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
                front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
            }
        }
        if(!isBack){
            backLayout.setEnabled(false);
            back.setImageDrawable(getResources().getDrawable(R.drawable.car_front));
            back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
        }
        else {
            back.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
            if(!isRight && !isFront&& !isLeft) {
                selected = 4;
                back.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
                back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
            }
        }
        setImages();
    }
    private void setImages(){
        ArrayList<String> imagesSelected = new ArrayList<>();
        if(selected == 1){
            imagesSelected.addAll(rightImages);
        }
        else if(selected == 2){
            imagesSelected.addAll(leftImages);
        }
        else if(selected == 3){
            imagesSelected.addAll(frontImages);
        }
        else if(selected == 4){
            imagesSelected.addAll(backImages);
        }

        mAdapter = new ImagesAdapter(OfferDetailsActivity.this, imagesSelected, language);
        viewPager.setAdapter(mAdapter);

        mBigImageAdapter = new BigImagesAdapter(OfferDetailsActivity.this, imagesSelected);
        bigImageView.setAdapter(mBigImageAdapter);
    }

    private String prepareJson(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("WorkshopId", userId);
            parentObj.put("RequestId", data.get(position).getRequestId());
            parentObj.put("MinQuote", fMin);
            parentObj.put("MaxQuote", fMax);
            parentObj.put("WorkShopComments", strComment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private class placeBidApi extends AsyncTask<String, Integer, String> {


        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(OfferDetailsActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<PlaceBidResponse> call = apiService.PlaceBid(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PlaceBidResponse>() {
                @Override
                public void onResponse(Call<PlaceBidResponse> call, Response<PlaceBidResponse> response) {
                    if(response.isSuccessful()){
                        PlaceBidResponse requestsResponse = response.body();
                        try {
                            if(requestsResponse.getStatus()){
                                showThankyouDialog();
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = requestsResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), OfferDetailsActivity.this);
                                }
                                else {
                                    String failureResponse = requestsResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), OfferDetailsActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<PlaceBidResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void showThankyouDialog(){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OfferDetailsActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if (language.equalsIgnoreCase("En")){
             layout = R.layout.dialog_thankyou;
        }
        else {
            layout = R.layout.dialog_thankyou_ar;
        }

        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView dialogbox1 =(TextView) dialogView.findViewById(R.id.desc1);
        TextView dialogbox2 =(TextView) dialogView.findViewById(R.id.desc2);
        if (language.equalsIgnoreCase("Ar")){
            dialogbox1.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
            dialogbox2.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        }

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.dialog_close);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OfferDetailsActivity.this, MainActivity.class);
                intent.putExtra("type", "1");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.75;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OfferDetailsActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
