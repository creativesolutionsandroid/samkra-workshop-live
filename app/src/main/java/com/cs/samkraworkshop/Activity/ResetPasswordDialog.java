package com.cs.samkraworkshop.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.ChangePasswordResponse;
import com.cs.samkraworkshop.Models.SignInResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordDialog extends Activity implements View.OnClickListener {

    TextInputLayout inputLayoutPassword, inputLayoutConfirmPassword;
    EditText inputPassword, inputConfirmPassword;
    String strPassword, strConfirmPassword, otp;
    Button buttonSubmit, buttonVerify;
    String strMobile;
    View rootView;
    private static String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    ImageView backBtn;
    AlertDialog loaderDialog;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.dialog_reset_password);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.dialog_reset_password_ar);
        }


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        strMobile = getIntent().getStringExtra("mobile");
        otp = getIntent().getStringExtra("otp");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        backBtn = (ImageView) findViewById(R.id.hy_back_btn);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        inputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.input_layout_retype_password);
        inputPassword = (EditText) findViewById(R.id.reset_input_password);
        inputConfirmPassword = (EditText) findViewById(R.id.reset_input_retype_password);

        buttonSubmit = (Button) findViewById(R.id.reset_submit_button);

//        setTypeface();

        buttonSubmit.setOnClickListener(this);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (language.equalsIgnoreCase("Ar")) {
            setTypeface();
        }


    }

    private void setTypeface() {
        inputLayoutPassword.setTypeface(Constants.getarTypeFace(ResetPasswordDialog.this));
        inputLayoutConfirmPassword.setTypeface(Constants.getarTypeFace(ResetPasswordDialog.this));
        inputPassword.setTypeface(Constants.getarTypeFace(ResetPasswordDialog.this));
        inputPassword.setTypeface(Constants.getarTypeFace(ResetPasswordDialog.this));
        inputConfirmPassword.setTypeface(Constants.getarTypeFace(ResetPasswordDialog.this));
        buttonSubmit.setTypeface(Constants.getarTypeFace(ResetPasswordDialog.this));
        ((TextView) findViewById(R.id.reset_password_title)).setTypeface(Constants.getarTypeFace(ResetPasswordDialog.this));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reset_submit_button:
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ResetPasswordDialog.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ResetPasswordApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private boolean validations() {
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strPassword.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            return false;
        } else if (strPassword.length() < 4 || strPassword.length() > 20) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            return false;
        } else if (strConfirmPassword.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            return false;
        } else if (strConfirmPassword.length() < 4 || strConfirmPassword.length() > 20) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            return false;
        } else if (!strPassword.equals(strConfirmPassword)) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_passwords_not_match));
            } else {
                inputLayoutConfirmPassword.setError("كلمة المرور غير مطابقة، يرجى إعادة كلمة المرور");
            }
            return false;
        }
        return true;
    }

    private String prepareResetPasswordJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("MobileNo", "966" + strMobile);
            parentObj.put("Password", strPassword);
            parentObj.put("Otp", otp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }

    private class ResetPasswordApi extends AsyncTask<String, Integer, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangePasswordResponse> call = apiService.resetPassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangePasswordResponse>() {
                @Override
                public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                    if (response.isSuccessful()) {
                        ChangePasswordResponse resetPasswordResponse = response.body();
                        try {
                            if (resetPasswordResponse.getStatus()) {
//                                status true case
                                int userId = resetPasswordResponse.getData().getUserId();
                                userPrefsEditor.putString("userId", Integer.toString(userId));
//                                userPrefsEditor.putString("name", resetPasswordResponse.getData().getUsername());
//                                userPrefsEditor.putString("email", resetPasswordResponse.getData().getEmail());
//                                userPrefsEditor.putString("mobile", resetPasswordResponse.getData().getPhone());
                                userPrefsEditor.commit();
                                if (language.equalsIgnoreCase("En")) {
                                    Toast.makeText(ResetPasswordDialog.this, R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ResetPasswordDialog.this, R.string.reset_success_msg_ar, Toast.LENGTH_SHORT).show();
                                }
                                Intent intent = new Intent(ResetPasswordDialog.this, MainActivity.class);
                                intent.putExtra("type", "1");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
//                                status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = resetPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), ResetPasswordDialog.this);
                                } else {
                                    String failureResponse = resetPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), ResetPasswordDialog.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ResetPasswordDialog.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ResetPasswordDialog.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    public void showloaderAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ResetPasswordDialog.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
