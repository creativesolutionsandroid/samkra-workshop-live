package com.cs.samkraworkshop.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.VerifyMobileResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.itextpdf.tool.xml.html.Image;

import org.json.JSONException;
import org.json.JSONObject;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener{

    public static boolean isOTPSuccessful = false;
    public static String otp = "";
    public static boolean isResetSuccessful = false;
    private TextInputLayout inputLayoutMobile;
    private EditText inputMobile;
    private String strMobile;
    private Button buttonSubmit;
    private String serverOtp;
    AlertDialog loaderDialog;
    ImageView backbtn;
    Context context;
    SharedPreferences languagePrefs;
    String language;
    SharedPreferences.Editor languagePrefsEditor;


    public static final String TAG = "TAG";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_forgot_password);
        }
        else {
            setContentView(R.layout.activity_forgot_password_ar);
        }
        context = this;

        inputLayoutMobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
        inputMobile = (EditText) findViewById(R.id.phone);
        buttonSubmit = (Button) findViewById(R.id.forgot_submit_button);
        backbtn = (ImageView) findViewById(R.id.hy_back_btn);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

//        setTypeFace();
//        inputMobile.setText(Constants.Country_Code);


        buttonSubmit.setOnClickListener(this);
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }
    private void setTypeface() {
        inputMobile.setTypeface(Constants.getarbooldTypeFace(ForgotPasswordActivity.this));
        buttonSubmit.setTypeface(Constants.getarbooldTypeFace(ForgotPasswordActivity.this));
        ((TextView) findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(ForgotPasswordActivity.this));
        ((TextView) findViewById(R.id.forgot_body)).setTypeface(Constants.getarTypeFace(ForgotPasswordActivity.this));

}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.forgot_submit_button:
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ForgotPasswordApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private void setTypeFace(){
//        inputMobile.setTypeface(Constants.getTypeFace(context));
//        buttonSubmit.setTypeface(Constants.getTypeFace(context));
//        ((TextView) findViewById(R.id.forgot_body)).setTypeface(Constants.getTypeFace(context));
    }

    private boolean validations(){
        strMobile = inputMobile.getText().toString();

        if (strMobile.length() == 0){
            if (language.equalsIgnoreCase("En")){
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            }
            else {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }

            Constants.requestEditTextFocus(inputMobile, ForgotPasswordActivity.this);
            return false;
        }
        else if (strMobile.length() != 9){
            if (language.equalsIgnoreCase("En")){
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            }
            else {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
            }

            Constants.requestEditTextFocus(inputMobile, ForgotPasswordActivity.this);
            return false;
        }
        return true;
    }

    private void clearErrors(){
        inputLayoutMobile.setErrorEnabled(false);
    }

    private void displayVerifyOTPDialog(){
        Intent intent = new Intent(ForgotPasswordActivity.this, VerifyOtpActivity.class);
        intent.putExtra("mobile", strMobile);
        intent.putExtra("screen", "forgot");
        startActivity(intent);
    }

    private String prepareForgotPasswordJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("MobileNo","966"+strMobile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private class ForgotPasswordApi extends AsyncTask<String, Integer, String> {


        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareForgotPasswordJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.forgotPassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if(response.isSuccessful()){
                        VerifyMobileResponse forgotPasswordResponse = response.body();
                        try {
                            if(forgotPasswordResponse.getStatus()){
                                Log.i(TAG, "onResponse: "+forgotPasswordResponse.getData().getOTP());
                                displayVerifyOTPDialog();
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = forgotPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), ForgotPasswordActivity.this);
                                }
                                else
                                {
                                    String failureResponse = forgotPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), ForgotPasswordActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(ForgotPasswordActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ForgotPasswordActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ForgotPasswordActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ForgotPasswordActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ForgotPasswordActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            if(loaderDialog != null){
//                loaderDialog.dismiss();
//            }
        }
        public void showloaderAlertDialog(){
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ForgotPasswordActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.loder_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
            loaderDialog = dialogBuilder.create();
            loaderDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = loaderDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth*0.85;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }
    }
}
