package com.cs.samkraworkshop.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.VerifyMobileResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    EditText inputName, inputNameAr, mobileEt, inputPassword, inputConfirmPassword;
    String strName, strNameAr, strMobile, strPassword, strConfirmPassword;
    private LinearLayout signUpLayout;
    AlertDialog customDialog;
    AlertDialog loaderDialog;
    private String TAG = "TAG";
    public static boolean isOTPVerified = false;
    private static int SIGNUP_STATUS = 1;
    ImageView checkboxTnC;
    RelativeLayout tncLayout;
    TextView tncText;
    Boolean isTermsChecked = false;
    ImageView backBtn;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_signup);
        }
        else {
            setContentView(R.layout.activity_signup_ar);
        }

//        showloaderAlertDialog();

        signUpLayout = (LinearLayout) findViewById(R.id.sign_up_layout);
        inputName = (EditText) findViewById(R.id.et_name);
        inputNameAr = (EditText) findViewById(R.id.et_name_ar);
        inputPassword= (EditText) findViewById(R.id.et_password);
        inputConfirmPassword = (EditText) findViewById(R.id.et_confirm_password);
        mobileEt = (EditText) findViewById(R.id.et_mobile);
        backBtn = (ImageView) findViewById(R.id.side_menu);

        checkboxTnC = (ImageView) findViewById(R.id.checkboxTnC);
        tncLayout = (RelativeLayout) findViewById(R.id.tncLayout);
        tncText = (TextView) findViewById(R.id.tNcText);
        inputNameAr.requestFocus();

        if (language.equalsIgnoreCase("En")){
            String desc ="<pre>By signing up you agree to our <strong><span style=\"text-decoration: underline;\"><a title=\"Contract\" href=\"http://workshop.samkra.com/about/contract\">Contract</a></span> </strong>and <strong><span style=\"text-decoration: underline;\"><a title=\"Terms and Conditions\" href=\"http://workshop.samkra.com/about/termsandcontidions\">Terms and Conditions</a></span> </strong></pre>";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tncText.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
            } else {
                tncText.setText(Html.fromHtml(desc));
            }
            tncText.setMovementMethod(LinkMovementMethod.getInstance());
        }
        else {
            String desc ="<pre>موافق على شروط والقوانين <strong><span style=\"text-decoration: underline;\"><a title=\"عقد\" href=\"http://workshop.samkra.com/about/contract\">عقد</a></span> </strong>و <strong><span style=\"text-decoration: underline;\"><a title=\"الرجاء قبول الشروط والأحكام\" href=\"http://workshop.samkra.com/about/termsandcontidionsar\">الرجاء قبول الشروط والأحكام</a></span> </strong></pre>";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tncText.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
            } else {
                tncText.setText(Html.fromHtml(desc));
            }
            tncText.setMovementMethod(LinkMovementMethod.getInstance());
//            tncText.setText("الرجاء قبول الشروط والأحكام");
        }

        checkboxTnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isTermsChecked){
                    isTermsChecked = true;
                    checkboxTnC.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                }
                else{
                    isTermsChecked = false;
                    checkboxTnC.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                }
            }
        });

//        tncText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent aboutIntent = new Intent(SignupActivity.this, WebViewActivity.class);
//                if(language.equalsIgnoreCase("En")) {
//                aboutIntent.putExtra("title", "Terms and Conditions");
//                aboutIntent.putExtra("url", "http://csadms.com/samkrabackend/About/TermsAndContidions");
//                }
//                else{
//                    aboutIntent.putExtra("title", "الأحكام والشروط");
//                    aboutIntent.putExtra("url", "http://csadms.com/Samkrabackend/About/TermsAndConditionsAr");
//                }
//                startActivity(aboutIntent);
//            }
//        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }

    private void setTypeface(){
        inputName.setTypeface(Constants.getarTypeFace(SignupActivity.this));
        inputNameAr.setTypeface(Constants.getarTypeFace(SignupActivity.this));
        mobileEt.setTypeface(Constants.getarTypeFace(SignupActivity.this));
        inputPassword.setTypeface(Constants.getarTypeFace(SignupActivity.this));
        inputConfirmPassword.setTypeface(Constants.getarTypeFace(SignupActivity.this));
        tncText.setTypeface(Constants.getarTypeFace(SignupActivity.this));
        ((TextView) findViewById(R.id.singup)).setTypeface(Constants.getarTypeFace(SignupActivity.this));
        ((TextView) findViewById(R.id.continue_btn)).setTypeface(Constants.getarTypeFace(SignupActivity.this));
    }

    public void signUpClickEvents(View view){
        switch (view.getId()){
            case R.id.continue_btn:
                if(validations()){
                    showtwoButtonsAlertDialog();
                }
                break;
        }
    }

    private boolean validations(){
        strName = inputName.getText().toString().trim();
        strNameAr = inputNameAr.getText().toString().trim();
        strMobile = mobileEt.getText().toString();
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();
        strMobile = strMobile.replace("+966 ","");

        if(strName.length() == 0){
            if (language.equalsIgnoreCase("En")){
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            }
            else {
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
            }

            return false;
        }
        else if(strNameAr.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputNameAr.setError(getResources().getString(R.string.signup_msg_invalid_name));
            }
            else {
                inputNameAr.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
            }
            return false;
        }
        else if (strMobile.length() == 0){
            if (language.equalsIgnoreCase("En")){
            mobileEt.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            }
            else {
                mobileEt.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }
            return false;
        }
        else if (strMobile.length() != 9){
            if (language.equalsIgnoreCase("En")) {
                mobileEt.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            }
            else {
                mobileEt.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
            }
            return false;
        }
        else if (strPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            }
            else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20){
            if (language.equalsIgnoreCase("En")) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            }
            else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            return false;
        }
        else if (strConfirmPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            }
            else {
                inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            return false;
        }
        else if (!strConfirmPassword.equals(strPassword)){
            if (language.equalsIgnoreCase("En")) {
                inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_passwords_not_match));
            }
            else {
                inputConfirmPassword.setError("كلمة المرور غير مطابقة، يرجى إعادة كلمة المرور");
            }
            return false;
        }
        else if (!isTermsChecked){
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_accept_terms),
                        getResources().getString(R.string.alert_terms), getResources().getString(R.string.ok), SignupActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_accept_terms_ar),
                        getResources().getString(R.string.alert_terms_ar), getResources().getString(R.string.ok_ar), SignupActivity.this);
            }
            return false;
        }
        return true;
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignupActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if(response.isSuccessful()){
                        VerifyMobileResponse verifyMobileResponse = response.body();
                        try {
                            if(verifyMobileResponse.getStatus()){
                                Log.i(TAG, "otp: "+verifyMobileResponse.getData().getOTP());
                                Intent intent = new Intent(SignupActivity.this, VerifyOtpActivity.class);
                                intent.putExtra("name", strName);
                                intent.putExtra("name_ar", strNameAr);
                                intent.putExtra("mobile", strMobile);
                                intent.putExtra("password", strPassword);
                                intent.putExtra("screen", "register");
                                startActivityForResult(intent, SIGNUP_STATUS);
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), SignupActivity.this);
                                }
                                else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), SignupActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(SignupActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(SignupActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignupActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignupActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignupActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignupActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignupActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignupActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("MobileNo","966"+strMobile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    public void showtwoButtonsAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignupActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        if (language.equalsIgnoreCase("En")){
            title.setText(R.string.opt_msg_verify);
            yes.setText(getResources().getString(R.string.ok));
            no.setText(getResources().getString(R.string.edit));
            desc.setText(getResources().getString(R.string.signup_alert_mobile_verify1)+"+966 "+strMobile+getResources().getString(R.string.signup_alert_mobile_verify2));

        }
        else {
            title.setText(R.string.opt_msg_verify_ar);
            yes.setText(getResources().getString(R.string.ok_ar));
            no.setText(getResources().getString(R.string.edit_ar));
            desc.setText(getResources().getString(R.string.signup_alert_mobile_verify1_ar)+"+966 "+strMobile+getResources().getString(R.string.signup_alert_mobile_verify2_ar));

        }
        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(SignupActivity.this));
            desc.setTypeface(Constants.getarTypeFace(SignupActivity.this));
            yes.setTypeface(Constants.getarTypeFace(SignupActivity.this));
            no.setTypeface(Constants.getarTypeFace(SignupActivity.this));
        }
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String networkStatus = NetworkUtil.getConnectivityStatusString(SignupActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new verifyMobileApi().execute();
                }
                else{
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }
                }
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileEt.requestFocus();
                mobileEt.setSelection(mobileEt.length());
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void setFilters() {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                return Character.isLetterOrDigit(c) || Character.isSpaceChar(c);
            }
        };

        inputName.setFilters(new InputFilter[] { filter });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SIGNUP_STATUS && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignupActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }


}
