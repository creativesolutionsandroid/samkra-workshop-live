package com.cs.samkraworkshop.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkraworkshop.Adapter.BigImagesAdapter;
import com.cs.samkraworkshop.Adapter.ImagesAdapter;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.FinalBidResponse;
import com.cs.samkraworkshop.Models.InboxResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.cs.samkraworkshop.Utils.NumberTextWatcher;
import com.daasuu.bl.BubbleLayout;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinalBidActivity extends Activity implements View.OnClickListener, com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener {

    private TextView requestId, make, model, year, userName, mobile;
    private EditText minQuote, maxQuote, userComments, workshopComments, finalPrice, finalBidComments, finalUserComments;
    private ImageView front, back, left, right;
    private TextView frontText, backText, leftText, rightText;
    private LinearLayout frontLayout, backLayout, leftLayout, rightLayout, bottomLayout;
    ImageView back_dot, front_dot, left_dot, right_dot;
    private LinearLayout submitLayout, quitLayout, callLayout;
    private BubbleLayout userCommentLayout, finalUserCommentLayout;
    Boolean isRight = false, isLeft = false, isFront = false, isBack = false;
    float finalP = 0;
    String strComment = "No Comments";
    AlertDialog loaderDialog;

    BigImagesAdapter mBigImageAdapter;
    ImagesAdapter mAdapter;
    public static ViewPager bigImageView;
    private ViewPager viewPager;
    AlertDialog customDialog;
    SharedPreferences languagePrefs;
    String language;

    private ImageView dummy1, dummy2;
    public static RelativeLayout bigImageLayout;
    Boolean isToday = false;

    int selected = 1;
    int position;
    ArrayList<InboxResponse.Data> data = new ArrayList<>();
    ArrayList<String> rightImages = new ArrayList<>();
    ArrayList<String> leftImages = new ArrayList<>();
    ArrayList<String> frontImages = new ArrayList<>();
    ArrayList<String> backImages = new ArrayList<>();

    private static final int CALL_INTENT = 1;
    private static final String[] CALL_PERMS = {
            Manifest.permission.CALL_PHONE
    };

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    ImageView backBtn;
    ScrollView scrollView;
    TextView estimatedDate, remainingChars;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String selectedDate = "", selectedDate1 = "", time = "", time1 = "";

    RelativeLayout mtimer_layout;
    LinearLayout mfinal_price_bit_layout, mfinal_bit_layout;
    EditText customercomment, wrokshopcomment, customerFinalComment, minimumquote, maximumquote, wrokshopnote, finalprice;
    ProgressBar progressBar;
    TextView tvTimeLeft,st1, dateleft;
    long remainingMillis = 60 * 2880 * 1000;
    ImageView redImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_final_bid);
        } else {
            setContentView(R.layout.activity_final_bid_ar);
        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", "");

        position = getIntent().getIntExtra("pos", 0);
        data = (ArrayList<InboxResponse.Data>) getIntent().getSerializableExtra("data");

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        requestId = (TextView) findViewById(R.id.request_code);
        make = (TextView) findViewById(R.id.car_make);
        model = (TextView) findViewById(R.id.car_model);
        year = (TextView) findViewById(R.id.year);
        userName = (TextView) findViewById(R.id.user_name);
        mobile = (TextView) findViewById(R.id.mobile);
        estimatedDate = (TextView) findViewById(R.id.date);
        remainingChars = (TextView) findViewById(R.id.comment_chars);

        minQuote = (EditText) findViewById(R.id.min_quote);
        maxQuote = (EditText) findViewById(R.id.max_quote);
        finalPrice = (EditText) findViewById(R.id.final_quote);
        userComments = (EditText) findViewById(R.id.user_comment);
        workshopComments = (EditText) findViewById(R.id.workshop_comment);
        finalBidComments = (EditText) findViewById(R.id.final_comment);
        finalUserComments = (EditText) findViewById(R.id.user_final_comment);

        front = (ImageView) findViewById(R.id.front_side);
        back = (ImageView) findViewById(R.id.back_side);
        left = (ImageView) findViewById(R.id.left_side);
        right = (ImageView) findViewById(R.id.right_side);
        dummy1 = (ImageView) findViewById(R.id.dummyView1);
        dummy2 = (ImageView) findViewById(R.id.dummyView2);

        back_dot = (ImageView) findViewById(R.id.back_dot);
        front_dot = (ImageView) findViewById(R.id.front_dot);
        left_dot = (ImageView) findViewById(R.id.left_dot);
        right_dot = (ImageView) findViewById(R.id.right_dot);

        frontText = (TextView) findViewById(R.id.front_text);
        backText = (TextView) findViewById(R.id.back_text);
        leftText = (TextView) findViewById(R.id.left_text);
        rightText = (TextView) findViewById(R.id.right_text);

        frontLayout = (LinearLayout) findViewById(R.id.front_layout);
        backLayout = (LinearLayout) findViewById(R.id.back_layout);
        leftLayout = (LinearLayout) findViewById(R.id.left_layout);
        rightLayout = (LinearLayout) findViewById(R.id.right_layout);
        bigImageLayout = (RelativeLayout) findViewById(R.id.imagePopup);

        submitLayout = (LinearLayout) findViewById(R.id.submit_layout);
        quitLayout = (LinearLayout) findViewById(R.id.home_layout);
        callLayout = (LinearLayout) findViewById(R.id.call_layout);
        bottomLayout = (LinearLayout) findViewById(R.id.bottom_layout);

        bigImageView = (ViewPager) findViewById(R.id.big_image_view);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        userCommentLayout = (BubbleLayout) findViewById(R.id.user_comment_layout);
        finalUserCommentLayout = (BubbleLayout) findViewById(R.id.user_final_comment_layout);

        mfinal_bit_layout = (LinearLayout) findViewById(R.id.final_bit_layout);
        mfinal_price_bit_layout = (LinearLayout) findViewById(R.id.final_price_bit_layout);
        mtimer_layout = (RelativeLayout) findViewById(R.id.timer_layout);

        customercomment = (EditText) findViewById(R.id.fp_user_comment);
        wrokshopcomment = (EditText) findViewById(R.id.fp_wrokshop_comment);
        customerFinalComment = (EditText) findViewById(R.id.fp_user_final_comment);
        minimumquote = (EditText) findViewById(R.id.fp_min_quote);
        maximumquote = (EditText) findViewById(R.id.fp_max_quote);
        wrokshopnote = (EditText) findViewById(R.id.fp_workshop_note);
        finalprice = (EditText) findViewById(R.id.fp_final_quote);

        tvTimeLeft = (TextView) findViewById(R.id.timeLeft);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        dateleft=(TextView)findViewById(R.id.fb_date);
        st1 =(TextView)findViewById(R.id.fb_estimated_text);
        redImage = (ImageView) findViewById(R.id.red_image); 

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        finalBidComments.setImeOptions(EditorInfo.IME_ACTION_DONE);
        finalBidComments.setRawInputType(InputType.TYPE_CLASS_TEXT);

        mobile.setText(data.get(position).getPhone());
        userName.setText(data.get(position).getUsername());
        requestId.setText("#" + data.get(position).getRequestCode());
        if (language.equalsIgnoreCase("En")) {
            make.setText(data.get(position).getCM().get(0).getCarMakerNameEn());
            model.setText(data.get(position).getCM().get(0).getMDL().get(0).getModelNameEn());
        } else {
            make.setText(data.get(position).getCM().get(0).getCarMakerNameAr());
            model.setText(data.get(position).getCM().get(0).getMDL().get(0).getModelNameAr());
        }


        year.setText("" + data.get(position).getYear());
        minQuote.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getMinQuote())));
        maxQuote.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getMaxQuote())));
        workshopComments.setText(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getWorkShopComments());
        finalUserComments.setText(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getUserAcceptedComment());

        if (data.get(position).getUserComments() == null || data.get(position).getUserComments().length() == 0) {
            userCommentLayout.setVisibility(View.GONE);
        } else {
            userComments.setText(data.get(position).getUserComments());
        }

        if (data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getUserAcceptedComment() == null ||
                data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getUserAcceptedComment().length() == 0) {
            finalUserCommentLayout.setVisibility(View.GONE);
        } else {
            finalUserComments.setText(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getUserAcceptedComment());
        }

        mfinal_bit_layout.setVisibility(View.VISIBLE);
        mtimer_layout.setVisibility(View.GONE);

        if (data.get(position).getRequestStatus().equalsIgnoreCase("Offer Accepted")) {
            finalPrice.setEnabled(true);
            finalBidComments.setEnabled(true);
            submitLayout.setVisibility(View.VISIBLE);
            bottomLayout.setVisibility(View.VISIBLE);
            mfinal_bit_layout.setVisibility(View.VISIBLE);
            mfinal_price_bit_layout.setVisibility(View.GONE);
            mtimer_layout.setVisibility(View.GONE);
        } else if (data.get(position).getRequestStatus().equalsIgnoreCase("Final price Pending")) {
            mfinal_bit_layout.setVisibility(View.VISIBLE);
            mfinal_price_bit_layout.setVisibility(View.GONE);
            mtimer_layout.setVisibility(View.GONE);
            bottomLayout.setVisibility(View.VISIBLE);
            finalPrice.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getFinalPrice())));
            finalBidComments.setText(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getFinalPriceWSComment());
            String dateStr = data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getEstimatedDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            SimpleDateFormat requiredFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.US);

            dateStr = dateStr.replace("T", " ");
            Calendar calendar = Calendar.getInstance();
            try {
                Date dt = sdf.parse(dateStr);
                calendar.setTime(dt);
                dateStr = requiredFormat.format(calendar.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            estimatedDate.setText(Constants.convertToArabic(dateStr));
        } else if (data.get(position).getRequestStatus().equalsIgnoreCase("Final price Accepted")) {
            mfinal_bit_layout.setVisibility(View.GONE);
            mfinal_price_bit_layout.setVisibility(View.VISIBLE);
            mtimer_layout.setVisibility(View.VISIBLE);
            callLayout.setVisibility(View.GONE);

            customercomment.setText(data.get(position).getUserComments());
            wrokshopcomment.setText(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getWorkShopComments());
            minimumquote.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getMinQuote())));
            maximumquote.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getMaxQuote())));
            wrokshopnote.setText(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getFinalPriceWSComment());
            customerFinalComment.setText(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getUserAcceptedComment());
            finalprice.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getFinalPrice())));

            if(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getEstimatedDate() != null){
                calculateMinutesLeft();
                mtimer_layout.setVisibility(View.VISIBLE);
            }

        }
        finalPrice.addTextChangedListener(new NumberTextWatcher(finalPrice, "##,##,###.##"));
        getImages();

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final int hour = c.get(Calendar.HOUR);
        final int minute = c.get(Calendar.MINUTE);
        final int seconds = c.get(Calendar.SECOND);
        int am_pm = c.get(Calendar.AM_PM);
        String am_pm_string = "";
        if (am_pm == 0) {
            am_pm_string = "AM";
        } else if (am_pm == 1) {
            am_pm_string = "PM";
        }

        estimatedDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.get(position).getRequestStatus().equalsIgnoreCase("Offer Accepted")) {
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(FinalBidActivity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                        isToday = true;
                                    } else {
                                        isToday = false;
                                    }
                                    mYear = year;
                                    mDay = dayOfMonth;
                                    mMonth = monthOfYear;

                                    final int hour = c.get(Calendar.HOUR_OF_DAY);
                                    final int minute = c.get(Calendar.MINUTE);

                                    com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                                            FinalBidActivity.this,
                                            hour,
                                            minute,
                                            false
                                    );
                                    tpd.setThemeDark(false);
                                    tpd.vibrate(false);
                                    tpd.setAccentColor(Color.parseColor("#76C8FC"));
                                    tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                        @Override
                                        public void onCancel(DialogInterface dialogInterface) {
                                            Log.d("TimePicker", "Dialog was cancelled");
                                        }
                                    });
                                    if (isToday) {
                                        tpd.setMinTime(hour + 1, minute, 00);
                                    } else {
                                        tpd.setMinTime(0, 0, 0);
                                    }
                                    tpd.show(getFragmentManager(), "Timepickerdialog");

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                    long max = TimeUnit.DAYS.toMillis(90);
                    datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + max);
                    datePickerDialog.setTitle("Select Date");
                    datePickerDialog.show();
                }
            }
        });

        finalBidComments.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                int len = 300 - (text.length());
                if (language.equalsIgnoreCase("En")) {
                    remainingChars.setText(len + " characters left");
                } else {
                    remainingChars.setText(len + " حرف متبقي");
                }
                if (text.length() > 0) {
                    strComment = text;
                } else {
                    strComment = "No Comments";
                }
            }
        });

        front.setOnClickListener(this);
        back.setOnClickListener(this);
        left.setOnClickListener(this);
        right.setOnClickListener(this);
        dummy1.setOnClickListener(this);
        dummy2.setOnClickListener(this);
        submitLayout.setOnClickListener(this);
        quitLayout.setOnClickListener(this);
        callLayout.setOnClickListener(this);

        if (language.equalsIgnoreCase("Ar")) {
            setTypeface();
        }

    }

    public void calculateMinutesLeft() {
        Date accept = null, expect = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);

        String accpetedDate = data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getAcceptedOn();
        String expectedDate = data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getEstimatedDate();
//        dateleft.setText(data.get(bidPos).getCM().get(0).getMDL().get(0).getRB().get(0).getEstimatedDate());


        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat sd2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm a", Locale.US);

        try {
            java.util.Date datetime = sdf1.parse(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getEstimatedDate());
            String str123 = sd2.format(datetime);
            dateleft.setText(Constants.convertToArabic(str123));
            Log.d("TAG", "str123 "+sd2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            accept = DateFormat.parse(accpetedDate);
            expect = sdf1.parse(expectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long startMillis = accept.getTime();
        long currentMillis = calendar.getTimeInMillis();
        long endMillis = expect.getTime();

        if (currentMillis < endMillis) {
            remainingMillis = endMillis - currentMillis;

            long maxMillis = endMillis - startMillis;
            final int maxTime = (int) maxMillis / 60000;
            progressBar.setMax(maxTime);


//        countDownTimer2 = new CountDownTimer(remainingMillis, 60000) {
//            // 1000 means, onTick function will be called at every 1000 milliseconds
//
//            @Override
//            public void onTick(long leftTimeInMilliseconds) {

            final int remTime = (int) remainingMillis / 60000;
            int percent = ((maxTime - remTime) / maxTime) * 100;
            progressBar.setProgress(percent);

            if (remTime > 1440) {
                int days = remTime / 1440;
                if (days == 1) {
                    if (language.equalsIgnoreCase("En")){
                        tvTimeLeft.setText(days + "\nday");
                    }
                    else {
                        tvTimeLeft.setText(days + "\nأيام");
                    }

                } else {
                    if (language.equalsIgnoreCase("En")){
                        tvTimeLeft.setText(days + "\ndays");
                    }
                    else {
                        tvTimeLeft.setText(days + "\n" +
                                "أيام");
                    }

                }
            } else if (remTime > 60) {
                int hours = remTime / 60;
                if (language.equalsIgnoreCase("En")){
                    tvTimeLeft.setText(hours + "\nhours");
                }else {
                    tvTimeLeft.setText(hours + "\nساعات");
                }

            } else {
                tvTimeLeft.setText(remTime + "\nmins");
            }
            dateleft.setTextColor(getResources().getColor(R.color.colorPrimary));
//            }
//            @Override
//            public void onFinish() {
//                tvTimeLeft.setText("");
//            }
//        }.start();
        }
        else {
            dateleft.setTextColor(Color.parseColor("#FF0000"));
            tvTimeLeft.setText("");
            progressBar.setVisibility(View.GONE);
            redImage.setVisibility(View.VISIBLE);
        }
    }

    private void setTypeface() {
        requestId.setTypeface(Constants.getarbooldTypeFace(FinalBidActivity.this));
        make.setTypeface(Constants.getarbooldTypeFace(FinalBidActivity.this));
        model.setTypeface(Constants.getarbooldTypeFace(FinalBidActivity.this));
        year.setTypeface(Constants.getarbooldTypeFace(FinalBidActivity.this));
        mobile.setTypeface(Constants.getarbooldTypeFace(FinalBidActivity.this));
        userName.setTypeface(Constants.getarbooldTypeFace(FinalBidActivity.this));
//        rightText.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
//        leftText.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
//        backText.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
//        frontText.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        userName.setTypeface(Constants.getarbooldTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.request_text)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.username)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.mobile)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.back_text)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.left_text)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.front_text)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.right_text)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st2)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st3)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st4)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st5)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st6)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st7)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st8)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st9)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.st10)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.mobiled)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.mobile)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.minimamst)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.maxmiumst)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        ((TextView) findViewById(R.id.finalpricest)).setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        minQuote.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        year.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        userComments.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        workshopComments.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        finalPrice.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        finalBidComments.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        remainingChars.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        estimatedDate.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        st1.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int selectedHour, int selectedMinute, int second) {
        String format = "";
        if (selectedHour == 0) {
//
            selectedHour += 12;

            format = "AM";
        } else if (selectedHour == 12) {

            format = "PM";

        } else if (selectedHour > 12) {

            selectedHour -= 12;

            format = "PM";

        } else {

            format = "AM";
        }
        if (mMonth < 9) {
            selectedDate = mDay + "/0" + (mMonth + 1) + "/" + mYear;
            selectedDate1 = "0" + (mMonth + 1) + "/" + mDay + "/" + mYear;
        } else {
            selectedDate = mDay + "/" + (mMonth + 1) + "/" + mYear;
            selectedDate1 = (mMonth + 1) + "/" + mDay + "/" + mYear;
        }
        time1 = String.format("%02d:%02d", selectedHour, selectedMinute) + " " + format;
        time = String.format("%02d:%02d:%02d", selectedHour, selectedMinute, second) + "" + format;
        estimatedDate.setText(Constants.convertToArabic(selectedDate + " " + time1));
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(FinalBidActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CALL_INTENT:
                if (canAccessPhonecalls()) {
                    String phone = data.get(position).getPhone();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_layout:
                float minP = data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getMinQuote();
                float maxP = data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getMaxQuote();
//                strComment = finalBidComments.getText().toString();
                if (finalPrice.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        finalPrice.setError("Please enter final price");
                    } else {
                        finalPrice.setError("الرجاء إدخالالسعر النهائي");
                    }

                    scrollView.fullScroll(View.FOCUS_DOWN);
                    finalPrice.requestFocus();
                } else {
                    finalP = Float.parseFloat(finalPrice.getText().toString().replaceAll(",", ""));
                }
                if (finalP == 0) {

                }
//                else if(finalP < minP || finalP > maxP){
//                    finalPrice.setError("Invalid final price");
//                    scrollView.fullScroll(View.FOCUS_DOWN);
//                    finalPrice.requestFocus();
//                }
//                else  if(strComment.length() == 0){
//                    finalBidComments.setError("Please enter comment");
//                    scrollView.fullScroll(View.FOCUS_DOWN);
//                    finalBidComments.requestFocus();
//                }
                else if (selectedDate.equals("") || time1.equals("")) {
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showOneButtonAlertDialog("Please select estimated date",
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok), FinalBidActivity.this);

                    } else {
                        Constants.showOneButtonAlertDialog("الرجاء اختيار تاريخ مناسب",
                                getResources().getString(R.string.app_name_ar), getResources().getString(R.string.ok_ar), FinalBidActivity.this);

                    }
                } else {
                    showtwoButtonsAlertDialog(finalP);
                }
                break;

            case R.id.home_layout:
                finish();
                break;

            case R.id.call_layout:
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(CALL_PERMS, CALL_INTENT);
                    } else {
                        String phone = data.get(position).getPhone();
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                        startActivity(intent);
                    }
                } else {
                    String phone = data.get(position).getPhone();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                    startActivity(intent);
                }
                break;

            case R.id.right_side:
                if (isRight) {
                    selected = 1;
                    setImages();
//                right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_selected));
//                left.setImageDrawable(getResources().getDrawable(R.drawable.car_left_unselected));
//                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_unselected));
//                back.setImageDrawable(getResources().getDrawable(R.drawable.car_back_unselected));
                    rightText.setTextColor(getResources().getColor(R.color.colorPrimary));
                    leftText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                    frontText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                    backText.setTextColor(getResources().getColor(R.color.bel_grey_text));

                    right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
                    left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                }
                break;

            case R.id.left_side:
                if (isLeft) {
                    selected = 2;
                    setImages();
//                right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_unselected));
//                left.setImageDrawable(getResources().getDrawable(R.drawable.car_left_selected));
//                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_unselected));
//                back.setImageDrawable(getResources().getDrawable(R.drawable.car_back_unselected));
                    rightText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                    leftText.setTextColor(getResources().getColor(R.color.colorPrimary));
                    frontText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                    backText.setTextColor(getResources().getColor(R.color.bel_grey_text));

                    right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
                    back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                }
                break;

            case R.id.front_side:
                if (isFront) {
                    selected = 3;
                    setImages();
//                right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_unselected));
//                left.setImageDrawable(getResources().getDrawable(R.drawable.car_left_unselected));
//                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
//                back.setImageDrawable(getResources().getDrawable(R.drawable.car_back_unselected));
                    rightText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                    leftText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                    frontText.setTextColor(getResources().getColor(R.color.colorPrimary));
                    backText.setTextColor(getResources().getColor(R.color.bel_grey_text));

                    right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
                }
                break;

            case R.id.back_side:
                if (isBack) {
                    selected = 4;
                    setImages();
//                right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_unselected));
//                left.setImageDrawable(getResources().getDrawable(R.drawable.car_left_unselected));
//                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_unselected));
//                back.setImageDrawable(getResources().getDrawable(R.drawable.car_back_selected));
                    rightText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                    leftText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                    frontText.setTextColor(getResources().getColor(R.color.bel_grey_text));
                    backText.setTextColor(getResources().getColor(R.color.colorPrimary));

                    right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                    back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
                    front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
                }
                break;

            case R.id.dummyView1:
                bigImageLayout.setVisibility(View.GONE);
                break;

            case R.id.dummyView2:
                bigImageLayout.setVisibility(View.GONE);
                break;
        }
    }

    public void showtwoButtonsAlertDialog(float finalPrice) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(FinalBidActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        final int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        if (language.equalsIgnoreCase("En")) {
            title.setText("Samkra");
            yes.setText(getResources().getString(R.string.yes));
            no.setText(getResources().getString(R.string.no));
            desc.setText("Do you want to confirm final price " + Constants.priceFormat.format(finalPrice) + " SR?");
        } else {
            title.setText("سمكره");
            yes.setText(getResources().getString(R.string.yes_ar));
            no.setText(getResources().getString(R.string.no_ar));
            desc.setText("هل السعر النهائي مناسب لك\n " + Constants.priceFormat.format(finalPrice) + " ريال؟");
        }

        if (language.equalsIgnoreCase("Ar")) {
            title.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
            desc.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
            yes.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
            no.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String networkStatus = NetworkUtil.getConnectivityStatusString(FinalBidActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new finalBidApi().execute();
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }
                }
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void getImages() {
        ArrayList<InboxResponse.RCI> images = new ArrayList<>();
        images.addAll(data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getRCI());

        for (int i = 0; i < images.size(); i++) {
            if (images.get(i).getDocumentType() == 1) {
                isRight = true;
                rightImages.add(images.get(i).getDocumentLocation());
            } else if (images.get(i).getDocumentType() == 2) {
                isFront = true;
                frontImages.add(images.get(i).getDocumentLocation());
            } else if (images.get(i).getDocumentType() == 3) {
                isLeft = true;
                leftImages.add(images.get(i).getDocumentLocation());
            } else if (images.get(i).getDocumentType() == 4) {
                isBack = true;
                backImages.add(images.get(i).getDocumentLocation());
            }
        }

        if (!isRight) {
            rightLayout.setEnabled(false);
            right.setImageDrawable(getResources().getDrawable(R.drawable.car_right));
            right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
        } else {
            selected = 1;
            right.setImageDrawable(getResources().getDrawable(R.drawable.car_right_blue));
            right_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
        }

        if (!isLeft) {
            leftLayout.setEnabled(false);
            left.setImageDrawable(getResources().getDrawable(R.drawable.car_left));
            left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
        } else {
            left.setImageDrawable(getResources().getDrawable(R.drawable.car_right_blue));
            if (!isRight && !isFront) {
                selected = 2;
                left.setImageDrawable(getResources().getDrawable(R.drawable.car_right_blue));
                left_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
            }
        }

        if (!isFront) {
            frontLayout.setEnabled(false);
            front.setImageDrawable(getResources().getDrawable(R.drawable.car_front));
            front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
        } else {
            front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
            if (!isRight) {
                selected = 3;
                front.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
                front_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
            }
        }
        if (!isBack) {
            backLayout.setEnabled(false);
            back.setImageDrawable(getResources().getDrawable(R.drawable.car_front));
            back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot));
        } else {
            back.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
            if (!isRight && !isFront && !isLeft) {
                selected = 4;
                back.setImageDrawable(getResources().getDrawable(R.drawable.car_front_selected));
                back_dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_selected));
            }
        }
        setImages();
    }

    private void setImages() {
        ArrayList<String> imagesSelected = new ArrayList<>();
        if (selected == 1) {
            imagesSelected.addAll(rightImages);
        } else if (selected == 2) {
            imagesSelected.addAll(leftImages);
        } else if (selected == 3) {
            imagesSelected.addAll(frontImages);
        } else if (selected == 4) {
            imagesSelected.addAll(backImages);
        }

        mAdapter = new ImagesAdapter(FinalBidActivity.this, imagesSelected, language);
        viewPager.setAdapter(mAdapter);

        mBigImageAdapter = new BigImagesAdapter(FinalBidActivity.this, imagesSelected);
        bigImageView.setAdapter(mBigImageAdapter);
    }

    private String prepareJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("BidId", data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getBidId());
            parentObj.put("WorkshopId", userId);
            parentObj.put("RequestId", data.get(position).getRequestId());
            parentObj.put("FinalPrice", finalP);
            parentObj.put("FinalPriceWSComment", strComment);
            parentObj.put("EstimatedDate", selectedDate1 + " " + time);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + strComment);
        return parentObj.toString();
    }

    private class finalBidApi extends AsyncTask<String, Integer, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(FinalBidActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<FinalBidResponse> call = apiService.FinalBid(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<FinalBidResponse>() {
                @Override
                public void onResponse(Call<FinalBidResponse> call, Response<FinalBidResponse> response) {
                    if (response.isSuccessful()) {
                        FinalBidResponse requestsResponse = response.body();
                        try {
                            if (requestsResponse.getStatus()) {
                                showThankyouDialog();
                            } else {
                                // status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = requestsResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), FinalBidActivity.this);
                                } else {
                                    String failureResponse = requestsResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), FinalBidActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(FinalBidActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(FinalBidActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(FinalBidActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(FinalBidActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<FinalBidResponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(FinalBidActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(FinalBidActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(FinalBidActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(FinalBidActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void showThankyouDialog() {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(FinalBidActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if (language.equalsIgnoreCase("En")) {
            layout = R.layout.dialog_thankyou;
        } else {
            layout = R.layout.dialog_thankyou_ar;
        }

        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        TextView dialogbox1 = (TextView) dialogView.findViewById(R.id.desc1);
        TextView dialogbox2 = (TextView) dialogView.findViewById(R.id.desc2);
        if (language.equalsIgnoreCase("Ar")) {
            dialogbox1.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
            dialogbox2.setTypeface(Constants.getarTypeFace(FinalBidActivity.this));
        }

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.dialog_close);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FinalBidActivity.this, MainActivity.class);
                intent.putExtra("type", "1");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.75;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showloaderAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(FinalBidActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
