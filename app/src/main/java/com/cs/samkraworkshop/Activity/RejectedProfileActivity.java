package com.cs.samkraworkshop.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.CountryFilteredData;
import com.cs.samkraworkshop.Models.DashboardResponse;
import com.cs.samkraworkshop.Models.GetListResponse;
import com.cs.samkraworkshop.Models.ImageDeleteResponse;
import com.cs.samkraworkshop.Models.UpdateDetailsResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RejectedProfileActivity extends Activity implements View.OnClickListener{

    EditText inputName, inputNameAr, mobileEt;
    String strName, strNameAr, strMobile, strPassword;
    private EditText etEmail, etDesc, etDescAr, etLandline;
    private EditText etStreet, etStreetAr, etZipcode, etDistrcit, etCity, etCountry;
    String strStreet, strStreetAr, strDistrict, strCity, strZipcode;
    String strEmail, strLandline, strDesc, strDescAr, cars = "";
    private TextView geoLocationEt, brandsSelected;
    private ImageView wImage1, wImage2, wImage3, wImage4, wLogoImage, wVatImage, wCRImage;
    private Spinner districtSpinner, citySpiner, countrySpinner;
    private ArrayAdapter<CountryFilteredData.Districts> districtAdapter;
    private ArrayAdapter<CountryFilteredData.Cities> cityAdapter;
    private ArrayAdapter<CountryFilteredData> countryAdapter;
    private LinearLayout detailsLayout, locationLayout, documentsLayout;
    AlertDialog loaderDialog;

    int imageSelected = 0;
    Bitmap thumbnail1 = null, thumbnail2 = null, thumbnail3 = null, thumbnail4 = null,
            thumbnail5 = null, thumbnail6 = null, thumbnail7 = null;

    private double latitude, longitude;

    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int PICK_IMAGE_FROM_CAMERA = 3;
    private static final int CAMERA_REQUEST = 4;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };

    public static List<GetListResponse.CarMakerList> CarMakerList = new ArrayList<>();
    public static List<GetListResponse.DistrictList> DistrictList;
    public static List<GetListResponse.CityList> CityList;
    public static List<GetListResponse.CountryList> CountryList;
    private ArrayList<CountryFilteredData> countryFilteredData = new ArrayList<>();

    LinearLayout detailsTitleLayout, locationTitleLayout, docsTitleLayout;
    ImageView detailsSuccess, locationSuccess, docsSuccess;
    TextView detailsText, locationText, docsText;
    private int cityPos = 0, districtPos = 0, countryPos = 0;
    private String districtSelected;
    String imagePath1;
    AlertDialog brandsDialog;
    Button updateBtn;
    SharedPreferences languagePrefs;
    String language;

    private byte[] logoBytes;
    private byte[] image1Bytes;
    private byte[] image2Bytes;
    private byte[] image3Bytes;
    private byte[] image4Bytes;
    private byte[] crBytes;
    private byte[] vatBytes;
    private String image1Str = "", image2Str = "", image3Str = "", image4Str = "", logoStr = "", vatStr = "", crStr = "";
    private boolean isLogoUploaded = false, isImagesUploaded = false, isCRUploaded = false, isVatUploaded = false;
    //    private boolean isLogoAvailable = false, isImage1Available = false, isImage2Available = false, isImage3Available = false
//            , isImage4Available = false, isCRAvailable = false, isVatAvailable = false;
    String logoName = "", crName = "", vatName = "", imagesName = "", imageNameForSingleUpload;
    private int image1Id = 0, image2Id  = 0, image3Id = 0, image4Id = 0, logoId = 0, vatId = 0, crId = 0;
    int deleteImageName;
    int updateStage = 0, pendingStages = 0;

    ArrayList<DashboardResponse.Data> data = new ArrayList<>();
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    AlertDialog customDialog;
    Boolean isDetailsUpdated = false, isLocationUpdated = false, isDocumentsUpdated = false;
    private RelativeLayout carBrandsLayout;
    RelativeLayout geoLocationLayout;
    Boolean isFirstTime = true;
    ImageView backBtn;
    Uri imageUri;
    boolean isGalleryImage = false;

    private String TAG = "TAG";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_rejected_profile);
        }
        else {
            setContentView(R.layout.activity_rejected_profile_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "");
        userPrefsEditor = userPrefs.edit();

//        data = DashboardFragment.data;

        districtSpinner = (Spinner) findViewById(R.id.district_spinner);
        citySpiner = (Spinner) findViewById(R.id.city_spinner);
        countrySpinner = (Spinner) findViewById(R.id.country_spinner);

        inputName = (EditText) findViewById(R.id.et_name);
        inputNameAr = (EditText) findViewById(R.id.et_name_ar);
        inputNameAr.requestFocus();
        mobileEt = (EditText) findViewById(R.id.et_mobile);
        etEmail = (EditText) findViewById(R.id.et_email);
        etLandline = (EditText) findViewById(R.id.et_landline);
        etDesc = (EditText) findViewById(R.id.et_description_ar);
        etDescAr = (EditText) findViewById(R.id.et_description);
        backBtn = (ImageView) findViewById(R.id.back_btn);

        etStreet = (EditText) findViewById(R.id.et_street_name);
        etStreetAr = (EditText) findViewById(R.id.et_street_name_ar);
        etCity = (EditText) findViewById(R.id.et_city);
        etCountry = (EditText) findViewById(R.id.et_country);
        etDistrcit = (EditText) findViewById(R.id.et_district);
        etZipcode = (EditText) findViewById(R.id.et_zipcode);
        geoLocationEt = (TextView) findViewById(R.id.et_geolocation);
        brandsSelected = (TextView) findViewById(R.id.selected_cars);

        wImage1 = (ImageView) findViewById(R.id.image1);
        wImage2 = (ImageView) findViewById(R.id.image2);
        wImage3 = (ImageView) findViewById(R.id.image3);
        wImage4 = (ImageView) findViewById(R.id.image4);
        wLogoImage = (ImageView) findViewById(R.id.logo);
        wVatImage = (ImageView) findViewById(R.id.vat_image);
        wCRImage = (ImageView) findViewById(R.id.cr_image);
        geoLocationLayout = (RelativeLayout) findViewById(R.id.geo_location_layout);

        detailsLayout = (LinearLayout) findViewById(R.id.details_layout);
        locationLayout = (LinearLayout) findViewById(R.id.location_layout);
        documentsLayout = (LinearLayout) findViewById(R.id.images_layout);

        detailsTitleLayout = (LinearLayout) findViewById(R.id.layout1);
        locationTitleLayout = (LinearLayout) findViewById(R.id.layout2);
        docsTitleLayout = (LinearLayout) findViewById(R.id.layout3);
        carBrandsLayout = (RelativeLayout) findViewById(R.id.car_brands_layout);

        detailsText = (TextView) findViewById(R.id.details_text);
        locationText = (TextView) findViewById(R.id.location_text);
        docsText = (TextView) findViewById(R.id.docs_text);
        updateBtn = (Button) findViewById(R.id.update_btn);

        detailsTitleLayout.setOnClickListener(this);
        locationTitleLayout.setOnClickListener(this);
        docsTitleLayout.setOnClickListener(this);
        carBrandsLayout.setOnClickListener(this);

        wImage1.setOnClickListener(this);
        wImage2.setOnClickListener(this);
        wImage3.setOnClickListener(this);
        wImage4.setOnClickListener(this);
        wLogoImage.setOnClickListener(this);
        wVatImage.setOnClickListener(this);
        wCRImage.setOnClickListener(this);

        updateBtn.setOnClickListener(this);
        geoLocationLayout.setOnClickListener(this);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        if(data.size() > 0){
//            initView();
//        }
//        else {
        String networkStatus = NetworkUtil.getConnectivityStatusString(RejectedProfileActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new dashboardApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error), getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), RejectedProfileActivity.this);
            } else {
                Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error_ar), getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
            }
        }

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }

    }
    private void setTypeface() {
        ((TextView) findViewById(R.id.docs_text)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.details_text)).setTypeface(Constants.getarbooldTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s1)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s2)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_name)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s3)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.ph_no_code)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_mobile)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s4)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_email)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s5)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s6)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_landline)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.selected_cars)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s7)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_description_ar)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s8)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_description)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.car_title)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.tv_country)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_city)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.tv_district)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s9)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_street_name_ar)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s10)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_street_name)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.S11)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_zipcode)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.et_geolocation)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.logo_title)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.workshop_images_layout)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.sub_title)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.s12)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.vat_title)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.update_btn)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        ((TextView) findViewById(R.id.location_text)).setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout1:
                detailsLayout.setVisibility(View.VISIBLE);
                locationLayout.setVisibility(View.GONE);
                documentsLayout.setVisibility(View.GONE);

                detailsText.setTextColor(getResources().getColor(R.color.white));
                detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));

                locationText.setTextColor(getResources().getColor(R.color.colorPrimary));
                locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                docsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));
                break;

            case R.id.layout2:
                detailsLayout.setVisibility(View.GONE);
                locationLayout.setVisibility(View.VISIBLE);
                documentsLayout.setVisibility(View.GONE);

                detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                locationText.setTextColor(getResources().getColor(R.color.white));
                locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));

                docsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));
                break;

            case R.id.layout3:
                detailsLayout.setVisibility(View.GONE);
                locationLayout.setVisibility(View.GONE);
                documentsLayout.setVisibility(View.VISIBLE);

                detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                locationText.setTextColor(getResources().getColor(R.color.colorPrimary));
                locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                docsText.setTextColor(getResources().getColor(R.color.white));
                docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));
                break;

            case R.id.image1:
                imageSelected = 1;
                deleteImageName = image1Id;
                showtwoButtonsAlertDialog();
                break;

            case R.id.image2:
                imageSelected = 2;
                deleteImageName = image2Id;
                showtwoButtonsAlertDialog();
                break;

            case R.id.image3:
                imageSelected = 3;
                deleteImageName = image3Id;
                showtwoButtonsAlertDialog();
                break;

            case R.id.image4:
                imageSelected = 4;
                deleteImageName = image4Id;
                showtwoButtonsAlertDialog();
                break;

            case R.id.logo:
                imageSelected = 5;
                deleteImageName = logoId;
                showtwoButtonsAlertDialog();
                break;

            case R.id.vat_image:
                imageSelected = 6;
                deleteImageName =  vatId;
                showtwoButtonsAlertDialog();
                break;

            case R.id.cr_image:
                imageSelected = 7;
                deleteImageName = crId;
                showtwoButtonsAlertDialog();
                break;

            case R.id.car_brands_layout:
                showBrandsDialog();
                break;

            case R.id.geo_location_layout:
                openPlacePicker();
                break;

            case R.id.update_btn:
                if(validations()) {
//                    if (isDetailsUpdated) {
                        updateStage = 1;
//                    }
//                    if (isLocationUpdated) {
//                        updateStage = 2;
//                    }
//                    if (isDetailsUpdated && isLocationUpdated) {
//                        updateStage = 1;
//                        pendingStages = 1;
//                    }

                    String networkStatus = NetworkUtil.getConnectivityStatusString(RejectedProfileActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new updateDetailsApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private void initView(){
        inputName.setText(data.get(0).getNameEn());
        inputNameAr.setText(data.get(0).getNameAr());
        etEmail.setText(data.get(0).getEmail());
        mobileEt.setText(data.get(0).getMobileNo().substring(3,12));
        try {
            etLandline.setText(data.get(0).getLandlineNo().substring(3,12));
        } catch (Exception e) {
            e.printStackTrace();
            etLandline.setText(data.get(0).getLandlineNo());
        }
        etDesc.setText(data.get(0).getAboutEn());
        etDescAr.setText(data.get(0).getAboutAr());
        etStreet.setText(data.get(0).getStreetNameEn());
        etStreetAr.setText(data.get(0).getStreetNameAr());
        etZipcode.setText(data.get(0).getZipCode());
        geoLocationEt.setText(data.get(0).getLatitude()+","+data.get(0).getLongitude());

        if(data.get(0).getLatitude() != null) {
            latitude = Double.parseDouble(data.get(0).getLatitude());
        }else {
            latitude = 0;
        }
        if(data.get(0).getLongitude() != null) {
            longitude = Double.parseDouble(data.get(0).getLongitude());
        }else {
            longitude = 0;
        }

        int wImageSet = 1;
        for (int i = 0; i < data.get(0).getWorkshopdocuments().size(); i++){
            if(data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 1){
                isLogoUploaded = true;
                logoId = data.get(0).getWorkshopdocuments().get(i).getId();
                Glide.with(RejectedProfileActivity.this)
                        .load(Constants.WORKSHOP_IMAGES+data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                        .into(wLogoImage);
            }
            else if(data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 4){
                isVatUploaded = true;
                vatId = data.get(0).getWorkshopdocuments().get(i).getId();
                Glide.with(RejectedProfileActivity.this)
                        .load(Constants.WORKSHOP_IMAGES+data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                        .into(wVatImage);
            }
            else if(data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 3){
                isCRUploaded = true;
                crId = data.get(0).getWorkshopdocuments().get(i).getId();
                Glide.with(RejectedProfileActivity.this)
                        .load(Constants.WORKSHOP_IMAGES+data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                        .into(wCRImage);
            }
            else if(data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 2){
                isImagesUploaded = true;
                if(wImageSet == 1) {
                    image1Id = data.get(0).getWorkshopdocuments().get(i).getId();
                    Glide.with(RejectedProfileActivity.this)
                            .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                            .into(wImage1);
                    wImageSet = wImageSet + 1;
                }
                else if(wImageSet == 2) {
                    image2Id = data.get(0).getWorkshopdocuments().get(i).getId();
                    Glide.with(RejectedProfileActivity.this)
                            .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                            .into(wImage2);
                    wImageSet = wImageSet + 1;
                }
                else if(wImageSet == 3) {
                    image3Id = data.get(0).getWorkshopdocuments().get(i).getId();
                    Glide.with(RejectedProfileActivity.this)
                            .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                            .into(wImage3);
                    wImageSet = wImageSet + 1;
                }
                else if(wImageSet == 4) {
                    image4Id = data.get(0).getWorkshopdocuments().get(i).getId();
                    Glide.with(RejectedProfileActivity.this)
                            .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                            .into(wImage4);
                    wImageSet = wImageSet + 1;
                }
            }
        }
    }

    private void openPlacePicker() {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            Intent intent = intentBuilder.build(RejectedProfileActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private class getDetailsApi extends AsyncTask<String, String, String> {


        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            Log.d("TAG", "inputStr: "+inputStr);
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(RejectedProfileActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<GetListResponse> call = apiService.getList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<GetListResponse>() {
                @Override
                public void onResponse(Call<GetListResponse> call, Response<GetListResponse> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        GetListResponse GetListResponse = response.body();
                        try {
                            if(GetListResponse.getStatus()){
                                CarMakerList.clear();
                                GetListResponse.CarMakerList car = new GetListResponse.CarMakerList();
//                                car.setChecked(false);
                                car.setNameEn("Select All");
                                car.setNameAr("اختر الكل");
                                car.setId(0);
                                CarMakerList.add(car);

                                CarMakerList.addAll(GetListResponse.getData().getCarMakerList());
                                DistrictList = GetListResponse.getData().getDistrictList();
                                CityList = GetListResponse.getData().getCityList();
                                CountryList = GetListResponse.getData().getCountryList();
                                setSpinner();

                                for (int j = 0; j < CarMakerList.size(); j++){
                                    if(CarMakerList.get(j).getChecked() && cars.length() == 0){
                                        cars = CarMakerList.get(j).getNameEn();
                                    }
                                    else if(CarMakerList.get(j).getChecked() && cars.length() > 0){
                                        cars = cars + ", "+ CarMakerList.get(j).getNameEn();
                                    }
                                }
                                brandsSelected.setText(cars);
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = GetListResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), RejectedProfileActivity.this);
                                } else {
                                    String failureResponse = GetListResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
//                            Toast.makeText(RejectedProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GetListResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("Id", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private void setSpinner(){
        final ArrayList<CountryFilteredData.Cities> cityData = new ArrayList<>();
        final ArrayList<CountryFilteredData.Districts> districtData = new ArrayList<>();
        for (int i = 0; i < CountryList.size(); i++){
//            try {
//                if(CountryList.get(i).getId() == data.get(0).getCountry().get(0).getId()) {
//                    countryPos = i;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
                countryPos = 0;
//            }
            CountryFilteredData brand = new CountryFilteredData();
            brand.setId(CountryList.get(i).getId());
            brand.setNameEn(CountryList.get(i).getNameEn());
            brand.setNameAr(CountryList.get(i).getNameAr());

            ArrayList<Integer> dummyCityList = new ArrayList<>();
            ArrayList<CountryFilteredData.Cities> cityList = new ArrayList<>();

            if(cityList == null || cityList.size() == 0){
                CountryFilteredData.Cities city = new CountryFilteredData.Cities();
                city.setNameAr("-- "+getResources().getString(R.string.city_ar)+" --");
                city.setNameEn("-- "+getResources().getString(R.string.city)+" --");
                city.setId(-1);

                CountryFilteredData.Districts branch = new CountryFilteredData.Districts();
                ArrayList<CountryFilteredData.Districts> districtArrayList = new ArrayList<>();
                branch.setNameEn("-- "+getResources().getString(R.string.district)+" --");
                branch.setNameAr("-- "+getResources().getString(R.string.district_ar)+" --");
                branch.setId(-1);
                districtArrayList.add(branch);
                city.setDistricts(districtArrayList);
                cityList.add(city);
            }

            for (int j = 0; j < CityList.size(); j++){
                if(!dummyCityList.contains(CityList.get(j).getId())){
                    if(CityList.get(j).getCountryId() == (CountryList.get(i).getId())) {
                        Log.d(TAG, "CityList: "+i+","+CityList.get(j).getNameEn());
                        dummyCityList.add(CityList.get(j).getId());
                        CountryFilteredData.Cities city = new CountryFilteredData.Cities();
                        for (GetListResponse.CityList cityList1 : CityList) {
                            if (CityList.get(j).getId() == (cityList1.getId())) {
                                city.setNameAr(cityList1.getNameAr());
                                city.setNameEn(cityList1.getNameEn());
                                city.setId(cityList1.getId());

                                ArrayList<CountryFilteredData.Districts> districtArrayList = new ArrayList<>();
                                for (int k = 0; k < DistrictList.size(); k++) {
                                    if(CountryList.get(i).getId() == (CityList.get(j).getCountryId())) {
                                        if (DistrictList.get(k).getCityId() == (cityList1.getId())) {
                                            Log.d(TAG, "DistrictList: "+i+","+DistrictList.get(k).getNameEn());
                                            CountryFilteredData.Districts branch = new CountryFilteredData.Districts();

                                            branch.setNameEn(DistrictList.get(k).getNameEn());
                                            branch.setNameAr(DistrictList.get(k).getNameAr());
                                            branch.setId(DistrictList.get(k).getId());
                                            districtArrayList.add(branch);
                                        }
                                    }
                                }
                                city.setDistricts(districtArrayList);
                                break;
                            }
                        }
                        cityList.add(city);
                    }
                }
            }
            brand.setCities(cityList);
            countryFilteredData.add(brand);
        }

        countryAdapter = new ArrayAdapter<CountryFilteredData>(getApplicationContext(), R.layout.list_spinner, countryFilteredData) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setText(countryFilteredData.get(position).getNameEn());
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        cityData.addAll(countryFilteredData.get(countryPos).getCities());
        for (int j = 0; j < cityData.size(); j++) {
            try {
               if (cityData.get(j).getId() == data.get(0).getCity().get(0).getId()) {
                    cityPos = j;
                }
            } catch (Exception e) {
                e.printStackTrace();
                cityPos = 0;
            }
        }
        Log.d(TAG, "cityPos: "+cityPos);
        Log.d(TAG, "cityData: "+cityData.size());
        cityAdapter = new ArrayAdapter<CountryFilteredData.Cities>(getApplicationContext(), R.layout.list_spinner, cityData) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
//                Log.d(TAG, "getDropDownView: "+countryFilteredData.get(countryPos).getCities().get(position).getNameEn());
                try {
                    if (language.equalsIgnoreCase("En")){
                        ((TextView) v).setText(cityData.get(position).getNameEn());
                    }
                    else {
                        ((TextView) v).setText(cityData.get(position).getNameAr());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        districtData.addAll(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts());
        for (int j = 0; j < districtData.size(); j++) {
                try {
                    if(districtData.get(j).getId() == data.get(0).getDistrict().get(0).getId()) {
                        districtPos = j;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    districtPos = 0;
            }
        }
        districtAdapter = new ArrayAdapter<CountryFilteredData.Districts>(getApplicationContext(), R.layout.list_spinner, districtData) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                if(language.equalsIgnoreCase("En")) {
                    ((TextView) v).setText(districtData.get(position).getNameEn());
                }
                else {
                    ((TextView) v).setText(districtData.get(position).getNameAr());
                }
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        countrySpinner.setAdapter(countryAdapter);
        citySpiner.setAdapter(cityAdapter);
        districtSpinner.setAdapter(districtAdapter);

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!isFirstTime) {
                    if (countryPos != i) {
                        etCountry.setText(countryFilteredData.get(i).getNameEn());
                        countryPos = i;
                        isLocationUpdated = true;
                        updateBtn.setVisibility(View.VISIBLE);
                        cityData.clear();
                        cityData.addAll(countryFilteredData.get(countryPos).getCities());
                        citySpiner.setAdapter(cityAdapter);
                        cityAdapter.notifyDataSetChanged();
                    } else {
                        etCountry.setText(countryFilteredData.get(i).getNameEn());
                    }
                }
                else {
                    etCountry.setText(countryFilteredData.get(countryPos).getNameEn());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!isFirstTime) {
                    if (cityPos != i) {
                        etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameEn());
                        cityPos = i;
                        isLocationUpdated = true;
                        updateBtn.setVisibility(View.VISIBLE);
                        districtData.clear();
                        districtData.addAll(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts());
                        districtSpinner.setAdapter(districtAdapter);
                        districtAdapter.notifyDataSetChanged();
                    } else {
                        etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameEn());
                    }
                }
                else {
                    etCity.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getNameEn());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!isFirstTime) {
                    if (districtPos != i) {
                        etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameEn());
                        districtPos = i;
                        isLocationUpdated = true;
                        updateBtn.setVisibility(View.VISIBLE);
//                        districtAdapter.notifyDataSetChanged();
                    } else {
                        etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameEn());
                    }
                }
                else {
                    etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(districtPos).getNameEn());
                    isFirstTime = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(RejectedProfileActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
                else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RejectedProfileActivity.this, "Storage permission denied, Unable to select pictures", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(RejectedProfileActivity.this, "تم رفض  التخزين ، غير قادر على تحديد الصور", Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case CAMERA_REQUEST:
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                break;
        }
    }

    public void showtwoButtonsAlertDialog(){
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            }
            else if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
            }
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RejectedProfileActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.images_alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        TextView cancel = (TextView) dialogView.findViewById(R.id.cancel_btn);

        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
            desc.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
            yes.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
            no.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
            title.setText("سمكره");
            no.setText("إستديو الصور");
            yes.setText("كاميرا");
            desc.setText("اختار من الخيارات التالية");
            cancel.setText("الرجاء قبول");
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, PICK_IMAGE_FROM_CAMERA);
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                customDialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                final Place place = PlacePicker.getPlace(RejectedProfileActivity.this, data);
                // TODO call location based filter
                LatLng latLong;
                latLong = place.getLatLng();
                latitude = latLong.latitude;
                longitude = latLong.longitude;
                geoLocationEt.setText(latitude+","+longitude);

                isLocationUpdated = true;
                updateBtn.setVisibility(View.VISIBLE);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {

        } else if (resultCode == RESULT_CANCELED) {

        }

        if(requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();

            try {
                if(imageSelected == 1) {
                    thumbnail1 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 2) {
                    thumbnail2 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 3) {
                    thumbnail3 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 4) {
                    thumbnail4 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 5) {
                    thumbnail5 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 6) {
                    thumbnail6 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 7) {
                    thumbnail7 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            convertGalleryImages();
            if(deleteImageName != 0) {
                isGalleryImage = true;
                new deleteImageApi().execute();
            }
            else{
                convertGalleryImages();
            }
        }
        else if(requestCode == PICK_IMAGE_FROM_CAMERA && resultCode == RESULT_OK) {

            if(deleteImageName != 0) {
                isGalleryImage = false;
                new deleteImageApi().execute();
            }
            else{
                convertCameraImages();
            }
//            convertGalleryImages();
        }
    }

    private void convertGalleryImages() {
        if(imageSelected == 1) {
            isImagesUploaded = true;
            thumbnail1 = Bitmap.createScaledBitmap(thumbnail1, 700, 500,
                    false);
            thumbnail1 = codec(thumbnail1, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail1);
            wImage1.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail1, "image", 1);
        }
        else if(imageSelected == 2) {
            isImagesUploaded = true;
            thumbnail2 = Bitmap.createScaledBitmap(thumbnail2, 700, 500,
                    false);
            thumbnail2 = codec(thumbnail2, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail2);
            wImage2.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail2, "image", 2);
        }
        else if(imageSelected == 3) {
            isImagesUploaded = true;
            thumbnail3 = Bitmap.createScaledBitmap(thumbnail3, 700, 500,
                    false);
            thumbnail3 = codec(thumbnail3, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail3);
            wImage3.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail3, "image", 3);
        }
        else if(imageSelected == 4) {
            isImagesUploaded = true;
            thumbnail4 = Bitmap.createScaledBitmap(thumbnail4, 700, 500,
                    false);
            thumbnail4 = codec(thumbnail4, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail4);
            wImage4.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail4, "image", 4);
        }
        else if(imageSelected == 5) {
            isLogoUploaded = true;
            thumbnail5 = Bitmap.createScaledBitmap(thumbnail5, 700, 500,
                    false);
            thumbnail5 = codec(thumbnail5, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail5);
            wLogoImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail5, "logo", 5);
        }
        else if(imageSelected == 6) {
            isVatUploaded = true;
            thumbnail6 = Bitmap.createScaledBitmap(thumbnail6, 700, 500,
                    false);
            thumbnail6 = codec(thumbnail6, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail6);
            wVatImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail6, "vat", 6);
        }
        else if(imageSelected == 7) {
            isCRUploaded = true;
            thumbnail7 = Bitmap.createScaledBitmap(thumbnail7, 700, 500,
                    false);
            thumbnail7 = codec(thumbnail7, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail7);
            wCRImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail7, "cr", 7);
        }
//        updateStage = 3;
//        pendingStages = 0;
//        new updateDetailsApi().execute();
        new uploadImageApi().execute();
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void convertCameraImages() {
        String path = getRealPathFromURI(imageUri);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inScaled = false;

        if(imageSelected == 1) {
            isImagesUploaded = true;

            thumbnail1 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail1, 800,
                    800, true);
            thumbnail1 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail1);
            wImage1.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail1, "image", 1);
        }
        else if(imageSelected == 2) {
            isImagesUploaded = true;
            thumbnail2 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail2, 800,
                    800, true);
            thumbnail2 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail2);
            wImage2.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail2, "image", 2);
        }
        else if(imageSelected == 3) {
            isImagesUploaded = true;
            thumbnail3 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail3, 800,
                    800, true);
            thumbnail3 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail3);
            wImage3.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail3, "image", 3);
        }
        else if(imageSelected == 4) {
            isImagesUploaded = true;
            thumbnail4 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail4, 800,
                    800, true);
            thumbnail4 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail4);
            wImage4.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail4, "image", 4);
        }
        else if(imageSelected == 5) {
            isLogoUploaded = true;
            thumbnail5 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail5, 800,
                    800, true);
            thumbnail5 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail5);
            wLogoImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail5, "logo", 5);
        }
        else if(imageSelected == 6) {
            isVatUploaded = true;
            thumbnail6 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail6, 800,
                    800, true);
            thumbnail6 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail6);
            wVatImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail6, "vat", 6);
        }
        else if(imageSelected == 7) {
            isCRUploaded = true;
            thumbnail7 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail7, 800,
                    800, true);
            thumbnail7 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail7);
            wCRImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail7, "cr", 7);
        }
        new uploadImageApi().execute();
    }

    public String StoreByteImage(Bitmap bitmap, String type, int num) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(new Date());
        String pictureName = "";
        if(type.equals("image") ) {
            pictureName = "IMG_" + userId + "_" + timeStamp + ".jpg";
            if(imagesName.length() == 0){
                imagesName = pictureName;
            }
            else {
                imagesName = imagesName + ","+ pictureName;
            }
        }
        else if(type.equals("logo") ) {
            pictureName = "LOGO_" + userId + "_" + timeStamp + ".jpg";
            logoName = pictureName;
        }
        else if(type.equals("cr") ) {
            pictureName = "CR_" + userId + "_" + timeStamp + ".jpg";
            crName = pictureName;
        }
        else if(type.equals("vat") ) {
            pictureName = "VAT_" + userId + "_" + timeStamp + ".jpg";
            vatName = pictureName;
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        if(num == 1){
            image1Bytes = stream.toByteArray();
            image1Str = Base64.encodeToString(image1Bytes, Base64.DEFAULT);
        }
        else if(num == 2){
            image2Bytes = stream.toByteArray();
            image2Str = Base64.encodeToString(image2Bytes, Base64.DEFAULT);
        }
        else if(num == 3){
            image3Bytes = stream.toByteArray();
            image3Str = Base64.encodeToString(image3Bytes, Base64.DEFAULT);
        }
        else if(num == 4){
            image4Bytes = stream.toByteArray();
            image4Str = Base64.encodeToString(image4Bytes, Base64.DEFAULT);
        }
        else if(num == 5){
            logoBytes = stream.toByteArray();
            logoStr = Base64.encodeToString(logoBytes, Base64.DEFAULT);
        }
        else if(num == 6){
            vatBytes = stream.toByteArray();
            vatStr = Base64.encodeToString(vatBytes, Base64.DEFAULT);
        }
        else if(num == 7){
            crBytes = stream.toByteArray();
            crStr = Base64.encodeToString(crBytes, Base64.DEFAULT);
        }

        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "samkra");
        folder.mkdir();

        File sdImageMainDirectory = new File(folder, pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

			/*Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,

			imageData.length,options);*/
            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    private class deleteImageApi extends AsyncTask<String, String, String> {


        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(RejectedProfileActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ImageDeleteResponse> call = apiService.DeleteWorkhopImage(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ImageDeleteResponse>() {
                @Override
                public void onResponse(Call<ImageDeleteResponse> call, Response<ImageDeleteResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()){
                        ImageDeleteResponse signInResponse = response.body();
                        try {
                            if(signInResponse.getStatus()){
                                deleteImageName = 0;
                                if (isGalleryImage) {
                                    convertGalleryImages();
                                }
                                else {
                                    convertCameraImages();
                                }
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = signInResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), RejectedProfileActivity.this);
                                } else {
                                    String failureResponse = signInResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ImageDeleteResponse> call, Throwable t) {
                    Log.d(TAG, "onResponse: "+t.toString());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("workshopId", userId);
            mobileObj.put("ImageId", deleteImageName);
            Log.d(TAG, "prepareVerifyMobileJson: "+mobileObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private void showBrandsDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RejectedProfileActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        final LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.car_brands_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        final LinearLayout brands = (LinearLayout) dialogView.findViewById(R.id.brands_layout);

        if (language.equalsIgnoreCase("Ar")){
            no.setText(getResources().getString(R.string.cancel_str_ar));
            yes.setText(getResources().getString(R.string.ok_ar));
            title.setText(getResources().getString(R.string.car_you_fix_ar));
            no.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
            yes.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
            title.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        }

        for (int i = 0; i < CarMakerList.size(); i++) {
            View v = inflater.inflate(R.layout.list_car_brands, null);
            CheckBox checkBox = v.findViewById(R.id.checkbox);
            if (language.equalsIgnoreCase("En")){
                checkBox.setText(CarMakerList.get(i).getNameEn());
            }
            else {
                checkBox.setText(CarMakerList.get(i).getNameAr());
            }
            if(CarMakerList.get(i).getChecked()){
                checkBox.setChecked(true);
            }

            final int finalI = i;
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(finalI == 0) {
                        if (b) {
                            for (int i = 0; i < CarMakerList.size(); i++) {
                                CarMakerList.get(i).setChecked(true);
                            }
                        } else {
                            for (int i = 0; i < CarMakerList.size(); i++) {
                                CarMakerList.get(i).setChecked(false);
                            }
                        }
                        brands.removeAllViews();
                        for (int i = 0; i < CarMakerList.size(); i++) {
                            View v = inflater.inflate(R.layout.list_car_brands, null);
                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                            if (language.equalsIgnoreCase("En")){
                                checkBox.setText(CarMakerList.get(i).getNameEn());
                            }
                            else {
                                checkBox.setText(CarMakerList.get(i).getNameAr());
                            }
                            if(CarMakerList.get(i).getChecked()){
                                checkBox.setChecked(true);
                            }

                            final int finalI = i;
                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if(finalI == 0) {
                                        if (b) {
                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                CarMakerList.get(i).setChecked(true);
                                            }
                                        } else {
                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                CarMakerList.get(i).setChecked(false);
                                            }
                                        }
                                        brands.removeAllViews();
                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                            if (language.equalsIgnoreCase("En")){
                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                            }
                                            else {
                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                            }
                                            if(CarMakerList.get(i).getChecked()){
                                                checkBox.setChecked(true);
                                            }

                                            final int finalI = i;
                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                @Override
                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                    if(finalI == 0) {
                                                        if (b) {
                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                CarMakerList.get(i).setChecked(true);
                                                            }
                                                        } else {
                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                CarMakerList.get(i).setChecked(false);
                                                            }
                                                        }
                                                        brands.removeAllViews();
                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                            if (language.equalsIgnoreCase("En")){
                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                            }
                                                            else {
                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                            }
                                                            if(CarMakerList.get(i).getChecked()){
                                                                checkBox.setChecked(true);
                                                            }

                                                            final int finalI = i;
                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                @Override
                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                    if(finalI == 0) {
                                                                        if (b) {
                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                CarMakerList.get(i).setChecked(true);
                                                                            }
                                                                        } else {
                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                CarMakerList.get(i).setChecked(false);
                                                                            }
                                                                        }
                                                                        brands.removeAllViews();
                                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                                            if (language.equalsIgnoreCase("En")){
                                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                                            }
                                                                            else {
                                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                                            }
                                                                            if(CarMakerList.get(i).getChecked()){
                                                                                checkBox.setChecked(true);
                                                                            }

                                                                            final int finalI = i;
                                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                                @Override
                                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                                    if(finalI == 0) {
                                                                                        if (b) {
                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                CarMakerList.get(i).setChecked(true);
                                                                                            }
                                                                                        } else {
                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                CarMakerList.get(i).setChecked(false);
                                                                                            }
                                                                                        }
                                                                                        brands.removeAllViews();
                                                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                                                            if (language.equalsIgnoreCase("En")){
                                                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                                                            }
                                                                                            else {
                                                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                                                            }
                                                                                            if(CarMakerList.get(i).getChecked()){
                                                                                                checkBox.setChecked(true);
                                                                                            }

                                                                                            final int finalI = i;
                                                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                                                @Override
                                                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                                                    if(finalI == 0) {
                                                                                                        if (b) {
                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                CarMakerList.get(i).setChecked(true);
                                                                                                            }
                                                                                                        } else {
                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                CarMakerList.get(i).setChecked(false);
                                                                                                            }
                                                                                                        }
                                                                                                        brands.removeAllViews();
                                                                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                                                                            if (language.equalsIgnoreCase("En")){
                                                                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                                                                            }
                                                                                                            else {
                                                                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                                                                            }
                                                                                                            if(CarMakerList.get(i).getChecked()){
                                                                                                                checkBox.setChecked(true);
                                                                                                            }

                                                                                                            final int finalI = i;
                                                                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                                                                @Override
                                                                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                                                                    if(finalI == 0) {
                                                                                                                        if (b) {
                                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                                CarMakerList.get(i).setChecked(true);
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                                CarMakerList.get(i).setChecked(false);
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                    else {
                                                                                                                        if (b) {
                                                                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                                                                        } else {
                                                                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            });
                                                                                                            brands.addView(v);
                                                                                                        }
                                                                                                    }
                                                                                                    else {
                                                                                                        if (b) {
                                                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                                                        } else {
                                                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                            brands.addView(v);
                                                                                        }
                                                                                    }
                                                                                    else {
                                                                                        if (b) {
                                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                                        } else {
                                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                            brands.addView(v);
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (b) {
                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                        } else {
                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                            brands.addView(v);
                                                        }
                                                    }
                                                    else {
                                                        if (b) {
                                                            CarMakerList.get(finalI).setChecked(true);
                                                        } else {
                                                            CarMakerList.get(finalI).setChecked(false);
                                                        }
                                                    }
                                                }
                                            });
                                            brands.addView(v);
                                        }
                                    }
                                    else {
                                        if (b) {
                                            CarMakerList.get(finalI).setChecked(true);
                                        } else {
                                            CarMakerList.get(finalI).setChecked(false);
                                        }
                                    }
                                }
                            });
                            brands.addView(v);
                        }
                    }
                    else {
                        if (b) {
                            CarMakerList.get(finalI).setChecked(true);
                        } else {
                            CarMakerList.get(finalI).setChecked(false);
                        }
                    }
                }
            });
            brands.addView(v);
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cars = "";
                boolean isAllSelected = false;
                for (int j = 0; j < CarMakerList.size(); j++) {
                    if (j != 0) {
                        if (CarMakerList.get(j).getChecked() && cars.length() == 0) {
                            if (language.equalsIgnoreCase("En")) {
                                cars = CarMakerList.get(j).getNameEn();
                            } else {
                                cars = CarMakerList.get(j).getNameAr();
                            }
                        } else if (CarMakerList.get(j).getChecked() && cars.length() > 0) {
                            if (language.equalsIgnoreCase("En")) {
                                cars = cars + ", " + CarMakerList.get(j).getNameEn();
                            } else {
                                cars = cars + ", " + CarMakerList.get(j).getNameAr();
                            }
                        }
                    }
                }
                brandsSelected.setText(cars);
                brandsDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                cars = "";
//                for (int j = 0; j < CarMakerList.size(); j++){
//                    if (j != 0) {
//                        if (CarMakerList.get(j).getChecked() && cars.length() == 0) {
//                            if (language.equalsIgnoreCase("En")) {
//                                cars = CarMakerList.get(j).getNameEn();
//                            } else {
//                                cars = CarMakerList.get(j).getNameAr();
//                            }
//                        } else if (CarMakerList.get(j).getChecked() && cars.length() > 0) {
//                            if (language.equalsIgnoreCase("En")) {
//                                cars = cars + ", " + CarMakerList.get(j).getNameEn();
//                            } else {
//                                cars = cars + ", " + CarMakerList.get(j).getNameAr();
//                            }
//                        }
//                    }
//                }
//                brandsSelected.setText(cars);
                brandsDialog.dismiss();
            }
        });

        brandsDialog = dialogBuilder.create();
        brandsDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = brandsDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private String prepareUpdateJson() {
        String ids = "";
        for (int j = 0; j < CarMakerList.size(); j++){
            if(CarMakerList.get(j).getChecked() && ids.length() == 0){
                ids = Integer.toString(CarMakerList.get(j).getId());
            }
            else if(CarMakerList.get(j).getChecked() && cars.length() > 0){
                ids = ids + ", "+ Integer.toString(CarMakerList.get(j).getId());
            }
        }
        JSONObject mobileObj = new JSONObject();
        JSONArray docsArray = new JSONArray();
        if(updateStage == 1) {
            try {
                mobileObj.put("Id", userId);
                mobileObj.put("NameEn", strName);
                mobileObj.put("NameAr", strNameAr);
                mobileObj.put("AboutEn", strDesc);
                mobileObj.put("AboutAr", strDescAr);
                mobileObj.put("EmailID", strEmail);
                mobileObj.put("LandLineNo", "966"+strLandline);
                mobileObj.put("SupportedCarMakerList", ids);
                mobileObj.put("profileUpdateStages", "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(updateStage == 2) {
            try {
                mobileObj.put("Id", userId);
                mobileObj.put("StreetNameEn", strStreet);
                mobileObj.put("StreetNameAr", strStreetAr);
                mobileObj.put("CountryID", countryFilteredData.get(countryPos).getId());
                mobileObj.put("CityID", countryFilteredData.get(countryPos).getCities().get(cityPos).getId());
                mobileObj.put("DistrictID", countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(districtPos).getId());
                mobileObj.put("Zipcode", strZipcode);
                mobileObj.put("Latitude", latitude);
                mobileObj.put("Longitude", longitude);
                mobileObj.put("profileUpdateStages", "2");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(updateStage == 3) {
            try {
                mobileObj.put("Id", userId);
                mobileObj.put("WorkshopLogo", logoName);
                mobileObj.put("diclogo", "");
                mobileObj.put("WorkshopImg", imagesName);
                if(thumbnail1 != null){
                    docsArray.put(image1Str);
                }
                if(thumbnail2 != null){
                    docsArray.put(image2Str);
                }
                if(thumbnail3 != null){
                    docsArray.put(image3Str);
                }
                if(thumbnail4 != null){
                    docsArray.put(image4Str);
                }
                mobileObj.put("dicimg","");
                mobileObj.put("CRD", crName);
                mobileObj.put("dicCr", "");
                mobileObj.put("VatCertificate", vatName);
                mobileObj.put("dicVat", "");
                mobileObj.put("profileUpdateStages", "3");
                userPrefsEditor.putString("pic", logoName);
                userPrefsEditor.commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mobileObj.toString();
    }

    private class updateDetailsApi extends AsyncTask<String, String, String> {


        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUpdateJson();
            Log.d(TAG, "inputStr: "+inputStr);
           showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(RejectedProfileActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UpdateDetailsResponse> call = apiService.updateDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateDetailsResponse>() {
                @Override
                public void onResponse(Call<UpdateDetailsResponse> call, Response<UpdateDetailsResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()){
                        UpdateDetailsResponse UpdateDetailsResponse = response.body();
                        try {
                            if(UpdateDetailsResponse.getStatus()){
                                if(updateStage == 1){
                                    updateStage = 2;
                                    new updateDetailsApi().execute();
                                }
                                else if(updateStage == 2){
                                    updateStage = 3;
                                    new updateDetailsApi().execute();
                                }
                                else if(updateStage == 3){
                                    if(language.equalsIgnoreCase("En")) {
                                        showDialog("Your workshop details has been submitted. Account is pending for verification",
                                                "Samkra", "Ok", RejectedProfileActivity.this);
                                    }
                                    else {
                                        showDialog("تم تسجيل حساب الورشة. في انتظار التحقق من تفعيل الحساب",
                                                getResources().getString(R.string.samkra_ar),
                                                getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
                                    }
                                }
                            }
                            else {


                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = UpdateDetailsResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), RejectedProfileActivity.this);
                                }
                                else {
                                    String failureResponse = UpdateDetailsResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
                                }


                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UpdateDetailsResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private boolean validations(){
        strName = inputName.getText().toString();
        strNameAr = inputNameAr.getText().toString();
        strEmail = etEmail.getText().toString().trim();
        strLandline = etLandline.getText().toString().trim();
        strDesc = etDesc.getText().toString();
        strDescAr = etDescAr.getText().toString();
        strLandline = strLandline.replace("+966 ","");
        strStreet = etStreet.getText().toString();
        strStreetAr = etStreetAr.getText().toString();
        strZipcode = etZipcode.getText().toString();

        if(strName.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            }
            else {
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
            }
            inputName.requestFocus();
            return false;
        }
        else if(strNameAr.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            }
            else {
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
            }
            inputName.requestFocus();
            return false;
        }
        else if(strEmail.length()> 0 && !Constants.isValidEmail(strEmail)){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
            }
            else {
                etEmail.setError(getResources().getString(R.string.signup_msg_invalid_email_ar));
            }
            etEmail.requestFocus();
            return false;
        }
        else if(strLandline.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etLandline.setError(getResources().getString(R.string.signup_msg_enter_landline));
            }
            else {
                etLandline.setError(getResources().getString(R.string.signup_msg_enter_landline_ar));
            }

            etLandline.requestFocus();
            return false;
        }
        else if(strLandline.length() != 9){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etLandline.setError(getResources().getString(R.string.signup_msg_valid_landline));
            }
            else {
                etLandline.setError(getResources().getString(R.string.signup_msg_valid_landline_ar));
            }

            etLandline.requestFocus();
            return false;
        }
        else if(strDesc.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etDesc.setError(getResources().getString(R.string.signup_msg_enter_about));
            }
            else {
                etDesc.setError(getResources().getString(R.string.signup_msg_enter_about_ar));
            }

            etDesc.requestFocus();
            return false;
        }
        else if(strDescAr.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etDescAr.setError(getResources().getString(R.string.signup_msg_enter_about));
            }
            else {
                etDescAr.setError(getResources().getString(R.string.signup_msg_enter_about_ar));
            }

            etDescAr.requestFocus();
            return false;
        }
        else if(cars.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_cars),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), RejectedProfileActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_cars_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
            }

            return false;
        }
        else if(cityPos == 0) {
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), RejectedProfileActivity.this);
            }
            else   {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
            }
            return false;
        }
        else if(strStreet.length() == 0){
            locationVisible();
            if (language.equalsIgnoreCase("En")){
                etStreet.setError(getResources().getString(R.string.signup_msg_enter_street));
            }
            else {
                etStreetAr.setError(getResources().getString(R.string.signup_msg_enter_street_ar));
            }

            etStreet.requestFocus();
            return false;
        }
        else if(strStreetAr.length() == 0){
            locationVisible();
            if (language.equalsIgnoreCase("En")){
                etStreet.setError(getResources().getString(R.string.signup_msg_enter_street));
            }
            else {
                etStreetAr.setError(getResources().getString(R.string.signup_msg_enter_street_ar));
            }
            etStreetAr.requestFocus();
            return false;
        }
        else if(strZipcode.length() == 0){
            locationVisible();
            if (language.equalsIgnoreCase("En")){
                etZipcode.setError(getResources().getString(R.string.signup_msg_enter_zipcode));
            }
            else {
                etZipcode.setError(getResources().getString(R.string.signup_msg_enter_zipcode_ar));
            }
            etZipcode.requestFocus();
            return false;
        }
        else if(latitude == 0 && longitude == 0){
            locationVisible();
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), RejectedProfileActivity.this);
            } else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
            }
            return false;
        }
        else if(!isLogoUploaded){
            docsVisible();
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_logo),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), RejectedProfileActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_logo_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
            }

            return false;
        }
        else if(!isImagesUploaded){
            docsVisible();
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_images),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), RejectedProfileActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_images_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
            }
            return false;
        }
        else if(!isCRUploaded){
            docsVisible();
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_cr),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), RejectedProfileActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_cr_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
            }

            return false;
        }
        else if(!isVatUploaded){
            docsVisible();
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_vat),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), RejectedProfileActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_vat_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
            }

            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private class dashboardApi extends AsyncTask<String, String, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(RejectedProfileActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<DashboardResponse> call = apiService.GetDashBoard(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DashboardResponse>() {
                @Override
                public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                    if (response.isSuccessful()) {
                        DashboardResponse dashboardResponse = response.body();
                        try {
                            if (dashboardResponse.getStatus()) {
                                data.clear();
                                data.addAll(dashboardResponse.getData());
                                initView();
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = dashboardResponse.getMessage();
                                    Constants.showOneButtonAlertDialogWithFinish(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), RejectedProfileActivity.this);
                                }
                                else {
                                    String failureResponse = dashboardResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialogWithFinish(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
//                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
//                                    getResources().getString(R.string.ok), RejectedProfileActivity.this);
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), RejectedProfileActivity.this);
                        } else {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }

                    String networkStatus = NetworkUtil.getConnectivityStatusString(RejectedProfileActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new getDetailsApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), RejectedProfileActivity.this);
                        } else {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<DashboardResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), RejectedProfileActivity.this);
                        } else {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), RejectedProfileActivity.this);
                        } else {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), RejectedProfileActivity.this);
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("WorkshopId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    private void showDialog(String descriptionStr, String titleStr, String buttonStr, Activity context){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
            desc.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
            yes.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
            no.setTypeface(Constants.getarTypeFace(RejectedProfileActivity.this));
        }

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                startActivity(new Intent(RejectedProfileActivity.this, SignInActivity.class));
                finalCustomDialog.dismiss();
                finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void detailsVisible(){
        detailsLayout.setVisibility(View.VISIBLE);
        locationLayout.setVisibility(View.GONE);
        documentsLayout.setVisibility(View.GONE);

        detailsText.setTextColor(getResources().getColor(R.color.white));
        detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));

        locationText.setTextColor(getResources().getColor(R.color.colorPrimary));
        locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

        docsText.setTextColor(getResources().getColor(R.color.colorPrimary));
        docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));
    }

    private void locationVisible(){
        detailsLayout.setVisibility(View.GONE);
        locationLayout.setVisibility(View.VISIBLE);
        documentsLayout.setVisibility(View.GONE);

        detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
        detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

        locationText.setTextColor(getResources().getColor(R.color.white));
        locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));

        docsText.setTextColor(getResources().getColor(R.color.colorPrimary));
        docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

    }

    private void docsVisible(){
        detailsLayout.setVisibility(View.GONE);
        locationLayout.setVisibility(View.GONE);
        documentsLayout.setVisibility(View.VISIBLE);

        detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
        detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

        locationText.setTextColor(getResources().getColor(R.color.colorPrimary));
        locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

        docsText.setTextColor(getResources().getColor(R.color.white));
        docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RejectedProfileActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private String prepareUploadImageJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            if (imageSelected == 1) {
                mobileObj.put("FileName", imageNameForSingleUpload);
                mobileObj.put("base64string", image1Str);
            }
            else if (imageSelected == 2) {
                mobileObj.put("FileName", imageNameForSingleUpload);
                mobileObj.put("base64string", image2Str);
            }
            else if (imageSelected == 3) {
                mobileObj.put("FileName", imageNameForSingleUpload);
                mobileObj.put("base64string", image3Str);
            }
            else if (imageSelected == 4) {
                mobileObj.put("FileName", imageNameForSingleUpload);
                mobileObj.put("base64string", image4Str);
            }
            else if (imageSelected == 5) {
                mobileObj.put("FileName", logoName);
                mobileObj.put("base64string", logoStr);
            }
            else if (imageSelected == 6) {
                mobileObj.put("FileName", vatName);
                mobileObj.put("base64string", vatStr);
            }
            else if (imageSelected == 7) {
                mobileObj.put("FileName", crName);
                mobileObj.put("base64string", crStr);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private class uploadImageApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUploadImageJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(RejectedProfileActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<String> call = apiService.uploadImage(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()){

                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RejectedProfileActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }
}