package com.cs.samkraworkshop.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkraworkshop.Adapter.CreateInvoiceAdapter;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Fragment.DashboardFragment;
import com.cs.samkraworkshop.Models.DashboardResponse;
import com.cs.samkraworkshop.Models.InboxResponse;
import com.cs.samkraworkshop.Models.InvoiceResponse;
import com.cs.samkraworkshop.Models.NewRequestsResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.CustomListView;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.cs.samkraworkshop.Utils.NumberTextWatcher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
//
//import cc.cloudist.acplibrary.ACProgressConstant;
//import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateInvoiceActivity extends Activity {

    public static TextView finalPrice;
    private TextView requestCode;
    public static int childCount = 0;
    private int selectedPos = -1;
    AlertDialog loaderDialog;
    private ListView itemsList;
    private CreateInvoiceAdapter mAdapter;
    public static ArrayList<String> itemName = new ArrayList<>();
    public static ArrayList<String> itemQty = new ArrayList<>();
    public static ArrayList<String> itemUnit = new ArrayList<>();
    public static ArrayList<String> itemUnitAr = new ArrayList<>();
    public static ArrayList<String> itemPrices = new ArrayList<>();
    private EditText etItemName, etItemPrice, etItemQty, etDiscount, etUnits;
    private TextView itemTite, priceTitle;
    private Button addItem, create;
    ArrayList<String> unitsResponse = new ArrayList<>();
    private Spinner unitsSpinner;
    private ArrayAdapter<String> cityAdapter;
    RelativeLayout itemsLayout;
    LinearLayout invoiceLayout, discountLayout;
    boolean isInvoice = false;
    float totalamt = 0, vatamt = 0, subamt = 0, discamt = 0, netamt = 0;
    TextView tvTotal, tvVat, tvSub, tvDisc, tvNet, date, itemTotal;
    ImageView deleteItem;
    AlertDialog customDialog;

    int position;
    ArrayList<InboxResponse.Data> data = new ArrayList<>();
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences invoicePrefs;
    SharedPreferences.Editor invoicePrefsEditor;
    String userId;
    ImageView backBtn;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_create_invoice);
        } else {
            setContentView(R.layout.activity_create_invoice_ar);
        }

        position = getIntent().getIntExtra("pos", 0);
        data = (ArrayList<InboxResponse.Data>) getIntent().getSerializableExtra("data");

        childCount = 0;
        itemName.clear();
        itemQty.clear();
        itemPrices.clear();
        itemUnit.clear();
//        itemUnit.add("");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        invoicePrefs = getSharedPreferences("INVOICE_PREFS", Context.MODE_PRIVATE);
        invoicePrefsEditor = invoicePrefs.edit();

        date = (TextView) findViewById(R.id.date);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        itemsList = (ListView) findViewById(R.id.items_list);
        requestCode = (TextView) findViewById(R.id.request_code);
        finalPrice = (TextView) findViewById(R.id.final_price);
        addItem = (Button) findViewById(R.id.add);
        itemTite = (TextView) findViewById(R.id.item_title);
        priceTitle = (TextView) findViewById(R.id.total_price_title);
        create = (Button) findViewById(R.id.create);

        etDiscount = (EditText) findViewById(R.id.et_discount);
        etItemName = (EditText) findViewById(R.id.et_item);
        etItemPrice = (EditText) findViewById(R.id.et_price);
        etItemQty = (EditText) findViewById(R.id.et_qty);
        etUnits = (EditText) findViewById(R.id.et_units);
        unitsSpinner = (Spinner) findViewById(R.id.units_spinner);

        tvTotal = (TextView) findViewById(R.id.totalamt);
        tvVat = (TextView) findViewById(R.id.vat);
        tvSub = (TextView) findViewById(R.id.subtotal);
        tvDisc = (TextView) findViewById(R.id.discountamt);
        tvNet = (TextView) findViewById(R.id.netamt);
        itemTotal = (TextView) findViewById(R.id.item_total);
        deleteItem = (ImageView) findViewById(R.id.delete);

        itemsLayout = (RelativeLayout) findViewById(R.id.items_layout);
        invoiceLayout = (LinearLayout) findViewById(R.id.invoice_layout);
        discountLayout = (LinearLayout) findViewById(R.id.discount_layout);

        setSpinner();
        if (language.equalsIgnoreCase("En")) {
            itemTite.setText("#Item " + (childCount + 1));
        } else {
            itemTite.setText("#مجموع " + (childCount + 1));
        }

        etItemPrice.addTextChangedListener(new NumberTextWatcher(etItemPrice, "##,##,###.##"));

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInvoice) {
                    isInvoice = false;
                    itemsLayout.setVisibility(View.VISIBLE);
                    invoiceLayout.setVisibility(View.GONE);
                    if (language.equalsIgnoreCase("En")) {
                        priceTitle.setText("Total Price");
                        create.setText("Next");
                    } else {
                        priceTitle.setText("المبلغ الكلي");
                        create.setText("التالى");
                    }

                    setPrice();
                } else {
                    if (totalamt > 0) {
                        showExitDialog();
                    } else {
                        finish();
                    }
                }
            }
        });

        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(CreateInvoiceActivity.this);
                etItemName.requestFocus();
                addLayout();
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (totalamt > 0) {
                    if (!isInvoice) {
                        isInvoice = true;
                        itemsLayout.setVisibility(View.GONE);
                        invoiceLayout.setVisibility(View.VISIBLE);
                        if (language.equalsIgnoreCase("En")) {
                            priceTitle.setText("Final Price");
                            create.setText("Create Invoice");
                        } else {
                            priceTitle.setText("المبلغ الكلي");
                            create.setText("انشاء فاتورة");
                        }

                        setFinalPrice();
//                        if (discamt == 0) {
//                            discountLayout.setVisibility(View.GONE);
//                        }
                    } else {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(CreateInvoiceActivity.this);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new generateInvoiceApi().execute();
//                            String inputStr = prepareJson();
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showOneButtonAlertDialog("Please enter invoice details", getResources().getString(R.string.app_name),
                                getResources().getString(R.string.ok), CreateInvoiceActivity.this);
                    } else {
                        Constants.showOneButtonAlertDialog("الرجاء ادخال تفاصيل الفاتورة", getResources().getString(R.string.app_name_ar),
                                getResources().getString(R.string.ok_ar), CreateInvoiceActivity.this);
                    }
                }
            }
        });

        etDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = etDiscount.getText().toString();
                if (text.length() == 0) {
                    discamt = 0;
//                    discountLayout.setVisibility(View.GONE);
                } else {
//                    discountLayout.setVisibility(View.VISIBLE);
                    discamt = Float.parseFloat(text);
                    if (discamt > totalamt) {
                        discamt = totalamt;
                        etDiscount.setText("" + Constants.convertToArabic(String.valueOf(totalamt)));
                        etDiscount.setSelection(etDiscount.getText().length());
                    }
                }
                setFinalPrice();
            }
        });

        etItemQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setItemTotal();
            }
        });

        etItemPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setItemTotal();
            }
        });

        retrieveData();
        requestCode.setText("#" + data.get(position).getRequestCode());
        String str = data.get(position).getCreatedOn();
        String[] array = str.split("T");
        String[] dt = array[0].split("-");

        date.setText(Constants.convertToArabic(dt[2] + "-" + dt[1] + "-" + dt[0]));

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(
                mMessageReceiver, new IntentFilter("edit_invoice"));

        if (language.equalsIgnoreCase("Ar")) {
            setTypeface();
        }
    }

    private void setTypeface() {
        ((TextView) findViewById(R.id.units_title)).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
        ((EditText) findViewById(R.id.et_units)).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.qty_title)).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.price_title)).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.item_title)).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.total_title)).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.total_price_title)).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.item_total)).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.create)).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.request_code)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.total_price_title)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.add)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.bid_count)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.applydiscounttext)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.applydiscounttext)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.st1)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.st2)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.st3)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.st6)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
        ((TextView) findViewById(R.id.st5)).setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
    }

    private void setItemTotal() {
        int qty = 0;
        float price = 0;
        String str = etItemQty.getText().toString();
        String str1 = etItemPrice.getText().toString().replaceAll(",", "");
        if (str.length() == 0) {
            qty = 0;
        } else {
            qty = Integer.parseInt(str);
        }
        if (str1.length() == 0) {
            price = 0;
        } else {
            price = Float.parseFloat(str1);
        }

        float netPrice = qty * price;

        if (netPrice == 0) {
            if (language.equalsIgnoreCase("En")) {
                itemTotal.setText("0.00 SR");
            } else {
                itemTotal.setText("0.00 ريال");
            }
        } else {
            if (language.equalsIgnoreCase("En")) {
                itemTotal.setText(Constants.convertToArabic(Constants.priceFormat1.format(netPrice)) + " SR");
            } else {
                itemTotal.setText(Constants.convertToArabic(Constants.priceFormat1.format(netPrice)) + " ريال");
            }
        }
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getBundleExtra("position");
            selectedPos = b.getInt("position");
            etItemName.setText(itemName.get(selectedPos));
            etItemQty.setText(itemQty.get(selectedPos));
            etItemPrice.setText(Constants.convertToArabic(itemPrices.get(selectedPos)));
            if (language.equalsIgnoreCase("En")) {
                itemTite.setText("#Item " + (selectedPos + 1));
            } else {
                itemTite.setText("#مجموع " + (selectedPos + 1));
            }

            for (int i = 0; i < unitsResponse.size(); i++) {
                if (unitsResponse.get(i).equals(itemUnit.get(selectedPos))) {
                    unitsSpinner.setSelection(i);
                    break;
                }
            }

            if (isInvoice) {
                isInvoice = false;
                itemsLayout.setVisibility(View.VISIBLE);
                invoiceLayout.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    priceTitle.setText("Total Price");
                    create.setText("Next");
                } else {
                    priceTitle.setText("المبلغ الكلي");
                    create.setText("التالى");
                }

                setPrice();
            }
        }
    };

    private void addLayout() {
        String name = etItemName.getText().toString();
        String qty = etItemQty.getText().toString();
        String price = etItemPrice.getText().toString().replaceAll(",", "");

        float finalPrice = data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getFinalPrice();
        float previousTotalPrice = 0 , presentItemPrice;
        presentItemPrice = Float.parseFloat(price);
        for (int i = 0; i < itemPrices.size(); i++) {
            previousTotalPrice = previousTotalPrice + Float.parseFloat(itemPrices.get(i));
        }

        int pos = 0;
        if (selectedPos == -1) {
            pos = childCount;
        } else {
            pos = selectedPos;
        }

        if (name.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                etItemName.setError("Please enter item name");
            } else {
                etItemName.setError("ارجوا ادخالبندالاسم");
            }
        } else if (qty.length() == 0) {
            etItemQty.setError("");
        } else if (price.length() == 0) {
            etItemPrice.setError("");
        }
        else if ((previousTotalPrice + presentItemPrice) > finalPrice) {
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialog("Invoice amount should be less than "+Constants.priceFormat.format(finalPrice)+" SAR", getResources().getString(R.string.app_name),
                        getResources().getString(R.string.ok), CreateInvoiceActivity.this);
            } else {
                Constants.showOneButtonAlertDialog("Invoice amount should be less than "+Constants.priceFormat.format(finalPrice)+" SAR", getResources().getString(R.string.samkra_ar),
                        getResources().getString(R.string.ok_ar), CreateInvoiceActivity.this);
            }
        }else if (itemUnit.get(pos).equalsIgnoreCase("-- units --") ||
                itemUnit.get(pos).equalsIgnoreCase("-- الوحدة --")) {
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialog("Please select measuring unit", getResources().getString(R.string.app_name),
                        getResources().getString(R.string.ok), CreateInvoiceActivity.this);
            } else {
                Constants.showOneButtonAlertDialog("الرجاء اختيار وحدة القياس", getResources().getString(R.string.samkra_ar),
                        getResources().getString(R.string.ok_ar), CreateInvoiceActivity.this);
            }
        } else {
            if (selectedPos == -1) {
                childCount = childCount + 1;
                if (language.equalsIgnoreCase("En")) {
                    itemTite.setText("#Item " + (childCount + 1));
                } else {
                    itemTite.setText("#مجموع " + (childCount + 1));
                }

                itemName.add(name);
                itemQty.add(qty);
                itemPrices.add(price);
                itemUnit.add("");
            } else {
                itemName.set(selectedPos, name);
                itemQty.set(selectedPos, qty);
                itemPrices.set(selectedPos, price);
                selectedPos = -1;
            }
            setPrice();

            etItemName.setText("");
            etItemQty.setText("");
            etItemPrice.setText("");
            if (language.equalsIgnoreCase("En")) {
                itemTotal.setText("0.00 SR");
            } else {
                itemTotal.setText("0.00 ريال");
            }

            deleteItem.setVisibility(View.INVISIBLE);
            unitsSpinner.setSelection(0);

            mAdapter.notifyDataSetChanged();
            itemsList.smoothScrollToPosition(selectedPos);
        }
    }

    private void setSpinner() {
        if (language.equalsIgnoreCase("En")) {
            unitsResponse.add("-- units --");
        } else {
            unitsResponse.add("-- الوحدة --");
        }

        if (DashboardFragment.data.size() == 0) {
            String networkStatus = NetworkUtil.getConnectivityStatusString(CreateInvoiceActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new dashboardApi().execute();
            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            for (int i = 0; i < DashboardFragment.data.get(0).getMeasuringUnits().size(); i++) {
                if (language.equalsIgnoreCase("En")) {
                    unitsResponse.add(DashboardFragment.data.get(0).getMeasuringUnits().get(i).getNameEn());
                } else {
                    unitsResponse.add(DashboardFragment.data.get(0).getMeasuringUnits().get(i).getNameAr());
                }
            }
        }

        cityAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, unitsResponse) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
                if (language.equalsIgnoreCase("Ar")) {
                    ((TextView) v).setTypeface(Constants.getarTypeFace(CreateInvoiceActivity.this));
                }
                return v;
            }
        };
        unitsSpinner.setAdapter(cityAdapter);

        unitsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (selectedPos == -1) {
                    itemUnit.set(childCount, unitsResponse.get(i));
                } else {
                    itemUnit.set(selectedPos, unitsResponse.get(i));
                }
                etUnits.setText(unitsResponse.get(i));
                hideKeyboard(CreateInvoiceActivity.this);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private class dashboardApi extends AsyncTask<String, String, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(CreateInvoiceActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<DashboardResponse> call = apiService.GetDashBoard(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DashboardResponse>() {
                @Override
                public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                    if (response.isSuccessful()) {
                        DashboardResponse dashboardResponse = response.body();
                        try {
                            if (dashboardResponse.getStatus()) {
                                DashboardFragment.data.addAll(dashboardResponse.getData());
                                setSpinner();
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = dashboardResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), CreateInvoiceActivity.this);
                                } else {
                                    String failureResponse = dashboardResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), CreateInvoiceActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<DashboardResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CreateInvoiceActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("WorkshopId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    private void setPrice() {
        totalamt = 0;
        for (int i = 0; i < itemName.size(); i++) {
            float price = 0;
            int qty = 0;
            price = Float.parseFloat(itemPrices.get(i));
            qty = Integer.parseInt(itemQty.get(i));
            totalamt = totalamt + (price * qty);
        }

        finalPrice.setText(Constants.convertToArabic(Constants.priceFormat.format(totalamt)));
    }

    private void setFinalPrice() {
        subamt = totalamt - discamt;
        vatamt = subamt * 0.15f; // Vat = 15%

        subamt = Float.parseFloat(String.format("%.2f", subamt));
        vatamt = Float.parseFloat(String.format("%.2f", vatamt));
        netamt = subamt + vatamt;

        tvTotal.setText(Constants.convertToArabic(Constants.priceFormat.format(totalamt)));
        tvVat.setText(Constants.convertToArabic(Constants.priceFormat.format(vatamt)));
        tvSub.setText(Constants.convertToArabic(Constants.priceFormat.format(subamt)));
        tvDisc.setText(Constants.convertToArabic(Constants.priceFormat.format(discamt)));
        tvNet.setText(Constants.convertToArabic(Constants.priceFormat.format(netamt)));
        finalPrice.setText(Constants.convertToArabic(Constants.priceFormat.format(netamt)));
    }

    private class generateInvoiceApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(CreateInvoiceActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<InvoiceResponse> call = apiService.CreateInvoice(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InvoiceResponse>() {
                @Override
                public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {
                    if (response.isSuccessful()) {
                        InvoiceResponse requestsResponse = response.body();
                        try {
                            if (requestsResponse.getStatus()) {
                                finish();
                            } else {
                                // status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = requestsResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), CreateInvoiceActivity.this);
                                } else {
                                    String failureResponse = requestsResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), CreateInvoiceActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessageAr();
                            if (language.equalsIgnoreCase("En")) {
                                Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), CreateInvoiceActivity.this);
                            } else {
                                Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.error_ar),
                                        getResources().getString(R.string.ok_ar), CreateInvoiceActivity.this);
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), CreateInvoiceActivity.this);
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.samkra_ar),
                                    getResources().getString(R.string.ok_ar), CreateInvoiceActivity.this);
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.str_connection_error), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), CreateInvoiceActivity.this);
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.str_connection_error_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), CreateInvoiceActivity.this);
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), CreateInvoiceActivity.this);
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.samkra_ar),
                                    getResources().getString(R.string.ok_ar), CreateInvoiceActivity.this);
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("WorkshopId", userId);
            parentObj.put("BidId", data.get(position).getCM().get(0).getMDL().get(0).getRB().get(0).getBidId());
            parentObj.put("RequestId", data.get(position).getRequestId());
            parentObj.put("UserId", data.get(position).getUserId());
            parentObj.put("TotalAmount", totalamt);
            parentObj.put("Discount", discamt);
            parentObj.put("SubTotal", subamt);
            parentObj.put("VatCharges", vatamt);
            parentObj.put("NetAmount", netamt);

            JSONArray itemsArray = new JSONArray();
            for (int i = 0; i < itemName.size(); i++) {
                JSONObject itemObj = new JSONObject();
                itemObj.put("Item", itemName.get(i));
                itemObj.put("Qty", itemQty.get(i));
                String unit = "";
                String unitAr = "";
                Log.d("TAG", "unit: " + itemUnit.get(i));
                for (int j = 0; j < DashboardFragment.data.get(0).getMeasuringUnits().size(); j++) {
                    if (language.equalsIgnoreCase("En")) {
                        if (DashboardFragment.data.get(0).getMeasuringUnits().get(j).getNameEn().equals(itemUnit.get(i))) {
                            unit = DashboardFragment.data.get(0).getMeasuringUnits().get(j).getNameEn();
                            unitAr = DashboardFragment.data.get(0).getMeasuringUnits().get(j).getNameAr();
                        }
//                            break;
                    } else {
                        if (DashboardFragment.data.get(0).getMeasuringUnits().get(j).getNameAr().equals(itemUnit.get(i))) {
                            unit = DashboardFragment.data.get(0).getMeasuringUnits().get(j).getNameEn();
                            unitAr = DashboardFragment.data.get(0).getMeasuringUnits().get(j).getNameAr();
                        }
//                            break;
                    }
                }
                itemObj.put("MeasuringUnits", unit);
                itemObj.put("MeasuringUnitsAr", unitAr);
                itemObj.put("UnitPrice", itemPrices.get(i));
                itemObj.put("ItemPrice", Float.parseFloat(itemPrices.get(i))
                        * Integer.parseInt(itemQty.get(i)));
                itemsArray.put(itemObj);
            }
            parentObj.put("Items", itemsArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareResetPasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }

    public void showExitDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CreateInvoiceActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        if (language.equalsIgnoreCase("En")) {
            title.setText("Samkra");
            yes.setText("Save & Exit");
            no.setText("Exit");
            desc.setText("Do you want to save this invoice details?");
        } else {
            title.setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
            yes.setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
            no.setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
            desc.setTypeface(Constants.getarbooldTypeFace(CreateInvoiceActivity.this));
            title.setText(getResources().getString(R.string.samkra_ar));
            yes.setText("حفظ و خروج");
            no.setText("خروج");
            desc.setText("هل تريد حفظ تفاصيل هذه الفاتورة");
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveInvoiceDetails();
                customDialog.dismiss();
                finish();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
                finish();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public void onBackPressed() {
        if (isInvoice) {
            isInvoice = false;
            itemsLayout.setVisibility(View.VISIBLE);
            invoiceLayout.setVisibility(View.GONE);
            if (language.equalsIgnoreCase("En")) {
                priceTitle.setText("Total Price");
                create.setText("Next");
            } else {
                priceTitle.setText("المبلغ الكلي");
                create.setText("التالى");
            }

            setPrice();
        } else {
            if (totalamt > 0) {
                showExitDialog();
            } else {
                finish();
            }
        }
    }

    private void saveInvoiceDetails() {
        String invoice = "";
        invoice = "" + (itemName.size() * 4);
        for (int i = 0; i < itemName.size(); i++) {
            invoice = invoice + "," + itemName.get(i);
            invoice = invoice + "," + itemQty.get(i);
            invoice = invoice + "," + itemUnit.get(i);
            invoice = invoice + "," + itemPrices.get(i);
        }

        invoicePrefsEditor.putString("" + data.get(position).getRequestId(), invoice);
        invoicePrefsEditor.commit();
    }

    private void retrieveData() {
        String invoice = invoicePrefs.getString("" + data.get(position).getRequestId(), "");
        if (!invoice.equals("")) {
            String[] details = invoice.split(",");
            int size = Integer.parseInt(details[0]);
            int nameNum = 1;
            int qtyNum = 2;
            int unitsNum = 3;
            int priceNum = 4;

            for (int i = 1; i <= size; i++) {
                if (i == nameNum) {
                    childCount = childCount + 1;
                    if (language.equalsIgnoreCase("En")) {
                        itemTite.setText("#Item " + (childCount + 1));
                    } else {
                        itemTite.setText("#مجموع " + (childCount + 1));
                    }

                    itemName.add(details[i]);
                    nameNum = nameNum + 4;
                }
                if (i == qtyNum) {
                    itemQty.add(details[i]);
                    qtyNum = qtyNum + 4;
                }
                if (i == unitsNum) {
                    itemUnit.add(details[i]);
                    unitsNum = unitsNum + 4;
                }
                if (i == priceNum) {
                    itemPrices.add(details[i]);
                    priceNum = priceNum + 4;
                }
            }
        }
        itemUnit.add("");

        mAdapter = new CreateInvoiceAdapter(CreateInvoiceActivity.this, childCount, "En");
        itemsList.setAdapter(mAdapter);
        setPrice();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showloaderAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CreateInvoiceActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
