package com.cs.samkraworkshop.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.CountryFilteredData;
import com.cs.samkraworkshop.Models.GetListResponse;
import com.cs.samkraworkshop.Models.UpdateDetailsResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etEmail, etDesc, etDescAr, etLandline;
    private EditText etStreet, etStreetAr, etZipcode, etDistrcit, etCity, etCountry;
    String strStreet, strStreetAr, strZipcode;
    String strEmail, strLandline, strDesc, strDescAr, cars = "";
    private TextView geoLocationEt, brandsSelected;
    private ImageView wImage1, wImage2, wImage3, wImage4, wLogoImage, wVatImage, wCRImage;
    private Spinner districtSpinner, citySpiner, countrySpinner;
    private int citySeleced;
    private String districtSelected;
    private boolean isLogoUploaded = false, isImagesUploaded = false, isCRUploaded = false, isVatUploaded = false;
    AlertDialog loaderDialog;

    android.app.AlertDialog brandsDialog;
    private String TAG = "TAG";
    private static final int PLACE_PICKER_REQUEST = 1;

    private LinearLayout detailsLayout, locationLayout, documentsLayout;
    private Button nextBtn, nextBtnLocation, uploadButton;
    private double latitude, longitude;
    RelativeLayout geoLocationLayout;
    private RelativeLayout carBrandsLayout;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;

    int imageSelected = 0;
    Bitmap thumbnail1 = null, thumbnail2 = null, thumbnail3 = null, thumbnail4 = null,
            thumbnail5 = null, thumbnail6 = null, thumbnail7 = null;

    private String imagePath1 = null;
    private String imagePath2 = null;
    private String imagePath3 = null;
    private String imagePath4 = null;
    private String imagePath5 = null;
    private String imagePath6 = null;
    private String imagePath7 = null;

    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int PICK_IMAGE_FROM_CAMERA = 3;
    private static final int CAMERA_REQUEST = 4;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };

    public static List<GetListResponse.CarMakerList> CarMakerList = new ArrayList<>();
    public static List<GetListResponse.DistrictList> DistrictList;
    public static List<GetListResponse.CityList> CityList;
    public static List<GetListResponse.CountryList> CountryList;
    private ArrayList<CountryFilteredData> countryFilteredData = new ArrayList<>();

    private int cityPos = 0, districtPos = 0, countryPos = 0;
    private ArrayAdapter<CountryFilteredData.Districts> districtAdapter;
    private ArrayAdapter<CountryFilteredData.Cities> cityAdapter;
    private ArrayAdapter<CountryFilteredData> countryAdapter;

    ScrollView scrollView;
    String logoName, crName, vatName, imagesName = "", imageNameForSingleUpload;
    ImageView logoutBtn;
    AlertDialog customDialog;
    String imageResponse;
    private DefaultHttpClient mHttpClient11;

    LinearLayout detailsTitleLayout, locationTitleLayout, docsTitleLayout;
    ImageView detailsSuccess, locationSuccess, docsSuccess;
    TextView detailsText, locationText, docsText;

    private byte[] logoBytes;
    private byte[] image1Bytes;
    private byte[] image2Bytes;
    private byte[] image3Bytes;
    private byte[] image4Bytes;
    private byte[] crBytes;
    private byte[] vatBytes;
    private String image1Str, image2Str, image3Str, image4Str, logoStr, vatStr, crStr;
    SharedPreferences languagePrefs;
    String language;
    Uri imageUri;
    boolean isGalleryImage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_update);
        }
        else {
            setContentView(R.layout.activity_update_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "");
        userPrefsEditor = userPrefs.edit();

        detailsLayout = (LinearLayout) findViewById(R.id.details_layout);
        geoLocationLayout = (RelativeLayout) findViewById(R.id.geo_location_layout);

        geoLocationEt = (TextView) findViewById(R.id.et_geolocation);
        brandsSelected = (TextView) findViewById(R.id.selected_cars);

        etEmail = (EditText) findViewById(R.id.et_email);
        etLandline = (EditText) findViewById(R.id.et_landline);
        etDesc = (EditText) findViewById(R.id.et_description);
        etDescAr = (EditText) findViewById(R.id.et_description_ar);

        etStreet = (EditText) findViewById(R.id.et_street_name);
        etStreetAr = (EditText) findViewById(R.id.et_street_name_ar);
        etCity = (EditText) findViewById(R.id.et_city);
        etCountry = (EditText) findViewById(R.id.et_country);
        etDistrcit = (EditText) findViewById(R.id.et_district);
        etZipcode = (EditText) findViewById(R.id.et_zipcode);

        wImage1 = (ImageView) findViewById(R.id.image1);
        wImage2 = (ImageView) findViewById(R.id.image2);
        wImage3 = (ImageView) findViewById(R.id.image3);
        wImage4 = (ImageView) findViewById(R.id.image4);
        wLogoImage = (ImageView) findViewById(R.id.logo);
        wVatImage = (ImageView) findViewById(R.id.vat_image);
        wCRImage = (ImageView) findViewById(R.id.cr_image);
        logoutBtn = (ImageView) findViewById(R.id.logout_btn);
        carBrandsLayout = (RelativeLayout) findViewById(R.id.car_brands_layout);

        detailsLayout = (LinearLayout) findViewById(R.id.details_layout);
        locationLayout = (LinearLayout) findViewById(R.id.location_layout);
        documentsLayout = (LinearLayout) findViewById(R.id.images_layout);

        detailsTitleLayout = (LinearLayout) findViewById(R.id.layout1);
        locationTitleLayout = (LinearLayout) findViewById(R.id.layout2);
        docsTitleLayout = (LinearLayout) findViewById(R.id.layout3);

        detailsText = (TextView) findViewById(R.id.details_text);
        locationText = (TextView) findViewById(R.id.location_text);
        docsText = (TextView) findViewById(R.id.docs_text);

        detailsSuccess = (ImageView) findViewById(R.id.details_success);
        locationSuccess = (ImageView) findViewById(R.id.location_success);
        docsSuccess = (ImageView) findViewById(R.id.docs_success);

        nextBtn = (Button) findViewById(R.id.next_btn);
        nextBtnLocation = (Button) findViewById(R.id.next_btn1);
        uploadButton = (Button) findViewById(R.id.upload_btn);

        districtSpinner = (Spinner) findViewById(R.id.district_spinner);
        citySpiner = (Spinner) findViewById(R.id.city_spinner);
        countrySpinner = (Spinner) findViewById(R.id.country_spinner);
        scrollView = (ScrollView) findViewById(R.id.scrollview);

        if(userPrefs.getString("stage", "1").equals("2")){
            detailsLayout.setVisibility(View.GONE);
            locationLayout.setVisibility(View.VISIBLE);

            detailsSuccess.setVisibility(View.VISIBLE);
            detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
            detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

            locationText.setTextColor(getResources().getColor(R.color.white));
            locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));
        }
        else if(userPrefs.getString("stage", "1").equals("3")){
            detailsLayout.setVisibility(View.GONE);
            locationLayout.setVisibility(View.GONE);
            documentsLayout.setVisibility(View.VISIBLE);

            detailsSuccess.setVisibility(View.VISIBLE);
            detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
            detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

            locationSuccess.setVisibility(View.VISIBLE);
            locationText.setTextColor(getResources().getColor(R.color.colorPrimary));
            locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

            docsText.setTextColor(getResources().getColor(R.color.white));
            docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));
        }

        wImage1.setOnClickListener(this);
        wImage2.setOnClickListener(this);
        wImage3.setOnClickListener(this);
        wImage4.setOnClickListener(this);
        wLogoImage.setOnClickListener(this);
        wVatImage.setOnClickListener(this);
        wCRImage.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        uploadButton.setOnClickListener(this);
        nextBtnLocation.setOnClickListener(this);
        carBrandsLayout.setOnClickListener(this);
        geoLocationLayout.setOnClickListener(this);
        logoutBtn.setOnClickListener(this);

        String networkStatus = NetworkUtil.getConnectivityStatusString(UpdateActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new getDetailsApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.next_btn:
                if(detailsValidations()){
                    String networkStatus = NetworkUtil.getConnectivityStatusString(UpdateActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new updateDetailsApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;

            case R.id.next_btn1:
                if(locationValidations()){
                    String networkStatus = NetworkUtil.getConnectivityStatusString(UpdateActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new updateDetailsApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;

            case R.id.upload_btn:
                if(imagesValidations()){
                    String networkStatus = NetworkUtil.getConnectivityStatusString(UpdateActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new updateDetailsApi().execute();
//                        String inputStr = prepareUpdateJson();
//                        int maxLogSize = 1000;
//                        for(int i = 0; i <= inputStr.length() / maxLogSize; i++) {
//                            int start = i * maxLogSize;
//                            int end = (i+1) * maxLogSize;
//                            end = end > inputStr.length() ? inputStr.length() : end;
//                            Log.v(TAG, inputStr.substring(start, end));
//                        }
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;

            case R.id.logout_btn:
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                startActivity(new Intent(UpdateActivity.this, SignInActivity.class));
                finish();
                break;
            case R.id.geo_location_layout:
                openPlacePicker();
                break;

            case R.id.image1:
                imageSelected = 1;
                showtwoButtonsAlertDialog();
                break;

            case R.id.image2:
                imageSelected = 2;
                showtwoButtonsAlertDialog();
                break;

            case R.id.image3:
                imageSelected = 3;
                showtwoButtonsAlertDialog();
                break;

            case R.id.image4:
                imageSelected = 4;
                showtwoButtonsAlertDialog();
                break;

            case R.id.logo:
                imageSelected = 5;
                showtwoButtonsAlertDialog();
                break;

            case R.id.vat_image:
                imageSelected = 6;
                showtwoButtonsAlertDialog();
                break;

            case R.id.cr_image:
                imageSelected = 7;
                showtwoButtonsAlertDialog();
                break;

            case R.id.car_brands_layout:
                showBrandsDialog();
                break;
        }
    }

    private Boolean detailsValidations(){
        strEmail = etEmail.getText().toString().trim();
        strLandline = etLandline.getText().toString().trim();
        strDesc = etDesc.getText().toString();
        strDescAr = etDescAr.getText().toString();
        strLandline = strLandline.replace("+966 ","");

        if(strEmail.length()> 0 && !Constants.isValidEmail(strEmail)){
            if (language.equalsIgnoreCase("En")){
                etEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
            }
            else {
                etEmail.setError(getResources().getString(R.string.signup_msg_invalid_email_ar));
            }

            return false;
        }
        else if(strLandline.length() == 0){
            if (language.equalsIgnoreCase("En")){
                etLandline.setError(getResources().getString(R.string.signup_msg_enter_landline));
            }
            else {
                etLandline.setError(getResources().getString(R.string.signup_msg_enter_landline_ar));
            }

            return false;
        }
        else if(strLandline.length() != 9){
            if (language.equalsIgnoreCase("En")){
                etLandline.setError(getResources().getString(R.string.signup_msg_enter_landline));
            }
            else {
                etLandline.setError(getResources().getString(R.string.signup_msg_enter_landline_ar));
            }

            return false;
        }
        else if(strDesc.length() == 0){
            if (language.equalsIgnoreCase("En")){
                etDesc.setError(getResources().getString(R.string.signup_msg_enter_about));
            }
            else {
                etDesc.setError(getResources().getString(R.string.signup_msg_enter_about_ar));
            }

            return false;
        }
        else if(strDescAr.length() == 0){
            if (language.equalsIgnoreCase("En")){
                etDescAr.setError(getResources().getString(R.string.signup_msg_enter_about));
            }
            else {
                etDescAr.setError(getResources().getString(R.string.signup_msg_enter_about_ar));
            }

            return false;
        }
        else if(cars.length() == 0){
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_cars),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), UpdateActivity.this);
            }{
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_cars_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), UpdateActivity.this);
            }

            return false;
        }
        return true;
    }

    private Boolean locationValidations(){
        strStreet = etStreet.getText().toString();
        strStreetAr = etStreetAr.getText().toString();
        strZipcode = etZipcode.getText().toString();

        if(cityPos == 0) {
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), UpdateActivity.this);
            }
            else   {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), UpdateActivity.this);
            }
            return false;
        }
        else if(strStreet.length() == 0){
            if (language.equalsIgnoreCase("En")){
                etStreet.setError(getResources().getString(R.string.signup_msg_enter_street));
            }
            else {
                etStreet.setError(getResources().getString(R.string.signup_msg_enter_street_ar));
            }

            return false;
        }
        else if(strStreetAr.length() == 0){
            if (language.equalsIgnoreCase("En")){
                etStreetAr.setError(getResources().getString(R.string.signup_msg_enter_street));
            }
            else {
                etStreetAr.setError(getResources().getString(R.string.signup_msg_enter_street_ar));
            }

            return false;
        }
        else if(strZipcode.length() == 0){
            if (language.equalsIgnoreCase("En")){
                etZipcode.setError(getResources().getString(R.string.signup_msg_enter_zipcode));
            }
            else {
                etZipcode.setError(getResources().getString(R.string.signup_msg_enter_zipcode_ar));
            }

            return false;
        }
        else if(latitude == 0 && longitude == 0){
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), UpdateActivity.this);
            }
          else   {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), UpdateActivity.this);
            }

            return false;
        }
        return true;
    }

    private Boolean imagesValidations(){
        if(!isLogoUploaded){
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_logo),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), UpdateActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_logo_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), UpdateActivity.this);
            }

            return false;
        }
        else if(!isImagesUploaded){
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_images),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), UpdateActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_images_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), UpdateActivity.this);
            }

            return false;
        }
        else if(!isCRUploaded){
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_cr),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), UpdateActivity.this);
            }
            else {
                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_cr_ar),
                            getResources().getString(R.string.error_ar),
                            getResources().getString(R.string.ok_ar), UpdateActivity.this);
            }



            return false;
        }
        else if(!isVatUploaded){
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_vat),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), UpdateActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_vat_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), UpdateActivity.this);
            }

            return false;
        }
        return true;
    }

    private void showBrandsDialog(){
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(UpdateActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        final LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.car_brands_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        final LinearLayout brands = (LinearLayout) dialogView.findViewById(R.id.brands_layout);

        if (language.equalsIgnoreCase("Ar")){
            no.setText(getResources().getString(R.string.no_ar));
            yes.setText(getResources().getString(R.string.ok_ar));
            title.setText(getResources().getString(R.string.car_you_fix_ar));
            no.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
            yes.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
            title.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
        }

        for (int i = 0; i < CarMakerList.size(); i++) {
            View v = inflater.inflate(R.layout.list_car_brands, null);
            CheckBox checkBox = v.findViewById(R.id.checkbox);
            if (language.equalsIgnoreCase("En")){
                checkBox.setText(CarMakerList.get(i).getNameEn());
            }
            else {
                checkBox.setText(CarMakerList.get(i).getNameAr());
            }
            if(CarMakerList.get(i).getChecked()){
                checkBox.setChecked(true);
            }

            final int finalI = i;
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(finalI == 0) {
                        if (b) {
                            for (int i = 0; i < CarMakerList.size(); i++) {
                                CarMakerList.get(i).setChecked(true);
                            }
                        } else {
                            for (int i = 0; i < CarMakerList.size(); i++) {
                                CarMakerList.get(i).setChecked(false);
                            }
                        }
                        brands.removeAllViews();
                        for (int i = 0; i < CarMakerList.size(); i++) {
                            View v = inflater.inflate(R.layout.list_car_brands, null);
                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                            if (language.equalsIgnoreCase("En")){
                                checkBox.setText(CarMakerList.get(i).getNameEn());
                            }
                            else {
                                checkBox.setText(CarMakerList.get(i).getNameAr());
                            }
                            if(CarMakerList.get(i).getChecked()){
                                checkBox.setChecked(true);
                            }

                            final int finalI = i;
                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if(finalI == 0) {
                                        if (b) {
                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                CarMakerList.get(i).setChecked(true);
                                            }
                                        } else {
                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                CarMakerList.get(i).setChecked(false);
                                            }
                                        }
                                        brands.removeAllViews();
                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                            if (language.equalsIgnoreCase("En")){
                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                            }
                                            else {
                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                            }
                                            if(CarMakerList.get(i).getChecked()){
                                                checkBox.setChecked(true);
                                            }

                                            final int finalI = i;
                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                @Override
                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                    if(finalI == 0) {
                                                        if (b) {
                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                CarMakerList.get(i).setChecked(true);
                                                            }
                                                        } else {
                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                CarMakerList.get(i).setChecked(false);
                                                            }
                                                        }
                                                        brands.removeAllViews();
                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                            if (language.equalsIgnoreCase("En")){
                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                            }
                                                            else {
                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                            }
                                                            if(CarMakerList.get(i).getChecked()){
                                                                checkBox.setChecked(true);
                                                            }

                                                            final int finalI = i;
                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                @Override
                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                    if(finalI == 0) {
                                                                        if (b) {
                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                CarMakerList.get(i).setChecked(true);
                                                                            }
                                                                        } else {
                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                CarMakerList.get(i).setChecked(false);
                                                                            }
                                                                        }
                                                                        brands.removeAllViews();
                                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                                            if (language.equalsIgnoreCase("En")){
                                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                                            }
                                                                            else {
                                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                                            }
                                                                            if(CarMakerList.get(i).getChecked()){
                                                                                checkBox.setChecked(true);
                                                                            }

                                                                            final int finalI = i;
                                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                                @Override
                                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                                    if(finalI == 0) {
                                                                                        if (b) {
                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                CarMakerList.get(i).setChecked(true);
                                                                                            }
                                                                                        } else {
                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                CarMakerList.get(i).setChecked(false);
                                                                                            }
                                                                                        }
                                                                                        brands.removeAllViews();
                                                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                                                            if (language.equalsIgnoreCase("En")){
                                                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                                                            }
                                                                                            else {
                                                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                                                            }
                                                                                            if(CarMakerList.get(i).getChecked()){
                                                                                                checkBox.setChecked(true);
                                                                                            }

                                                                                            final int finalI = i;
                                                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                                                @Override
                                                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                                                    if(finalI == 0) {
                                                                                                        if (b) {
                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                CarMakerList.get(i).setChecked(true);
                                                                                                            }
                                                                                                        } else {
                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                CarMakerList.get(i).setChecked(false);
                                                                                                            }
                                                                                                        }
                                                                                                        brands.removeAllViews();
                                                                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                                                                            if (language.equalsIgnoreCase("En")){
                                                                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                                                                            }
                                                                                                            else {
                                                                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                                                                            }
                                                                                                            if(CarMakerList.get(i).getChecked()){
                                                                                                                checkBox.setChecked(true);
                                                                                                            }

                                                                                                            final int finalI = i;
                                                                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                                                                @Override
                                                                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                                                                    if(finalI == 0) {
                                                                                                                        if (b) {
                                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                                CarMakerList.get(i).setChecked(true);
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                                CarMakerList.get(i).setChecked(false);
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                    else {
                                                                                                                        if (b) {
                                                                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                                                                        } else {
                                                                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            });
                                                                                                            brands.addView(v);
                                                                                                        }
                                                                                                    }
                                                                                                    else {
                                                                                                        if (b) {
                                                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                                                        } else {
                                                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                            brands.addView(v);
                                                                                        }
                                                                                    }
                                                                                    else {
                                                                                        if (b) {
                                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                                        } else {
                                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                            brands.addView(v);
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (b) {
                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                        } else {
                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                            brands.addView(v);
                                                        }
                                                    }
                                                    else {
                                                        if (b) {
                                                            CarMakerList.get(finalI).setChecked(true);
                                                        } else {
                                                            CarMakerList.get(finalI).setChecked(false);
                                                        }
                                                    }
                                                }
                                            });
                                            brands.addView(v);
                                        }
                                    }
                                    else {
                                        if (b) {
                                            CarMakerList.get(finalI).setChecked(true);
                                        } else {
                                            CarMakerList.get(finalI).setChecked(false);
                                        }
                                    }
                                }
                            });
                            brands.addView(v);
                        }
                    }
                    else {
                        if (b) {
                            CarMakerList.get(finalI).setChecked(true);
                        } else {
                            CarMakerList.get(finalI).setChecked(false);
                        }
                    }
                }
            });
            brands.addView(v);
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cars = "";
                boolean isAllSelected = false;
                for (int j = 0; j < CarMakerList.size(); j++) {
                    if (j != 0) {
                        if (CarMakerList.get(j).getChecked() && cars.length() == 0) {
                            if (language.equalsIgnoreCase("En")) {
                                cars = CarMakerList.get(j).getNameEn();
                            } else {
                                cars = CarMakerList.get(j).getNameAr();
                            }
                        } else if (CarMakerList.get(j).getChecked() && cars.length() > 0) {
                            if (language.equalsIgnoreCase("En")) {
                                cars = cars + ", " + CarMakerList.get(j).getNameEn();
                            } else {
                                cars = cars + ", " + CarMakerList.get(j).getNameAr();
                            }
                        }
                    }
                }
                brandsSelected.setText(cars);
                brandsDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                cars = "";
//                for (int j = 0; j < CarMakerList.size(); j++){
//                    if (j != 0) {
//                        if (CarMakerList.get(j).getChecked() && cars.length() == 0) {
//                            if (language.equalsIgnoreCase("En")) {
//                                cars = CarMakerList.get(j).getNameEn();
//                            } else {
//                                cars = CarMakerList.get(j).getNameAr();
//                            }
//                        } else if (CarMakerList.get(j).getChecked() && cars.length() > 0) {
//                            if (language.equalsIgnoreCase("En")) {
//                                cars = cars + ", " + CarMakerList.get(j).getNameEn();
//                            } else {
//                                cars = cars + ", " + CarMakerList.get(j).getNameAr();
//                            }
//                        }
//                    }
//                }
//                brandsSelected.setText(cars);
                brandsDialog.dismiss();
            }
        });

        brandsDialog = dialogBuilder.create();
        brandsDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = brandsDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void openPlacePicker() {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            Intent intent = intentBuilder.build(UpdateActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                final Place place = PlacePicker.getPlace(UpdateActivity.this, data);
                // TODO call location based filter
                LatLng latLong;
                latLong = place.getLatLng();
                latitude = latLong.latitude;
                longitude = latLong.longitude;
                geoLocationEt.setText(latitude+","+longitude);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {

        } else if (resultCode == RESULT_CANCELED) {

        }

        if(requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();

            try {
                if(imageSelected == 1) {
                    thumbnail1 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 2) {
                    thumbnail2 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 3) {
                    thumbnail3 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 4) {
                    thumbnail4 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 5) {
                    thumbnail5 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 6) {
                    thumbnail6 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
                else if(imageSelected == 7) {
                    thumbnail7 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            convertGalleryImages();
        }
        else if(requestCode == PICK_IMAGE_FROM_CAMERA && resultCode == RESULT_OK) {
            convertCameraImages();
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void convertCameraImages() {
        String path = getRealPathFromURI(imageUri);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inScaled = false;

        if(imageSelected == 1) {
            isImagesUploaded = true;

            thumbnail1 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail1, 800,
                    800, true);
            thumbnail1 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail1);
            wImage1.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail1, "image", 1);
        }
        else if(imageSelected == 2) {
            isImagesUploaded = true;
            thumbnail2 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail2, 800,
                    800, true);
            thumbnail2 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail2);
            wImage2.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail2, "image", 2);
        }
        else if(imageSelected == 3) {
            isImagesUploaded = true;
            thumbnail3 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail3, 800,
                    800, true);
            thumbnail3 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail3);
            wImage3.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail3, "image", 3);
        }
        else if(imageSelected == 4) {
            isImagesUploaded = true;
            thumbnail4 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail4, 800,
                    800, true);
            thumbnail4 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail4);
            wImage4.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail4, "image", 4);
        }
        else if(imageSelected == 5) {
            isLogoUploaded = true;
            thumbnail5 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail5, 800,
                    800, true);
            thumbnail5 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail5);
            wLogoImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail5, "logo", 5);
        }
        else if(imageSelected == 6) {
            isVatUploaded = true;
            thumbnail6 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail6, 800,
                    800, true);
            thumbnail6 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail6);
            wVatImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail6, "vat", 6);
        }
        else if(imageSelected == 7) {
            isCRUploaded = true;
            thumbnail7 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail7, 800,
                    800, true);
            thumbnail7 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail7);
            wCRImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail7, "cr", 7);
        }

        new uploadImageApi().execute();
    }

    private void convertGalleryImages() {
        if(imageSelected == 1) {
            isImagesUploaded = true;
            thumbnail1 = Bitmap.createScaledBitmap(thumbnail1, 700, 500,
                    false);
            thumbnail1 = codec(thumbnail1, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail1);
            wImage1.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail1, "image", 1);

//            mImage1Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 2) {
            isImagesUploaded = true;
            thumbnail2 = Bitmap.createScaledBitmap(thumbnail2, 700, 500,
                    false);
            thumbnail2 = codec(thumbnail2, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail2);
            wImage2.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail2, "image", 2);
//            mImage2Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 3) {
            isImagesUploaded = true;
            thumbnail3 = Bitmap.createScaledBitmap(thumbnail3, 700, 500,
                    false);
            thumbnail3 = codec(thumbnail3, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail3);
            wImage3.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail3, "image", 3);
//            mImage3Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 4) {
            isImagesUploaded = true;
            thumbnail4 = Bitmap.createScaledBitmap(thumbnail4, 700, 500,
                    false);
            thumbnail4 = codec(thumbnail4, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail4);
            wImage4.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail4, "image", 4);
//            mImage4Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 5) {
            isLogoUploaded = true;
            thumbnail5 = Bitmap.createScaledBitmap(thumbnail5, 700, 500,
                    false);
            thumbnail5 = codec(thumbnail5, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail5);
            wLogoImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail5, "logo", 5);
//            mImage5Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 6) {
            isVatUploaded = true;
            thumbnail6 = Bitmap.createScaledBitmap(thumbnail6, 700, 500,
                    false);
            thumbnail6 = codec(thumbnail6, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail6);
            wVatImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail6, "vat", 6);
//            mImage6Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 7) {
            isCRUploaded = true;
            thumbnail7 = Bitmap.createScaledBitmap(thumbnail7, 700, 500,
                    false);
            thumbnail7 = codec(thumbnail7, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail7);
            wCRImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail7, "cr", 7);
//            mImage7Delete.setVisibility(View.VISIBLE);
        }
        new uploadImageApi().execute();
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(UpdateActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
                else {
                    Toast.makeText(UpdateActivity.this, "Storage permission denied, Unable to select pictures", Toast.LENGTH_LONG).show();
                }
                break;

            case CAMERA_REQUEST:
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                break;
        }
    }

    private class getDetailsApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            Log.d(TAG, "inputStr: "+inputStr);
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(UpdateActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<GetListResponse> call = apiService.getList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<GetListResponse>() {
                @Override
                public void onResponse(Call<GetListResponse> call, Response<GetListResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()){
                        GetListResponse GetListResponse = response.body();
                        try {
                            if(GetListResponse.getStatus()){
                                CarMakerList.clear();
                                GetListResponse.CarMakerList car = new GetListResponse.CarMakerList();
//                                car.setChecked(false);
                                car.setNameEn("Select All");
                                car.setNameAr("اختر الكل");
                                car.setId(0);
                                CarMakerList.add(car);

                                CarMakerList.addAll(GetListResponse.getData().getCarMakerList());
                                DistrictList = GetListResponse.getData().getDistrictList();
                                CityList = GetListResponse.getData().getCityList();
                                CountryList = GetListResponse.getData().getCountryList();
                                setSpinner();
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = GetListResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), UpdateActivity.this);
                                }
                                else {
                                    String failureResponse = GetListResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), UpdateActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GetListResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private class updateDetailsApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUpdateJson();
            Log.d(TAG, "inputStr: "+inputStr);
           showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(UpdateActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UpdateDetailsResponse> call = apiService.updateDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateDetailsResponse>() {
                @Override
                public void onResponse(Call<UpdateDetailsResponse> call, Response<UpdateDetailsResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()){
                        UpdateDetailsResponse UpdateDetailsResponse = response.body();
                        try {
                            if(UpdateDetailsResponse.getStatus()){
                                if(userPrefs.getString("stage", "1").equals("1")) {
                                    userPrefsEditor.putString("stage", "2");
                                    userPrefsEditor.commit();
                                    detailsLayout.setVisibility(View.GONE);
                                    locationLayout.setVisibility(View.VISIBLE);

                                    detailsSuccess.setVisibility(View.VISIBLE);
                                    detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                                    locationText.setTextColor(getResources().getColor(R.color.white));
                                    locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));
                                }
                                else if(userPrefs.getString("stage", "1").equals("2")) {
                                    userPrefsEditor.putString("stage", "3");
                                    userPrefsEditor.commit();
                                    locationLayout.setVisibility(View.GONE);
                                    documentsLayout.setVisibility(View.VISIBLE);

                                    detailsSuccess.setVisibility(View.VISIBLE);
                                    detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                                    locationSuccess.setVisibility(View.VISIBLE);
                                    locationText.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                                    docsText.setTextColor(getResources().getColor(R.color.white));
                                    docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));
                                }
                                else if(userPrefs.getString("stage", "1").equals("3")) {
                                    docsSuccess.setVisibility(View.VISIBLE);
                                    docsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));
                                    userPrefsEditor.putString("stage", "4");
                                    userPrefsEditor.commit();
                                    if(language.equalsIgnoreCase("En")) {
                                        showDialog("Workshop registration completed. Your account is pending for verification",
                                                "Samkra", "Ok", UpdateActivity.this);
                                    }
                                    else {
                                        showDialog("تم تسجيل حساب الورشة. في انتظار التحقق من تفعيل الحساب",
                                                getResources().getString(R.string.samkra_ar), getResources().getString(R.string.ok_ar), UpdateActivity.this);
                                    }
                                }
                                scrollView.scrollTo(0,0);
                                if(loaderDialog != null){
                                    loaderDialog.dismiss();
                                }
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = UpdateDetailsResponse.getMessage();
                                    Constants.showOneButtonAlertDialogWithFinish(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), UpdateActivity.this);
                                }
                                else {
                                    String failureResponse = UpdateDetailsResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialogWithFinish(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), UpdateActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UpdateDetailsResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("Id", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private String prepareUpdateJson() {
        String ids = "";
        for (int j = 0; j < CarMakerList.size(); j++){
            if(CarMakerList.get(j).getChecked() && ids.length() == 0){
                ids = Integer.toString(CarMakerList.get(j).getId());
            }
            else if(CarMakerList.get(j).getChecked() && cars.length() > 0){
                ids = ids + ", "+ Integer.toString(CarMakerList.get(j).getId());
            }
        }
        JSONObject mobileObj = new JSONObject();
        JSONArray docsArray = new JSONArray();
        if(userPrefs.getString("stage", "1").equals("1")) {
            try {
                mobileObj.put("Id", userId);
                mobileObj.put("NameEn", userPrefs.getString("NameEn", ""));
                mobileObj.put("NameAr", userPrefs.getString("NameAr", ""));
                mobileObj.put("AboutEn", strDesc);
                mobileObj.put("AboutAr", strDescAr);
                mobileObj.put("EmailID", strEmail);
                mobileObj.put("LandLineNo", "966"+strLandline);
                mobileObj.put("SupportedCarMakerList", ids);
                mobileObj.put("profileUpdateStages", "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(userPrefs.getString("stage", "1").equals("2")) {
            try {
                mobileObj.put("Id", userId);
                mobileObj.put("StreetNameEn", strStreet);
                mobileObj.put("StreetNameAr", strStreetAr);
                mobileObj.put("CountryID", countryFilteredData.get(countryPos).getId());
                mobileObj.put("CityID", countryFilteredData.get(countryPos).getCities().get(cityPos).getId());
                mobileObj.put("DistrictID", countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(districtPos).getId());
                mobileObj.put("Zipcode", strZipcode);
                mobileObj.put("Latitude", latitude);
                mobileObj.put("Longitude", longitude);
                mobileObj.put("profileUpdateStages", "2");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(userPrefs.getString("stage", "1").equals("3")) {
            try {
                mobileObj.put("Id", userId);
                mobileObj.put("WorkshopLogo", logoName);
                mobileObj.put("diclogo", "");
                mobileObj.put("WorkshopImg", imagesName);
                if(thumbnail1 != null){
                    docsArray.put(image1Str);
                }
                if(thumbnail2 != null){
                    docsArray.put(image2Str);
                }
                if(thumbnail3 != null){
                    docsArray.put(image3Str);
                }
                if(thumbnail4 != null){
                    docsArray.put(image4Str);
                }
                mobileObj.put("dicimg",docsArray);
                mobileObj.put("dicimg","");
                mobileObj.put("CRD", crName);
                mobileObj.put("dicCr", "");
                mobileObj.put("VatCertificate", vatName);
                mobileObj.put("dicVat", "");
                mobileObj.put("profileUpdateStages", "3");
                userPrefsEditor.putString("NameAr", logoName);
                userPrefsEditor.commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mobileObj.toString();
    }

    private void setSpinner(){
        final ArrayList<CountryFilteredData.Cities> cityData = new ArrayList<>();
        final ArrayList<CountryFilteredData.Districts> districtData = new ArrayList<>();
        for (int i = 0; i < CountryList.size(); i++){
            CountryFilteredData brand = new CountryFilteredData();
            brand.setId(CountryList.get(i).getId());
            brand.setNameEn(CountryList.get(i).getNameEn());
            brand.setNameAr(CountryList.get(i).getNameAr());
            Log.d(TAG, "country: "+i+","+CountryList.get(i).getNameEn());
            ArrayList<Integer> dummyCityList = new ArrayList<>();
            ArrayList<CountryFilteredData.Cities> cityList = new ArrayList<>();

            if(cityList == null || cityList.size() == 0){
                CountryFilteredData.Cities city = new CountryFilteredData.Cities();
                city.setNameAr("-- "+getResources().getString(R.string.city_ar)+" --");
                city.setNameEn("-- "+getResources().getString(R.string.city)+" --");
                city.setId(-1);

                CountryFilteredData.Districts branch = new CountryFilteredData.Districts();
                ArrayList<CountryFilteredData.Districts> districtArrayList = new ArrayList<>();
                branch.setNameEn("-- "+getResources().getString(R.string.district)+" --");
                branch.setNameAr("-- "+getResources().getString(R.string.district_ar)+" --");
                branch.setId(-1);
                districtArrayList.add(branch);
                city.setDistricts(districtArrayList);
                cityList.add(city);
            }

            for (int j = 0; j < CityList.size(); j++){
                if(!dummyCityList.contains(CityList.get(j).getId())){
                    if(CityList.get(j).getCountryId() == (CountryList.get(i).getId())) {
                        Log.d(TAG, "CityList: "+i+","+CityList.get(j).getNameEn());
                        dummyCityList.add(CityList.get(j).getId());
                        CountryFilteredData.Cities city = new CountryFilteredData.Cities();
                        for (GetListResponse.CityList cityList1 : CityList) {
                            if (CityList.get(j).getId() == (cityList1.getId())) {
                                city.setNameAr(cityList1.getNameAr());
                                city.setNameEn(cityList1.getNameEn());
                                city.setId(cityList1.getId());

                                ArrayList<CountryFilteredData.Districts> districtArrayList = new ArrayList<>();
                                for (int k = 0; k < DistrictList.size(); k++) {
                                    if(CountryList.get(i).getId() == (CityList.get(j).getCountryId())) {
                                        if (DistrictList.get(k).getCityId() == (cityList1.getId())) {
                                            Log.d(TAG, "DistrictList: "+i+","+DistrictList.get(k).getNameEn());
                                            CountryFilteredData.Districts branch = new CountryFilteredData.Districts();

                                            branch.setNameEn(DistrictList.get(k).getNameEn());
                                            branch.setNameAr(DistrictList.get(k).getNameAr());
                                            branch.setId(DistrictList.get(k).getId());
                                            districtArrayList.add(branch);
                                        }
                                    }
                                }
                                city.setDistricts(districtArrayList);
                                break;
                            }
                        }
                        cityList.add(city);
                    }
                }
            }
            brand.setCities(cityList);
            countryFilteredData.add(brand);
        }

        countryAdapter = new ArrayAdapter<CountryFilteredData>(getApplicationContext(), R.layout.list_spinner, countryFilteredData) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setText(countryFilteredData.get(position).getNameEn());
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        cityData.addAll(countryFilteredData.get(countryPos).getCities());
        cityAdapter = new ArrayAdapter<CountryFilteredData.Cities>(getApplicationContext(), R.layout.list_spinner, cityData) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
//                Log.d(TAG, "getDropDownView: "+countryFilteredData.get(countryPos).getCities().get(position).getNameEn());
                try {
                    if (language.equalsIgnoreCase("En")){
                        ((TextView) v).setText(cityData.get(position).getNameEn());
                    }
                    else {
                        ((TextView) v).setText(cityData.get(position).getNameAr());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        districtData.addAll(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts());
        districtAdapter = new ArrayAdapter<CountryFilteredData.Districts>(getApplicationContext(), R.layout.list_spinner, districtData) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                if(language.equalsIgnoreCase("En")) {
                    ((TextView) v).setText(districtData.get(position).getNameEn());
                }
                else {
                    ((TextView) v).setText(districtData.get(position).getNameAr());
                }
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        countrySpinner.setAdapter(countryAdapter);
        citySpiner.setAdapter(cityAdapter);
        districtSpinner.setAdapter(districtAdapter);

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (countryPos != i) {
                    etCountry.setText(countryFilteredData.get(i).getNameEn());
                    countryPos = i;
                    cityData.clear();
                    cityData.addAll(countryFilteredData.get(countryPos).getCities());
                    citySpiner.setAdapter(cityAdapter);
                    cityAdapter.notifyDataSetChanged();
                } else {
                    etCountry.setText(countryFilteredData.get(i).getNameEn());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (cityPos != i) {
                    if(language.equalsIgnoreCase("En")) {
                        etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameEn());
                    }
                    else {
                        etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameAr());
                    }
                    cityPos = i;
                    districtData.clear();
                    districtData.addAll(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts());
                    districtSpinner.setAdapter(districtAdapter);
                    districtAdapter.notifyDataSetChanged();
                } else {
                    if(language.equalsIgnoreCase("En")) {
                        etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameEn());
                    }
                    else {
                        etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameAr());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (districtPos != i) {
                    if(language.equalsIgnoreCase("En")) {
                        etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameEn());
                    }
                    else {
                        etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameAr());
                    }
                    districtPos = i;
                } else {
                    if(language.equalsIgnoreCase("En")) {
                        etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameEn());
                    }
                    else {
                        etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameAr());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public String StoreByteImage(Bitmap bitmap, String type, int num) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(new Date());
        String pictureName = "";
        if(type.equals("image") ) {
            pictureName = "IMG_" + userId + "_" + timeStamp + ".jpg";
            imageNameForSingleUpload = pictureName;
            if(imagesName.length() == 0){
                imagesName = pictureName;
            }
            else {
                imagesName = imagesName + ","+ pictureName;
            }
        }
        else if(type.equals("logo") ) {
            pictureName = "LOGO_" + userId + "_" + timeStamp + ".jpg";
            logoName = pictureName;
        }
        else if(type.equals("cr") ) {
            pictureName = "CR_" + userId + "_" + timeStamp + ".jpg";
            crName = pictureName;
        }
        else if(type.equals("vat") ) {
            pictureName = "VAT_" + userId + "_" + timeStamp + ".jpg";
            vatName = pictureName;
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        if(num == 1){
            image1Bytes = stream.toByteArray();
            image1Str = Base64.encodeToString(image1Bytes, Base64.DEFAULT);
        }
        else if(num == 2){
            image2Bytes = stream.toByteArray();
            image2Str = Base64.encodeToString(image2Bytes, Base64.DEFAULT);
        }
        else if(num == 3){
            image3Bytes = stream.toByteArray();
            image3Str = Base64.encodeToString(image3Bytes, Base64.DEFAULT);
        }
        else if(num == 4){
            image4Bytes = stream.toByteArray();
            image4Str = Base64.encodeToString(image4Bytes, Base64.DEFAULT);
        }
        else if(num == 5){
            logoBytes = stream.toByteArray();
            logoStr = Base64.encodeToString(logoBytes, Base64.DEFAULT);
        }
        else if(num == 6){
            vatBytes = stream.toByteArray();
            vatStr = Base64.encodeToString(vatBytes, Base64.DEFAULT);
        }
        else if(num == 7){
            crBytes = stream.toByteArray();
            crStr = Base64.encodeToString(crBytes, Base64.DEFAULT);
        }

        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "samkra");
        folder.mkdir();

        File sdImageMainDirectory = new File(folder, pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

			/*Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,

			imageData.length,options);*/
            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }

    private void showDialog(String descriptionStr, String titleStr, String buttonStr, Activity context){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
            desc.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
            yes.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
            no.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
        }

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                startActivity(new Intent(UpdateActivity.this, SignInActivity.class));
                finalCustomDialog.dismiss();
                finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showtwoButtonsAlertDialog(){
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            }
            else if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
            }
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpdateActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.images_alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        TextView cancel = (TextView) dialogView.findViewById(R.id.cancel_btn);

        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
            desc.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
            yes.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
            no.setTypeface(Constants.getarTypeFace(UpdateActivity.this));
            title.setText("سمكره");
            no.setText("إستديو الصور");
            yes.setText("كاميرا");
            desc.setText("اختار من الخيارات التالية");
            cancel.setText("الرجاء قبول");
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, PICK_IMAGE_FROM_CAMERA);
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                customDialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    private Intent getFileChooserIntent() {
        String[] mimeTypes = {"image/*", "application/pdf"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";

            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }

            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }

        return intent;
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpdateActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private String prepareUploadImageJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            if (imageSelected == 1) {
                mobileObj.put("FileName", imageNameForSingleUpload);
                mobileObj.put("base64string", image1Str);
            }
            else if (imageSelected == 2) {
                mobileObj.put("FileName", imageNameForSingleUpload);
                mobileObj.put("base64string", image2Str);
            }
            else if (imageSelected == 3) {
                mobileObj.put("FileName", imageNameForSingleUpload);
                mobileObj.put("base64string", image3Str);
            }
            else if (imageSelected == 4) {
                mobileObj.put("FileName", imageNameForSingleUpload);
                mobileObj.put("base64string", image4Str);
            }
            else if (imageSelected == 5) {
                mobileObj.put("FileName", logoName);
                mobileObj.put("base64string", logoStr);
            }
            else if (imageSelected == 6) {
                mobileObj.put("FileName", vatName);
                mobileObj.put("base64string", vatStr);
            }
            else if (imageSelected == 7) {
                mobileObj.put("FileName", crName);
                mobileObj.put("base64string", crStr);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private class uploadImageApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUploadImageJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(UpdateActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<String> call = apiService.uploadImage(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()){

                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

}
