package com.cs.samkraworkshop.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.ChangePasswordResponse;
import com.cs.samkraworkshop.Models.SignInResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

//import cc.cloudist.acplibrary.ACProgressConstant;
//import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePassword extends Activity {

    private String response12 = null;
    ProgressDialog dialog;

    EditText oldPassword, newPassword, confirmPassword;
    Button submit;
    SharedPreferences userPrefs;
    String response, userId,strPhone,strPassword,strNewpassword;
    Toolbar toolbar;
    AlertDialog loaderDialog;
    SharedPreferences languagePrefs;
    String language;
    ImageView backBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")){
            setContentView(R.layout.change_password);
        }
        else {
            setContentView(R.layout.change_password_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        strPhone = getIntent().getStringExtra("mobile");

        backBtn = (ImageView) findViewById(R.id.back_btn);

        oldPassword = (EditText) findViewById(R.id.old_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        submit = (Button) findViewById(R.id.change_password_button);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPwd = oldPassword.getText().toString();
                String newPwd = newPassword.getText().toString();
                String confirmPwd = confirmPassword.getText().toString();
                strPassword = oldPwd;
                strNewpassword = newPwd;
                if (oldPwd.length() == 0) {
                    if (language.equalsIgnoreCase("En")){
                        oldPassword.setError("Please enter old password");
                    }
                    else {
                        oldPassword.setError("الرجاء إدخال كلمة المرور");
                    }
                } else if (newPwd.length() == 0) {
                    if (language.equalsIgnoreCase("En")){
                        newPassword.setError("Please enter new password");
                    }
                    else {
                        newPassword.setError("الرجاء إدخال كلمة المرور");
                    }

                } else if (newPwd.length() < 4 || newPwd.length() > 20) {
                    if (language.equalsIgnoreCase("En")) {
                        newPassword.setError("Password must be 4 to 20 characters");
                    } else {
                        newPassword.setError("يجب أن تتكون كلمة المرور من 4 إلى 20 حرفًا");
                    }
                } else if (confirmPwd.length() == 0) {
                    if (language.equalsIgnoreCase("En")){
                        confirmPassword.setError("Please retype password");
                    } else {
                        confirmPassword.setError("الرجاء أعد ادخال كلمة المرور");
                    }
                } else if (!newPwd.equals(confirmPwd)) {
                    if (language.equalsIgnoreCase("En")) {
                        confirmPassword.setError("Passwords not match, please retype");
                    } else {
                        confirmPassword.setError("كلمة المرور غير مطابقة، يرجى إعادة كلمة المرور");
                    }
                } else {
                    new ChangePasswordApi().execute();

                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }
    private void setTypeface() {
        oldPassword.setTypeface(Constants.getarTypeFace(ChangePassword.this));
        newPassword.setTypeface(Constants.getarTypeFace(ChangePassword.this));
        confirmPassword.setTypeface(Constants.getarTypeFace(ChangePassword.this));
        submit.setTypeface(Constants.getarTypeFace(ChangePassword.this));
        ((TextView) findViewById(R.id.tv1)).setTypeface(Constants.getarTypeFace(ChangePassword.this));
    }
    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("MobileNo",""+strPhone);
            parentObj.put("Password",strPassword);
            parentObj.put("NewPassword", strNewpassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG","prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private class ChangePasswordApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangePasswordResponse> call = apiService.changePassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangePasswordResponse>() {
                @Override
                public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        ChangePasswordResponse resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
                                if (language.equalsIgnoreCase("En")){
                                    Toast.makeText(ChangePassword.this, "Change password successful", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Toast.makeText(ChangePassword.this, "تم تغيير كلمة المرور بنجاح", Toast.LENGTH_SHORT).show();
                                }
                                finish();
                            }
                            else {
//                                status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = resetPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), ChangePassword.this);
                                }
                                else {
                                    String failureResponse = resetPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), ChangePassword.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(ChangePassword.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ChangePassword.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ChangePassword.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ChangePassword.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePassword.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ChangePassword.this, getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ChangePassword.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ChangePassword.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ChangePassword.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChangePassword.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
