package com.cs.samkraworkshop.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.cs.samkraworkshop.Activity.WebViewActivity;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import static com.cs.samkraworkshop.Activity.MainActivity.drawer;

public class MoreFragment extends Fragment implements OnMapReadyCallback {

    TextView emailId, privacyPolicy, description, appversion, website;
    ImageView backBtn, fb, twitter, instagram;
    View view;
    private GoogleMap map;
    String language;
    SharedPreferences languagePrefs;
    ImageView sidemenu;
    VideoView videoView;
    LinearLayout ratingLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs =getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            view = inflater.inflate(R.layout.fragment_more, container,false);
        }else if(language.equalsIgnoreCase("Ar")){
            view = inflater.inflate(R.layout.fragment_more_ar, container,false);
        }

        ratingLayout = (LinearLayout) view.findViewById(R.id.rating_layout);
        emailId = (TextView) view.findViewById(R.id.email_id);
        privacyPolicy = (TextView) view.findViewById(R.id.privacy_policy);
        description = (TextView) view.findViewById(R.id.description);
        appversion = (TextView) view.findViewById(R.id.appversion);
        website = (TextView) view.findViewById(R.id.website);
        backBtn = (ImageView) view.findViewById(R.id.back_btn);
        sidemenu =(ImageView)view.findViewById(R.id.side_menu);
        fb =(ImageView)view.findViewById(R.id.fb);
        twitter =(ImageView)view.findViewById(R.id.twitter);
        instagram =(ImageView)view.findViewById(R.id.insta);

//        videoView =(VideoView) view.findViewById(R.id.videoView);
//        videoView.requestFocus();

//        videoView.setVideoURI(Uri.parse("https://www.youtube.com/watch?v=9Vo-Lg2AbYA"));
//        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mediaPlayer) {
//                videoView.start();
//            }
//        });

        sidemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuClick();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(MoreFragment.this);

//        String desc = "<p><span style=\"font-size: 12.0pt; font-family: 'Calibri','sans-serif'; color: #b8bab9;\">Stay tuned with <strong>SAMKRA</strong> to find the best offers for car repair services. Our offers are time limited so make sure to use them as they appear</span><span style=\"font-size: 12.0pt; font-family: 'Calibri','sans-serif';\">.<span style=\"color: #2a44a1;\"> <a href=\"http://csadms.com/samkrabackend/About/About\">Read More</a></span></span></p>";
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            description.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
//        } else {
//            description.setText(Html.fromHtml(desc));
//        }

        description.setMovementMethod(LinkMovementMethod.getInstance());

        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version12 = pInfo.versionName;
            appversion.setText("App version v"+version12+"("+pInfo.versionCode+")");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        emailId.setPaintFlags(emailId.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        privacyPolicy.setPaintFlags(privacyPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        emailId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("plain/text");
                    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@samkra.com"});
                    i.putExtra(Intent.EXTRA_SUBJECT, "Samkra workshop Feedback");
                    i.putExtra(Intent.EXTRA_TITLE  , "Samkra workshop Feedback");
                    final PackageManager pm = getActivity().getPackageManager();
                    final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                    String className = null;
                    for (final ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                            className = info.activityInfo.name;

                            if(className != null && !className.isEmpty()){
                                break;
                            }
                        }
                    }
                    i.setClassName("com.google.android.gm", className);
                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(getActivity(), "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(language.equalsIgnoreCase("En")) {
                    Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                    fbIntent.putExtra("title", "Privacy policy");
                    fbIntent.putExtra("url", "http://workshop.samkra.com/about/privacypolicy");
                    startActivity(fbIntent);
                }
                else {
                    Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                    fbIntent.putExtra("title", "سياسة الخصوصية");
                    fbIntent.putExtra("url", "http://workshop.samkra.com/about/privacypolicyar");
                    startActivity(fbIntent);
                }
            }
        });

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                fbIntent.putExtra("title", "Facebook");
                fbIntent.putExtra("url", "http://www.facebook.com");
                startActivity(fbIntent);
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                fbIntent.putExtra("title", "Twitter");
                fbIntent.putExtra("url", "https://twitter.com/SamkraApp");
                startActivity(fbIntent);
            }
        });

        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                fbIntent.putExtra("title", "Instagram");
                fbIntent.putExtra("url", "https://www.instagram.com/samkra.app/");
                startActivity(fbIntent);
            }
        });

        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                fbIntent.putExtra("title", "Mawhiba Technologies");
                fbIntent.putExtra("url", "http://samkra.com/");
                startActivity(fbIntent);
            }
        });

        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(language.equalsIgnoreCase("En")) {
                    Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                    fbIntent.putExtra("title", "About Us");
                    fbIntent.putExtra("url", "http://workshop.samkra.com/About/About");
                    startActivity(fbIntent);
                }
                else {
                    Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                    fbIntent.putExtra("title", "من نحن");
                    fbIntent.putExtra("url", "http://workshop.samkra.com/About/Aboutar");
                    startActivity(fbIntent);
                }
            }
        });

        ratingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.samkraworkshop")));
            }
        });

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
        return view;
    }
    private void setTypeface() {
        ((TextView) view.findViewById(R.id.title)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.description)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.sel_address1)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.media_title)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.privacy_policy)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.website)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.email_id)).setTypeface(Constants.getarTypeFace(getActivity()));
    }
    public void menuClick(){
        if (language.equalsIgnoreCase("En")) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        }else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);

            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng latLng = new LatLng(24.682427,46.687831);
        MarkerOptions markerOptions = new MarkerOptions();
        map.clear();
        // Show the current location in Google Map
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        // Zoom in the Google Map
        map.animateCamera(CameraUpdateFactory.zoomTo(12.0f));

        MarkerOptions marker = new MarkerOptions().position(new LatLng(24.682427,46.687831));

        // Changing marker icon
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

        // adding marker
        map.addMarker(marker);
    }
}
