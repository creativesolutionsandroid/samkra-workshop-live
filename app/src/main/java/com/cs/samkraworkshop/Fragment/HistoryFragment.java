package com.cs.samkraworkshop.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.samkraworkshop.Activity.InvoiceActivity;
import com.cs.samkraworkshop.Adapter.HistoryAdapter;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.HistoryResponce;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.samkraworkshop.Activity.MainActivity.drawer;

public class HistoryFragment extends Fragment {


    ImageView back_btn,testcount;
//    TextView invoicenumber,offernumber;
    ListView requestsList;
    HistoryAdapter mAdapter;
    ArrayList<HistoryResponce.Data> data = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";
    AlertDialog loaderDialog;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    TextView offernumber;
    View view;
    ImageView sidemenu;
    String language;
    SharedPreferences languagePrefs;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs =getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            view = inflater.inflate(R.layout.historory_layout, container,false);
        }else if(language.equalsIgnoreCase("Ar")){
            view = inflater.inflate(R.layout.historory_layout_ar, container,false);
        }



        back_btn=(ImageView) view.findViewById(R.id.hy_back_btn);
        testcount=(ImageView) view.findViewById(R.id.test_count);
//        invoicenumber=(TextView)findViewById(R.id.hy_invoicenumber);
//        offernumber=(TextView)findViewById(R.id.hy_accpetnumber);
        requestsList=(ListView) view.findViewById(R.id.hy_listview);
        offernumber=(TextView) view.findViewById(R.id.n_bid_count);
        sidemenu =(ImageView)view.findViewById(R.id.side_menu);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new userRequestsApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
            }
        }

        requestsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), InvoiceActivity.class);
                intent.putExtra("data", data);
                intent.putExtra("pos",i);
                startActivity(intent);
            }
        });
        testcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        sidemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuClick();
            }
        });
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }

        return view;
    }
    private void setTypeface(){
        ((TextView)view.findViewById(R.id.st1)).setTypeface(Constants.getarbooldTypeFace(getActivity()));

    }
    public void menuClick(){
        if (language.equalsIgnoreCase("En")) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        }else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);

            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        }
    }
    private class  userRequestsApi extends AsyncTask<String, String, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();

        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<HistoryResponce> call = apiService.GetListInvoicePrepared(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<HistoryResponce>() {
                @Override
                public void onResponse(Call<HistoryResponce> call, Response<HistoryResponce> response) {
                    if (response.isSuccessful()) {
                        data.clear();
                        Log.d(TAG, "onResponse: "+response);
                        HistoryResponce verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                data = verifyMobileResponse.getData();
                                if(data.size() == 0){
                                    if (language.equalsIgnoreCase("En")){
                                        Constants.showOneButtonAlertDialog("No Invoices Found", getResources().getString(R.string.app_name),
                                                getResources().getString(R.string.ok), getActivity());
                                    }
                                   else {
                                        Constants.showOneButtonAlertDialog("لا يوجد عروض أسعار", getResources().getString(R.string.app_name_ar),
                                                getResources().getString(R.string.ok_ar), getActivity());
                                    }
                                }
                                mAdapter = new HistoryAdapter(getActivity(), data, Language);
                                requestsList.setAdapter(mAdapter);
                                offernumber.setText(""+data.size());
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                else{
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<HistoryResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("WorkshopId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }


    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }


}