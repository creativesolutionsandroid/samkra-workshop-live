package com.cs.samkraworkshop.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkraworkshop.Activity.SignupActivity;
import com.cs.samkraworkshop.Adapter.SwipeDeckAdapter;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Fragment.DashboardFragment;
import com.cs.samkraworkshop.Models.NewRequestsResponse;
import com.cs.samkraworkshop.Models.SwipeDeckData;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.daprlabs.cardstack.SwipeDeck;
import com.daprlabs.cardstack.SwipeFrameLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.samkraworkshop.Activity.MainActivity.drawer;

public class BidsFragment extends Fragment {

    TextView bidCount;
    ImageView backBtn,testcount;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    ImageView sidemenu;
    AlertDialog loaderDialog;
    View view;
    ArrayList<NewRequestsResponse.Data> dataArrayList = new ArrayList<>();
    ArrayList<SwipeDeckData> deckArrayList = new ArrayList<>();
    LinearLayout noBidsLayout;
    TextView refresh;
    SwipeDeck cardStack;
    SwipeFrameLayout swipeFrameLayout;
    CountDownTimer countDownTimer;
    public static int cardViewPosition = 0;
    String language;
    SharedPreferences languagePrefs;
    private Timer timer = new Timer();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs =getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            view = inflater.inflate(R.layout.fragment_bids, container,false);
        }else if(language.equalsIgnoreCase("Ar")){
            view = inflater.inflate(R.layout.fragment_bids_ar, container,false);
        }

        cardViewPosition = 0;

        userPrefs = getContext().getSharedPreferences("USER_PREFS",Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId","");

        noBidsLayout = (LinearLayout) view.findViewById(R.id.no_bids_found);
        refresh = (TextView) view.findViewById(R.id.refresh);
        bidCount = (TextView) view.findViewById(R.id.bid_count);
        backBtn = (ImageView) view.findViewById(R.id.back_btn);
        cardStack = (SwipeDeck) view.findViewById(R.id.swipe_deck);
        sidemenu =(ImageView)view.findViewById(R.id.side_menu);
        testcount =(ImageView)view.findViewById(R.id.testcount);
        swipeFrameLayout = (SwipeFrameLayout) view.findViewById(R.id.swipe_layout);

        String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new getNewRequestsApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
            }
        }

//        timer.schedule(new MyTimerTask(), 30000, 30000);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noBidsLayout.setVisibility(View.GONE);
                String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new getNewRequestsApi().execute();
                }
                else{
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        testcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                mMessageReceiver, new IntentFilter("cancel_request"));

        sidemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuClick();
            }
        });
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }

        return view;
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new getNewRequestsApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }

    private void setTypeface() {
        ((TextView) view.findViewById(R.id.st1)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.refresh)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st2)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
    }
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new getNewRequestsApi().execute();
            }
            else{
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getContext(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private String prepareJson(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("WorkshopId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }
    public void menuClick(){
        if (language.equalsIgnoreCase("En")) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        }else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);

            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        }
    }
    private class getNewRequestsApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<NewRequestsResponse> call = apiService.GetWorkshopNewRequests(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<NewRequestsResponse>() {
                @Override
                public void onResponse(Call<NewRequestsResponse> call, Response<NewRequestsResponse> response) {
                    dataArrayList.clear();
                    if(response.isSuccessful()){
                        NewRequestsResponse requestsResponse = response.body();
                        try {
                            if(requestsResponse.getStatus()){
                                dataArrayList = requestsResponse.getData();
                                bidCount.setText(""+dataArrayList.size());
                                cardViewPosition = 0;
                                if(dataArrayList.size() == 0){
                                    swipeFrameLayout.setVisibility(View.GONE);
                                    noBidsLayout.setVisibility(View.VISIBLE);
                                }
                                else {
                                    setdata();
                                }
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = requestsResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                else {
                                    String failureResponse = requestsResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessage();
                            if (language.equalsIgnoreCase("En")) {
                                Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            } else {
                                Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.error_ar),
                                        getResources().getString(R.string.ok_ar), getActivity());
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), getActivity());
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<NewRequestsResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.str_connection_error), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.str_connection_error_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), getActivity());
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), getActivity());
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    public void setdata(){
        refreshDeck("");
        final SwipeDeckAdapter adapter = new SwipeDeckAdapter(dataArrayList, deckArrayList, getActivity());
        cardStack.setAdapter(adapter);
//        cardStack.animate();

//        countDownTimer = new CountDownTimer(60 * 2880 * 1000, 1000) {
//            // 1000 means, onTick function will be called at every 1000 milliseconds
//            @Override
//            public void onTick(long leftTimeInMilliseconds) {
//                SwipeDeckAdapter adapter = new SwipeDeckAdapter(dataArrayList, deckArrayList, getContext());
//                cardStack.setAdapter(adapter);
//                cardStack.setSelection(cardViewPosition);
////                adapter.notifyDataSetChanged();
//                cardStack.clearAnimation();
//            }
//            @Override
//            public void onFinish() {
//
//            }
//        }.start();

        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
//                if(position > 0) {
                refreshDeck(deckArrayList.get(position).getRequestCode());
                SwipeDeckAdapter adapter = new SwipeDeckAdapter(dataArrayList, deckArrayList, getActivity());
                cardStack.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
//                cardStack.animate();
                if(cardViewPosition != 0){
                    cardViewPosition = cardViewPosition - 1;
                }

//                countDownTimer = new CountDownTimer(60 * 2880 * 1000, 1000) {
//                    // 1000 means, onTick function will be called at every 1000 milliseconds
//                    @Override
//                    public void onTick(long leftTimeInMilliseconds) {
//                        SwipeDeckAdapter adapter = new SwipeDeckAdapter(dataArrayList, deckArrayList, BidsFragment.this);
//                        cardStack.setAdapter(adapter);
//                        cardStack.setSelection(cardViewPosition);
////                adapter.notifyDataSetChanged();
//                        cardStack.clearAnimation();
//                    }
//                    @Override
//                    public void onFinish() {
//
//                    }
//                }.start();
//                }
//                else{
//                    refreshDeck(deckArrayList.get(position).getRequestCode());
//                    cardStack.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
//                    cardStack.animate();
//                }
            }

            @Override
            public void cardSwipedRight(int position) {
//                Log.d("TAG", "deckArrayList: "+position);
                cardViewPosition = cardViewPosition + 1;
                if(position == (dataArrayList.size()-1)){
                    SwipeDeckAdapter adapter = new SwipeDeckAdapter(dataArrayList, deckArrayList, getActivity());
                    cardStack.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
//                    cardStack.animate();
                }
//                deckArrayList.remove(position);
            }

            @Override
            public void cardsDepleted() {

            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });
    }

    private void refreshDeck(String reqCode){
        try {
            countDownTimer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        int startPosition = 0;
        if(!reqCode.equals("")) {
            for (int i = 0; i < dataArrayList.size(); i++) {
                if (reqCode.equals(dataArrayList.get(i).getRequestCode())) {
                    if (i > 0) {
                        startPosition = i - 1;
                    }
                    break;
                }
            }
        }

        Log.d("TAG", "refreshDeck: "+startPosition);
        deckArrayList.clear();
        for (int i = startPosition; i < dataArrayList.size(); i++){
            SwipeDeckData swipeDeckData = new SwipeDeckData();
            swipeDeckData.setRequestCode(dataArrayList.get(i).getRequestCode());
            if (language.equalsIgnoreCase("En")){
                swipeDeckData.setModelName(dataArrayList.get(i).getCM().get(0).getCarMakerNameEn());
            }
            else {
                swipeDeckData.setModelName(dataArrayList.get(i).getCM().get(0).getCarMakerNameAr());
            }

            swipeDeckData.setCreatedOn(dataArrayList.get(i).getCreatedOn());
            swipeDeckData.setExpiresOn(dataArrayList.get(i).getExpiresOn());
            swipeDeckData.setRemainingTime(dataArrayList.get(i).getRemainingTime());
            swipeDeckData.setImage(dataArrayList.get(i).getCM().get(0).getMDL().get(0).getRCI().get(0).getDocumentLocation());
            swipeDeckData.setRequestCount(""+dataArrayList.get(i).getRequestCount());

            Location me   = new Location("");
            Location dest = new Location("");

            try {
                me.setLatitude(Double.parseDouble(dataArrayList.get(i).getLatitude()));
                me.setLongitude(Double.parseDouble(dataArrayList.get(i).getLongitude()));

                dest.setLatitude(Double.parseDouble(DashboardFragment.data.get(0).getLatitude()));
                dest.setLongitude(Double.parseDouble(DashboardFragment.data.get(0).getLongitude()));

                float dist = (me.distanceTo(dest))/1000;
                swipeDeckData.setDistance(""+(int)Math.ceil(dist));
            } catch (Exception e) {
                e.printStackTrace();
                swipeDeckData.setDistance("0");
            }
            deckArrayList.add(swipeDeckData);
        }
        Log.d("TAG", "refreshDeck: "+deckArrayList.size());
    }

    private void addDeck(int i){
//        for (int i = startPosition; i < dataArrayList.size(); i++){
        SwipeDeckData swipeDeckData = new SwipeDeckData();
        swipeDeckData.setRequestCode(dataArrayList.get(i).getRequestCode());
        swipeDeckData.setModelName(dataArrayList.get(i).getCM().get(0).getCarMakerNameEn());
        swipeDeckData.setCreatedOn(dataArrayList.get(i).getCreatedOn());
        swipeDeckData.setExpiresOn(dataArrayList.get(i).getExpiresOn());
        swipeDeckData.setRemainingTime(dataArrayList.get(i).getRemainingTime());
        swipeDeckData.setImage(dataArrayList.get(i).getCM().get(0).getMDL().get(0).getRCI().get(0).getDocumentLocation());
        swipeDeckData.setRequestCount(""+dataArrayList.get(i).getRequestCount());

        Location me   = new Location("");
        Location dest = new Location("");

        try {
            me.setLatitude(Double.parseDouble(dataArrayList.get(i).getLatitude()));
            me.setLongitude(Double.parseDouble(dataArrayList.get(i).getLongitude()));

            dest.setLatitude(Double.parseDouble(DashboardFragment.data.get(0).getLatitude()));
            dest.setLongitude(Double.parseDouble(DashboardFragment.data.get(0).getLongitude()));

            float dist = (me.distanceTo(dest))/1000;
            swipeDeckData.setDistance(""+(int)Math.ceil(dist));
        } catch (Exception e) {
            e.printStackTrace();
            swipeDeckData.setDistance("0");
        }
        deckArrayList.add(swipeDeckData);
//        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }



}
