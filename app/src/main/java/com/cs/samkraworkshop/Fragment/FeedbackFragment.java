package com.cs.samkraworkshop.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Adapter.FeedbackAdapter;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.FeedbackResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.samkraworkshop.Activity.MainActivity.drawer;

public class FeedbackFragment extends Fragment {

    ListView mListView;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    String TAG = "TAG";
    AlertDialog loaderDialog;
    FeedbackAdapter mAdapter;
    TextView bidCount, star1Count, star2Count, star3Count, star4Count, star5Count;
    int total = 0, one = 0, two = 0, three = 0, four = 0, five = 0;
    TextView averageRating, name;
    CircleImageView profilePic;
    ImageView backBtn;
    View view;
    ImageView sidemenu;
    String language;
    SharedPreferences languagePrefs;

    ArrayList<FeedbackResponse.Wr> data = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs =getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            view = inflater.inflate(R.layout.reviews_layout, container,false);
        }else if(language.equalsIgnoreCase("Ar")){
            view = inflater.inflate(R.layout.reviews_layout_ar, container,false);
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        mListView = (ListView) view.findViewById(R.id.review_list);
        bidCount = (TextView) view.findViewById(R.id.bid_count);
        star1Count = (TextView) view.findViewById(R.id.star1_count);
        star2Count = (TextView) view.findViewById(R.id.star2_count);
        star3Count = (TextView) view.findViewById(R.id.star3_count);
        star4Count = (TextView) view.findViewById(R.id.star4_count);
        star5Count = (TextView) view.findViewById(R.id.star5_count);
        averageRating = (TextView) view.findViewById(R.id.average);
        sidemenu = (ImageView) view.findViewById(R.id.side_menu);

        name = (TextView) view.findViewById(R.id.workshop_name);
        profilePic = (CircleImageView) view.findViewById(R.id.profile_pic);

        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new feedBackApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
            }
        }
        sidemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuClick();
            }
        });

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
        return view;
    }

    private void setTypeface() {
        ((TextView) view.findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st2)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st3)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st4)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st5)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st6)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st7)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.workshop_name)).setTypeface(Constants.getarTypeFace(getActivity()));
//        ((TextView) view.findViewById(R.id.st9)).setTypeface(Constants.getarTypeFace(getActivity()));
    }

    public void menuClick(){
        if (language.equalsIgnoreCase("En")) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        }else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);

            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        }
    }

    private class feedBackApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<FeedbackResponse> call = apiService.GetListFeedback(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<FeedbackResponse>() {
                @Override
                public void onResponse(Call<FeedbackResponse> call, Response<FeedbackResponse> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: "+response);
                        FeedbackResponse verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                data = verifyMobileResponse.getData().get(0).getWr();
//                                if(data.size() == 0){
//                                    if(language.equalsIgnoreCase("En")) {
//                                        Constants.showOneButtonAlertDialog(verifyMobileResponse.getMessage(), getResources().getString(R.string.app_name),
//                                                getResources().getString(R.string.ok), getActivity());
//                                    }
//                                    else {
//                                        Constants.showOneButtonAlertDialog(verifyMobileResponse.getMessageAr(), getResources().getString(R.string.samkra_ar),
//                                                getResources().getString(R.string.ok_ar), getActivity());
//                                    }
//                                }
                                mAdapter = new FeedbackAdapter(getActivity(), data, "En");
                                mListView.setAdapter(mAdapter);
                                setData();

                                try {
                                    if (language.equalsIgnoreCase("En")){
                                        name.setText(verifyMobileResponse.getData().get(0).getNameEn());
                                    }
                                    else {
                                        name.setText(verifyMobileResponse.getData().get(0).getNameAr());
                                    }

                                    Glide.with(FeedbackFragment.this)
                                            .load(Constants.WORKSHOP_IMAGES+verifyMobileResponse.getData().get(0).getLogo())
                                            .into(profilePic);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<FeedbackResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("WorkshopId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private void setData(){
        for (int i = 0; i < data.size(); i++){
            if(data.get(i).getRating() == 1){
                one = one + 1;
                total = total + 1;
            }
            else if(data.get(i).getRating() == 2){
                two = two + 1;
                total = total + 2;
            }
            else if(data.get(i).getRating() == 3){
                three = three + 1;
                total = total + 3;
            }
            else if(data.get(i).getRating() == 4){
                four = four + 1;
                total = total + 4;
            }
            else if(data.get(i).getRating() == 5){
                five = five + 1;
                total = total + 5;
            }
        }

        bidCount.setText(""+data.size());
        if(one < 9) {
            star1Count.setText("0" + one);
        }
        else {
            star1Count.setText(""+one);
        }

        if(two < 9) {
            star2Count.setText("0" + two);
        }
        else {
            star2Count.setText(""+two);
        }

        if(three < 9) {
            star3Count.setText("0" + three);
        }
        else {
            star3Count.setText(""+three);
        }

        if(four < 9) {
            star4Count.setText("0" + four);
        }
        else {
            star4Count.setText(""+four);
        }

        if(five < 9) {
            star5Count.setText("0" + five);
        }
        else {
            star5Count.setText(""+five);
        }

        if(data.size() > 0) {
            float avg = (float) total / data.size();
            averageRating.setText(Constants.convertToArabic(Constants.priceFormat.format(avg)));
        }
    }

    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }

}
