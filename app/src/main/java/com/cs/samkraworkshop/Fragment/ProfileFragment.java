package com.cs.samkraworkshop.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Activity.ChangePassword;
import com.cs.samkraworkshop.Activity.MainActivity;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.CountryFilteredData;
import com.cs.samkraworkshop.Models.DashboardResponse;
import com.cs.samkraworkshop.Models.GetListResponse;
import com.cs.samkraworkshop.Models.ImageDeleteResponse;
import com.cs.samkraworkshop.Models.UpdateDetailsResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.cs.samkraworkshop.Activity.MainActivity.drawer;

public class ProfileFragment extends Fragment implements View.OnClickListener{

    EditText inputName, inputNameAr, mobileEt, inputPassword;
    ImageView nameEdit, nameArEdit, passwordEdit, emailEdit, descEdit, descArEdit, landlineEdit;
    String strName, strNameAr, strMobile, strPassword;
    private EditText etEmail, etDesc, etDescAr, etLandline;
    private EditText etStreet, etStreetAr, etZipcode, etDistrcit, etCity, etCountry;
    private ImageView streetEdit, streetArEdit, zipcodeEdit;
    String strStreet, strStreetAr, strDistrict, strCity, strZipcode;
    String strEmail, strLandline, strDesc, strDescAr, cars = "";
    private TextView geoLocationEt, brandsSelected;
    private ImageView wImage1, wImage2, wImage3, wImage4, wLogoImage, wVatImage, wCRImage;
    private ImageView image1Delete, image2Delete, image3Delete, image4Delete, logoDelete, vatDelete, crDelete;
    private Spinner districtSpinner, citySpiner, countrySpinner;
    private ArrayAdapter<CountryFilteredData.Districts> districtAdapter;
    private ArrayAdapter<CountryFilteredData.Cities> cityAdapter;
    private ArrayAdapter<CountryFilteredData> countryAdapter;
    private LinearLayout detailsLayout, locationLayout, documentsLayout;
    AlertDialog loaderDialog;
    AlertDialog loaderDialogForUpdate;

    int imageSelected = 0;
    Bitmap thumbnail1 = null, thumbnail2 = null, thumbnail3 = null, thumbnail4 = null,
            thumbnail5 = null, thumbnail6 = null, thumbnail7 = null;

    private double latitude, longitude;

    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int PICK_IMAGE_FROM_CAMERA = 3;
    private static final int CAMERA_REQUEST = 4;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };

    public static List<GetListResponse.CarMakerList> CarMakerList = new ArrayList<>();
    public static List<GetListResponse.DistrictList> DistrictList;
    public static List<GetListResponse.CityList> CityList;
    public static List<GetListResponse.CountryList> CountryList;
    private ArrayList<CountryFilteredData> countryFilteredData = new ArrayList<>();

    LinearLayout detailsTitleLayout, locationTitleLayout, docsTitleLayout;
    ImageView detailsSuccess, locationSuccess, docsSuccess;
    TextView detailsText, locationText, docsText;
    private int cityPos = 0, districtPos = 0, countryPos = 0;
    private String districtSelected;
    String imagePath1;
    AlertDialog brandsDialog;
    Button updateBtn;

    private byte[] logoBytes;
    private byte[] image1Bytes;
    private byte[] image2Bytes;
    private byte[] image3Bytes;
    private byte[] image4Bytes;
    private byte[] crBytes;
    private byte[] vatBytes;
    private String image1Str = "", image2Str = "", image3Str = "", image4Str = "", logoStr = "", vatStr = "", crStr = "";
    private boolean isLogoUploaded = false, isImagesUploaded = false, isCRUploaded = false, isVatUploaded = false;
    private boolean isLogoAvailable = false, isImage1Available = false, isImage2Available = false, isImage3Available = false
            , isImage4Available = false, isCRAvailable = false, isVatAvailable = false;
    String logoName = "", crName = "", vatName = "", imagesName = "";
    private int image1Id = 0, image2Id  = 0, image3Id = 0, image4Id = 0, logoId = 0, vatId = 0, crId = 0;
    int deleteImageName;
    int updateStage = 0, pendingStages = 0;

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;

    ArrayList<DashboardResponse.Data> data = new ArrayList<>();
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    AlertDialog customDialog;
    Boolean isDetailsUpdated = false, isLocationUpdated = false, isDocumentsUpdated = false;
    private RelativeLayout carBrandsLayout;
    RelativeLayout geoLocationLayout;
    Boolean isFirstTime = true;
    ImageView sidemenu;
    Uri imageUri;
    boolean isGalleryImage = false;

    View view;
    private String TAG = "TAG";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            view = inflater.inflate(R.layout.activity_edit_profile, container,false);
        }else if (language.equalsIgnoreCase("Ar")){
            view = inflater.inflate(R.layout.activity_edit_profile_arabic, container,false);
        }


        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "");
        userPrefsEditor = userPrefs.edit();

        data = DashboardFragment.data;

        districtSpinner = (Spinner) view.findViewById(R.id.district_spinner);
        citySpiner = (Spinner) view.findViewById(R.id.city_spinner);
        countrySpinner = (Spinner) view.findViewById(R.id.country_spinner);
        sidemenu =(ImageView)view.findViewById(R.id.side_menu);

        inputName = (EditText) view.findViewById(R.id.et_name);
        inputNameAr = (EditText) view.findViewById(R.id.et_name_ar);
        inputPassword= (EditText) view.findViewById(R.id.et_password);
        mobileEt = (EditText) view.findViewById(R.id.et_mobile);
        etEmail = (EditText) view.findViewById(R.id.et_email);
        etLandline = (EditText) view.findViewById(R.id.et_landline);
        etDesc = (EditText) view.findViewById(R.id.et_description);
        etDescAr = (EditText) view.findViewById(R.id.et_description_ar);

        nameEdit = (ImageView) view.findViewById(R.id.name_edit);
        nameArEdit = (ImageView) view.findViewById(R.id.name_ar_edit);
        passwordEdit= (ImageView) view.findViewById(R.id.password_edit);
        emailEdit = (ImageView) view.findViewById(R.id.email_edit);
        descEdit = (ImageView) view.findViewById(R.id.description_edit);
        descArEdit = (ImageView) view.findViewById(R.id.description_ar_edit);
        landlineEdit= (ImageView) view.findViewById(R.id.landline_edit);
        streetEdit= (ImageView) view.findViewById(R.id.street_edit);
        streetArEdit = (ImageView) view.findViewById(R.id.street_ar_edit);
        zipcodeEdit = (ImageView) view.findViewById(R.id.zipcode_edit);

        etStreet = (EditText) view.findViewById(R.id.et_street_name);
        etStreetAr = (EditText) view.findViewById(R.id.et_street_name_ar);
        etCity = (EditText) view.findViewById(R.id.et_city);
        etCountry = (EditText) view.findViewById(R.id.et_country);
        etDistrcit = (EditText) view.findViewById(R.id.et_district);
        etZipcode = (EditText) view.findViewById(R.id.et_zipcode);
        geoLocationEt = (TextView) view.findViewById(R.id.et_geolocation);
        brandsSelected = (TextView) view.findViewById(R.id.selected_cars);

        wImage1 = (ImageView) view.findViewById(R.id.image1);
        wImage2 = (ImageView) view.findViewById(R.id.image2);
        wImage3 = (ImageView) view.findViewById(R.id.image3);
        wImage4 = (ImageView) view.findViewById(R.id.image4);
        wLogoImage = (ImageView) view.findViewById(R.id.logo);
        wVatImage = (ImageView) view.findViewById(R.id.vat_image);
        wCRImage = (ImageView) view.findViewById(R.id.cr_image);

        image1Delete = (ImageView) view.findViewById(R.id.delete_image1);
        image2Delete = (ImageView) view.findViewById(R.id.delete_image2);
        image3Delete = (ImageView) view.findViewById(R.id.delete_image3);
        image4Delete = (ImageView) view.findViewById(R.id.delete_image4);
        logoDelete = (ImageView) view.findViewById(R.id.delete_logo);
        vatDelete = (ImageView) view.findViewById(R.id.delete_vat);
        crDelete = (ImageView) view.findViewById(R.id.delete_cr);
        geoLocationLayout = (RelativeLayout) view.findViewById(R.id.geo_location_layout);

        detailsLayout = (LinearLayout) view.findViewById(R.id.details_layout);
        locationLayout = (LinearLayout) view.findViewById(R.id.location_layout);
        documentsLayout = (LinearLayout) view.findViewById(R.id.images_layout);

        detailsTitleLayout = (LinearLayout) view.findViewById(R.id.layout1);
        locationTitleLayout = (LinearLayout) view.findViewById(R.id.layout2);
        docsTitleLayout = (LinearLayout) view.findViewById(R.id.layout3);
        carBrandsLayout = (RelativeLayout) view.findViewById(R.id.car_brands_layout);

        detailsText = (TextView) view.findViewById(R.id.details_text);
        locationText = (TextView) view.findViewById(R.id.location_text);
        docsText = (TextView) view.findViewById(R.id.docs_text);
        updateBtn = (Button) view.findViewById(R.id.update_btn);

        detailsTitleLayout.setOnClickListener(this);
        locationTitleLayout.setOnClickListener(this);
        docsTitleLayout.setOnClickListener(this);
        carBrandsLayout.setOnClickListener(this);

        wImage1.setOnClickListener(this);
        wImage2.setOnClickListener(this);
        wImage3.setOnClickListener(this);
        wImage4.setOnClickListener(this);
        wLogoImage.setOnClickListener(this);
        wVatImage.setOnClickListener(this);
        wCRImage.setOnClickListener(this);

        image1Delete.setOnClickListener(this);
        image2Delete.setOnClickListener(this);
        image3Delete.setOnClickListener(this);
        image4Delete.setOnClickListener(this);
        logoDelete.setOnClickListener(this);
        vatDelete.setOnClickListener(this);
        crDelete.setOnClickListener(this);
        updateBtn.setOnClickListener(this);
        geoLocationLayout.setOnClickListener(this);

        nameEdit.setOnClickListener(this);
        nameArEdit.setOnClickListener(this);
        passwordEdit.setOnClickListener(this);
        emailEdit.setOnClickListener(this);
        descEdit.setOnClickListener(this);
        descArEdit.setOnClickListener(this);
        landlineEdit.setOnClickListener(this);
        streetEdit.setOnClickListener(this);
        streetArEdit.setOnClickListener(this);
        zipcodeEdit.setOnClickListener(this);

        sidemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuClick();
            }
        });


        if(data.size() > 0){
            initView();
        }
        else {
            String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new dashboardApi().execute();
            }
            else{
                if (language.equalsIgnoreCase("En")) {
                    Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error), getResources().getString(R.string.error),
                            getResources().getString(R.string.ok), getActivity());
                } else {
                    Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error_ar), getResources().getString(R.string.error_ar),
                            getResources().getString(R.string.ok_ar), getActivity());
                }
            }
        }

        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new getDetailsApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error), getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), getActivity());
            } else {
                Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error_ar), getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), getActivity());
            }
        }

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
        return view;
    }
    private void setTypeface() {
        ((TextView) view.findViewById(R.id.docs_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.location_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.et_name_ar)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st2)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st3)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.et_mobile)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st4)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st5)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st6)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st7)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st8)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.selected_cars)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.tv_country)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.tv_city)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.tv_district)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.car_title)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st9)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((EditText) view.findViewById(R.id.et_street_name_ar)).setTypeface(Constants.getarTypeFace(getActivity()));
//        ((TextView) view.findViewById(R.id.street_name)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st10)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st11)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.et_geolocation)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.logo_title)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.workshop_images_layout)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.sub_title)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.update_btn)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st12)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.vat_title)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.st10)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.details_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.et_district)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.et_city)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.et_country)).setTypeface(Constants.getarTypeFace(getActivity()));
//        ((TextView) view.findViewById(R.id.workshop_images_layout)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
//        ((TextView) view.findViewById(R.id.workshop_images_layout)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
//        ((TextView) view.findViewById(R.id.workshop_images_layout)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
//
    }
    public void menuClick(){
        if (language.equalsIgnoreCase("En")) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        }else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);

            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout1:
                detailsLayout.setVisibility(View.VISIBLE);
                locationLayout.setVisibility(View.GONE);
                documentsLayout.setVisibility(View.GONE);

                detailsText.setTextColor(getResources().getColor(R.color.white));
                detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));

                locationText.setTextColor(getResources().getColor(R.color.colorPrimary));
                locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                docsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));
                break;

            case R.id.layout2:
                detailsLayout.setVisibility(View.GONE);
                locationLayout.setVisibility(View.VISIBLE);
                documentsLayout.setVisibility(View.GONE);

                detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                locationText.setTextColor(getResources().getColor(R.color.white));
                locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));

                docsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));
                break;

            case R.id.layout3:
                detailsLayout.setVisibility(View.GONE);
                locationLayout.setVisibility(View.GONE);
                documentsLayout.setVisibility(View.VISIBLE);

                detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
                detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                locationText.setTextColor(getResources().getColor(R.color.colorPrimary));
                locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

                docsText.setTextColor(getResources().getColor(R.color.white));
                docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));
                break;

            case R.id.image1:
                if(!isImage1Available) {
                    imageSelected = 1;
                    deleteImageName = image1Id;
                    showtwoButtonsAlertDialog();
                }
                break;

            case R.id.image2:
                if(!isImage2Available) {
                    imageSelected = 2;
                    deleteImageName = image2Id;
                    showtwoButtonsAlertDialog();
                }
                break;

            case R.id.image3:
                if(!isImage3Available) {
                    imageSelected = 3;
                    deleteImageName = image3Id;
                    showtwoButtonsAlertDialog();
                }
                break;

            case R.id.image4:
                if(!isImage4Available) {
                    imageSelected = 4;
                    deleteImageName = image4Id;
                    showtwoButtonsAlertDialog();
                }
                break;

            case R.id.logo:
                if(!isLogoAvailable) {
                    imageSelected = 5;
                    deleteImageName = logoId;
                    showtwoButtonsAlertDialog();
                }
                break;

            case R.id.vat_image:
                if(!isVatAvailable) {
                    imageSelected = 6;
                    deleteImageName =  vatId;
                    showtwoButtonsAlertDialog();
                }
                break;

            case R.id.cr_image:
                if(!isCRAvailable) {
                    imageSelected = 7;
                    deleteImageName = crId;
                    showtwoButtonsAlertDialog();
                }
                break;

            case R.id.delete_image1:
                deleteImageName = image1Id;
                imageSelected = 1;
                showtwoButtonsAlertDialog();
                break;

            case R.id.delete_image2:
                deleteImageName = image2Id;
                imageSelected = 2;
                showtwoButtonsAlertDialog();
                break;

            case R.id.delete_image3:
                deleteImageName = image3Id;
                imageSelected = 3;
                showtwoButtonsAlertDialog();
                break;

            case R.id.delete_image4:
                deleteImageName = image4Id;
                imageSelected = 4;
                showtwoButtonsAlertDialog();
                break;

            case R.id.delete_logo:
                deleteImageName = logoId;
                imageSelected = 5;
                showtwoButtonsAlertDialog();
                break;

            case R.id.delete_vat:
                deleteImageName = vatId;
                imageSelected = 6;
                showtwoButtonsAlertDialog();
                break;

            case R.id.delete_cr:
                deleteImageName = crId;
                imageSelected = 7;
                showtwoButtonsAlertDialog();
                break;

            case R.id.car_brands_layout:
                showBrandsDialog();
                break;

            case R.id.geo_location_layout:
                openPlacePicker();
                break;

            case R.id.name_edit:
                isDetailsUpdated = true;
                inputName.setEnabled(true);
                inputName.setSelection(inputName.getText().toString().length());
                nameEdit.setVisibility(View.INVISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                break;

            case R.id.name_ar_edit:
                isDetailsUpdated = true;
                inputNameAr.setEnabled(true);
                inputNameAr.setSelection(inputNameAr.getText().toString().length());
                nameArEdit.setVisibility(View.INVISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                break;

            case R.id.email_edit:
                isDetailsUpdated = true;
                etEmail.setEnabled(true);
                etEmail.setSelection(etEmail.getText().toString().length());
                emailEdit.setVisibility(View.INVISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                break;

            case R.id.landline_edit:
                isDetailsUpdated = true;
                etLandline.setEnabled(true);
                etLandline.setSelection(etLandline.getText().toString().length());
                landlineEdit.setVisibility(View.INVISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                break;

            case R.id.description_edit:
                isDetailsUpdated = true;
                etDesc.setEnabled(true);
                etDesc.setSelection(etDesc.getText().toString().length());
                descEdit.setVisibility(View.INVISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                break;

            case R.id.description_ar_edit:
                isDetailsUpdated = true;
                etDescAr.setEnabled(true);
                etDescAr.setSelection(etDescAr.getText().toString().length());
                descArEdit.setVisibility(View.INVISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                break;

            case R.id.street_edit:
                isLocationUpdated = true;
                etStreet.setEnabled(true);
                etStreet.setSelection(etStreet.getText().toString().length());
                streetEdit.setVisibility(View.INVISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                break;

            case R.id.street_ar_edit:
                isLocationUpdated = true;
                etStreetAr.setEnabled(true);
                etStreetAr.setSelection(etStreetAr.getText().toString().length());
                streetArEdit.setVisibility(View.INVISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                break;

            case R.id.zipcode_edit:
                isLocationUpdated = true;
                etZipcode.setEnabled(true);
                etZipcode.setSelection(etZipcode.getText().toString().length());
                zipcodeEdit.setVisibility(View.INVISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                break;

            case R.id.update_btn:
                if(validations()) {
                    if (isDetailsUpdated) {
                        updateStage = 1;
                    }
                    if (isLocationUpdated) {
                        updateStage = 2;
                    }
                    if (isDetailsUpdated && isLocationUpdated) {
                        updateStage = 1;
                        pendingStages = 1;
                    }

                    String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new updateDetailsApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;

            case R.id.password_edit:
                Intent intent = new Intent(getActivity(), ChangePassword.class);
                intent.putExtra("mobile", data.get(0).getMobileNo());
                startActivity(intent);
                break;
        }
    }

    private void initView(){
        inputName.setText(data.get(0).getNameEn());
        inputNameAr.setText(data.get(0).getNameAr());
        etEmail.setText(data.get(0).getEmail());
        mobileEt.setText(data.get(0).getMobileNo().substring(3,12));
        inputPassword.setText("1234");
        try {
            etLandline.setText(data.get(0).getLandlineNo().substring(3,12));
        } catch (Exception e) {
            e.printStackTrace();
            etLandline.setText(data.get(0).getLandlineNo());
        }
        etDesc.setText(data.get(0).getAboutEn());
        etDescAr.setText(data.get(0).getAboutAr());
        etStreet.setText(data.get(0).getStreetNameEn());
        etStreetAr.setText(data.get(0).getStreetNameAr());
        etZipcode.setText(data.get(0).getZipCode());
        geoLocationEt.setText(data.get(0).getLatitude()+","+data.get(0).getLongitude());
        try {
            latitude = Double.parseDouble(data.get(0).getLatitude());
            longitude = Double.parseDouble(data.get(0).getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
        }

        int wImageSet = 1;
        for (int i = 0; i < data.get(0).getWorkshopdocuments().size(); i++){
            if(data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 1){
                logoId = data.get(0).getWorkshopdocuments().get(i).getId();
                Glide.with(ProfileFragment.this)
                        .load(Constants.WORKSHOP_IMAGES+data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                        .into(wLogoImage);
                logoDelete.setVisibility(View.VISIBLE);
                isLogoAvailable = true;
            }
            else if(data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 4){
                vatId = data.get(0).getWorkshopdocuments().get(i).getId();
                Glide.with(ProfileFragment.this)
                        .load(Constants.WORKSHOP_IMAGES+data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                        .into(wVatImage);
                vatDelete.setVisibility(View.VISIBLE);
                isVatAvailable = true;
            }
            else if(data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 3){
                crId = data.get(0).getWorkshopdocuments().get(i).getId();
                Glide.with(ProfileFragment.this)
                        .load(Constants.WORKSHOP_IMAGES+data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                        .into(wCRImage);
                crDelete.setVisibility(View.VISIBLE);
                isCRAvailable = true;
            }
            else if(data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 2){
                if(wImageSet == 1) {
                    image1Id = data.get(0).getWorkshopdocuments().get(i).getId();
                    Glide.with(ProfileFragment.this)
                            .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                            .into(wImage1);
                    wImageSet = wImageSet + 1;
                    image1Delete.setVisibility(View.VISIBLE);
                    isImage1Available = true;
                }
                else if(wImageSet == 2) {
                    image2Id = data.get(0).getWorkshopdocuments().get(i).getId();
                    Glide.with(ProfileFragment.this)
                            .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                            .into(wImage2);
                    wImageSet = wImageSet + 1;
                    image2Delete.setVisibility(View.VISIBLE);
                    isImage2Available = true;
                }
                else if(wImageSet == 3) {
                    image3Id = data.get(0).getWorkshopdocuments().get(i).getId();
                    Glide.with(ProfileFragment.this)
                            .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                            .into(wImage3);
                    wImageSet = wImageSet + 1;
                    image3Delete.setVisibility(View.VISIBLE);
                    isImage3Available = true;
                }
                else if(wImageSet == 4) {
                    image4Id = data.get(0).getWorkshopdocuments().get(i).getId();
                    Glide.with(ProfileFragment.this)
                            .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                            .into(wImage4);
                    wImageSet = wImageSet + 1;
                    image4Delete.setVisibility(View.VISIBLE);
                    isImage4Available = true;
                }
            }
        }
        if(loaderDialog != null){
            loaderDialog.dismiss();
        }
    }

    private void openPlacePicker() {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            Intent intent = intentBuilder.build(getActivity());
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private class getDetailsApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            Log.d("TAG", "inputStr: "+inputStr);
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<GetListResponse> call = apiService.getList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<GetListResponse>() {
                @Override
                public void onResponse(Call<GetListResponse> call, Response<GetListResponse> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        GetListResponse GetListResponse = response.body();
                        try {
                            if(GetListResponse.getStatus()){
                                CarMakerList.clear();
                                GetListResponse.CarMakerList car = new GetListResponse.CarMakerList();
//                                car.setChecked(false);
                                car.setNameEn("Select All");
                                car.setNameAr("اختر الكل");
                                car.setId(0);
                                CarMakerList.add(car);

                                CarMakerList.addAll(GetListResponse.getData().getCarMakerList());
                                DistrictList = GetListResponse.getData().getDistrictList();
                                CityList = GetListResponse.getData().getCityList();
                                CountryList = GetListResponse.getData().getCountryList();
                                setSpinner();

                                for (int j = 0; j < CarMakerList.size(); j++) {
                                    if (language.equalsIgnoreCase("En")) {
                                        if (CarMakerList.get(j).getChecked() && cars.length() == 0) {
                                            cars = CarMakerList.get(j).getNameEn();
                                        } else if (CarMakerList.get(j).getChecked() && cars.length() > 0) {
                                            cars = cars + ", " + CarMakerList.get(j).getNameEn();
                                        }
                                    }
                                    else {
                                        if (CarMakerList.get(j).getChecked() && cars.length() == 0) {
                                            cars = CarMakerList.get(j).getNameAr();
                                        } else if (CarMakerList.get(j).getChecked() && cars.length() > 0) {
                                            cars = cars + ", " + CarMakerList.get(j).getNameAr();
                                        }
                                    }
                                }
                                brandsSelected.setText(cars);
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En"))
                                {
                                    String failureResponse = GetListResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                else {
                                    String failureResponse = GetListResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GetListResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("Id", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private void setSpinner(){
        final ArrayList<CountryFilteredData.Cities> cityData = new ArrayList<>();
        final ArrayList<CountryFilteredData.Districts> districtData = new ArrayList<>();
        for (int i = 0; i < CountryList.size(); i++){
            if(CountryList.get(i).getId() == data.get(0).getCountry().get(0).getId()) {
                countryPos = i;
            }
            CountryFilteredData brand = new CountryFilteredData();
            brand.setId(CountryList.get(i).getId());
            brand.setNameEn(CountryList.get(i).getNameEn());
            brand.setNameAr(CountryList.get(i).getNameAr());
            Log.d(TAG, "country: "+i+","+CountryList.get(i).getNameEn());
            ArrayList<Integer> dummyCityList = new ArrayList<>();
            ArrayList<CountryFilteredData.Cities> cityList = new ArrayList<>();
            for (int j = 0; j < CityList.size(); j++){
                if(!dummyCityList.contains(CityList.get(j).getId())){
                    if(CityList.get(j).getCountryId() == (CountryList.get(i).getId())) {
                        Log.d(TAG, "CityList: "+i+","+CityList.get(j).getNameEn());
                        dummyCityList.add(CityList.get(j).getId());
                        CountryFilteredData.Cities city = new CountryFilteredData.Cities();
                        for (GetListResponse.CityList cityList1 : CityList) {
                            if (CityList.get(j).getId() == (cityList1.getId())) {
                                city.setNameAr(cityList1.getNameAr());
                                city.setNameEn(cityList1.getNameEn());
                                city.setId(cityList1.getId());

                                ArrayList<CountryFilteredData.Districts> districtArrayList = new ArrayList<>();
                                for (int k = 0; k < DistrictList.size(); k++) {
                                    if(CountryList.get(i).getId() == (CityList.get(j).getCountryId())) {
                                        if (DistrictList.get(k).getCityId() == (cityList1.getId())) {
                                            Log.d(TAG, "DistrictList: "+i+","+DistrictList.get(k).getNameEn());
                                            CountryFilteredData.Districts branch = new CountryFilteredData.Districts();

                                            branch.setNameEn(DistrictList.get(k).getNameEn());
                                            branch.setNameAr(DistrictList.get(k).getNameAr());
                                            branch.setId(DistrictList.get(k).getId());
                                            districtArrayList.add(branch);
                                        }
                                    }
                                }
                                city.setDistricts(districtArrayList);
                                break;
                            }
                        }
                        cityList.add(city);
                    }
                }
            }
            brand.setCities(cityList);
            countryFilteredData.add(brand);
        }

        Log.d(TAG, "setSpinner: "+countryPos+","+cityPos+","+districtPos);

        countryAdapter = new ArrayAdapter<CountryFilteredData>(getActivity(), R.layout.list_spinner, countryFilteredData) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                if (language.equalsIgnoreCase("En")){
                    ((TextView) v).setText(countryFilteredData.get(position).getNameEn());
                }
                else {
                    ((TextView) v).setText(countryFilteredData.get(position).getNameAr());
                }

                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
                if (language.equalsIgnoreCase("Ar")) {
                    ((TextView) v).setTypeface(Constants.getarTypeFace(getActivity()));
                }
                return v;
            }
        };

        cityData.addAll(countryFilteredData.get(countryPos).getCities());
        for (int j = 0; j < cityData.size(); j++) {
            if (cityData.get(j).getId() == data.get(0).getCity().get(0).getId()) {
                cityPos = j;
            }
        }
        cityAdapter = new ArrayAdapter<CountryFilteredData.Cities>(getActivity(), R.layout.list_spinner, cityData) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
//                Log.d(TAG, "getDropDownView: "+countryFilteredData.get(countryPos).getCities().get(position).getNameEn());

                try {
                    if (language.equalsIgnoreCase("En")){
                        ((TextView) v).setText(cityData.get(position).getNameEn());
                    }
                    else {
                        ((TextView) v).setText(cityData.get(position).getNameAr());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
                if (language.equalsIgnoreCase("Ar")) {
                    ((TextView) v).setTypeface(Constants.getarTypeFace(getActivity()));
                }
                return v;
            }
        };

        districtData.addAll(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts());
        for (int j = 0; j < districtData.size(); j++) {
            if(districtData.get(j).getId() == data.get(0).getDistrict().get(0).getId()) {
                districtPos = j;
            }
        }
        districtAdapter = new ArrayAdapter<CountryFilteredData.Districts>(getActivity(), R.layout.list_spinner, districtData) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                if (language.equalsIgnoreCase("En")){
                    ((TextView) v).setText(districtData.get(position).getNameEn());
                }
                else {
                    ((TextView) v).setText(districtData.get(position).getNameAr());
                }

                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
                if (language.equalsIgnoreCase("Ar")) {
                    ((TextView) v).setTypeface(Constants.getarTypeFace(getActivity()));
                }
                return v;
            }
        };

//        countrySpinner.setAdapter(countryAdapter);
        citySpiner.setAdapter(cityAdapter);
        districtSpinner.setAdapter(districtAdapter);

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!isFirstTime) {
                    if (countryPos != i) {
                        if (language.equalsIgnoreCase("En")){
                            etCountry.setText(countryFilteredData.get(i).getNameEn());
                        }
                        else {
                            etCountry.setText(countryFilteredData.get(i).getNameAr());
                        }

                        countryPos = i;
                        isLocationUpdated = true;
                        updateBtn.setVisibility(View.VISIBLE);
                        cityData.clear();
                        cityData.addAll(countryFilteredData.get(countryPos).getCities());
                        citySpiner.setAdapter(cityAdapter);
                        cityAdapter.notifyDataSetChanged();
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            etCountry.setText(countryFilteredData.get(i).getNameEn());
                        }
                        else {
                            etCountry.setText(countryFilteredData.get(i).getNameAr());
                        }
                    }
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        etCountry.setText(countryFilteredData.get(countryPos).getNameEn());
                    }
                    else {
                        etCountry.setText(countryFilteredData.get(countryPos).getNameAr());
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!isFirstTime) {
                    if (cityPos != i) {
                        if (language.equalsIgnoreCase("En")){
                            etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameEn());
                        }
                        else {
                            etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameAr());
                        }

                        cityPos = i;
                        isLocationUpdated = true;
                        updateBtn.setVisibility(View.VISIBLE);
                        districtData.clear();
                        districtData.addAll(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts());
                        districtSpinner.setAdapter(districtAdapter);
                        districtAdapter.notifyDataSetChanged();
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameEn());
                        }
                       else {
                            etCity.setText(countryFilteredData.get(countryPos).getCities().get(i).getNameAr());
                        }
                    }
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        etCity.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getNameEn());
                    }
                    else {
                        etCity.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getNameAr());
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!isFirstTime) {
                    if (districtPos != i) {
                        if (language.equalsIgnoreCase("En")){
                            etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameEn());
                        }
                        else {
                            etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameAr());
                        }
                        districtPos = i;
                        isLocationUpdated = true;
                        updateBtn.setVisibility(View.VISIBLE);
//                        districtAdapter.notifyDataSetChanged();
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameEn());

                        }
                        else {
                            etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(i).getNameAr());

                        }
                    }
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(districtPos).getNameEn());
                    }
                  else   {
                        etDistrcit.setText(countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(districtPos).getNameAr());
                    }
                    isFirstTime = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
                else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), "Storage permission denied, Unable to select pictures", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "تم رفض  التخزين ، غير قادر على تحديد الصور", Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case CAMERA_REQUEST:
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                break;
        }
    }

    public void showtwoButtonsAlertDialog(){
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            }
            else if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
            }
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.images_alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        TextView cancel = (TextView) dialogView.findViewById(R.id.cancel_btn);
        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(getContext()));
            desc.setTypeface(Constants.getarTypeFace(getContext()));
            yes.setTypeface(Constants.getarTypeFace(getContext()));
            no.setTypeface(Constants.getarTypeFace(getContext()));
            title.setText("سمكره");
            no.setText("إستديو الصور");
            yes.setText("كاميرا");
            desc.setText("اختار من الخيارات التالية");
            cancel.setText("الرجاء قبول");
        }
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, PICK_IMAGE_FROM_CAMERA);

                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getContext().getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, PICK_IMAGE_FROM_CAMERA);
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                customDialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                final Place place = PlacePicker.getPlace(getActivity(), data);
                // TODO call location based filter
                LatLng latLong;
                latLong = place.getLatLng();
                latitude = latLong.latitude;
                longitude = latLong.longitude;
                geoLocationEt.setText(latitude+","+longitude);

                isLocationUpdated = true;
                updateBtn.setVisibility(View.VISIBLE);

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {

        } else if (resultCode == RESULT_CANCELED) {

        }

        if(requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();

            try {
                if(imageSelected == 1) {
                    thumbnail1 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                }
                else if(imageSelected == 2) {
                    thumbnail2 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                }
                else if(imageSelected == 3) {
                    thumbnail3 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                }
                else if(imageSelected == 4) {
                    thumbnail4 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                }
                else if(imageSelected == 5) {
                    thumbnail5 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                }
                else if(imageSelected == 6) {
                    thumbnail6 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                }
                else if(imageSelected == 7) {
                    thumbnail7 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            convertGalleryImages();
            if(deleteImageName != 0) {
                isGalleryImage = true;
                new deleteImageApi().execute();
            }
            else{
                convertGalleryImages();
            }
        }
        else if(requestCode == PICK_IMAGE_FROM_CAMERA && resultCode == RESULT_OK) {
//            Uri uri = data.getData();
            if(deleteImageName != 0) {
                isGalleryImage = false;
                new deleteImageApi().execute();
            }
            else{
                convertCameraImages();
            }
//            convertGalleryImages();
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void convertCameraImages() {
        String path = getRealPathFromURI(imageUri);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inScaled = false;

        if(imageSelected == 1) {
            isImagesUploaded = true;

            thumbnail1 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail1, 800,
                    800, true);
            thumbnail1 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail1);
            wImage1.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail1, "image", 1);
        }
        else if(imageSelected == 2) {
            isImagesUploaded = true;
            thumbnail2 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail2, 800,
                    800, true);
            thumbnail2 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail2);
            wImage2.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail2, "image", 2);
        }
        else if(imageSelected == 3) {
            isImagesUploaded = true;
            thumbnail3 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail3, 800,
                    800, true);
            thumbnail3 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail3);
            wImage3.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail3, "image", 3);
        }
        else if(imageSelected == 4) {
            isImagesUploaded = true;
            thumbnail4 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail4, 800,
                    800, true);
            thumbnail4 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail4);
            wImage4.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail4, "image", 4);
        }
        else if(imageSelected == 5) {
            isLogoUploaded = true;
            thumbnail5 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail5, 800,
                    800, true);
            thumbnail5 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail5);
            wLogoImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail5, "logo", 5);
        }
        else if(imageSelected == 6) {
            isVatUploaded = true;
            thumbnail6 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail6, 800,
                    800, true);
            thumbnail6 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail6);
            wVatImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail6, "vat", 6);
        }
        else if(imageSelected == 7) {
            isCRUploaded = true;
            thumbnail7 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail7, 800,
                    800, true);
            thumbnail7 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail7);
            wCRImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail7, "cr", 7);
        }
        updateStage = 3;
        pendingStages = 0;
        new updateDetailsApi().execute();
    }

    private void convertGalleryImages() {
        if(imageSelected == 1) {
            isImagesUploaded = true;
            thumbnail1 = Bitmap.createScaledBitmap(thumbnail1, 800, 800,
                    false);
            thumbnail1 = codec(thumbnail1, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail1);
            wImage1.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail1, "image", 1);
        }
        else if(imageSelected == 2) {
            isImagesUploaded = true;
            thumbnail2 = Bitmap.createScaledBitmap(thumbnail2, 800, 800,
                    false);
            thumbnail2 = codec(thumbnail2, Bitmap.CompressFormat.PNG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail2);
            wImage2.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail2, "image", 2);
        }
        else if(imageSelected == 3) {
            isImagesUploaded = true;
            thumbnail3 = Bitmap.createScaledBitmap(thumbnail3, 800, 800,
                    false);
            thumbnail3 = codec(thumbnail3, Bitmap.CompressFormat.PNG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail3);
            wImage3.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail3, "image", 3);
        }
        else if(imageSelected == 4) {
            isImagesUploaded = true;
            thumbnail4 = Bitmap.createScaledBitmap(thumbnail4, 800, 800,
                    false);
            thumbnail4 = codec(thumbnail4, Bitmap.CompressFormat.PNG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail4);
            wImage4.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail4, "image", 4);
        }
        else if(imageSelected == 5) {
            isLogoUploaded = true;
            thumbnail5 = Bitmap.createScaledBitmap(thumbnail5, 800, 800,
                    false);
            thumbnail5 = codec(thumbnail5, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail5);
            wLogoImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail5, "logo", 5);
        }
        else if(imageSelected == 6) {
            isVatUploaded = true;
            thumbnail6 = Bitmap.createScaledBitmap(thumbnail6, 800, 800,
                    false);
            thumbnail6 = codec(thumbnail6, Bitmap.CompressFormat.PNG, 0);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail6);
            wVatImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail6, "vat", 6);
        }
        else if(imageSelected == 7) {
            isCRUploaded = true;
            thumbnail7 = Bitmap.createScaledBitmap(thumbnail7, 800, 800,
                    false);
            thumbnail7 = codec(thumbnail7, Bitmap.CompressFormat.PNG, 100);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail7);
            wCRImage.setImageDrawable(drawable);
            imagePath1 = StoreByteImage(thumbnail7, "cr", 7);
        }
        updateStage = 3;
        pendingStages = 0;
        new updateDetailsApi().execute();
    }

    public String StoreByteImage(Bitmap bitmap, String type, int num) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(new Date());
        String pictureName = "";
        if(type.equals("image") ) {
            pictureName = "IMG_" + userId + "_" + timeStamp + ".jpg";
            if(imagesName.length() == 0){
                imagesName = pictureName;
            }
            else {
                imagesName = imagesName + ","+ pictureName;
            }
        }
        else if(type.equals("logo") ) {
            pictureName = "LOGO_" + userId + "_" + timeStamp + ".jpg";
            logoName = pictureName;
        }
        else if(type.equals("cr") ) {
            pictureName = "CR_" + userId + "_" + timeStamp + ".jpg";
            crName = pictureName;
        }
        else if(type.equals("vat") ) {
            pictureName = "VAT_" + userId + "_" + timeStamp + ".jpg";
            vatName = pictureName;
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        if(num == 1){
            image1Bytes = stream.toByteArray();
            image1Str = Base64.encodeToString(image1Bytes, Base64.DEFAULT);
        }
        else if(num == 2){
            image2Bytes = stream.toByteArray();
            image2Str = Base64.encodeToString(image2Bytes, Base64.DEFAULT);
        }
        else if(num == 3){
            image3Bytes = stream.toByteArray();
            image3Str = Base64.encodeToString(image3Bytes, Base64.DEFAULT);
        }
        else if(num == 4){
            image4Bytes = stream.toByteArray();
            image4Str = Base64.encodeToString(image4Bytes, Base64.DEFAULT);
        }
        else if(num == 5){
            logoBytes = stream.toByteArray();
            logoStr = Base64.encodeToString(logoBytes, Base64.DEFAULT);
        }
        else if(num == 6){
            vatBytes = stream.toByteArray();
            vatStr = Base64.encodeToString(vatBytes, Base64.DEFAULT);
        }
        else if(num == 7){
            crBytes = stream.toByteArray();
            crStr = Base64.encodeToString(crBytes, Base64.DEFAULT);
        }

        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "samkra");
        folder.mkdir();

        File sdImageMainDirectory = new File(folder, pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

			/*Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,

			imageData.length,options);*/
            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.PNG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    private class deleteImageApi extends AsyncTask<String, String, String> {


        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
          showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ImageDeleteResponse> call = apiService.DeleteWorkhopImage(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ImageDeleteResponse>() {
                @Override
                public void onResponse(Call<ImageDeleteResponse> call, Response<ImageDeleteResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()){
                        ImageDeleteResponse signInResponse = response.body();
                        try {
                            if(signInResponse.getStatus()){
//                                if(loaderDialog != null){
//                                    loaderDialog.dismiss();
//                                }
                                deleteImageName = 0;
                                if (isGalleryImage) {
                                    convertGalleryImages();
                                }
                                else {
                                    convertCameraImages();
                                }
                            }
                            else {
                                // status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = signInResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                else {
                                    String failureResponse = signInResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

//                    if(loaderDialog != null){
//                        loaderDialog.dismiss();
//                    }
                }

                @Override
                public void onFailure(Call<ImageDeleteResponse> call, Throwable t) {
                    Log.d(TAG, "onResponse: "+t.toString());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson(){
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("workshopId", userId);
            mobileObj.put("ImageId", deleteImageName);
            Log.d(TAG, "prepareVerifyMobileJson: "+mobileObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private void showBrandsDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        final LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.car_brands_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        final LinearLayout brands = (LinearLayout) dialogView.findViewById(R.id.brands_layout);


        if (language.equalsIgnoreCase("Ar")){
            no.setText(getResources().getString(R.string.no_ar));
            yes.setText(getResources().getString(R.string.ok_ar));
            title.setText(getResources().getString(R.string.car_you_fix_ar));
            no.setTypeface(Constants.getarTypeFace(getContext()));
            yes.setTypeface(Constants.getarTypeFace(getContext()));
            title.setTypeface(Constants.getarTypeFace(getContext()));
        }

        for (int i = 0; i < CarMakerList.size(); i++) {
            View v = inflater.inflate(R.layout.list_car_brands, null);
            CheckBox checkBox = v.findViewById(R.id.checkbox);
            if (language.equalsIgnoreCase("En")){
                checkBox.setText(CarMakerList.get(i).getNameEn());
            }
            else {
                checkBox.setText(CarMakerList.get(i).getNameAr());
            }
            if(CarMakerList.get(i).getChecked()){
                checkBox.setChecked(true);
            }

            final int finalI = i;
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(finalI == 0) {
                        if (b) {
                            for (int i = 0; i < CarMakerList.size(); i++) {
                                CarMakerList.get(i).setChecked(true);
                            }
                        } else {
                            for (int i = 0; i < CarMakerList.size(); i++) {
                                CarMakerList.get(i).setChecked(false);
                            }
                        }
                        brands.removeAllViews();
                        for (int i = 0; i < CarMakerList.size(); i++) {
                            View v = inflater.inflate(R.layout.list_car_brands, null);
                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                            if (language.equalsIgnoreCase("En")){
                                checkBox.setText(CarMakerList.get(i).getNameEn());
                            }
                            else {
                                checkBox.setText(CarMakerList.get(i).getNameAr());
                            }
                            if(CarMakerList.get(i).getChecked()){
                                checkBox.setChecked(true);
                            }

                            final int finalI = i;
                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if(finalI == 0) {
                                        if (b) {
                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                CarMakerList.get(i).setChecked(true);
                                            }
                                        } else {
                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                CarMakerList.get(i).setChecked(false);
                                            }
                                        }
                                        brands.removeAllViews();
                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                            if (language.equalsIgnoreCase("En")){
                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                            }
                                            else {
                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                            }
                                            if(CarMakerList.get(i).getChecked()){
                                                checkBox.setChecked(true);
                                            }

                                            final int finalI = i;
                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                @Override
                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                    if(finalI == 0) {
                                                        if (b) {
                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                CarMakerList.get(i).setChecked(true);
                                                            }
                                                        } else {
                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                CarMakerList.get(i).setChecked(false);
                                                            }
                                                        }
                                                        brands.removeAllViews();
                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                            if (language.equalsIgnoreCase("En")){
                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                            }
                                                            else {
                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                            }
                                                            if(CarMakerList.get(i).getChecked()){
                                                                checkBox.setChecked(true);
                                                            }

                                                            final int finalI = i;
                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                @Override
                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                    if(finalI == 0) {
                                                                        if (b) {
                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                CarMakerList.get(i).setChecked(true);
                                                                            }
                                                                        } else {
                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                CarMakerList.get(i).setChecked(false);
                                                                            }
                                                                        }
                                                                        brands.removeAllViews();
                                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                                            if (language.equalsIgnoreCase("En")){
                                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                                            }
                                                                            else {
                                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                                            }
                                                                            if(CarMakerList.get(i).getChecked()){
                                                                                checkBox.setChecked(true);
                                                                            }

                                                                            final int finalI = i;
                                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                                @Override
                                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                                    if(finalI == 0) {
                                                                                        if (b) {
                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                CarMakerList.get(i).setChecked(true);
                                                                                            }
                                                                                        } else {
                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                CarMakerList.get(i).setChecked(false);
                                                                                            }
                                                                                        }
                                                                                        brands.removeAllViews();
                                                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                                                            if (language.equalsIgnoreCase("En")){
                                                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                                                            }
                                                                                            else {
                                                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                                                            }
                                                                                            if(CarMakerList.get(i).getChecked()){
                                                                                                checkBox.setChecked(true);
                                                                                            }

                                                                                            final int finalI = i;
                                                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                                                @Override
                                                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                                                    if(finalI == 0) {
                                                                                                        if (b) {
                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                CarMakerList.get(i).setChecked(true);
                                                                                                            }
                                                                                                        } else {
                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                CarMakerList.get(i).setChecked(false);
                                                                                                            }
                                                                                                        }
                                                                                                        brands.removeAllViews();
                                                                                                        for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                            View v = inflater.inflate(R.layout.list_car_brands, null);
                                                                                                            CheckBox checkBox = v.findViewById(R.id.checkbox);
                                                                                                            if (language.equalsIgnoreCase("En")){
                                                                                                                checkBox.setText(CarMakerList.get(i).getNameEn());
                                                                                                            }
                                                                                                            else {
                                                                                                                checkBox.setText(CarMakerList.get(i).getNameAr());
                                                                                                            }
                                                                                                            if(CarMakerList.get(i).getChecked()){
                                                                                                                checkBox.setChecked(true);
                                                                                                            }

                                                                                                            final int finalI = i;
                                                                                                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                                                                @Override
                                                                                                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                                                                                    if(finalI == 0) {
                                                                                                                        if (b) {
                                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                                CarMakerList.get(i).setChecked(true);
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            for (int i = 0; i < CarMakerList.size(); i++) {
                                                                                                                                CarMakerList.get(i).setChecked(false);
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                    else {
                                                                                                                        if (b) {
                                                                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                                                                        } else {
                                                                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            });
                                                                                                            brands.addView(v);
                                                                                                        }
                                                                                                    }
                                                                                                    else {
                                                                                                        if (b) {
                                                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                                                        } else {
                                                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                            brands.addView(v);
                                                                                        }
                                                                                    }
                                                                                    else {
                                                                                        if (b) {
                                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                                        } else {
                                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                            brands.addView(v);
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (b) {
                                                                            CarMakerList.get(finalI).setChecked(true);
                                                                        } else {
                                                                            CarMakerList.get(finalI).setChecked(false);
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                            brands.addView(v);
                                                        }
                                                    }
                                                    else {
                                                        if (b) {
                                                            CarMakerList.get(finalI).setChecked(true);
                                                        } else {
                                                            CarMakerList.get(finalI).setChecked(false);
                                                        }
                                                    }
                                                }
                                            });
                                            brands.addView(v);
                                        }
                                    }
                                    else {
                                        if (b) {
                                            CarMakerList.get(finalI).setChecked(true);
                                        } else {
                                            CarMakerList.get(finalI).setChecked(false);
                                        }
                                    }
                                }
                            });
                            brands.addView(v);
                        }
                    }
                    else {
                        if (b) {
                            CarMakerList.get(finalI).setChecked(true);
                        } else {
                            CarMakerList.get(finalI).setChecked(false);
                        }
                    }
                }
            });
            brands.addView(v);
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cars = "";
                boolean isAllSelected = false;
                for (int j = 0; j < CarMakerList.size(); j++) {
                    if (j != 0) {
                        if (CarMakerList.get(j).getChecked() && cars.length() == 0) {
                            if (language.equalsIgnoreCase("En")) {
                                cars = CarMakerList.get(j).getNameEn();
                            } else {
                                cars = CarMakerList.get(j).getNameAr();
                            }
                        } else if (CarMakerList.get(j).getChecked() && cars.length() > 0) {
                            if (language.equalsIgnoreCase("En")) {
                                cars = cars + ", " + CarMakerList.get(j).getNameEn();
                            } else {
                                cars = cars + ", " + CarMakerList.get(j).getNameAr();
                            }
                        }
                    }
                }
                brandsSelected.setText(cars);
                brandsDialog.dismiss();
                isDetailsUpdated = true;
                updateBtn.setVisibility(View.VISIBLE);

            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                cars = "";
//                for (int j = 0; j < CarMakerList.size(); j++){
//                    if (j != 0) {
//                        if (CarMakerList.get(j).getChecked() && cars.length() == 0) {
//                            if (language.equalsIgnoreCase("En")) {
//                                cars = CarMakerList.get(j).getNameEn();
//                            } else {
//                                cars = CarMakerList.get(j).getNameAr();
//                            }
//                        } else if (CarMakerList.get(j).getChecked() && cars.length() > 0) {
//                            if (language.equalsIgnoreCase("En")) {
//                                cars = cars + ", " + CarMakerList.get(j).getNameEn();
//                            } else {
//                                cars = cars + ", " + CarMakerList.get(j).getNameAr();
//                            }
//                        }
//                    }
//                }
//                brandsSelected.setText(cars);
                brandsDialog.dismiss();
            }
        });

        brandsDialog = dialogBuilder.create();
        brandsDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = brandsDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private String prepareUpdateJson() {
        String ids = null;
        Log.d(TAG, "updateStage: "+updateStage);
        try {
            ids = "";
            for (int j = 0; j < CarMakerList.size(); j++){
                if(CarMakerList.get(j).getChecked() && ids.length() == 0){
                    ids = Integer.toString(CarMakerList.get(j).getId());
                }
                else if(CarMakerList.get(j).getChecked() && cars.length() > 0){
                    ids = ids + ", "+ Integer.toString(CarMakerList.get(j).getId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject mobileObj = new JSONObject();
        JSONArray docsArray = new JSONArray();
        if(updateStage == 1) {
            try {
                mobileObj.put("Id", userId);
                mobileObj.put("NameEn", strName);
                mobileObj.put("NameAr", strNameAr);
                mobileObj.put("AboutEn", strDesc);
                mobileObj.put("AboutAr", strDescAr);
                mobileObj.put("EmailID", strEmail);
                mobileObj.put("LandLineNo", "966"+strLandline);
                mobileObj.put("SupportedCarMakerList", ids);
                mobileObj.put("profileUpdateStages", "1");
                Log.d(TAG, "prepareUpdateJson: "+mobileObj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(updateStage == 2) {
            try {
                mobileObj.put("Id", userId);
                mobileObj.put("StreetNameEn", strStreet);
                mobileObj.put("StreetNameAr", strStreetAr);
                mobileObj.put("CountryID", countryFilteredData.get(countryPos).getId());
                mobileObj.put("CityID", countryFilteredData.get(countryPos).getCities().get(cityPos).getId());
                mobileObj.put("DistrictID", countryFilteredData.get(countryPos).getCities().get(cityPos).getDistricts().get(districtPos).getId());
                mobileObj.put("Zipcode", strZipcode);
                mobileObj.put("Latitude", latitude);
                mobileObj.put("Longitude", longitude);
                mobileObj.put("profileUpdateStages", "2");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(updateStage == 3) {
            try {
                mobileObj.put("Id", userId);
                mobileObj.put("WorkshopLogo", logoName);
                mobileObj.put("diclogo", logoStr);
                mobileObj.put("WorkshopImg", imagesName);
                if(thumbnail1 != null){
                    docsArray.put(image1Str);
                }
                if(thumbnail2 != null){
                    docsArray.put(image2Str);
                }
                if(thumbnail3 != null){
                    docsArray.put(image3Str);
                }
                if(thumbnail4 != null){
                    docsArray.put(image4Str);
                }
                mobileObj.put("dicimg",docsArray);
                mobileObj.put("CRD", crName);
                mobileObj.put("dicCr", crStr);
                mobileObj.put("VatCertificate", vatName);
                mobileObj.put("dicVat", vatStr);
                mobileObj.put("profileUpdateStages", "3");
                userPrefsEditor.putString("pic", logoName);
                userPrefsEditor.commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mobileObj.toString();
    }

    private class updateDetailsApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUpdateJson();
            Log.d(TAG, "inputStr for update profile: "+inputStr);
        showloaderAlertDialogForUpdate();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UpdateDetailsResponse> call = apiService.updateDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateDetailsResponse>() {
                @Override
                public void onResponse(Call<UpdateDetailsResponse> call, Response<UpdateDetailsResponse> response) {
                    Log.d(TAG, "onResponse for update profile: "+response);
                    if(response.isSuccessful()){
                        UpdateDetailsResponse UpdateDetailsResponse = response.body();
                        try {
                            if(UpdateDetailsResponse.getStatus()){
                                if(updateStage == 1){
                                    if(pendingStages == 1){
                                        updateStage = 2;
                                        pendingStages = 0;
                                        new updateDetailsApi().execute();
                                    }
                                    else {
                                        updateStage = 0;
                                        pendingStages = 0;
                                    }
                                }
                                else if(updateStage == 2){
                                    updateStage = 0;
                                    pendingStages = 0;
                                }
                                else if(updateStage == 3){
                                    logoName = "";
                                    crName = "";
                                    vatName = "";
                                    imagesName = "";
                                    image1Str = "";
                                    image2Str = "";
                                    image3Str = "";
                                    image4Str = "";
                                    logoStr = "";
                                    vatStr = "";
                                    crStr = "";
                                }
                                if(loaderDialog != null){
                                    loaderDialog.dismiss();
                                }
                                resetViews();

                                new dashboardApi().execute();
                            }
                            else {
                                // status false case
                                if(language.equalsIgnoreCase("En")) {
                                    String failureResponse = UpdateDetailsResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                else {
                                    String failureResponse = UpdateDetailsResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialogForUpdate != null){
                        loaderDialogForUpdate.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UpdateDetailsResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialogForUpdate != null){
                        loaderDialogForUpdate.dismiss();
                    }
                }
            });

            if(loaderDialogForUpdate != null){
                loaderDialogForUpdate.dismiss();
            }

            return null;
        }
    }

    private boolean validations(){
        strName = inputName.getText().toString();
        strNameAr = inputNameAr.getText().toString();
        strEmail = etEmail.getText().toString().trim();
        strLandline = etLandline.getText().toString().trim();
        strDesc = etDesc.getText().toString();
        strDescAr = etDescAr.getText().toString();
        strLandline = strLandline.replace("+966 ","");
        strStreet = etStreet.getText().toString();
        strStreetAr = etStreetAr.getText().toString();
        strZipcode = etZipcode.getText().toString();


        if(strName.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            }
            else {
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
            }
            inputName.requestFocus();
            return false;
        }
        else if(strNameAr.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            }
            else {
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
            }
            inputName.requestFocus();
            return false;
        }
        else if(strEmail.length()> 0 && !Constants.isValidEmail(strEmail)){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
            }
            else {
                etEmail.setError(getResources().getString(R.string.signup_msg_invalid_email_ar));
            }
            etEmail.requestFocus();
            return false;
        }
        else if(strLandline.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etLandline.setError(getResources().getString(R.string.signup_msg_enter_landline));
            }
            else {
                etLandline.setError(getResources().getString(R.string.signup_msg_enter_landline_ar));
            }

            etLandline.requestFocus();
            return false;
        }
        else if(strLandline.length() != 9){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etLandline.setError(getResources().getString(R.string.signup_msg_valid_landline));
            }
            else {
                etLandline.setError(getResources().getString(R.string.signup_msg_valid_landline_ar));
            }

            etLandline.requestFocus();
            return false;
        }
        else if(strDesc.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etDesc.setError(getResources().getString(R.string.signup_msg_enter_about));
            }
            else {
                etDesc.setError(getResources().getString(R.string.signup_msg_enter_about_ar));
            }

            etDesc.requestFocus();
            return false;
        }
        else if(strDescAr.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                etDescAr.setError(getResources().getString(R.string.signup_msg_enter_about));
            }
            else {
                etDescAr.setError(getResources().getString(R.string.signup_msg_enter_about_ar));
            }

            etDescAr.requestFocus();
            return false;
        }
        else if(cars.length() == 0){
            detailsVisible();
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_cars),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), getActivity());
            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_cars_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), getActivity());
            }

            return false;
        }
        else if(strStreet.length() == 0){
            locationVisible();
            if (language.equalsIgnoreCase("En")){
                etStreet.setError(getResources().getString(R.string.signup_msg_enter_street));
            }
            else {
                etStreetAr.setError(getResources().getString(R.string.signup_msg_enter_street_ar));
            }

            etStreet.requestFocus();
            return false;
        }
        else if(strStreetAr.length() == 0){
            locationVisible();
            if (language.equalsIgnoreCase("En")){
                etStreet.setError(getResources().getString(R.string.signup_msg_enter_street));
            }
            else {
                etStreetAr.setError(getResources().getString(R.string.signup_msg_enter_street_ar));
            }
            etStreetAr.requestFocus();
            return false;
        }
        else if(strZipcode.length() == 0){
            locationVisible();
            if (language.equalsIgnoreCase("En")){
                etZipcode.setError(getResources().getString(R.string.signup_msg_enter_zipcode));
            }
            else {
                etZipcode.setError(getResources().getString(R.string.signup_msg_enter_zipcode_ar));
            }
            etZipcode.requestFocus();
            return false;
        }
        else if(latitude == 0 && longitude == 0){
            locationVisible();
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo),
                        getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), getActivity());
            } else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_select_geo_ar),
                        getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok_ar), getActivity());
            }
            return false;
        }
        return true;
    }

    private void detailsVisible(){
        detailsLayout.setVisibility(View.VISIBLE);
        locationLayout.setVisibility(View.GONE);
        documentsLayout.setVisibility(View.GONE);

        detailsText.setTextColor(getResources().getColor(R.color.white));
        detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));

        locationText.setTextColor(getResources().getColor(R.color.colorPrimary));
        locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

        docsText.setTextColor(getResources().getColor(R.color.colorPrimary));
        docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));
    }

    private void locationVisible(){
        detailsLayout.setVisibility(View.GONE);
        locationLayout.setVisibility(View.VISIBLE);
        documentsLayout.setVisibility(View.GONE);

        detailsText.setTextColor(getResources().getColor(R.color.colorPrimary));
        detailsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

        locationText.setTextColor(getResources().getColor(R.color.white));
        locationTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_selected));

        docsText.setTextColor(getResources().getColor(R.color.colorPrimary));
        docsTitleLayout.setBackground(getResources().getDrawable(R.drawable.shape_unselected));

    }

    private void resetViews(){
        isDetailsUpdated = false;
        isLocationUpdated = false;

        inputName.setEnabled(false);
        nameEdit.setVisibility(View.VISIBLE);

        inputNameAr.setEnabled(false);
        nameArEdit.setVisibility(View.VISIBLE);

        etEmail.setEnabled(false);
        emailEdit.setVisibility(View.VISIBLE);

        etLandline.setEnabled(false);
        landlineEdit.setVisibility(View.VISIBLE);

        etDesc.setEnabled(false);
        descEdit.setVisibility(View.VISIBLE);

        etDescAr.setEnabled(false);
        descArEdit.setVisibility(View.VISIBLE);

        etStreet.setEnabled(false);
        streetEdit.setVisibility(View.VISIBLE);

        etStreetAr.setEnabled(false);
        streetArEdit.setVisibility(View.VISIBLE);

        etZipcode.setEnabled(false);
        zipcodeEdit.setVisibility(View.VISIBLE);
        updateBtn.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private class dashboardApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<DashboardResponse> call = apiService.GetDashBoard(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DashboardResponse>() {
                @Override
                public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                    if (response.isSuccessful()) {
                        DashboardResponse dashboardResponse = response.body();
                        try {
                            if (dashboardResponse.getStatus()) {
                                data.clear();
                                data.addAll(dashboardResponse.getData());
                                for (int i = 0; i < data.get(0).getWorkshopdocuments().size(); i++) {
                                    if (data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 1) {
                                        Glide.with(getActivity())
                                                .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                                                .into(MainActivity.profilePic);
                                    }
                                }
                                initView();
                                if(loaderDialog != null){
                                    loaderDialog.dismiss();
                                }
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = dashboardResponse.getMessage();
                                    Constants.showOneButtonAlertDialogWithFinish(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {
                                    String failureResponse = dashboardResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialogWithFinish(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            } else {
                                Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.error_ar),
                                        getResources().getString(R.string.ok_ar), getActivity());
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), getActivity());
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<DashboardResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.str_connection_error_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), getActivity());
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialogWithFinish(getResources().getString(R.string.cannot_reach_server_ar), getResources().getString(R.string.error_ar),
                                    getResources().getString(R.string.ok_ar), getActivity());
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("WorkshopId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
    }

    public void showloaderAlertDialogForUpdate(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        loaderDialogForUpdate = dialogBuilder.create();
        loaderDialogForUpdate.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialogForUpdate.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
    }
}