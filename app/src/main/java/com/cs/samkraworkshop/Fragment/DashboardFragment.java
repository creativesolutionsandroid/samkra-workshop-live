package com.cs.samkraworkshop.Fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Activity.MainActivity;
import com.cs.samkraworkshop.Activity.SignInActivity;
import com.cs.samkraworkshop.Activity.UpdateActivity;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.DashboardResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.samkraworkshop.Activity.MainActivity.drawer;

public class DashboardFragment extends Fragment implements View.OnClickListener {

    private TextView workShopName, cityName, bidCount, dlanguage;
    private TextView numOffers, numReviews, numAccepted;
    private TextView salesWeekly, salesMonthly, salesYearly;
    private RelativeLayout bidsLayout, inboxLayout, historyLayout, notificationsLayout;
    private LinearLayout offersLayout, acceptedLayout, doneLayout;
    private CircleImageView profilePic;
    private Typeface regular, light, condensed;
    View view;
    AlertDialog loaderDialog;
    ImageView sidemenu;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    String TAG = "TAG";
    public static ArrayList<DashboardResponse.Data> data = new ArrayList<>();
    String language;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        languagePrefsEditor = languagePrefs.edit();
        if (language.equalsIgnoreCase("En")) {
            view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            view = inflater.inflate(R.layout.fragment_dashboard_ar, container, false);
        }
        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        workShopName = (TextView) view.findViewById(R.id.name_workshop);
        cityName = (TextView) view.findViewById(R.id.city);

        bidsLayout = (RelativeLayout) view.findViewById(R.id.bids_layout);
        inboxLayout = (RelativeLayout) view.findViewById(R.id.inbox_layout);
        historyLayout = (RelativeLayout) view.findViewById(R.id.receipts_layout);
        notificationsLayout = (RelativeLayout) view.findViewById(R.id.notifications_layout);

        offersLayout = (LinearLayout) view.findViewById(R.id.offers_layout);
        acceptedLayout = (LinearLayout) view.findViewById(R.id.accepted_layout);
        doneLayout = (LinearLayout) view.findViewById(R.id.done_layout);

        numOffers = (TextView) view.findViewById(R.id.num_offers);
        numReviews = (TextView) view.findViewById(R.id.num_reviews);
        numAccepted = (TextView) view.findViewById(R.id.num_accepted);

        salesWeekly = (TextView) view.findViewById(R.id.sales_weekly);
        salesMonthly = (TextView) view.findViewById(R.id.sales_monthly);
        salesYearly = (TextView) view.findViewById(R.id.sales_yearly);

        bidCount = (TextView) view.findViewById(R.id.bid_count);
        dlanguage = (TextView) view.findViewById(R.id.language);
        sidemenu = (ImageView) view.findViewById(R.id.side_menu);

        profilePic = (CircleImageView) view.findViewById(R.id.profile_pic);
        setTypeFace(view);

//        setStatusBarGradiant(getActivity());
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getActivity().getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        }
        dlanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    getActivity().finish();
                } else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });

        sidemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuClick();
            }
        });

        bidsLayout.setOnClickListener(this);
        inboxLayout.setOnClickListener(this);
        historyLayout.setOnClickListener(this);
        notificationsLayout.setOnClickListener(this);
        offersLayout.setOnClickListener(this);
        acceptedLayout.setOnClickListener(this);
        doneLayout.setOnClickListener(this);
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment bidsFragment = new ProfileFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, bidsFragment).commit();
            }
        });
        if (language.equalsIgnoreCase("Ar")) {
            setTypeface();
        }
        return view;
    }

    private void setTypeface() {
        ((TextView) view.findViewById(R.id.num_accepted)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.accepted_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.reviews_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.num_reviews)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.offers_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.num_offers)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.sales_yearly)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.yearly_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.sales_monthly)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.monthly_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.sales_weekly)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.weekly_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.inbox_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.bid_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.notifications_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.receipt_text)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.name_workshop)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.sales_title)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.city)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.num_reviews)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) view.findViewById(R.id.num_offers)).setTypeface(Constants.getarbooldTypeFace(getActivity()));


    }

    @Override
    public void onResume() {
        super.onResume();
        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new dashboardApi().execute();
        } else {
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void setTypeFace(View view) {
        regular = Typeface.createFromAsset(getActivity().getAssets(),
                "helvetica_regualr.ttf");
        light = Typeface.createFromAsset(getActivity().getAssets(),
                "Helvetica_Condensed_Light.otf");
        condensed = Typeface.createFromAsset(getActivity().getAssets(),
                "Helvetica_Condensed_Bold.otf");

        workShopName.setTypeface(condensed);
        cityName.setTypeface(regular);

        numOffers.setTypeface(regular);
        numAccepted.setTypeface(regular);
        numReviews.setTypeface(regular);

        salesWeekly.setTypeface(condensed);
        salesMonthly.setTypeface(condensed);
        salesYearly.setTypeface(condensed);

        ((TextView) view.findViewById(R.id.offers_text)).setTypeface(regular);
        ((TextView) view.findViewById(R.id.reviews_text)).setTypeface(regular);
        ((TextView) view.findViewById(R.id.accepted_text)).setTypeface(regular);

        ((TextView) view.findViewById(R.id.sales_title)).setTypeface(regular);

        ((TextView) view.findViewById(R.id.weekly_text)).setTypeface(regular);
        ((TextView) view.findViewById(R.id.monthly_text)).setTypeface(regular);
        ((TextView) view.findViewById(R.id.yearly_text)).setTypeface(regular);

        ((TextView) view.findViewById(R.id.bid_text)).setTypeface(regular);
        ((TextView) view.findViewById(R.id.notifications_text)).setTypeface(regular);
        ((TextView) view.findViewById(R.id.receipt_text)).setTypeface(regular);
        ((TextView) view.findViewById(R.id.inbox_text)).setTypeface(regular);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.header);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bids_layout:
                FragmentManager fragmentManager = getFragmentManager();
                Fragment bidsFragment = new BidsFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, bidsFragment).commit();
                break;

            case R.id.inbox_layout:
                FragmentManager fragmentManager1 = getFragmentManager();
                Fragment inboxFragment = new InboxFragment();
                Bundle mBundle7 = new Bundle();
                mBundle7.putString("mode", "");
                inboxFragment.setArguments(mBundle7);
                fragmentManager1.beginTransaction().replace(R.id.fragment_layout, inboxFragment).commit();
                break;

            case R.id.receipts_layout:
                FragmentManager fragmentManager2 = getFragmentManager();
                Fragment historyFragment = new HistoryFragment();
                fragmentManager2.beginTransaction().replace(R.id.fragment_layout, historyFragment).commit();
//                startActivity(new Intent(getActivity(), HistoryFragment.class));
                break;

            case R.id.notifications_layout:
                FragmentManager fragmentManager3 = getFragmentManager();
                Fragment notificationFragment = new NotificationFragment();
                fragmentManager3.beginTransaction().replace(R.id.fragment_layout, notificationFragment).commit();
//                startActivity(new Intent(getActivity(), NotificationFragment.class));
                break;

            case R.id.offers_layout:
                FragmentManager fragmentManager4 = getFragmentManager();
                Fragment inboxFragment1 = new InboxFragment();
                Bundle mBundle71 = new Bundle();
                mBundle71.putString("mode", "offers");
                inboxFragment1.setArguments(mBundle71);
                fragmentManager4.beginTransaction().replace(R.id.fragment_layout, inboxFragment1).commit();
//                Intent intent12 = new Intent(getActivity(), InboxFragment.class);
//                intent12.putExtra("mode", "");
//                startActivity(intent12);
                break;

            case R.id.accepted_layout:
                FragmentManager fragmentManager5 = getFragmentManager();
                Fragment inboxFragment2 = new InboxFragment();
                Bundle mBundle72 = new Bundle();
                mBundle72.putString("mode", "accepted");
                inboxFragment2.setArguments(mBundle72);
                fragmentManager5.beginTransaction().replace(R.id.fragment_layout, inboxFragment2).commit();
//                Intent intent11 = new Intent(getActivity(), InboxFragment.class);
//                intent11.putExtra("mode", "accepted");
//                startActivity(intent11);
                break;

            case R.id.done_layout:
                FragmentManager fragmentManager6 = getFragmentManager();
                Fragment historyFragment1 = new HistoryFragment();
                fragmentManager6.beginTransaction().replace(R.id.fragment_layout, historyFragment1).commit();
//                startActivity(new Intent(getActivity(), HistoryFragment.class));
                break;
        }
    }

    public void menuClick() {
        if (language.equalsIgnoreCase("En")) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        } else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);

            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        }
    }

    private class dashboardApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<DashboardResponse> call = apiService.GetDashBoard(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DashboardResponse>() {
                @Override
                public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                    if (response.isSuccessful()) {
                        DashboardResponse dashboardResponse = response.body();
                        try {
                            if (dashboardResponse.getStatus()) {
                                data.clear();
                                data.addAll(dashboardResponse.getData());
                                setDashboardData();
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = dashboardResponse.getMessage();
                                    if (failureResponse.equalsIgnoreCase(
                                            "Your account has been terminated by administrator. Please contact admin office")) {
                                        accountBlocked(failureResponse, getResources().getString(R.string.error),
                                                getResources().getString(R.string.ok), getActivity());
                                    } else {
                                        Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                                getResources().getString(R.string.ok), getActivity());
                                    }
                                } else {
                                    String failureResponse = dashboardResponse.getMessageAr();
                                    if (failureResponse.equalsIgnoreCase(
                                            "Your account has been terminated by administrator. Please contact admin office (Ar)")) {
                                        accountBlocked(failureResponse, getResources().getString(R.string.error_ar),
                                                getResources().getString(R.string.ok_ar), getActivity());
                                    } else {
                                        Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                                getResources().getString(R.string.ok_ar), getActivity());
                                    }

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
//                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<DashboardResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("WorkshopId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    public void setDashboardData() {
        numOffers.setText("" + data.get(0).getOffers());
        numAccepted.setText("" + data.get(0).getDone());
        numReviews.setText("" + data.get(0).getAccepted());
        if (data.get(0).getBids() > 0) {
            bidCount.setText("" + data.get(0).getBids());
        } else {
            bidCount.setVisibility(View.INVISIBLE);
        }

        salesWeekly.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(0).getWeeklyPrice())));
        salesMonthly.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(0).getMonthlyPrice())));
        Log.i(TAG, "saleyear: " + Constants.priceFormat1.format(data.get(0).getYearlyPrice()));
        salesYearly.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(0).getYearlyPrice())));
        Log.i(TAG, "saleyear1: " + Constants.convertToArabic(Constants.priceFormat1.format(data.get(0).getYearlyPrice())));
        if (language.equalsIgnoreCase("En")) {
            MainActivity.workshopName.setText(data.get(0).getNameEn());
            workShopName.setText(data.get(0).getNameEn());
            cityName.setText(data.get(0).getCity().get(0).getCityNameEn());
        } else {
            MainActivity.workshopName.setText(data.get(0).getNameAr());
            workShopName.setText(data.get(0).getNameAr());
            cityName.setText(data.get(0).getCity().get(0).getCityNameAr());

        }

        for (int i = 0; i < data.get(0).getWorkshopdocuments().size(); i++) {
            if (data.get(0).getWorkshopdocuments().get(i).getDocumentType() == 1) {
                Glide.with(getActivity())
                        .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                        .into(profilePic);

                Glide.with(getActivity())
                        .load(Constants.WORKSHOP_IMAGES + data.get(0).getWorkshopdocuments().get(i).getDocumentLocation())
                        .into(MainActivity.profilePic);

                break;
            }
        }
    }

    public void showloaderAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }

    public void accountBlocked(String descriptionStr, String titleStr, String buttonStr, final Activity context) {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        if (language.equalsIgnoreCase("Ar")) {
            title.setTypeface(Constants.getarTypeFace(getContext()));
            desc.setTypeface(Constants.getarTypeFace(getContext()));
            yes.setTypeface(Constants.getarTypeFace(getContext()));
            no.setTypeface(Constants.getarTypeFace(getContext()));
        }

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                finalCustomDialog.dismiss();
                context.startActivity(new Intent(context, SignInActivity.class));
                context.finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }


}
