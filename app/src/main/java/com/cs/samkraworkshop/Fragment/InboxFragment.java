package com.cs.samkraworkshop.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkraworkshop.Adapter.InboxAdapter;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.Models.InboxResponse;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Rest.APIInterface;
import com.cs.samkraworkshop.Rest.ApiClient;
import com.cs.samkraworkshop.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.samkraworkshop.Activity.MainActivity.drawer;

public class InboxFragment extends Fragment {

    ImageView backBtn,testcount;
    TextView count;
    ListView offersList;
    InboxAdapter mAdapter;
    ArrayList<InboxResponse.Data> data = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";
    String requestId = "";
    TextView title;
    ImageView sidemenu;
    AlertDialog loaderDialog;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    View view;
    String language;
    SharedPreferences languagePrefs;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs =getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            view = inflater.inflate(R.layout.inbox_activity, container,false);
        }else if(language.equalsIgnoreCase("Ar")){
            view = inflater.inflate(R.layout.inbox_activity_ar, container,false);
        }

        requestId = getArguments().getString("id");

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        title = (TextView) view.findViewById(R.id.title);
        backBtn = (ImageView) view.findViewById(R.id.back_btn);
        testcount = (ImageView) view.findViewById(R.id.testcount);
        count = (TextView) view.findViewById(R.id.bid_count);
        offersList = (ListView) view.findViewById(R.id.offers_list);
        sidemenu =(ImageView)view.findViewById(R.id.side_menu);

        sidemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuClick();
            }
        });

//        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
//                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
        testcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;

    }
    private void setTypeface(){
        ((TextView)view.findViewById(R.id.title)).setTypeface(Constants.getarbooldTypeFace(getActivity()));

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onBroadcastReceive: ");
            // Get extra data included in the Intent
            String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new offersApi().execute();
            }
            else{
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                }
            }
        }

    };
    @Override
    public void onResume() {
        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new offersApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
            }
        }
        super.onResume();
    }
    public void menuClick(){
        if (language.equalsIgnoreCase("En")) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        }else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);

            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        }
    }
    private class offersApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<InboxResponse> call = apiService.InboxResponse(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InboxResponse>() {
                @Override
                public void onResponse(Call<InboxResponse> call, Response<InboxResponse> response) {
                    data.clear();
                    if (response.isSuccessful()) {
                        InboxResponse verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                if(getArguments().getString("mode").equals("accepted")) {
                                    if (language.equalsIgnoreCase("En")){
                                        title.setText("Accepted Offers");
                                    }
                                    else {
                                        title.setText("مقبول عروض");
                                    }

                                    for (int i = 0; i < verifyMobileResponse.getData().size(); i++) {
                                        if (!verifyMobileResponse.getData().get(i).getRequestStatus().equals("Offer Pending")
                                                && !verifyMobileResponse.getData().get(i).getRequestStatus().equals("Offer Rejected")) {
                                            data.add(verifyMobileResponse.getData().get(i));
                                        }
                                    }
                                }
                                else if(getArguments().getString("mode").equals("offers")) {
                                    if (language.equalsIgnoreCase("En")){
                                        title.setText("Offers");
                                    }
                                    else {
                                        title.setText("عروض");
                                    }

                                    for (int i = 0; i < verifyMobileResponse.getData().size(); i++) {
                                        if (verifyMobileResponse.getData().get(i).getRequestStatus().equals("Offer Pending")) {
                                            data.add(verifyMobileResponse.getData().get(i));
                                        }
                                    }
                                }
                                else{
                                    data = verifyMobileResponse.getData();
                                }
                                mAdapter = new InboxAdapter(getActivity(), data, Language);
                                offersList.setAdapter(mAdapter);
                                count.setText(""+data.size());
                                if(data.size() == 0){
                                    if (language.equalsIgnoreCase("En")){
                                        Constants.showOneButtonAlertDialog(verifyMobileResponse.getMessage(), getResources().getString(R.string.app_name),
                                                getResources().getString(R.string.ok), getActivity());
                                    }
                                    else {
                                        Constants.showOneButtonAlertDialog(verifyMobileResponse.getMessageAr(), getResources().getString(R.string.app_name_ar),
                                                getResources().getString(R.string.ok_ar), getActivity());
                                    }

                                }
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<InboxResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("WorkshopId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }
}
